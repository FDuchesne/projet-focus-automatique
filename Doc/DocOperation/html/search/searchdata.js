var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwxy~",
  1: "cdlmpqrs",
  2: "dlmpqrs",
  3: "abcdefghiklmnopqrstuvwxy~",
  4: "abcdfilmnopst",
  5: "c",
  6: "bcefist",
  7: "abcdeginoprst",
  8: "q"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

