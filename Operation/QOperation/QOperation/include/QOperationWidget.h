/*! \class QOperationWidget
*
*	\brief The main widget used during operations
*
*	The main widget used during opeations.
*	The user will have to first open a csv file of a calibration.
*	Then, after positionning himself, he only needs to press the take image button to take a focus image of the observed object.
*
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 17/07/2018
*/

#ifndef QOPERATION_WIDGET_H
#define QOPERATION_WIDGET_H

#include "QtLUT/QLUT/QLUT.h"

class LutAbstract;
class QLutWidget;
class QPushButton;
class QErrorMessage;
class QRangeFinderAR2000;
class QLabel;
class QMatroxJaiSp20000Grabber;
class QAlgoFocus;



class QOperationWidget : public QLut
{
	Q_OBJECT

public:
	///Constructor
	QOperationWidget(QWidget *parent = nullptr);

	///Destructor
	~QOperationWidget();

protected:
	/**
		Redfinition of the closeEvent so that on widget close, both focuser and range finder port are closed.
	*/
	void closeEvent(QCloseEvent *event) override;

private:
	///Range finder controls
	QRangeFinderAR2000 * mRangeFinder;
	
	///Focuseur control
	//QFocusseur * mFocuser;

	///Picture grabber control
	QMatroxJaiSp20000Grabber * mMatroxSimpleGrabber;

	///Allow the use of some algorithm for auto focusing
	QAlgoFocus * mAlgoFocus;


	/**
		On press, trigger the range finder (ask it to take a single measurement)

		@see triggerRangeFinder()
	*/
	QPushButton* mTakePicture, *mStartRangeFinder;

	/**
		Shows the current status of the application
	*/
	QLabel * mStatus;

public slots:
	/**
		Calculate the distance of the focuser based on the opened calibration file.
		Then try to connect to the focuser.
		On successful connection, will move the focuser to the given distance and take a picture.

		@param distanceFromObject distance of the operator from the observed object. It ISN'T the distance of the focuser
	*/
	void moveFocuser(double distanceFromObject);

	/**
		Stops the range finder tracking
	*/
	void stopTracking(double distanceFromObject, double quality, double temperature);

	/**
		Starts the range finder continuous tracking.
	*/
	void startRangeFinder();

	/**
		Get the distance from the object based on the average distance detected by the range finder
	*/
	void getDistance();

	/**
		Enables the take picture button
	*/
	void enableTakePicture();

	/**
		Disables the take picture button
	*/
	void disableTakePicture();

	/**
		Updates the application status based on returns from the range finder

		@see QAsyncSerialPort::updated
	*/
	void statusUpdatedRangeFinder(QString updateMessage);

	/**
		Updates the application status based on returns from the focuser

		@see QAsyncSerialPort::updated
	*/
	void statusUpdatedFocuser(QString updateMessage);
	
	/**
		Change the status to file is open

		@see QLut::fileSuccessfullyOpened
	*/
	void fileOpened();

	/**
		Prompt a file dialog so the user can save the picture

		@param picture The picutre to be saved
	*/
	void savePicture(QImage * picture);

	/**
		Change the status to file was empty
	*/
	void lutEmpty();

	/**
		take a picutre after moving the focuser
	*/
	void takePicutre(int blurryLevel);
};

#endif //QOPERATION_WIDGET_H
