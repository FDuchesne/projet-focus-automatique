/*! \class QWizardSelector
*
*	\brief A Widget that allowd the user the select and use multiple other widgets
*
*	A Widget that allowd the user the select and use multiple other widgets.
*	This widget is able to open the range finder, focuser and lut userInterface as well as the main operation widget.
*	
*	@author Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.1 17/07/2018
*/

#ifndef Q_WIZARD_SELECTOR_H
#define Q_WIZARD_SELECTOR_H

#include <QWidget>

class QRangeFinderGUI;
class QFocusseurGUI;
class QLut;
class QPushButton;
class QOperationWidget;

class QWizardSelector : public QWidget
{
	Q_OBJECT

public:
	///Constructor
	QWizardSelector(QWidget *parent = Q_NULLPTR);

	///Destructor
	~QWizardSelector();

public slots:
	/**
		Open the range finder widget.
	*/
	void openRangeFinderWidget();
	/**
		Open the focuser widget.
	*/
	void openFocusseurWidget();
	/**
		Open the lut widget.
	*/
	void openLutWidget();
	/**
		Open the operation widget.
	*/
	void openOperationWidget();
	/**
		Close the range finder widget.
	*/
	void closeRangeFinderWidget();
	/**
		Close the focuser widget.
	*/
	void closeFocusseurWidget();
	/**
		Close the lut widget.
	*/
	void closeLutWidget();
	/**
		Close the operation widget.
	*/
	void closeOperationWidget();

protected:
	///The lut widget
	QLut * mLutWidget;
	///The range finder widget
	QRangeFinderGUI * mRangeFinderWidget;
	///The focuser widget.
	QFocusseurGUI * mFocusseurWidget;
	///The operation widget.
	QOperationWidget * mOperationWidget;

	//@{
	/**The buttons that opens each widgets. */
	QPushButton * mRangeFinderButton, *mFocusseurButton, *mLutButton, *mOperationButton;
	//@}
};

#endif //Q_WIZARD_SELECTOR_H
