/*! \class QImageDisplay
*
*	\brief A Widget that displays an image it is given.
*
*	Emit a signal when the user specifie that he wants to save the image.
*
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 17/07/2018
*/

#ifndef Q_IMAGE_DISPLAY_H
#define Q_IMAGE_DISPLAY_H

#include <QWidget>

class QImageDisplay : public QWidget
{
	Q_OBJECT


public:
	///Constructor
	QImageDisplay(QImage *image, QWidget *parent = nullptr);

	///Destructor
	~QImageDisplay();

	/**
		The image displayed on the screen
	*/
	QImage * mImage;

public slots:
	/**
		Event triggered when the save button is pressed
	*/
	void saveButtonPressed();

	/**
		Event triggered when any button is pressed
	*/
	void buttonPressed();

signals:
	/**
		Signal emitted when user press on the save button
	*/
	void saveButtonEvent(QImage * image);
};

#endif // Q_IMAGE_DISPLAY_H
