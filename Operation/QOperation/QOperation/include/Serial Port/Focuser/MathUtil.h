#ifndef MATHUTIL_H
#define MATHUTIL_H

#include  <type_traits>
#include <QObject>
#include <QString>

class MathUtil : public QObject
{
	Q_OBJECT

public:
	

	MathUtil();
	~MathUtil() = default;
	static const double pi;
	static const double pi_1_2;
	static const double pi_2;
	template <typename t> static t radToDeg(t rad) { static_assert(std::is_floating_point<t>::value, "non floating-point template value"); return rad*rad2Deg; }
	template <typename t> static t degToRad(t deg) { static_assert(std::is_floating_point<t>::value, "non floating-point template value"); return deg*deg2Rad; }
	//static unsigned int hexToDec(QString hex);
private:
	static const double deg2Rad;
	static const double rad2Deg;


};
#endif MATHUTIL_H
