/*! \class QFocusseurGUI
*
*	\brief Interface graphique permettant de contrôler et paramétrer le focusseur par le biais de sa classe.
*
*	Regroupe les onglets d'opérations, de paramétrage et de dévelopement, ainsi qu'une barre d'état.
*
*/


#ifndef QFOCUSSEURGUI_H
#define QFOCUSSEURGUI_H

#include <QWidget>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QMessageBox>
#include "QOperationsTabFocusseur.h"
#include "QSettingsTabFocusseur.h"
#include "QDevelopmentTabFocusseur.h"
#include "QFocusseur.h"
#include "QLegendTabFocusseur.h"

class QRFStatusBarFocusseur;


class QFocusseurGUI : public QWidget {
	Q_OBJECT

public:
	QFocusseurGUI(QWidget * parent = Q_NULLPTR);
	~QFocusseurGUI();
	QFocusseur const & rangeFinder() { return mFocusseur; }

protected:
	void closeEvent(QCloseEvent *event);


private slots:
	void tabSelected(int index);

signals:
	void widgetClosed();

private:
	QFocusseur mFocusseur;
	QTabWidget * mTabs;
	QVBoxLayout * mQvbl;
	QDevelopmentTabFocusseur * mDevelopment;
	QSettingsTabFocusseur * mParams;
	QOperationsTabFocusseur * mOperations;
	QRFStatusBarFocusseur * mStatusBar;
	QLegendTabFocusseur * mLegends;
};
#endif //QFOCUSSEURGUI_H