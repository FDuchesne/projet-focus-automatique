/*! \class QOperationsControlWidgetFocusseur
*
*	\brief Widget qui contient les boutons de contrôle pour la classe QFocusseur.
*
*	Utilisé dans la classe QOperationsTabFocusseur
*/



#ifndef QOPERATIONSCONTROLWIDGETFOCUSSEUR_H
#define QOPERATIONSCONTROLWIDGETFOCUSSEUR_H

#include <QWidget>
#include <QPushButton>
#include <QRadioButton>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QSizePolicy>
#include <QButtonGroup>
#include <QGroupBox>
#include "QFocusseur.h"
#include <QSpinBox>
#include <QLineEdit>


class QOperationsControlWidgetFocusseur : public QWidget {
	Q_OBJECT

public:
	QOperationsControlWidgetFocusseur(QFocusseur * focusseur, QWidget * parent = Q_NULLPTR);
	~QOperationsControlWidgetFocusseur();

	void adjustToRangeFinder();
	bool inSlowMotionMode() { return mSlowMotionMode; }
	void setAbsolutePosition(QString text) { mAbsolutePosition->setText(text); }

signals:
	void commandWithParamsSent();

private slots:
	void goToAbsolute();
	void toggleConnection();
	void adjustButtons(bool connected);
	void adjustTestButtons();
	void adjustConnectButton(bool on);
	void test();
	void stopTest();
	void getPosition();
	void initBtnClicked();
	void relativePlus();
	void relativeMinus();
	void enableSlowMo();


private:
	bool mSlowMotionMode;
	QHBoxLayout * mTestLayout;
	QFocusseur * mFocusseur;
	QGroupBox * mMoveGroup, * mTestGroup, * mManualGroup/*, * mBox*/;
	QGridLayout * mMoveGrid, * mGrid, * mManualGrid/*, *mTrackingGrid*/;
	QButtonGroup * mGroup;
	QLineEdit * mAbsolutePosition, *mRelativePosition;
	QPushButton * mConnect, * mTest, * mStopTest, * mGetPos, * mInit, * mGoAbs, * mRelPlus, * mRelMinus, * mSlowMo/*, *mLaser, *mTracking*/;

	bool exitDevelopmentMode();
};
#endif //QOPERATIONSCONTROLWIDGETFOCUSSEUR_H