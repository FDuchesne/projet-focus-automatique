﻿/*! \class QRFStatusBarFocusseur
*
*	\brief Affiche les réponses et messages du focusseur ainsi que différents états de celui-ci.
*/



#ifndef QRFSTATUSBARFOCUSSEUR_H
#define QRFSTATUSBARFOCUSSEUR_H

#include<QStatusBar>

class QHBoxLayout;
class QLabel;
class QLEDLight;
class QTimer;
class QFocusseur;


class QRFStatusBarFocusseur : public QStatusBar {
	Q_OBJECT

public:
	QRFStatusBarFocusseur(QFocusseur * focusseur, QWidget * parent = Q_NULLPTR);
	~QRFStatusBarFocusseur();

private:
	QHBoxLayout * mQhbl;
	QLabel * mMsg;
	QLEDLight *mConnection, * mCommunication;
	QTimer * mTimer;
	QFocusseur * mFocusseur;

private slots:
	void updateConnectionLED(bool connected);
	void updateToReadyStatus();
	void updateToCommunicatingStatus();
	void updateToNoCommunicationStatus();
	void updateDevMode(bool devMode);
	void updateMsg(QString message);
};
#endif // QRFSTATUSBARFOCUSSEUR_H