/*! \class QDebugCommandLineFocusseur
*	
*	\brief Widget qui représente une ligne de commande.
*
*	Utilisé dans la classe QDevelopmentTabFocusseur.
*/


#ifndef QDEBUGCOMMANDLINEFOCUSSEUR_H
#define QDEBUGCOMMANDLINEFOCUSSEUR_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>

class QDebugCommandLineFocusseur : public QWidget {
	Q_OBJECT

public:
	QDebugCommandLineFocusseur(QString terminator, QWidget * parent = Q_NULLPTR);
	~QDebugCommandLineFocusseur();
	QString * input;
	bool localEchoEnabled;
	QCheckBox * echoBox() { return mEchoCB; }
signals:
	void messageSent(QString s);

	public slots:
	void writeCmd(QString s);
	private slots:
	void sendInput();

private:
	QString mTerminator;
	QLabel * mCommand;
	QLabel * mEcho;
	QLineEdit * mCommandLine;
	QPushButton * mSendButton;
	QCheckBox * mEchoCB;
	QHBoxLayout * mQhbl;
};

#endif // QDEBUGCOMMANDLINEFOCUSSEUR_H