#ifndef QLEGENDTABFOCUSSEUR_H
#define QLEGENDTABFOCUSSEUR_H

#include <QWidget>

class QLegendTabFocusseur : public QWidget
{
	Q_OBJECT

public:
	QLegendTabFocusseur(QWidget * parent = Q_NULLPTR);
	~QLegendTabFocusseur();

	static int const NB_MESSAGES{ 8 };
	static QString const POSSIBLE_ERRORS[NB_MESSAGES][3];
};

#endif // QLEGENDTABFOCUSSEUR_H
