/*! \class QFocusseur
*
*	\brief %Point d'entrée des commandes et de sortie des réponses.
*	
*	Contient les dictionnaires de QSerialCommand (mSerialCommands) et de messages d'erreurs de l'appareil (mDeviceMessages).
*	Ils doivent être remplis dans le constructeur à l'aide des méthodes fillDictionary() et fillDeviceMessages().
*	L'envoie de commande s'initie à partir de cette classe. Les méthodes sans paramètres s'envoient avec sendCommandNoParams(), et les autres possèdent leur méthode spécifique.
*	De plus, le traitement des réponses y est effectué avec la méthode handleMatchingResponse() et des messages avec la méthode handleMessageReceived().
*/


#ifndef QFOCUSSEUR_H
#define QFOCUSSEUR_H

#include <QObject>
#include <QList>
#include <QMap>
#include "..\Global\QMovingStatistics.h"
#include "..\Global\QCommandSerialPort.h"
#include "QFocusseurSettings.h"


class QFocusseur : public QObject
{
	Q_OBJECT

public:
	QFocusseur();
	~QFocusseur();


	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Attributes
	////////////////////////////////////////////////////////////////////////////////////////////////////

	// Const
	//==================================================================================================

	enum class Command // Repertoire des commandes disponibles
	{
		InitSeq = 0,
		GetAccLength = 1,
		GetBaseSpeed = 2,
		GetMaxAccLength = 3,
		GetMaxTestLength = 4,
		GetMinAccLength = 5,
		GetMinTestLength = 6,
		GetPosition = 7,
		GetSpeed = 8,
		GetTestLength = 9,
		GoToAbs = 10,
		RelativePlusSeq = 11,
		RelativeMinusSeq = 12,
		SetAccLength = 13,
		SetBaseSpeed = 14,
		SetSpeed = 15,
		SetTestLength = 16,
		SlowMotionMinus = 17,
		SlowMotionPlus = 18,
		Stop = 19,
		Test = 20,
	};

	static QString const SEPARATOR;
	static QString const TERMINATOR;
	static QString const DEFAULT_INI;
	static QString const CURRENT_INI;
	static QString const TEMP_INI;


	// Var
	//==================================================================================================

	QSerialCommand mTestCommand;
	QSerialCommand mTestCommand2;
	QSerialCommand mTestCommand3;
	QSerialCommand mTestCommand4;
	QSerialCommand mTestCommand5;
	QSerialCommand mTestCommand6;

	QCommandSerialPort mSerial;  // public?
	QMap<Command, QSerialCommand const *> mSerialCommands;
	QFocusseurSettings mSettingsFocusseur;

	bool mDeviceConnected/*, mLaserOn, mIsTracking*/;


	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////

	void testCommand();
	void testCommand2();
	void testCommand3();
	void testCommand4();
	void testCommand5();
	void testCommand6();

	void sendCommandNoParams(Command command);
	void setAutoStart(int command);
	void setBaudRate(QAsyncSerialPort::BaudRate baudRate);
	void setStopBits(QSerialPort::StopBits stopBits);
	bool fileExists(QString path);


private:
	QMap<QString, QString> mDeviceMessages;

	/**
		Load the .ini file containing the default settings of the focuser.
		If no .ini file exist, create a new one with some predefined settings.
	*/
	void initSettings();
	void fillDictionary();
	void fillDeviceMessages(); //Why

	void setOutputFormat(int a, int b, int c, int d); //Why


public slots:

	/**
		Applies the settings currently loaded to the focuser.
	*/
	void applySettings();

	/**
		Open the serial port in order to connect with the focuser device.
	*/
	void connectSerialPort();

	/**
		Connect with the device.
		If the device is already connected, disconnect it instead.
	*/
	void connectionUpdated(bool connected);


private slots :

	/**
		React accordingly to the response received from the focuser.
	*/
	void handleMatchingResponse(QByteArray const &response, QSerialCommand const &command);
	void handleMessageReceived(QString const &message);


signals:

	void accLengthReceived(QString response);
	void baseSpeedReceived(QString response);
	void speedReceived(QString response);
	void testLengthReceived(QString response);
	void positionReceived(QString response);
	void maxAccLengthReceived(QString response);
	void maxTestLengthReceived(QString response);
	void minAccLengthReceived(QString response);
	void minTestLengthReceived(QString response);
	void messageReceived(QString const &response);
	void measurementReceived(double distance, double quality, double temperature);
	void matchingResponseReceived(QString response);
	void message(QString message);
};

#endif // QFOCUSSEUR_H
