/*! \class QOperationsOutputWidgetFocusseur
*
*	\brief Affiche les mesures provenant du signal measurementReceived() de la classe QFocusseur
*
*	Utilisé dans la classe QOperationsTabFocusseur
*/


#ifndef QOPERATIONSOUTPUTWIDGETFOCUSSEUR_H
#define QOPERATIONSOUTPUTWIDGETFOCUSSEUR_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>

class QFocusseur;

class QOperationsOutputWidgetFocusseur : public QWidget {
	Q_OBJECT

public:
	QOperationsOutputWidgetFocusseur(QFocusseur * rangeFinder, QWidget * parent = Q_NULLPTR);
	void showPosition(int noKey);
	~QOperationsOutputWidgetFocusseur();

public slots:
	void showPosition(QString response);

private:
	QFocusseur * mRangeFinder;
	QGridLayout * mGrid;
	QLineEdit * mPosition;
};
#endif //QOPERATIONSOUTPUTWIDGETFOCUSSEUR_H
