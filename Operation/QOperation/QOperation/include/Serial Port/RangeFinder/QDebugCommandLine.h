/*! \class QDebugCommandLine
*	
*	\brief Widget qui représente une ligne de commande.
*
*	Utilisé dans la classe QDevelopmentTab.
*/


#ifndef QDEBUGCOMMANDLINE_H
#define QDEBUGCOMMANDLINE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>

class QDebugCommandLine : public QWidget {
	Q_OBJECT

public:
	QDebugCommandLine(QString terminator, QWidget * parent = Q_NULLPTR);
	~QDebugCommandLine();
	QString * input;
	bool  localEchoEnabled;
	QCheckBox * echoBox() { return mEchoCB; }
signals:
	void messageSent(QString s);

	public slots:
	void writeCmd(QString s);
	private slots:
	void sendInput();

private:
	QString mTerminator;
	QLabel * mCommand;
	QLabel * mEcho;
	QLineEdit * mCommandLine;
	QPushButton * mSendButton;
	QCheckBox * mEchoCB;
	QHBoxLayout * mQhbl;
};

#endif // QDEBUGCOMMANDLINE_H