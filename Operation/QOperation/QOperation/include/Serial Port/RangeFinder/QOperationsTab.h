/*! \class QOperationsTab
*
*	\brief Widget d'opération pour la classe QRangeFinderAR2000.
*
*	Contient les boutons de contrôle et affiche les mesures et les statistiques.
*/



#ifndef QOPERATIONSTAB_H
#define QOPERATIONSTAB_H

#include <QWidget>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QTabWidget>
#include "QStatisticsWidget.h"
#include "QOperationsOutputWidget.h"
#include "QOperationsControlWidget.h"
#include "QRangeFinderAR2000.h"

class QMovingStatistics;
class QProgressBar;
class QStackedLayout;
class QLabel;

class QOperationsTab : public QWidget {
	Q_OBJECT

public:
	QOperationsTab(QRangeFinderAR2000 &rangeFinder, QWidget * parent = Q_NULLPTR);
	~QOperationsTab();

private:
	QRangeFinderAR2000 * mRangeFinder;
	QVBoxLayout * mQvbl, *mStatsBoxLayout, *mOperBoxLayout, *mOutputBoxLayout;
	QStatisticsWidget * mDistanceStats;
	QStatisticsWidget * mSignalStats;
	QStatisticsWidget * mTemperatureStats;
	QTabWidget * mStats;
	QOperationsOutputWidget * mOperations;
	QOperationsControlWidget * mControl;
	QGroupBox * mStatsBox, *mOutputBox, *mOperBox;
	QPushButton * mResetStats;
	QHBoxLayout * mResetLayout;

	private slots:
	void resetStats();


signals:
	void connectionUpdated(bool connected);
	void connectionRequested(bool connect); //true for connect, false for disconnect
	void trackingRequested(QRangeFinderAR2000::TrackingMode trackingMode);
	void trackingChanged(bool isTracking);
	void measurementReceived(double distance, double quality, double temperature);
	void distanceStatsUpdated(QMovingStatistics const &stats);
	void qualityStatsUpdated(QMovingStatistics const &stats);
	void temperatureStatsUpdated(QMovingStatistics const &stats);
};
#endif //QOPERATIONSTAB_H