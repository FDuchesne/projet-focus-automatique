/*! \class QOperationsControlWidget
*
*	\brief Widget qui contient les boutons de contrôle pour la classe QRangeFinderAR2000.
*
*	Utilisé dans la classe QOperationsTab
*/



#ifndef QOPERATIONSCONTROLWIDGET_H
#define QOPERATIONSCONTROLWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QRadioButton>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QSizePolicy>
#include <QButtonGroup>
#include <QGroupBox>
#include "QRangeFinderAR2000.h"


class QOperationsControlWidget : public QWidget {
	Q_OBJECT

public:
	QOperationsControlWidget(QRangeFinderAR2000 * rangeFinder, QWidget * parent = Q_NULLPTR);
	~QOperationsControlWidget();

	void adjustToRangeFinder();

private slots:
	void startTracking();
	void toggleConnection();
	void toggleLaser();
	void adjustButtons(bool connected);
	void adjustConnectButton(bool on);
	void adjustTrackingButton(bool on);
	void adjustLaserButton(bool on);

private:
	QRangeFinderAR2000 * mRangeFinder;
	QGroupBox * mBox;
	QGridLayout * mGrid, *mTrackingGrid;
	QButtonGroup * mGroup;
	QPushButton * mConnect, *mLaser, *mTracking;
	QRadioButton * mContinuousTracking, *mDistanceTracking, *mSingleMeasurement;

	bool exitDevelopmentMode();
};
#endif //QOPERATIONSCONTROLWIDGET_H