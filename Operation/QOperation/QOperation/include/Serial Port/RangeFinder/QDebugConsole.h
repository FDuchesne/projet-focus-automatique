/*! \class QDebugConsole
*	\brief Widget qui représente une console dans laquelle les données envoyées et reçues d'un port série sont affichées.
*
*	Utilisé dans la class QDevelopmentTab
*
*/



#ifndef QDEBUGCONSOLE_H
#define QDEBUGCONSOLE_H

#include <QWidget>
#include <QPlainTextEdit>
//#include "ui_qdebugconsole.h"


class QDebugConsole : public QPlainTextEdit {
	Q_OBJECT

public:
	QDebugConsole(QWidget * parent = Q_NULLPTR);
	~QDebugConsole();

	public slots:
	void appendMessage(const QString &text);
	void setEcho(bool echo);


private:
	//Ui::QDebugConsole ui;
	bool mEcho;
};

#endif  //QDEBUGCONSOLE_H