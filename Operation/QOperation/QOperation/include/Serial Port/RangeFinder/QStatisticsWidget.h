/*! \class QStatisticsWidget
*
*	\brief Affiche les statistiques fournies par la classe QMovingStatistics.
*
*	Le bouton reset permet de réinitialiser le tampon et le QProgressBar permet de connaître son état.
*
*/



#ifndef QSTATISTICSWIDGET_H
#define QSTATISTICSWIDGET_h

#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include "..\Global\RingBuffer.h"

class QMovingStatistics;
class QProgressBar;
class QStackedLayout;
class QLabel;

class QStatisticsWidget : public QWidget {
	Q_OBJECT

public:
	QStatisticsWidget(QWidget * parent = Q_NULLPTR);
	~QStatisticsWidget();

	public slots:
	void showStats(QMovingStatistics const &stats);
	void updateProgBar(QMovingStatistics const &stats);

private:
	QVBoxLayout * mQvbl;
	QLineEdit * mMin, *mMax, *mAverage, *mStdDev;
	QPushButton * mReset;
	QGridLayout * mGrid;
	QHBoxLayout *  mQhbl[4];
	QProgressBar * mProgress;
	QStackedLayout * mStackLayout;
	QLabel * mFull;

signals:
	void resetPressed();
};
#endif // QSTATISTICSWIDGET_H