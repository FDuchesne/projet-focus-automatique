/*! \class QSettingsTab
*
*	\brief Widget servant à paramétrer la classe QRangeFinderAR2000 à l'aide de la classe QRangeFinderSettings.
*
*	Possède des boutons pour sauvegarder et charger les paramètres.
*
*/


#ifndef QSETTINGSTAB_H
#define QSETTINGSTAB_H

#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QTooltip>
#include <QFormLayout>
#include <QSerialPort>
#include "..\Global\QAsyncSerialPort.h"

class QRangeFinderAR2000;
class QSpinBox;
class QDoubleSpinBox;
class QLineEdit;

class QSettingsTab : public QWidget {
	Q_OBJECT

public:
	QSettingsTab(QRangeFinderAR2000 &rangeFinder, QWidget * parent = Q_NULLPTR);
	~QSettingsTab();
	private slots:
	void setFields();
	void updateButtons();
	void updateAverage(int val);
	void updateFrequency(double val);
	void updateMinWindow(double val);
	void updateMaxWindow(double val);
	void updateOffset(double val);
	void updatePortName(QString val);
	void updateDevBaudRate(QString val);
	void updateDevStopBits(QString val);
	void updatePortBaudRate(QString val);
	void updatePortStopBits(QString val);
	void updateParity(QString val);
	void updateFlowControl(QString val);
	void updateDataBits(QString val);
	void save();
	void ok();
	void cancel();
	void revert();
	void defaultClicked();
signals:
	void changeMade();

private:
	QRangeFinderAR2000 * mRangeFinder;
	QFormLayout  *mSerialLayout, *mMeasLayout, *mDeviceLayout;
	QGroupBox * mMeasBox, *mCommBox, *mPortBox, *mDeviceBox;
	QComboBox * mBaudRate, *mParity, *mFlowControl, *mStopBits, *mDataBits, *mPortBaudRate, *mPortStopBits;
	QPushButton * mRevert, *mSave, *mDefault, *mOk, *mCancel;
	QGridLayout *mMeasGrid, *mCommGrid;
	QVBoxLayout * mQvbl, *mCommLayout;
	QHBoxLayout * mButtonLayout;
	QSpinBox * mAverage;
	QDoubleSpinBox * mFrequency, *mWindowMin, *mWindowMax, *mOffset;
	QLineEdit * mPortName;



	static const QMap <QAsyncSerialPort::BaudRate, QString> QSettingsTab::mBRtoStr;
	static const QMap <QString, QAsyncSerialPort::BaudRate> QSettingsTab::mStrtoBR;
	static const QMap <QSerialPort::StopBits, QString> QSettingsTab::mSBtoStr;
	static const QMap <QString, QSerialPort::StopBits> QSettingsTab::mStrtoSB;
	static const QMap <QSerialPort::Parity, QString> QSettingsTab::mParitytoStr;
	static const QMap <QString, QSerialPort::Parity> QSettingsTab::mStrtoParity;
	static const QMap <QSerialPort::FlowControl, QString> QSettingsTab::mFCtoStr;
	static const QMap <QString, QSerialPort::FlowControl> QSettingsTab::mStrtoFC;
	static const QMap <QSerialPort::DataBits, QString> QSettingsTab::mDBtoStr;
	static const QMap <QString, QSerialPort::DataBits> QSettingsTab::mStrtoDB;
};
#endif //QSETTINGSTAB_H