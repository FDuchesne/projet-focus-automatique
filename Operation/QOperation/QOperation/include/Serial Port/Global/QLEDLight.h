/*! \class QLEDLight
*
*	\brief Affiche un icône et un ToolTip propre à son état. On peut ajouter des états avec la fonction addStatus().
*
*	Utilisé dans la classe QRFStatusBar
*/


#ifndef QLEDLIGHT_H
#define QLEDLIGHT_H

#include <QLabel>
#include <QMap>
#include <QIcon>
#include <QSize>

class QLEDLight : public QLabel
{
	Q_OBJECT

public:
	QLEDLight(QSize const & size = QSize(32, 32), QWidget *parent = nullptr);
	~QLEDLight();

	bool addStatus(int status, QString const & description, QIcon const & icon);
	bool setStatus(int status);

	int status() { return mStatus; }
	QString const & description() { return mStatusInfo.contains(mStatus) ? mStatusInfo[mStatus].first : QString(); }
	QIcon const & icon() { return mStatusInfo.contains(mStatus) ? mStatusInfo[mStatus].second : QIcon(); }

protected:
	int mStatus;
	QString mDescription;
	QIcon mIcon;
	QMap<int, QPair<QString, QIcon>> mStatusInfo;
	const QSize mSize;

	virtual void paintEvent(QPaintEvent * event) override;
};

#endif // QLEDLIGHT_H
