/*! \class QDebugCommandList
*
*	\brief Widget qui affiche le dictionnaire de commandes d'un appareil sous la forme d'un QTableView.
*
*	Utilis� dans la classe QDevelopmentTab.
*/


#ifndef QDEBUGCOMMANDLIST_H
#define QDEBUGCOMMANDLIST_H

#include <QWidget>
#include <QListView>
#include "QSerialCommand.h"
#include <QTableView>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QSizePolicy>
#include <QMap>
#include "..\RangeFinder\QRangeFinderAR2000.h"

class QDebugCommandList : public QTableView {
	Q_OBJECT

public:
	QDebugCommandList(QList<QSerialCommand const *> const &commands, QWidget * parent = Q_NULLPTR);
	~QDebugCommandList();

signals:
	void infoSent(QString s);
	void cmdSent(QString s);

	private slots:
	void sendInfo();
	void sendCmd();

private:
	QStringList mNameList;
	QStringList mCmdList;
	QStringList mShortDescList;
	QStringList mFamilyList;
	QList<QStringList> mList = { mNameList, mCmdList, mFamilyList, mShortDescList };
	QTableView * mTableView;
	QStandardItemModel * mModel;
};

#endif // QDEBUGCOMMANDLIST_H