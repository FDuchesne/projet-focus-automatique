#ifndef Q_MATROX_SIMPLE_GRABBER_H
#define Q_MATROX_SIMPLE_GRABBER_H

#include <mil.h>
#include <QImage>
#include <cstdint>

class QMatroxSimpleGrabber
{
public:
	QMatroxSimpleGrabber();
	virtual ~QMatroxSimpleGrabber();

	bool initialize(MIL_CONST_TEXT_PTR deviceConfigurationFile, uint32_t imageBand, QSize const & imageSize);

	bool isAvailable() const { return mAvailable; }

	void grab(QImage & image);

protected:
	bool mAvailable{ false };

	uint32_t mImageBand;
	QSize mImageSize;
	
	MIL_ID mMilApplication;
	MIL_ID mMilSystem;
	MIL_ID mMilDigitizer;
	MIL_ID mMilImage;

private:
	void freeMilResources();
};



class QMatroxJaiGo5000Grabber : public QMatroxSimpleGrabber
{
public:
	QMatroxJaiGo5000Grabber();

private:
	using QMatroxSimpleGrabber::initialize;
};


class QMatroxJaiSp20000Grabber : public QMatroxSimpleGrabber
{
public:
	QMatroxJaiSp20000Grabber();

private:
	using QMatroxSimpleGrabber::initialize;
};


#endif // Q_MATROX_SIMPLE_GRABBER_H