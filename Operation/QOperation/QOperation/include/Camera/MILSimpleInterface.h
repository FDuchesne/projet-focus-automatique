#pragma once

#include <QtWidgets/QMainWindow>
//#include "ui_MILSimpleInterface.h"

#include "QMatroxSimpleGrabber.h"
#include <QLabel>
#include <QScrollArea>
#include <QPushButton>

class MILSimpleInterface : public QMainWindow
{
	Q_OBJECT

public:
	MILSimpleInterface(QWidget *parent = Q_NULLPTR);
	~MILSimpleInterface();

private:
	//Ui::MILSimpleInterfaceClass ui;

	QScrollArea *mScrollArea;
	QLabel *mImage;
	QPushButton * mGrabButton;
	QMatroxSimpleGrabber * mMatroxSimpleGrabber;

private slots:
	QImage grab();
};
