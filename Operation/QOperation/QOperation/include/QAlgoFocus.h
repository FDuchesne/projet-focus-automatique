/*! \class QAlgoFocus
*
*	\brief An algorithm for automatic focus
*
*	An algorithm for automatic focus. Takes multiple images with different focuser position to determine the best one.
*
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 17/07/2018
*/

#ifndef QALGO_FOCUS_H
#define QALGO_FOCUS_H

#include <QObject>
#include <QVector>
class QFocusseur;
class QMatroxJaiSp20000Grabber;
class QFocusseurGUI;

/**
	\brief Structure that contains the blurryLevel of an image took at a specififed location.
*/
struct colorVariationAndPosition
{
public:
	double mColorVariationLevel;
	int mPos;

	/**
		Redefinition of the '==' operator.
	*/
	bool operator==(const colorVariationAndPosition& a) const
	{
		return (mColorVariationLevel == a.mColorVariationLevel && mPos == a.mPos);
	}
};


class QAlgoFocus : public QObject
{
	Q_OBJECT



public:
	///Constructor
	QAlgoFocus(QObject *parent= nullptr);
	///Destructor
	~QAlgoFocus();

	/**
		The beggining of the auto focus algorithm used during the calibration. It isn't the full algorithm. Sets basic settings and then take the first three points.
		The algorithm carry on in the algo2 function.

		@see QAlgoFocus::moveAndTakeImage(), QAlgoFocus::blurryLevel, QAlgoFocus::colorToGray
	*/
	void algorithme(QVector<int> col, QVector<int> row, QVector<int> width, QVector<int> height);

	/**
		The beggining of the auto focus algorithm used during the operation. It isn't the full algorithm. Does the same thing as the calibration algorithme, but the first 3 points positions
		are based on the content of a calibration csv file. The rest of the algorithm is done the same way as the calibration algorithm

		@param position The theoritical position of the focuser based on the csv file.
	*/
	void algorithmeOperation(int position);




private:
	int mFocuserLength, mNbOfLoop, mMaxNbOfLoop;

	///A position value determined by a calibration file. Only used during operations.
	int mOperationPosition;

	///Determine if the algorithm is an operation or a calibration algorithm
	bool mOperation;

	/**
		Pointer to an object that takes a picture

		@see QMatroxSimpleGrabber.h
	*/
	QMatroxJaiSp20000Grabber * mMatroxSimpleGrabber;

	/**
		Refer to the first 8 distance where an image is took in the algorithm.

		@see QAlgoFocus::algorithme()
		index 0-1-2 : starter points
		index 3 : min
		index 4 : max
		index 5 : temp
		index 6-7 : left - right
	*/
	colorVariationAndPosition mPoints[8];

	/**
		The index of the image currently being used by the algorithm 

		@see mPoints[8]
	*/
	int mIndexImage;

	/**Used to control the focuser*/
	QFocusseur * mFocuseur;

	//Global functions

	/**
		Moves the focuser to the given location.
	*/
	void moveFocuseur(double pos);

	/**
		@param message Not used : Just to match the signal's return
	*/
	void takeImage(QString message);

	//Algorithm functions
	
	/**
		@see QAlgoFocus::algorithme()
		Move the focuser and take three photos to determine the best possible focus.
		Recursive function : called automatically after a photo is taken. Exit after the 3rd photo.
	*/
	void getStarterPoints();


	/**
		Function that sets the position of a blurryLevelAndPosition object based on the specified position.
		Then the focuseur is moved using the moveFocuseur function.

		@see QAlgoFocus::moveAndTakeImage, QAlgoFocus::takeImage
	*/
	void moveAndSetPosition(double position);
	
	/**
		@see QAlgoFocus::getStarterPoints()
		Second part of the focus algorithm
		Determine which photo is the less blurry, then calls the getLeftAndRight function based on the photo.
	*/
	void algo2();

	/**
		@see QAlgoFocus::algo2()
		Take 3 photos inside the search field.
		Recursive function : called automatically after a photo is taken.
		Exit after the 3rd photo.
	*/
	void getLeftAndRight();

	/**
		@see QAlgoFocus::getLeftAndRight()
		Third part of the focus algorithm.
		Sets the minBlurryLevelbased on the result of the getLeftAndRight function.
		Then find the best focus by recusively calling the findBestFocus function.

	*/
	void algo3();

	/**
		@see QAlgoFocus::algo3()
		Determine which photo is the more and less blurry
		Recursive function : called automatically after a photo is taken.
		Exit when the maximum number of photos is reached or when the focus is on point.
	*/
	void findBestFocus();


	/**
		Interprate the photo took by the camera during the findBestFocus function process.
		Call the findBestFocus function back if no criteria is filled.
		If a criteria is filled emit the algorithmDone signals with the final position of the focuseur.
	*/
	void interprateReturn();

	/**
	Convert a color image to a gray scale image programatically.

	@param image A reference to the color image
	@param imageSize The color image size
	@param imageGray A reference to the gray image that'll be generated.
	*/
	void colorToGray(QImage & image, QSize imageSize, QImage & imageGray);

	/**
		Programatically calculate the blurry level of the pixel.

		@param pixel The pixel for which the blurry level is calculated.
		@param imgWidth the image width, which is used in the algorithm.
	*/
	double colorVariationLevelOfAPixel(const uchar * pixel, int imgWidth);

	/**
		Return the blurry level of the given grayScale image between the given coordonate.
		For further information about the algorithm look at the function definition.

		@see QAlgoFocus::blurryLevelOfAPixel

		@param grayScale The gray scale image
		@param x The x coordinate of the top left corner of the calculated zone. By default = 0, so it takes the whole image.
		@param y The y coordinate of the top left corner of the calculated zone. By default = 0, so it takes the whole image.
		@param width The width of the calculated zone. By default = -1, which refers to the image width.
		@param height The height of the calculated zone. By default = -1, which refers to the image width.
	*/
	double colorVariationLevel(QImage & grayScale, int x = 0, int y = 0, int width = -1, int height= -1);

	/**
		Returns the combined blurry level of multiple spots in the given gray scale image.
		The x, y corrdinate and the width / height of the spots are determined in the beggining of the algorithme

		@see algorithme()
		@see mCol
		@see mRow
		@see mWidth
		@see mHeight
	*/
	double colorVariationLevelOfMultipleSpots(QImage & grayScale);

	///Presence of multiple different spots to focus
	bool multipleSpots;

	//@{
		/**Vectors in which the x, y coordinates and the width / height of the zone(s) to focus are stored */
		QVector<int> mCol, mRow, mWidth, mHeight;
	//@}



	//Testing variables
	bool mTesting;
	QString mFileNames[41] = { "BackGround", "BackGround2", "BackGround3","BackGround4","BackGround5","BackGround6","BackGround7","BackGround8",
		"BackGround9","BackGround10","BackGround11","BackGround12","BackGround13","BackGround14","BackGround15","BackGround16","BackGround17","BackGround18","BackGround19",
		"BackGround20","BackGround21", "BackGround22","BackGround23","BackGround24","BackGround25","BackGround26","BackGround27",
		"BackGround28","BackGround29","BackGround30","BackGround31","BackGround32","BackGround33","BackGround34","BackGround35","BackGround36","BackGround37","BackGround38",
		"BackGround39", "BackGround40"
	};

	signals:
		///Signals emited when a image is successfully grabed.
		void doneTakingImage();
		///Signals emited when the algorithm is done.
		void algorithmeDone(int blurryLevel);
};

#endif //QALGO_FOCUS_H