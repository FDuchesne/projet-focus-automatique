/*! \class LutWithoutFunction
*
*	\brief Basic template for a Look Up Table not using an interpolation equation.
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.1 03/07/2018
*/

#ifndef LUT_WITHOUTFUNCTION_H
#define LUT_WITHOUTFUNCTION_H

#include "LutAbstract.h"

class LutWithoutFunction : public LutAbstract
{
    public:

		/// Constructor
        LutWithoutFunction();

		/// Default Destructor
        virtual ~LutWithoutFunction() = default;

    protected:

		/**
			Calculate the previous and next point of a given X value and assigns them in to the passed references.

			@see Point.h
			@param x X coordonate value.
			@param previousValue Previous point
			@param nextValue Next point
		*/
        void getCloseValues(double x, Point & previousValue, Point & nextValue) const;

};

#endif // LUT_WITHOUTFUNCTION_H
