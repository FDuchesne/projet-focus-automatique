/*! \class Point
*
*	\brief Store the x and y values of a point in a cartesian plane.
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef POINT_H
#define POINT_H

class Point
{
    public:

		/**
			Constructor.
			Initialise both point coordinates at the specified value.

			@param a coordonate value.
		*/
        Point(double a = 0);

		/**
			Constructor.
			Initialise point coordinates at the specified values.

			@param x X coordonate value.
			@param y Y coordonate value.
		*/
        Point(double x, double y);

		/// Default Destructor.
        ~Point() = default;

		/// Default Basic assignment operator.
        Point& operator=(Point const & p) = default;


		/// Default move copy constructor
        Point& operator=(Point && p) = default;
        
		/// Default lhs copy constructor
		Point(Point const & p) = default;

		/// Default rhs copy constructor
		Point(Point&& p) = default;

		/**
			Addition operator.
			Add the respective X and Y coordinates together.
		*/
        Point operator+(Point const & point);

		/**
			Substraction operator.
			Substract the respective X and Y coordinates together.
		*/
        Point operator-(Point const & point);

		/**
			Multiplication operator.
			Multiply both coordinates with the given value.
		*/
        Point operator*(double value);

		/**
			Less Than comparison operator.
			Compare points based on the X coordinate.
		*/
        bool operator<(Point const & point) const;

		/**
			Equal comparison operator.
			Compare points based on the X coordinate.
		*/
        bool operator==(Point const & point) const;

		/// returns the referance to the X coordinate
		double & rx() { return mX; };

		/// returns the referance to the Y coordinate
		double & ry() { return mY; };

		/// returns the value of the X coordinate
        double x() const { return mX; };

		/// returns the value of the Y coordinate
        double y() const { return mY; };

		/// Sets the value of the X coordinate
		void setX(double x) {mX = x;}

		/// Sets the value of the Y coordinate
		void setY(double y) {mY = y;}

    private:
		///X and Y coordinates
		double mX, mY;
};

#endif // POINT_H
