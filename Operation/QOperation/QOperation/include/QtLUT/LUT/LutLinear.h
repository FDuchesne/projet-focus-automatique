/*! \class LutLinear
*
*	\brief Look Up Table using a partial linear algorithm.
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef LUT_LINEAR_H
#define LUT_LINEAR_H

#include "LutWithoutFunction.h"

class LutLinear : public LutWithoutFunction
{
public:

	// Constructor
	LutLinear();

	// Default Destructor
	~LutLinear() = default;

	/**
		Calculate the Y coordinate value of a given X value based on the accumulated points in the LUT.
		Uses a partial linear interpolation and extrapolation algorithm to determine the Y value.
		The Y value is equal to the Y value of a linear equation created with the point before and after the given X value.

		@see Point.h
		@param x X coordonate value.
		@return y Y coordinate value.
	*/
	double y(double x) const override;

};
#endif // LUT_LINEAR_H