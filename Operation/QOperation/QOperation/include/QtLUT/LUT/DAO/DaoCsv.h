/*! \class DaoCsv
*
*	\brief Data Accses Object to CSV Files
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef DAO_CSV_H
#define DAO_CSV_H

#include "DaoFile.h"

class DaoCsv : public DaoFile
{
public:
	/**
		Default constructor.
	*/
	DaoCsv(std::string path = "csv.csv");


	///Default destructor.
	virtual ~DaoCsv() = default;

	/// Default Basic assignment operator.
	DaoCsv& operator=(DaoCsv const & daoCSV) = default;

	///Default move copy constructor.
	DaoCsv& operator=(DaoCsv && daoCSV) = default;

	///Default lhs copy constructor.
	DaoCsv(DaoCsv const & daoCSV) = default;

	/// Default rhs copy constructor.
	DaoCsv(DaoCsv && daoCSV) = default;

	/**
		Redefinition of the write function.
		Made to write CSV file.
	*/
	virtual void write(std::vector<Point> const & vector) const override;

	/**
		Redefinition of the read function.
		Made to read CSV file.
	*/
	virtual std::vector<Point> read() override;

	double readParameterA();

	double readParameterB();

private:
	double readParameter(std::string parameterName);
};

#endif // DAO_CSV_H
