/*! \class DaoFile
*
*	\brief Basic template for a Data Accses Object accessing to files
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef DAO_FILE_H
#define DAO_FILE_H

#include "DaoAbstract.h"
#include <string>
#include <fstream>

class DaoFile : public DaoAbstract
{
public:
	/**
		Constructor.
		Acces the file at the specified path.
		
		@param path The path of the file.
	*/
	DaoFile(std::string path = "csv.csv");

	///Default destructor.
	virtual ~DaoFile() = default;

	/// Default Basic assignment operator.
	DaoFile& operator=(DaoFile const & daoFile) = default;

	///Default move copy constructor.
	DaoFile& operator=(DaoFile && daoFile) = default;

	///Default lhs copy constructor.
	DaoFile(DaoFile const & daoFile) = default;

	/// Default rhs copy constructor.
	DaoFile(DaoFile && daoFile) = default;

protected:

	///The file path.
	std::string mPath;

private:
};

#endif // DAO_FILE_H
