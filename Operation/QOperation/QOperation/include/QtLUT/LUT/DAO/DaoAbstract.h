/*! \class DaoAbstract
*
*	\brief Basic template for a Data Accses Object
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef DAO_ABSTRACT_H
#define DAO_ABSTRACT_H

#include <vector>

#include "../../LUT/Point.h"

class DaoAbstract
{
public:
	/**
		Default constructor
	*/
	DaoAbstract();
	
	///Default destructor
	virtual ~DaoAbstract() = default;

	/// Default Basic assignment operator.
	DaoAbstract& operator=(DaoAbstract const & daoAbstract) = default;

	///Default move copy constructor.
	DaoAbstract& operator=(DaoAbstract && daoAbstract) = default;
	
	///Default lhs copy constructor.
	DaoAbstract(DaoAbstract const & daoAbstract) = default;
	
	/// Default rhs copy constructor.
	DaoAbstract(DaoAbstract && daoAbstract) = default;
	
	/**
		Insert a vector into the specified database

		@param vector to insert
	*/
	virtual void write(std::vector<Point> const & vector) const = 0;

	/**
		Read the databse and return a vector of point based on the data
	*/
	virtual std::vector<Point> read() = 0;

};

#endif // DAO_ABSTRACT_H
