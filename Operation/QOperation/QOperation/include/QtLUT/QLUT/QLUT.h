/*! \class QLut
*
*	\brief GUI for basic Lut manipulation
*
*	A GUI that allows users to create, open and edit a Look Up Table.
*	Currently supports the tier, linear and linearRegression algorithm.
*	
*	@see LutEchelon, LutLinear, LutLinearRegression
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 03/07/2018
*/

#ifndef Q_LUT_H
#define Q_LUT_H


#include <QtWidgets/QMainWindow>

#include <QMenuBar>


class LutAbstract;
class QWithoutFunction;
class QWithFunction;
class QLutWidget;
class QAction;
class QMenuBar;
class QVBoxLayout;


class QLut : public QMainWindow
{
	Q_OBJECT

public:
	
	///Constructor
	QLut(QWidget *parent = Q_NULLPTR);

	///Main Widget
	QWidget * mMainWidget;

	/**
		The QLutWidget

		@see QLutWidget.h
	*/
	QLutWidget * mLutWidget;

	///Main vecrtical layout
	QVBoxLayout * mVLayout;	

	///The currently opened file path
	QString mFilePath;
	

protected:
	
	/**
		Redefinition of the event : closeEvent.
		If any change occured during the operation, at exit, ask the user if he wants to save his work.
	*/
	virtual void closeEvent(QCloseEvent *event);

	///Main Lut, only one to read the database and transfer the information to other luts
	LutAbstract * mLutAbs;
private:



	QMenuBar * mMenuBar;
	//Menubar menus and actions
	QMenu * mFileMenu;
	QMenu * mHelpMenu;
	QAction* mNewFileAction;
	QAction* mSaveFileAction;
	QAction* mSaveFileAsAction;
	QAction* mLoadFileAction;
	QAction* mQuitFileAction;
	QAction* mAboutAction;
	QAction* mViewSrc;
	QTabWidget * mLutWidgets;

	///Check if there's any unsaved changes
	bool unsavedChanges;

	//� voir avec Lee
	void writeSettings();
	void readSettings();
	void createStatusBar();

	/**
		Create the actions that populate the Menu Bar.

		@see QAction
	*/
	void createActions();

	/**
		Saves the changes in the current open file.
	*/
	bool _saveFile();
signals:
	///Signals emited when the QLutWidget is closed.
	void widgetClosed();

	///Signals emited when a file is successfully opened.
	void fileSuccessfullyOpened();

	///Signals emited when the open file is empty.
	void fileIsEmpty();

private slots:
	
	/**
		Determine how the current Lut will be saved
		If the lut provides from a .csv file that the user opened, overwrite it.
		Else, ask the user where he want to store the data
	*/
	bool save();

	/**
		Create a new file in which the current lut will be saved
	*/	
	bool saveAs();
	
	/**
		Load an existing file in the GUI
	*/
	void loadFile();

	/**
		Generates a Dialog box to verify if the user wants to save his unsaved changes
	*/
	bool maybeSave();//Check if the data is unchanged, request Dialog box to save if wanted
	
	//� VOIR AVEC LEE
	void newFile();
	
	/**
		Open the browser at the project bitBucket URL.
	*/
	void viewSrcInBrowser();

	/**
		Show a message box that explain what this program is about.
	*/
	void about();

	/**
		Put the unsavedChange variable to true when a change occurs.
	*/
	void onDataChange();

};

#endif // Q_LUT_H
