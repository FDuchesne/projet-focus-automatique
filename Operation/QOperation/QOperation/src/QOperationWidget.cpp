#include "../include/QOperationWidget.h"
#include "../include/QtLUT/LUT/LutAbstract.h"
#include "../include/QtLUT/LUT/DAO/DaoCsv.h"
#include "../include/QtLUT/LUT/DAO/DTO.h"
#include "../include/Serial Port/RangeFinder/QRangeFinderAR2000.h"
#include "../include/QtLUT/QLUT/QLutWidget.h"
#include "../include/QImageDisplay.h"
#include "..\include\Camera\MILSimpleInterface.h"
#include "..\include\Camera\QMatroxSimpleGrabber.h"
#include "..\include\QAlgoFocus.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QImage>
#include <QLabel>
#include <QFileDialog>

QOperationWidget::QOperationWidget(QWidget *parent)
	: QLut(parent),
	mTakePicture{ new QPushButton("Take a picture") },
	mStartRangeFinder{ new QPushButton("Start range finder") },
	mRangeFinder{ new QRangeFinderAR2000() },
	mMatroxSimpleGrabber{ new QMatroxJaiSp20000Grabber() },
	mAlgoFocus{ new QAlgoFocus() },
	mStatus{ new QLabel("Please open a calibration file") }
{
	mTakePicture->setEnabled(false);
	mVLayout->addWidget(mStartRangeFinder);
	mVLayout->addWidget(mTakePicture);
	mVLayout->addWidget(mStatus);
	setLayout(mVLayout);
	setFixedSize(1350, 1000);

	connect(mTakePicture, &QPushButton::clicked, this, &QOperationWidget::getDistance);
	connect(mStartRangeFinder, &QPushButton::clicked, this, &QOperationWidget::startRangeFinder);
	connect(&mRangeFinder->mSerial, &QAsyncSerialPort::updated, this, &QOperationWidget::statusUpdatedRangeFinder);
	connect(this, &QLut::fileSuccessfullyOpened, this, &QOperationWidget::fileOpened);
	connect(this, &QOperationWidget::fileSuccessfullyOpened, this, &QOperationWidget::enableTakePicture);
	connect(this, &QLut::fileIsEmpty, this, &QOperationWidget::lutEmpty);
	connect(this, &QLut::fileIsEmpty, this, &QOperationWidget::disableTakePicture);
	connect(mAlgoFocus, &QAlgoFocus::algorithmeDone, this, &QOperationWidget::takePicutre);
}

QOperationWidget::~QOperationWidget()
{
}

void QOperationWidget::stopTracking(double distanceFromObject, double quality, double temperature)
{
	mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::DeactivateLaser);
	mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::StopTracking);
}

void QOperationWidget::startRangeFinder()
{
	if (!mRangeFinder->mDeviceConnected) {
		mRangeFinder->connectSerialPort();
	}
	if (mRangeFinder->mDeviceConnected) {
		mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::StartContinuousTracking);
	}
	else {
		mStatus->setText("Could not connect to the range finder");
	}
}

void QOperationWidget::getDistance()
{
	moveFocuser(mRangeFinder->mDistanceStats.average());
}

void QOperationWidget::moveFocuser(double distanceFromObject)
{
	mLutWidget->setFindYValue(distanceFromObject);
	mLutWidget->findY(distanceFromObject);
	if (!mLutWidget->errorDuringFindY()) {
		mAlgoFocus->algorithmeOperation(mLutWidget->findYResultLabel().toInt());
	}
	else {
		mStatus->setText("Error during the find Y");
	}
}

void QOperationWidget::takePicutre(int blurryLevel)
{
	QImage *picture = new QImage();
	picture->convertToFormat(QImage::Format::Format_ARGB32);
	mMatroxSimpleGrabber->grab(*picture);
	QImageDisplay * imageDisplay = new QImageDisplay(picture);
	connect(imageDisplay, &QImageDisplay::saveButtonEvent, this, &QOperationWidget::savePicture);
	imageDisplay->show();
}

void QOperationWidget::savePicture(QImage * picture)
{
	if (mFilePath.endsWith(".csv")) {
		int dotPosition = mFilePath.lastIndexOf(QChar('.'));
		mFilePath.remove(dotPosition, 4);
	}
	QString imagePath = QFileDialog::getSaveFileName(
		this,
		tr("Save File"),
		mFilePath,
		tr("JPEG (*.jpg *.jpeg);;PNG (*.png)")
	);
	picture->save(imagePath);
	delete picture;
	mStatus->setText("Picture successfully saved");

}

void QOperationWidget::enableTakePicture()
{
	mTakePicture->setEnabled(true);
}

void QOperationWidget::disableTakePicture()
{
	mTakePicture->setEnabled(false);
}

void QOperationWidget::closeEvent(QCloseEvent *event)
{
	mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::DeactivateLaser);
	mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::StopTracking);
	mRangeFinder->mSerial.closeSerialPort();
	delete mRangeFinder;
	QLut::closeEvent(event);
}

void QOperationWidget::statusUpdatedRangeFinder(QString message)
{
	mStatus->setText("RangeFinder : " + message);
}

void QOperationWidget::statusUpdatedFocuser(QString message)
{
	mStatus->setText("Focuser : " + message);
}

void QOperationWidget::fileOpened()
{
	mStatus->setText("File is now open");
}

void QOperationWidget::lutEmpty()
{
	mStatus->setText("The file was empty");

}