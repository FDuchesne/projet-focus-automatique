#include "..\include\QImageDisplay.h"
#include <QGraphicsScene>
#include <QVBoxLayout>
#include <QGraphicsView>
#include <QPushButton>

QImageDisplay::QImageDisplay(QImage *image, QWidget *parent)
	: QWidget(parent)
{
	mImage = image;
	QPixmap imagePix;
	QGraphicsScene *scene;

	imagePix = QPixmap::fromImage(*mImage);
	double x = mImage->size().height();

	scene = new QGraphicsScene(this);
	scene->addPixmap(imagePix);
	scene->setSceneRect(imagePix.rect());
	QGraphicsView * mGraphicView = new QGraphicsView(scene);
	QVBoxLayout * vBox = new QVBoxLayout();
	QPushButton * okButton = new QPushButton("Save");
	QPushButton *cancelButton = new QPushButton("Cancel");

	vBox->addWidget(mGraphicView);
	QHBoxLayout * hBox = new QHBoxLayout();
	hBox->addWidget(okButton);
	hBox->addWidget(cancelButton);
	vBox->addLayout(hBox);
	setLayout(vBox);

	connect(okButton, &QPushButton::clicked, this, &QImageDisplay::saveButtonPressed);
	connect(cancelButton, &QPushButton::clicked, this, &QImageDisplay::buttonPressed);
}	

QImageDisplay::~QImageDisplay()
{
}

void QImageDisplay::saveButtonPressed()
{
	emit hide();
	emit saveButtonEvent(mImage);
	buttonPressed();
}

void QImageDisplay::buttonPressed()
{
	emit close();
}