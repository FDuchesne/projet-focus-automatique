/*! \mainpage 
*
* \section intro_sec Introduction
*
*
*	Ce programme permet de prendre une photo d'un éolienne lors d'une opération. Il utilise la mise au foyer automatique actif & passif
*	pour une obtention d'image d'une bonne qualité rapidement. Il utilise le QRangeFinderAR2000 pour déterminer sa distance par rapport à
*	l'objet observé, le QFocusseur pour faire sa mise au foyer, le LutAbstract et ses sous branches pour rapidement se placer près du foyer
*	et le QMatroxSimpleGrabber pour prendre l'image. Le widget de départ permet à l'utilisateur de partir les interfaces graphiques des
*	différentes composantes (RangeFinder, Focusseur, Lut) de façon indépendantes. Le widget opération est celui qui s'occupe de prendre l'image.
	*Fonctionnement: Simplement ouvrir un fichier csv d'une calibration antérieur et partir le range finder afin de déterminer la distance
	avant de prendre une image. Se référer au classe respective pour plus de détails.
*
*
*
*
*	
* \section install_sec Installation
*
*
* 
*/



#include <QtWidgets/QApplication>
#include <QDebug>
#include "..\include\QWizardSelector.h"




int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QWizardSelector b;
	b.show();

	//QFocusseurGUI b;
	//b.show();

	//QRangeFinderGUI b;
	//b.show();

	//QCGEPro cge;

	return a.exec();
}
