#include "..\..\..\include\Serial Port\RangeFinder\QStatisticsWidget.h"
#include "..\..\..\include\Serial Port\Global\QMovingStatistics.h"
#include <QProgressBar>
#include <QStackedLayout>

QStatisticsWidget::QStatisticsWidget(QWidget * parent)
	: QWidget(parent)
{

	mQvbl = new QVBoxLayout;
	setLayout(mQvbl);

	mProgress = new QProgressBar;
	mProgress->setOrientation(Qt::Horizontal);
	mFull = new QLabel("Full");
	mStackLayout = new QStackedLayout;
	mStackLayout->insertWidget(0, mProgress);
	mStackLayout->insertWidget(1, mFull);
	mStackLayout->setCurrentIndex(0);

	mQhbl[0] = new QHBoxLayout;
	mQhbl[0]->addWidget(new QLabel("Buffer :"));
	mQhbl[0]->addLayout(mStackLayout);
	mQhbl[0]->addStretch();
	mReset = new QPushButton("Reset");
	mQhbl[0]->addWidget(mReset);
	mQvbl->addLayout(mQhbl[0]);

	mGrid = new QGridLayout;
	mQvbl->addLayout(mGrid);


	mGrid->addWidget(new QLabel("Minimum : "), 0, 0);
	mMin = new QLineEdit;
	mMin->setReadOnly(true);
	mGrid->addWidget(mMin, 0, 1);
	mGrid->addWidget(new QLabel("m"), 0, 2);

	mGrid->addWidget(new QLabel("Maximum : "), 1, 0);
	mMax = new QLineEdit;
	mMax->setReadOnly(true);
	mGrid->addWidget(mMax, 1, 1);
	mGrid->addWidget(new QLabel("m"), 1, 2);

	mGrid->addWidget(new QLabel("Average : "), 2, 0);
	mAverage = new QLineEdit;
	mAverage->setReadOnly(true);
	mGrid->addWidget(mAverage, 2, 1);
	mGrid->addWidget(new QLabel("m"), 2, 2);

	mGrid->addWidget(new QLabel("Standard Deviation : "), 3, 0);
	mStdDev = new QLineEdit;
	mStdDev->setReadOnly(true);
	mGrid->addWidget(mStdDev, 3, 1);
	mGrid->addWidget(new QLabel("m"), 3, 2);


	connect(mReset, &QPushButton::clicked, this, &QStatisticsWidget::resetPressed);
}

QStatisticsWidget::~QStatisticsWidget() {

}

void QStatisticsWidget::showStats(QMovingStatistics const &stats) {
	mMin->setText(QString::number(stats.min()));
	mMax->setText(QString::number(stats.max()));
	mAverage->setText(QString::number(stats.average()));
	mStdDev->setText(QString::number(stats.stdDeviation()));

	mProgress->setMaximum(stats.data().size());
	mProgress->setValue(stats.data().count());
	if (mProgress->value() == mProgress->maximum()) {
		mStackLayout->setCurrentIndex(1);
	}
	else
	{
		mStackLayout->setCurrentIndex(0);
	}
}

void QStatisticsWidget::updateProgBar(QMovingStatistics const &stats) {

}