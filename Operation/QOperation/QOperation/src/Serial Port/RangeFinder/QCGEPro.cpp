#include "..\..\..\include\Serial Port\RangeFinder\QCGEPro.h"
#include <Windows.h>
#include <QDebug>


//mSerial.sendMessage("V");
////Sleep(100);
//mSerial.sendMessage("T.");
////Sleep(1000);
//mSerial.sendMessage(QByteArray::fromHex("50011105") + "..." + QByteArray::fromHex("01"));
////Sleep(100);
//mSerial.sendMessage(QByteArray::fromHex("50011105") + "..." + QByteArray::fromHex("01"));
////Sleep(100);
//mSerial.sendMessage(QByteArray::fromHex("500110") + "�..." + QByteArray::fromHex("02"));
////Sleep(100);
//mSerial.sendMessage(QByteArray::fromHex("500110") + "�..." + QByteArray::fromHex("02"));
////Sleep(100);
//mSerial.sendMessage(QByteArray::fromHex("500110") + "�..." + QByteArray::fromHex("02"));
////Sleep(100);
//mSerial.sendMessage(QByteArray::fromHex("50041004") + "@...");
////Sleep(100);
//mSerial.sendMessage(QByteArray::fromHex("50041004") + "@...");
////Sleep(100);
//mSerial.sendMessage(QByteArray::fromHex("500410") + ":�...");
////Sleep(100);
////mSerial.sendMessage("P..8....");
//mSerial.sendMessage(QByteArray::fromHex("50011038") + "....");
////Sleep(100);
//mSerial.sendMessage(QByteArray::fromHex("500410") + ":�...");
////Sleep(100);
//mSerial.sendMessage(QByteArray::fromHex("50011038") + "....");
////Sleep(100);
//// V..#T.#P.......#P.......#P..�......#P..�......#P..�......#P...@...#P...@...#P..:�...#P..8....#P..:�...#P..8....#
//mSerial.sendMessage(QByteArray::fromHex("50041006") + "...." + QByteArray::fromHex("50041106") + "....");
//for (int i = 0; i < 200; ++i) {
//	mSerial.sendMessage(QByteArray::fromHex("5002102409") + "..."); 
//	//mSerial.sendMessage("P..$....");
//}


QString const QCGEPro::SEPARATOR{ "," };
QString const QCGEPro::TERMINATOR{ "" };


QCGEPro::QCGEPro(QObject *parent)
	: QObject(parent)
{
	connect(&mSerial, &QCommandSerialPort::responseMatchesCommand, this, &QCGEPro::handleMatchingResponse);
	connect(&mSerial, &QAsyncSerialPort::connectionUpdated, this, &QCGEPro::deviceConnected);
	
	fillDictionary();
	mSerial.openSerialPort("COM3", QAsyncSerialPort::BaudRate::BR9600, QSerialPort::DataBits::Data8, QSerialPort::Parity::NoParity, QSerialPort::StopBits::OneStop, QSerialPort::FlowControl::SoftwareControl);
}

QCGEPro::~QCGEPro()
{
	
}


void QCGEPro::sendCommandNoParams(Command command) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[command], QList<QVariant>());
	mSerial.writeToBuffer(commandAndParams);
}

void QCGEPro::deviceConnected(bool connected) {
	if (connected) {
		qDebug() << "Connected";
		//sendCommandNoParams(Command::InitSwitchAX);
		//sendCommandNoParams(Command::InitSwitchAY);
		sendCommandNoParams(Command::InitSwitchBX);
		sendCommandNoParams(Command::InitSwitchBY);
		sendCommandNoParams(Command::IsGoingToSwitchX);
	}
}

void QCGEPro::handleMatchingResponse(QByteArray const &response, const QSerialCommand &command) {
	if (command == *mSerialCommands[Command::IsGoingToSwitchX]) {
		if (response == "\xff#") {
			sendCommandNoParams(Command::IsGoingToSwitchY);
		}
		else {
			sendCommandNoParams(Command::IsGoingToSwitchX);
		}
	}
	else if (command == *mSerialCommands[Command::IsGoingToSwitchY]) {
		if (response != "\xff#") {
			sendCommandNoParams(Command::IsGoingToSwitchY);
		}
		else {
			sendCommandNoParams(Command::GetPosition);
		}
	}
	else if (command == *mSerialCommands[Command::GetPosition]) {
		qDebug() << "Position";
	}
}

void QCGEPro::testCommand() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QCGEPro::testCommand2() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand2, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QCGEPro::testCommand3() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand3, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QCGEPro::testCommand4() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand4, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QCGEPro::testCommand5() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand5, QList<QVariant>());
	mSerial.writeToBuffer(command);
}

void QCGEPro::fillDictionary() {

	mSerialCommands[Command::InitSwitchAX] = new QSerialCommand( 
		QByteArray("P\x04\x10\x06\x00\x00\x00\x00", 8),				// cmd
		"InitSwitchAX",												// name
		QSerialCommand::IOType::In,									// IOType
		0,															// # params
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,	// BlockingMode
		SerialOperationMode::FluxMode::Pull,						// FluxMode	
		TERMINATOR,													// eol car
		SEPARATOR,													// separator
		"",															// family
		"",															// short desc
		QList<QByteArray>{ QByteArray("#") }						// response
		);

	mSerialCommands[Command::InitSwitchAY] = new QSerialCommand( 
		QByteArray("P\x04\x11\x06\x00\x00\x00\x00", 8),
		"InitSwitchAY",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"",
		"",
		QList<QByteArray>{ QByteArray("#") }
		);

	mSerialCommands[Command::InitSwitchBX] = new QSerialCommand( 
		QByteArray("P\x01\x10\x0b\x00\x00\x00\x00", 8),
		"InitSwitchBX",															
		QSerialCommand::IOType::In,									
		0,															
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Pull,						
		TERMINATOR,													
		SEPARATOR,													
		"",															
		"",															
		QList<QByteArray>{ QByteArray("#") }
	);

	mSerialCommands[Command::InitSwitchBY] = new QSerialCommand( 
		QByteArray("P\x01\x11\x0b\x00\x00\x00\x00", 8),
		"InitSwitchBY",															
		QSerialCommand::IOType::In,									
		0,															
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Pull,						
		TERMINATOR,													
		SEPARATOR,													
		"",															
		"",															
		QList<QByteArray>{ QByteArray("#") }
	);

	mSerialCommands[Command::IsGoingToSwitchX] = new QSerialCommand( 
		QByteArray("P\x01\x10\x12\x00\x00\x00\x01", 8),
		"InitSwitchBY",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"",
		"",
		QList<QByteArray>{ QByteArray("\x00#", 2), QByteArray("\xff#", 2) }
	);

	mSerialCommands[Command::IsGoingToSwitchY] = new QSerialCommand( 
		QByteArray("P\x01\x11\x12\x00\x00\x00\x01", 8),
		"InitSwitchBY",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"",
		"",
		QList<QByteArray>{ QByteArray("\x00#", 2), QByteArray("\xff#", 2) }
	);

	mSerialCommands[Command::GetPosition] = new QSerialCommand(
		"Z",
		"GetPosition",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"",
		"",
		QRegularExpression(".{4},.{4}")
	);
}
