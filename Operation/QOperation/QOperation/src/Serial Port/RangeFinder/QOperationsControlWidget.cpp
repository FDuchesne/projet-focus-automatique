#include "..\..\..\include\Serial Port\RangeFinder\QOperationsControlWidget.h"
#include <QMessageBox>

QOperationsControlWidget::QOperationsControlWidget(QRangeFinderAR2000 * rangeFinder, QWidget * parent)
	:	QWidget(parent),
		mRangeFinder{ rangeFinder }
{
	mGrid = new QGridLayout;
	setLayout(mGrid);

	mConnect = new QPushButton("Connect");
	mConnect->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mLaser = new QPushButton("Start Laser");
	mLaser->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mTracking = new QPushButton("Start");
	mTracking->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	

	mContinuousTracking = new QRadioButton("Continuous Tracking");
	mDistanceTracking = new QRadioButton("Distance Tracking");
	mSingleMeasurement = new QRadioButton("Single Measurement");
	mGroup = new QButtonGroup;
	mGroup->addButton(mContinuousTracking);
	mGroup->addButton(mDistanceTracking);
	mGroup->addButton(mSingleMeasurement);

	mTrackingGrid = new QGridLayout;
	mTrackingGrid->addWidget(mTracking, 0, 0, 3, 1);
	mTrackingGrid->addWidget(mContinuousTracking, 0, 1);
	mTrackingGrid->addWidget(mDistanceTracking, 1, 1);
	mTrackingGrid->addWidget(mSingleMeasurement, 2, 1);
	mBox = new QGroupBox("Tracking");
	mBox->setLayout(mTrackingGrid);

	mGrid->addWidget(mConnect, 0, 0, 2, 1);
	mGrid->addWidget(mLaser, 0, 1, 2, 1);
	mGrid->addWidget(mBox, 2, 0, 3, 2);
	/*mGrid->addWidget(mTracking, 0, 1, 2, 1);
	mGrid->addWidget(mContinuousTracking, 2, 1);
	mGrid->addWidget(mDistanceTracking, 3, 1);
	mGrid->addWidget(mSingleMeasurement, 4, 1);*/

	mLaser->setEnabled(false);
	mTracking->setEnabled(false);
	mSingleMeasurement->setChecked(true);

	connect(mConnect, &QPushButton::clicked, this, &QOperationsControlWidget::toggleConnection);
	connect(&mRangeFinder->mSerial, &QAsyncSerialPort::connectionUpdated, this, &QOperationsControlWidget::adjustConnectButton);
	connect(mTracking, &QPushButton::clicked, this, &QOperationsControlWidget::startTracking);
	connect(mRangeFinder, &QRangeFinderAR2000::trackingChanged, this, &QOperationsControlWidget::adjustTrackingButton);
	connect(mLaser, &QPushButton::clicked, this, &QOperationsControlWidget::toggleLaser);
	connect(mRangeFinder, &QRangeFinderAR2000::laserChanged, this, &QOperationsControlWidget::adjustLaserButton);
	connect(&mRangeFinder->mSerial, &QAsyncSerialPort::connectionUpdated, this, &QOperationsControlWidget::adjustButtons);
}

void QOperationsControlWidget::adjustToRangeFinder() {
	adjustTrackingButton(mRangeFinder->mIsTracking);
	adjustLaserButton(!mRangeFinder->mLaserOn);
}

void QOperationsControlWidget::toggleConnection() {
	mConnect->setEnabled(false);
	if (mRangeFinder->mDeviceConnected) {
		mRangeFinder->mSerial.closeSerialPort();
	}
	else {
		mRangeFinder->connectSerialPort();
	}
}

void QOperationsControlWidget::toggleLaser() {
	if (exitDevelopmentMode()) {
		mLaser->setEnabled(false);
		if (mRangeFinder->mLaserOn) {
			mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::DeactivateLaser);
		}
		else {
			mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::ActivateLaser);
		}
	}
}

void QOperationsControlWidget::startTracking() {
	if (exitDevelopmentMode()) {
		if (mRangeFinder->mIsTracking) {
			mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::StopTracking);
			mTracking->setEnabled(false);
		}
		else {
			if (mDistanceTracking->isChecked()) {
				mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::StartDistanceTracking);
				mTracking->setEnabled(false);
			}
			else if (mContinuousTracking->isChecked()) {
				mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::StartContinuousTracking);
				mTracking->setEnabled(false);
			}
			else if (mSingleMeasurement->isChecked()) {
				mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::DoSingleMeasurement);
			}
		}
	}
}

void QOperationsControlWidget::adjustConnectButton(bool connected) {
	mConnect->setEnabled(true);
	if (connected) {
		mConnect->setText("Disconnect");
	}
	else {
		mConnect->setText("Connect");
	}
}
void QOperationsControlWidget::adjustTrackingButton(bool on) {
	mTracking->setEnabled(true);
	if (on) {
		mTracking->setText("Stop");
	}
	else {
		mTracking->setText("Start");
	}
}
void QOperationsControlWidget::adjustLaserButton(bool on) {
	mLaser->setEnabled(true);
	if (on) {
		mLaser->setText("Stop Laser");
	}
	else {
		mLaser->setText("Start Laser");
	}
}

void QOperationsControlWidget::adjustButtons(bool connected) {
	mTracking->setEnabled(connected);
	mLaser->setEnabled(connected);
}

bool QOperationsControlWidget::exitDevelopmentMode() {
	if (mRangeFinder->mSerial.developmentMode()) {
		QMessageBox opWarning;
		opWarning.setWindowTitle("Switching to Operation Mode");
		opWarning.setText("Switching to Operation mode. Some changes made will be discarded. Continue?");
		opWarning.setIcon(QMessageBox::Question);
		opWarning.setStandardButtons(QMessageBox::Yes);
		opWarning.addButton(QMessageBox::No);
		opWarning.setDefaultButton(QMessageBox::Yes);
		if (opWarning.exec() == QMessageBox::Yes) {
			mRangeFinder->applySettings();
			mRangeFinder->mSerial.setDevelopmentMode(false);
			adjustToRangeFinder();
			return true;
		}
		return false;
	}
	return true;
}


QOperationsControlWidget::~QOperationsControlWidget() {

}
