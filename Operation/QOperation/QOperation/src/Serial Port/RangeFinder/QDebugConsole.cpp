#include "..\..\..\include\Serial Port\RangeFinder\QDebugConsole.h"
#include  <QScrollBar>
#include <QtCore/QDebug>

QDebugConsole::QDebugConsole(QWidget * parent) 
	: QPlainTextEdit(parent), mEcho(true) 
{
	//ui.setupUi(this);
	document()->setMaximumBlockCount(1000);
	QPalette p = palette();
	p.setColor(QPalette::Base, Qt::black);
	p.setColor(QPalette::Text, Qt::white);
	setPalette(p);
	setReadOnly(true);


}

void QDebugConsole::appendMessage(const QString &text)
{
	if (mEcho) {
		moveCursor(QTextCursor::End);
		insertPlainText(text);
	}

}
void QDebugConsole::setEcho(bool echo)
{
	mEcho = echo;
}

QDebugConsole::~QDebugConsole() {

}
