#include "..\..\..\include\Serial Port\Global\QDebugCommandList.h"
#include "..\..\..\include\Serial Port\RangeFinder\QRangeFinderAR2000.h"


QDebugCommandList::QDebugCommandList(QList<QSerialCommand const *> const &commands, QWidget * parent)
	: QTableView(parent) 
{
	/*for ( QSerialCommand  command : commands){
	mList.append(command.name());
	}*/

	//mModel = new QStandardItemModel(0,3, this);

	for (QSerialCommand const * command : commands) {
		mNameList.append(command->name());
		mCmdList.append(command->command());
		mShortDescList.append(command->shortDesc());
		mFamilyList.append(command->family());
	}

	mModel = new QStandardItemModel(mNameList.count(), mList.count(), this);
	setModel(mModel);
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setEditTriggers(QAbstractItemView::NoEditTriggers);
	verticalHeader()->setVisible(false);
	mModel->setHorizontalHeaderLabels(QString("Name;Command;Family;Description").split(";"));
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);


	for (int i = 0; i < mNameList.count(); i++) {
		mModel->setItem(i, 0, new QStandardItem(QString(mNameList[i])));
		mModel->setItem(i, 1, new QStandardItem(QString(mCmdList[i])));
		mModel->setItem(i, 2, new QStandardItem(QString(mFamilyList[i])));
		mModel->setItem(i, 3, new QStandardItem(QString(mShortDescList[i])));
		/*for (int j = 0;j < mList.count(); j++) {
		mModel->setItem(i, j , new QStandardItem(QString(mList[i][j])));
		}*/
	}
	resizeColumnsToContents();

	//connect(this, &QDebugCommandList::clicked, this, &QDebugCommandList::sendInfo);
	connect(this, &QDebugCommandList::doubleClicked, this, &QDebugCommandList::sendCmd);
}

void QDebugCommandList::sendInfo() {
	QString cmd = currentIndex().data().toString();
	emit infoSent(cmd);
}
void QDebugCommandList::sendCmd() {
	setCurrentIndex(mModel->index(currentIndex().row(), 1, QModelIndex()));
	emit cmdSent(currentIndex().data().toString());
}

QDebugCommandList::~QDebugCommandList() {

}
