#include "..\..\..\include\Serial Port\Focuser\QDevelopmentTabFocusseur.h"
#include "..\..\..\include\Serial Port\Global\QAsyncSerialPort.h"
#include <QMessageBox>

QDevelopmentTabFocusseur::QDevelopmentTabFocusseur(QCommandSerialPort * serial, QList<QSerialCommand const *> const &serialCommands, QWidget *parent)
	: QWidget(parent), mSerial{ serial }
{
	mConsole = new QDebugConsoleFocusseur;
	mCommandLine = new QDebugCommandLineFocusseur(serialCommands[0]->eolCar());
	mCommandList = new QDebugCommandList(serialCommands);
	mQvbl = new QVBoxLayout;
	mQvbl->addWidget(mConsole);
	mQvbl->addWidget(mCommandList);
	mQvbl->addWidget(mCommandLine);

	setLayout(mQvbl);
	resize(500, height());

	connect(mCommandList, &QDebugCommandList::cmdSent, mCommandLine, &QDebugCommandLineFocusseur::writeCmd);
	connect(mCommandLine, &QDebugCommandLineFocusseur::messageSent, mConsole, &QDebugConsoleFocusseur::appendMessage);
	connect(mCommandLine->echoBox(), &QCheckBox::stateChanged, mConsole, &QDebugConsoleFocusseur::setEcho);
	connect(mCommandLine, &QDebugCommandLineFocusseur::messageSent, this, &QDevelopmentTabFocusseur::switchToDevMode);
	connect(mSerial, &QAsyncSerialPort::dataRead, this, &QDevelopmentTabFocusseur::handleResponse);
}

void QDevelopmentTabFocusseur::sendMessage(QString message) {
	mSerial->setDevelopmentMode(true); // TODO : show warning that commandManagement will completely stop
	mSerial->sendMessage(message);
}

void QDevelopmentTabFocusseur::echoCommand(QString s)
{
	if (mCommandLine->localEchoEnabled) {
		mConsole->moveCursor(QTextCursor::End);
		mConsole->insertPlainText(s);
	}
}

void QDevelopmentTabFocusseur::handleResponse(QByteArray data) {
	QString response(data);
	mConsole->moveCursor(QTextCursor::End);
	mConsole->insertPlainText(response);
}
void QDevelopmentTabFocusseur::switchToDevMode(QString message) {
	if (mSerial->developmentMode()) {
		sendMessage(message); //alrdy in devMode, just send Msg
	}
	else {
		QMessageBox devModeWarning;
		devModeWarning.setWindowTitle("Switching to Development Mode");
		devModeWarning.setText("Switching to Development Mode. Operations will not be managed. Are you sure?");
		devModeWarning.setIcon(QMessageBox::Question);
		devModeWarning.setStandardButtons(QMessageBox::Yes);
		devModeWarning.addButton(QMessageBox::No);
		devModeWarning.setDefaultButton(QMessageBox::Yes);
		if (devModeWarning.exec() == QMessageBox::Yes) {
			sendMessage(message);
		}
	}
}

QDevelopmentTabFocusseur::~QDevelopmentTabFocusseur() {

}
