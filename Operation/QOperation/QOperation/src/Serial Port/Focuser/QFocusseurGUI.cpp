#include "..\..\..\include\Serial Port\Focuser\QFocusseurGUI.h"

#include <QLayout>
#include "..\..\..\include\Serial Port\Focuser\QRFStatusBarFocusseur.h"


QFocusseurGUI::QFocusseurGUI(QWidget * parent)
	: QWidget(parent)
{
	mTabs = new QTabWidget;
	mOperations = new QOperationsTabFocusseur(mFocusseur);
	mDevelopment = new QDevelopmentTabFocusseur(&mFocusseur.mSerial, mFocusseur.mSerialCommands.values());
	mParams = new QSettingsTabFocusseur(mFocusseur);
	mLegends = new QLegendTabFocusseur();
	mTabs->addTab(mOperations, "Operations");
	mTabs->addTab(mParams, "Parameters");
	mTabs->addTab(mDevelopment, "Development");
	mTabs->addTab(mLegends, "Error codes");
	setWindowTitle("Focusseur");

	mStatusBar = new QRFStatusBarFocusseur(&mFocusseur);
	mQvbl = new QVBoxLayout;
	setLayout(mQvbl);
	mQvbl->addWidget(mTabs);
	mQvbl->addWidget(mStatusBar);

	mQvbl->setMargin(5);

	connect(mTabs, &QTabWidget::tabBarClicked, this, &QFocusseurGUI::tabSelected); //TODO :: voir cmt connecter seulement pour 1 onglet??

}
void QFocusseurGUI::tabSelected(int index) {
	// Operation mode
	if (index == 0) {
		if (mFocusseur.mSerial.developmentMode()) {
			QMessageBox opWarning;
			opWarning.setWindowTitle("Switching to Operation Mode");
			opWarning.setText("Switching to Operation mode. Some changes made will be discarded. Continue?");
			opWarning.setIcon(QMessageBox::Question);
			opWarning.setStandardButtons(QMessageBox::Yes);
			opWarning.addButton(QMessageBox::No);
			opWarning.setDefaultButton(QMessageBox::Yes);
			if (opWarning.exec() == QMessageBox::Yes) {
				mFocusseur.applySettings();
				mFocusseur.mSerial.setDevelopmentMode(false);
			}
		}
	}

	else if (index == 1) {
		mFocusseur.mSettingsFocusseur.save(QFocusseur::TEMP_INI);
	}
}

QFocusseurGUI::~QFocusseurGUI() {

}


void QFocusseurGUI::closeEvent(QCloseEvent * event)
{
	emit widgetClosed();
}