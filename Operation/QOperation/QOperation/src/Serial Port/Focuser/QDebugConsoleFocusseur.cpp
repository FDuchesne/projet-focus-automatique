#include "..\..\..\include\Serial Port\Focuser\QDebugConsoleFocusseur.h"
#include  <QScrollBar>
#include <QtCore/QDebug>

QDebugConsoleFocusseur::QDebugConsoleFocusseur(QWidget * parent) 
	: QPlainTextEdit(parent), mEcho(true) 
{
	document()->setMaximumBlockCount(1000);
	QPalette p = palette();
	p.setColor(QPalette::Base, Qt::black);
	p.setColor(QPalette::Text, Qt::white);
	setPalette(p);
	setReadOnly(true);


}

void QDebugConsoleFocusseur::appendMessage(const QString &text)
{
	if (mEcho) {
		moveCursor(QTextCursor::End);
		insertPlainText(text);
	}

}
void QDebugConsoleFocusseur::setEcho(bool echo)
{
	mEcho = echo;
}

QDebugConsoleFocusseur::~QDebugConsoleFocusseur() {

}
