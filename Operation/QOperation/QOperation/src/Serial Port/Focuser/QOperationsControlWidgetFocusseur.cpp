#include "..\..\..\include\Serial Port\Focuser\QOperationsControlWidgetFocusseur.h"
#include <QMessageBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QHBoxLayout>
#include <Windows.h>

QOperationsControlWidgetFocusseur::QOperationsControlWidgetFocusseur(QFocusseur * focusseur, QWidget * parent)
	:	QWidget(parent),
		mFocusseur{ focusseur }
{
	mSlowMotionMode = false;
	mGrid = new QGridLayout;
	setLayout(mGrid);

	mConnect = new QPushButton("Connect");
	mConnect->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mInit = new QPushButton("Init");
	mInit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mInit->setEnabled(false);
	mTest = new QPushButton("Test");
	mTest->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mTest->setEnabled(false);
	mStopTest = new QPushButton("Stop test");
	mStopTest->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mStopTest->setEnabled(false);
	mAbsolutePosition = new QLineEdit;
	mAbsolutePosition->setPlaceholderText("ex : 56.1");
	mGoAbs = new QPushButton("Go");
	mGoAbs->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mGoAbs->setEnabled(false);
	mRelativePosition = new QLineEdit;
	mRelativePosition->setPlaceholderText("ex : 56.1");
	mRelPlus = new QPushButton("+");
	mRelPlus->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mRelPlus->setEnabled(false);
	mRelMinus = new QPushButton("-");
	mRelMinus->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mRelMinus->setEnabled(false);

	mGetPos = new QPushButton("Get position");
	mGetPos->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mGetPos->setEnabled(false);
	mSlowMo = new QPushButton("Manually controlled motion");
	mSlowMo->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	mSlowMo->setEnabled(false);
	mGroup = new QButtonGroup;
	mMoveGrid = new QGridLayout;
	mMoveGrid->addWidget(mInit, 0, 0, 1, 4);
	mMoveGrid->addWidget(new QLabel("Absoute : "), 1, 0);
	mMoveGrid->addWidget(mAbsolutePosition, 1, 1);
	mMoveGrid->addWidget(mGoAbs, 1, 2, 1, 2);
	mMoveGrid->addWidget(new QLabel("Relative : "), 2, 0);
	mMoveGrid->addWidget(mRelativePosition, 2, 1);
	mMoveGrid->addWidget(mRelPlus, 2, 2);
	mMoveGrid->addWidget(mRelMinus, 2, 3);
	mMoveGroup = new QGroupBox("Moving");
	mMoveGroup->setLayout(mMoveGrid);

	mTestLayout = new QHBoxLayout;
	mTestLayout->addWidget(mTest);
	mTestLayout->addWidget(mStopTest);
	mTestGroup = new QGroupBox("Testing");
	mTestGroup->setLayout(mTestLayout);

	mManualGrid = new QGridLayout;
	mManualGrid->setSpacing(1);
	QLabel * slowMoSpeedLabel = new QLabel("Step size / speed");
	slowMoSpeedLabel->setAlignment(Qt::AlignCenter);
	slowMoSpeedLabel->setStyleSheet("QLabel { background-color: gray; padding : 5; color : white; }");
	QLabel * slowMoInLabel = new QLabel("Going inward");
	slowMoInLabel->setAlignment(Qt::AlignCenter);
	slowMoInLabel->setStyleSheet("QLabel { background-color: gray; padding : 5; color : white; }");
	QLabel * slowMoOutLabel = new QLabel("Going outward");
	slowMoOutLabel->setAlignment(Qt::AlignCenter);
	slowMoOutLabel->setStyleSheet("QLabel { background-color: gray; padding : 5; color : white; }");
	QLabel * slowMoInstructLabel = new QLabel("Press long for continuous motion.");
	slowMoInstructLabel->setAlignment(Qt::AlignCenter);
	slowMoInstructLabel->setStyleSheet("QLabel { padding : 15; }");
	QLabel * slowMoX1Label = new QLabel("small / x1");
	slowMoX1Label->setAlignment(Qt::AlignCenter);
	slowMoX1Label->setStyleSheet("QLabel { background-color: darkgray; padding : 5; }");
	QLabel * slowMoX2Label = new QLabel("medium / x2");
	slowMoX2Label->setAlignment(Qt::AlignCenter);
	slowMoX2Label->setStyleSheet("QLabel { background-color: darkgray; padding : 5; }");
	QLabel * slowMoX4Label = new QLabel("big / x4");
	slowMoX4Label->setAlignment(Qt::AlignCenter);
	slowMoX4Label->setStyleSheet("QLabel { background-color: darkgray; padding : 5; }");
	QLabel * slowMoQLabel = new QLabel("'Q' key");
	slowMoQLabel->setAlignment(Qt::AlignCenter);
	slowMoQLabel->setStyleSheet("QLabel { background-color: darkgray; padding : 5; }");
	QLabel * slowMoALabel = new QLabel("'A' key");
	slowMoALabel->setAlignment(Qt::AlignCenter);
	slowMoALabel->setStyleSheet("QLabel { background-color: darkgray; padding : 5; }");
	QLabel * slowMoZLabel = new QLabel("'Z' key");
	slowMoZLabel->setAlignment(Qt::AlignCenter);
	slowMoZLabel->setStyleSheet("QLabel { background-color: darkgray; padding : 5; }");
	QLabel * slowMoWLabel = new QLabel("'W' key");
	slowMoWLabel->setAlignment(Qt::AlignCenter);
	slowMoWLabel->setStyleSheet("QLabel { background-color: darkgray; padding : 5; }");
	QLabel * slowMoSLabel = new QLabel("'S' key");
	slowMoSLabel->setAlignment(Qt::AlignCenter);
	slowMoSLabel->setStyleSheet("QLabel { background-color: darkgray; padding : 5; }");
	QLabel * slowMoXLabel = new QLabel("'X' key");
	slowMoXLabel->setAlignment(Qt::AlignCenter);
	slowMoXLabel->setStyleSheet("QLabel { background-color: darkgray; padding : 5; }");
	mManualGrid->addWidget(slowMoSpeedLabel, 0, 1, 1, 6);
	mManualGrid->addWidget(slowMoInLabel, 0, 7, 1, 3);
	mManualGrid->addWidget(slowMoOutLabel, 0, 10, 1, 3);
	mManualGrid->addWidget(slowMoX1Label, 1, 1, 1, 6);
	mManualGrid->addWidget(slowMoX2Label, 2, 1, 1, 6);
	mManualGrid->addWidget(slowMoX4Label, 3, 1, 1, 6);
	mManualGrid->addWidget(slowMoQLabel, 1, 7, 1, 3);
	mManualGrid->addWidget(slowMoALabel, 2, 7, 1, 3);
	mManualGrid->addWidget(slowMoZLabel, 3, 7, 1, 3);
	mManualGrid->addWidget(slowMoWLabel, 1, 10, 1, 3);
	mManualGrid->addWidget(slowMoSLabel, 2, 10, 1, 3);
	mManualGrid->addWidget(slowMoXLabel, 3, 10, 1, 3);
	mManualGrid->addWidget(slowMoInstructLabel, 4, 0, 1, 14);
	mManualGrid->addWidget(mSlowMo, 5, 0, 1, 14);
	mManualGroup = new QGroupBox("Manual mode");
	mManualGroup->setLayout(mManualGrid);

	mGrid->addWidget(mConnect, 0, 0);

	mGrid->addWidget(mMoveGroup, 1, 0);
	mGrid->addWidget(mGetPos, 2, 0);
	mGrid->addWidget(mManualGroup, 3, 0);
	mGrid->addWidget(mTestGroup, 4, 0);
	connect(mConnect, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::toggleConnection);
	connect(mInit, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::initBtnClicked);
	connect(mGoAbs, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::goToAbsolute);
	connect(mRelPlus, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::relativePlus);
	connect(mRelMinus, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::relativeMinus);
	connect(mTest, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::adjustTestButtons);
	connect(mTest, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::test);
	connect(mStopTest, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::adjustTestButtons);
	connect(mStopTest, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::stopTest);
	connect(mGetPos, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::getPosition);
	connect(mSlowMo, &QPushButton::clicked, this, &QOperationsControlWidgetFocusseur::enableSlowMo);
	connect(&mFocusseur->mSerial, &QAsyncSerialPort::connectionUpdated, this, &QOperationsControlWidgetFocusseur::adjustConnectButton);
	connect(&mFocusseur->mSerial, &QAsyncSerialPort::connectionUpdated, this, &QOperationsControlWidgetFocusseur::adjustButtons);
}

void QOperationsControlWidgetFocusseur::adjustToRangeFinder() {
}

void QOperationsControlWidgetFocusseur::toggleConnection() {
	mConnect->setEnabled(false);
	if (mFocusseur->mDeviceConnected) {
		mFocusseur->mSerial.closeSerialPort();
	}
	else {
		mFocusseur->connectSerialPort();
	}
}

void QOperationsControlWidgetFocusseur::test()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	mFocusseur->sendCommandNoParams(QFocusseur::Command::Test);
}

void QOperationsControlWidgetFocusseur::stopTest()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	mFocusseur->sendCommandNoParams(QFocusseur::Command::Stop);
}

void QOperationsControlWidgetFocusseur::getPosition()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	mFocusseur->sendCommandNoParams(QFocusseur::Command::GetPosition);
}

void QOperationsControlWidgetFocusseur::enableSlowMo()
{
	mConnect->setEnabled(mSlowMotionMode);
	mInit->setEnabled(mSlowMotionMode);
	mAbsolutePosition->setEnabled(mSlowMotionMode);
	mGoAbs->setEnabled(mSlowMotionMode);
	mRelativePosition->setEnabled(mSlowMotionMode);
	mRelPlus->setEnabled(mSlowMotionMode);
	mRelMinus->setEnabled(mSlowMotionMode);
	mTest->setEnabled(mSlowMotionMode);
	mGetPos->setEnabled(mSlowMotionMode);

	if (mSlowMotionMode)
	{
		mSlowMotionMode = false;
		mAbsolutePosition->setReadOnly(false);
		mRelativePosition->setReadOnly(false);
		mSlowMo->setText("Manually controlled motion");

		mStopTest->setEnabled(mSlowMotionMode);
	}
	else
	{
		mSlowMotionMode = true;
		mAbsolutePosition->setReadOnly(true);
		mRelativePosition->setReadOnly(true);

		mSlowMo->setText("Back to normal");
		getPosition();
	}
}

void QOperationsControlWidgetFocusseur::adjustConnectButton(bool connected) {
	mConnect->setEnabled(true);
	if (connected) {
		mConnect->setText("Disconnect");
		Sleep(2000);
		mFocusseur->sendCommandNoParams(QFocusseur::Command::InitSeq);
	}
	else {
		mConnect->setText("Connect");
	}
}
void QOperationsControlWidgetFocusseur::adjustTestButtons()
{
	mTest->setEnabled(!mTest->isEnabled());
	mStopTest->setEnabled(!mStopTest->isEnabled());
}

void QOperationsControlWidgetFocusseur::adjustButtons(bool connected) {
	mInit->setEnabled(connected);
	mAbsolutePosition->setEnabled(connected);
	mGoAbs->setEnabled(connected);
	mRelativePosition->setEnabled(connected);
	mRelPlus->setEnabled(connected);
	mRelMinus->setEnabled(connected);
	mTest->setEnabled(connected);
	mGetPos->setEnabled(connected);
	mSlowMo->setEnabled(connected);
	if (!connected)
		mStopTest->setEnabled(false);
}

void QOperationsControlWidgetFocusseur::initBtnClicked()
{
	mFocusseur->sendCommandNoParams(QFocusseur::Command::InitSeq);
}

void QOperationsControlWidgetFocusseur::goToAbsolute()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	double entry = mAbsolutePosition->text().toDouble() / 1000;
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::GoToAbs], QList<QVariant>{ entry });
	mFocusseur->mSerial.writeToBuffer(commandAndParams);
	mAbsolutePosition->clear();
}
void QOperationsControlWidgetFocusseur::relativePlus()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	double entry = mRelativePosition->text().toDouble() / 1000;
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::RelativePlusSeq], QList<QVariant>{ entry });
	mFocusseur->mSerial.writeToBuffer(commandAndParams);
	mRelativePosition->clear();
}
void QOperationsControlWidgetFocusseur::relativeMinus()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	double entry = mRelativePosition->text().toDouble() / 1000;
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::RelativeMinusSeq], QList<QVariant>{ entry });
	mFocusseur->mSerial.writeToBuffer(commandAndParams);
	mRelativePosition->clear();
}

bool QOperationsControlWidgetFocusseur::exitDevelopmentMode() {
	if (mFocusseur->mSerial.developmentMode()) {
		QMessageBox opWarning;
		opWarning.setWindowTitle("Switching to Operation Mode");
		opWarning.setText("Switching to Operation mode. Some changes made will be discarded. Continue?");
		opWarning.setIcon(QMessageBox::Question);
		opWarning.setStandardButtons(QMessageBox::Yes);
		opWarning.addButton(QMessageBox::No);
		opWarning.setDefaultButton(QMessageBox::Yes);
		if (opWarning.exec() == QMessageBox::Yes) {
			mFocusseur->applySettings();
			mFocusseur->mSerial.setDevelopmentMode(false);
			adjustToRangeFinder();
			return true;
		}
		return false;
	}
	return true;
}


QOperationsControlWidgetFocusseur::~QOperationsControlWidgetFocusseur() {

}
