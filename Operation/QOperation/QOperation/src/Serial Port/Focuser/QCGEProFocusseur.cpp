#include "..\..\..\include\Serial Port\Focuser\QCGEProFocusseur.h"
#include <Windows.h>
#include <QDebug>
#include "..\..\..\include\Serial Port\Focuser\MathUtil.h"

QString const QCGEProFocusseur::SEPARATOR{ "," };
QString const QCGEProFocusseur::TERMINATOR{ "" };


QCGEProFocusseur::QCGEProFocusseur(QObject *parent)
	: QObject(parent),
	isGoingtoSwitchPos {false},
	zerosSet{false}

{
	connect(&mSerial, &QCommandSerialPort::responseMatchesCommand, this, &QCGEProFocusseur::handleMatchingResponse);
	connect(&mSerial, &QAsyncSerialPort::connectionUpdated, this, &QCGEProFocusseur::deviceConnected);
	
	fillDictionary();
	mSerial.openSerialPort("COM3", QAsyncSerialPort::BaudRate::BR9600, QSerialPort::DataBits::Data8, QSerialPort::Parity::NoParity, QSerialPort::StopBits::OneStop, QSerialPort::FlowControl::SoftwareControl);
}

QCGEProFocusseur::~QCGEProFocusseur()
{}


void QCGEProFocusseur::sendCommandNoParams(Command command) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[command], QList<QVariant>());
	mSerial.writeToBuffer(commandAndParams);
}

void QCGEProFocusseur::deviceConnected(bool connected) {
	if (connected) {
		qDebug() << "Connected";
		sendCommandNoParams(Command::InitSwitchBX);
		sendCommandNoParams(Command::InitSwitchBY);
		sendCommandNoParams(Command::IsGoingToSwitchX);
	}
}

void QCGEProFocusseur::handleMatchingResponse(QByteArray const &response, const QSerialCommand &command) {
	//go to switch position and initialize zeros
	if (command == *mSerialCommands[Command::IsGoingToSwitchX]) {
		isGoingtoSwitchPos = true;
		if (response == "\xff#") {
			sendCommandNoParams(Command::IsGoingToSwitchY);
		}
		else {
			sendCommandNoParams(Command::IsGoingToSwitchX);
		}
	}
	else if (command == *mSerialCommands[Command::IsGoingToSwitchY]) {
		if (response == "\xff#") {
			sendCommandNoParams(Command::GetPosition);
		}
		else {
			sendCommandNoParams(Command::IsGoingToSwitchY);
		}
	}
	else if (command == *mSerialCommands[Command::GetPosition]) {
		qDebug() << "Position";
		if (isGoingtoSwitchPos && !zerosSet) {
			QString sResponse(response);
			QStringList zeros = sResponse.split(",");
			bool ok;
			setZeros(zeros[0].toUInt(&ok, 16), zeros[1].toUInt(&ok, 16)); //convert hex to dec
			isGoingtoSwitchPos = false;
			zerosSet = true;
			qDebug() << mZeroPosRA << " " << mZeroPosDEC;
			goToPos(static_cast<unsigned int>(16384), static_cast<unsigned int>(16384));
		}
	}
}

void QCGEProFocusseur::testCommand() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QCGEProFocusseur::testCommand2() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand2, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QCGEProFocusseur::testCommand3() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand3, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QCGEProFocusseur::testCommand4() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand4, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QCGEProFocusseur::testCommand5() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand5, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QCGEProFocusseur::goToPos(double ra, double dec) {
	unsigned int raSteps = (ra / MathUtil::pi_2) * MAX_STEPS;
	unsigned int decSteps;
}
void QCGEProFocusseur::goToPos(unsigned int ra, unsigned int dec) {
	int raSteps = (ra + mZeroPosRA) %  MAX_STEPS;
	int decSteps = (dec + mZeroPosDEC) % MAX_STEPS;
	qDebug() << raSteps << " " << decSteps;
	QString raHex = QString("%1").arg(raSteps, 0, 16);
	QString decHex = QString("%1").arg(decSteps, 0, 16);
	qDebug() << raHex << " " << decHex;
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::GoToPos], QList<QVariant>{ raHex, decHex });
	mSerial.writeToBuffer(commandAndParams);
}

void QCGEProFocusseur::setZeros(unsigned int ra, unsigned int dec)
{
	mZeroPosRA = ra;
	mZeroPosDEC = dec;
}

void QCGEProFocusseur::fillDictionary() {

	mSerialCommands[Command::InitSwitchAX] = new QSerialCommand( 
		QByteArray("P\x04\x10\x06\x00\x00\x00\x00", 8),				// cmd
		"InitSwitchAX",												// name
		QSerialCommand::IOType::In,									// IOType
		0,															// # params
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,	// BlockingMode
		SerialOperationMode::FluxMode::Pull,						// FluxMode	
		TERMINATOR,													// eol car
		SEPARATOR,													// separator
		"",															// family
		"",															// short desc
		QList<QByteArray>{ QByteArray("#") }						// response
		);

	mSerialCommands[Command::InitSwitchAY] = new QSerialCommand( 
		QByteArray("P\x04\x11\x06\x00\x00\x00\x00", 8),
		"InitSwitchAY",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"",
		"",
		QList<QByteArray>{ QByteArray("#") }
		);

	mSerialCommands[Command::InitSwitchBX] = new QSerialCommand( 
		QByteArray("P\x01\x10\x0b\x00\x00\x00\x00", 8),
		"InitSwitchBX",															
		QSerialCommand::IOType::In,									
		0,															
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Pull,						
		TERMINATOR,													
		SEPARATOR,													
		"",															
		"",															
		QList<QByteArray>{ QByteArray("#") }
	);

	mSerialCommands[Command::InitSwitchBY] = new QSerialCommand( 
		QByteArray("P\x01\x11\x0b\x00\x00\x00\x00", 8),
		"InitSwitchBY",															
		QSerialCommand::IOType::In,									
		0,															
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Pull,						
		TERMINATOR,													
		SEPARATOR,													
		"",															
		"",															
		QList<QByteArray>{ QByteArray("#") }
	);

	mSerialCommands[Command::IsGoingToSwitchX] = new QSerialCommand( 
		QByteArray("P\x01\x10\x12\x00\x00\x00\x01", 8),
		"InitSwitchBY",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"",
		"",
		QList<QByteArray>{ QByteArray("\x00#", 2), QByteArray("\xff#", 2) }
	);

	mSerialCommands[Command::IsGoingToSwitchY] = new QSerialCommand( 
		QByteArray("P\x01\x11\x12\x00\x00\x00\x01", 8),
		"InitSwitchBY",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"",
		"",
		QList<QByteArray>{ QByteArray("\x00#", 2), QByteArray("\xff#", 2) }
	);

	mSerialCommands[Command::GetPosition] = new QSerialCommand(
		"Z",
		"GetPosition",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"",
		"",
		QRegularExpression(".{4},.{4}")
	);
	mSerialCommands[Command::GoToPos] = new QSerialCommand(
		"B",
		"GoToPos",
		QSerialCommand::IOType::In,
		2,
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"",
		"",
		QRegularExpression("#")
		);
}
