#include "..\..\..\include\Serial Port\Focuser\QLegendTabFocusseur.h"
#include <QGridLayout>
#include <QHBoxLayout>
#include "..\..\..\include\Serial Port\Global\QLEDLight.h"
#include <QLabel>

QString const QLegendTabFocusseur::POSSIBLE_ERRORS[8][3]{
	{ "G.", "solid green", "no alarm, motor disabled" },
	{ "GG.", "flashing green", "no alarm, motor enabled" },
	{ "RR.", "flashing red", "configuration or memory error\ncontact factory for assistance." },
	{ "GRRRR.", "1 green, 4 red", "power supply voltage too high fault" },
	{ "GRRRRR.", "1 green, 5 red", "over current / short circuit fault" },
	{ "GRRRRRR.", "1 green, 6 red", "open motor winding fault" },
	{ "GGRRR.", "2 green, 3 red", "internal voltage out of range fault" },
	{ "GGRRRR.", "2 green, 4 red", "power supply voltage too low alarm" }
};

QLegendTabFocusseur::QLegendTabFocusseur(QWidget *parent)
	: QWidget(parent)
{
	QVBoxLayout * forSpacing{ new QVBoxLayout };

	QGridLayout * mainLayout{ new QGridLayout };
	mainLayout->setSpacing(1);

	QLabel * desc = new QLabel("Description");
	QLabel * code = new QLabel("Light code");
	QLabel * sign = new QLabel("Signification");
	desc->setStyleSheet("QLabel { background-color: darkgray; padding-top : 12; }");
	code->setStyleSheet("QLabel { background-color: darkgray; padding-top : 12; }");
	sign->setStyleSheet("QLabel { background-color: darkgray; padding-top : 12; }");
	desc->setMargin(12);
	code->setMargin(12);
	sign->setMargin(12);
	mainLayout->addWidget(desc, 0, 0);
	mainLayout->addWidget(code, 0, 1);
	mainLayout->addWidget(sign, 0, 2);

	for (int i{ 0 }; i < NB_MESSAGES; ++i)
	{
		QHBoxLayout * leds{ new QHBoxLayout };
		leds->setSpacing(8);
		QString ledSequence = POSSIBLE_ERRORS[i][0];
		int j{ 0 };
		QChar temp{ ledSequence.at(j) };
		do
		{
			QLEDLight * light{ new QLEDLight(QSize(12,12)) };
			QString description{ temp };
			QString * path{ new QString(":/QSerialComm/") };
			if (temp == 'G')
			{
				path->append("greenLed");
			}
			else
			{
				path->append("redLed");
			}
			light->addStatus(0, description, QIcon(*path));
			light->setStatus(0);
			leds->addWidget(light);
			++j;
			temp = ledSequence.at(j);
		}
		while (temp != '.');
		leds->addStretch();
		leds->setMargin(12);

		QLabel * indication{ new QLabel(POSSIBLE_ERRORS[i][1]) };
		indication->setMargin(12);
		QLabel * message{ new QLabel(POSSIBLE_ERRORS[i][2]) };
		message->setMargin(12);

		mainLayout->addWidget(indication, i + 1, 0);
		mainLayout->addLayout(leds, i + 1, 1);
		mainLayout->addWidget(message, i + 1, 2);
	}

	forSpacing->addLayout(mainLayout);
	forSpacing->addStretch();
	setLayout(forSpacing);
}

QLegendTabFocusseur::~QLegendTabFocusseur()
{

}
