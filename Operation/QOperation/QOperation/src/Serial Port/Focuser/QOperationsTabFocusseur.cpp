#include "..\..\..\include\Serial Port\Focuser\QOperationsTabFocusseur.h"
#include <QKeyEvent>

QVector<int> const QOperationsTabFocusseur::MINUS_CODES{ Qt::Key_Q , Qt::Key_A , Qt::Key_Z };
QVector<int> const QOperationsTabFocusseur::PLUS_CODES{ Qt::Key_W , Qt::Key_S , Qt::Key_X };
double const QOperationsTabFocusseur::MESURES[]{ 0.00025, 0.0005, 0.002 };

QOperationsTabFocusseur::QOperationsTabFocusseur(QFocusseur &focusseur, QWidget * parent)
	: QWidget(parent), mFocusseur{ &focusseur }
{
	mKeys = new QVector<int>;
	mMoving = false;
	mInSlowMo = false;
	mAutoRepeatCheck = new QTimer;
	mAutoRepeatCheck->setTimerType(Qt::PreciseTimer);
	mAutoRepeatCheck->setInterval(500);

	mQvbl = new QVBoxLayout;
	setLayout(mQvbl);

	mOutputBox = new QGroupBox("Output");
	mOutputBoxLayout = new QVBoxLayout;
	mOutputBox->setLayout(mOutputBoxLayout);
	mOperations = new QOperationsOutputWidgetFocusseur(mFocusseur);
	mOutputBoxLayout->addWidget(mOperations);

	mOperBox = new QGroupBox("Operations");
	mOperBoxLayout = new QVBoxLayout;
	mOperBox->setLayout(mOperBoxLayout);
	mControl = new QOperationsControlWidgetFocusseur(mFocusseur);
	mOperBoxLayout->addWidget(mControl);

	mQvbl->addWidget(mOperBox);
	mQvbl->addWidget(mOutputBox);
	
	connect(mAutoRepeatCheck, &QTimer::timeout, this, &QOperationsTabFocusseur::stopChrono);
}

void QOperationsTabFocusseur::keyPressEvent(QKeyEvent * event)
{

	if (mControl->inSlowMotionMode())
	{
		int theKey = event->key();

		if (MINUS_CODES.contains(theKey))
		{
			//if (mMoving)
			if (event->isAutoRepeat()/* && !mInSlowMo*/)
			{
				if (!mInSlowMo)
				{
					mFocusseur->mSerial.setDevelopmentMode(false);
					QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::SlowMotionMinus], QList<QVariant>{ MINUS_CODES.indexOf(theKey) + 1 });
					mFocusseur->mSerial.writeToBuffer(commandAndParams);
					mInSlowMo = true;
				}
				mAutoRepeatCheck->stop();
			}
			else
			{
				mFocusseur->mSerial.setDevelopmentMode(false);
				QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::RelativeMinusSeq], QList<QVariant>{ MESURES[MINUS_CODES.indexOf(theKey)] });
				mFocusseur->mSerial.writeToBuffer(commandAndParams);
				mMoving = true;
				mInSlowMo = false;
			}
		}
		else if (PLUS_CODES.contains(theKey))
		{
			//if (mMoving)
			if (event->isAutoRepeat()/* && !mInSlowMo*/)
			{
				if (!mInSlowMo)
				{
					mFocusseur->mSerial.setDevelopmentMode(false);
					QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::SlowMotionPlus], QList<QVariant>{ PLUS_CODES.indexOf(theKey) + 1 });
					mFocusseur->mSerial.writeToBuffer(commandAndParams);
					mInSlowMo = true;
				}
				mAutoRepeatCheck->stop();
			}
			else
			{
				mFocusseur->mSerial.setDevelopmentMode(false);
				QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::RelativePlusSeq], QList<QVariant>{ MESURES[PLUS_CODES.indexOf(theKey)] });
				mFocusseur->mSerial.writeToBuffer(commandAndParams);
				mMoving = true;
				mInSlowMo = false;
			}
		}
	
	}
	/*if (mControl->inSlowMotionMode())
	{
		mKeys->append(event->key());
		sendSlowMoCmd();
	}*/
}

void QOperationsTabFocusseur::keyReleaseEvent(QKeyEvent * event)
{
	int theKey = event->key();
	if (mControl->inSlowMotionMode() && (MINUS_CODES.contains(theKey) || PLUS_CODES.contains(theKey))
		/*(theKey == MINUS1 || theKey == MINUS2 || theKey == MINUS3
			|| theKey == PLUS1 || theKey == PLUS2 || theKey == PLUS3)*/)
	{
		mMoving = false;

		//mOperations->showPosition(-1);
		//mOperations->showPosition(event->key());

		if (mInSlowMo)
		{
			mAutoRepeatCheck->start(100);
		}
	}
}

void QOperationsTabFocusseur::stopChrono()
{
	mAutoRepeatCheck->stop();
	mFocusseur->mSerial.setDevelopmentMode(false);
	mFocusseur->sendCommandNoParams(QFocusseur::Command::Stop);
	mInSlowMo = false;
}

void QOperationsTabFocusseur::sendSlowMoCmd()
{
	int param = 1;
	if (mKeys->contains(Qt::Key_Control))
	{
		param = 3;
	}
	else if (mKeys->contains(Qt::Key_Shift))
	{
		param = 2;
	}

	int cmd = -5;

	if (mKeys->contains(Qt::Key_Equal))
	{
		cmd = 1;
	}
	else if (mKeys->contains(Qt::Key_Minus))
	{
		cmd = -1;
	}
	else if (mMoving)
	{
		cmd = 0;
	}

	if (cmd != -5)
	{
		mFocusseur->mSerial.setDevelopmentMode(false);
		if (cmd == 0)
		{
			mFocusseur->sendCommandNoParams(QFocusseur::Command::Stop);
			mMoving = false;
		}
		else if (!mMoving)
		{
			if (cmd = -1)
			{
				QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::SlowMotionMinus], QList<QVariant>{ param });
				mFocusseur->mSerial.writeToBuffer(commandAndParams);
			}
			else
			{
				QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::SlowMotionPlus], QList<QVariant>{ param });
				mFocusseur->mSerial.writeToBuffer(commandAndParams);
			}
			mMoving = true;
		}
	}
}

QOperationsTabFocusseur::~QOperationsTabFocusseur() {
}

void QOperationsTabFocusseur::resetStats() {
}
