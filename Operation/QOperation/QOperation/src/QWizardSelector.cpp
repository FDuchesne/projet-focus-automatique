#include "..\include\QWizardSelector.h"
#include "..\include\Serial Port\RangeFinder\QRangeFinderGUI.h"
#include "..\include\Serial Port\Focuser\QFocusseurGUI.h"
#include "..\include\QOperationWidget.h"
#include "..\include\QtLUT\QLUT\QLUT.h"
#include <QPushButton>
#include <QVBoxLayout>

QWizardSelector::QWizardSelector(QWidget *parent)
	: QWidget(parent),
	mRangeFinderButton{ new QPushButton("Range finder") },
	mFocusseurButton{ new QPushButton("Focusseur") },
	mLutButton{ new QPushButton("LUT") },
	mOperationButton{ new QPushButton("Operation") }

{
	QVBoxLayout * mainLayout = new QVBoxLayout();
	mainLayout->addWidget(mRangeFinderButton);
	mainLayout->addWidget(mFocusseurButton);
	mainLayout->addWidget(mLutButton);
	mainLayout->addWidget(mOperationButton);
	setLayout(mainLayout);

	connect(mRangeFinderButton, &QPushButton::clicked, this, &QWizardSelector::openRangeFinderWidget);
	connect(mFocusseurButton, &QPushButton::clicked, this, &QWizardSelector::openFocusseurWidget);
	connect(mLutButton, &QPushButton::clicked, this, &QWizardSelector::openLutWidget);
	connect(mOperationButton, &QPushButton::clicked, this, &QWizardSelector::openOperationWidget);

}

QWizardSelector::~QWizardSelector()
{
}

void QWizardSelector::openRangeFinderWidget()
{
	mRangeFinderWidget = new QRangeFinderGUI();
	mRangeFinderWidget->show();
	mRangeFinderButton->setEnabled(false);
	connect(mRangeFinderWidget, &QRangeFinderGUI::widgetClosed, this, &QWizardSelector::closeRangeFinderWidget);
}

void QWizardSelector::openFocusseurWidget()
{
	mFocusseurWidget = new QFocusseurGUI();
	mFocusseurWidget->show();
	mFocusseurButton->setEnabled(false);
	connect(mFocusseurWidget, &QFocusseurGUI::widgetClosed, this, &QWizardSelector::closeFocusseurWidget);
}

void QWizardSelector::openLutWidget()
{
	mLutWidget = new QLut();
	mLutWidget->show();
	mLutButton->setEnabled(false);
	connect(mLutWidget, &QLut::widgetClosed, this, &QWizardSelector::closeLutWidget);
}

void QWizardSelector::openOperationWidget()
{
	mOperationWidget = new QOperationWidget();
	mOperationWidget->show();
	mOperationButton->setEnabled(false);
	connect(mOperationWidget, &QOperationWidget::widgetClosed, this, &QWizardSelector::closeOperationWidget);
}

void QWizardSelector::closeRangeFinderWidget()
{
	//mRangeFinderWidget = nullptr;
	mRangeFinderButton->setEnabled(true);
}

void QWizardSelector::closeFocusseurWidget()
{
	mFocusseurButton->setEnabled(true);
}

void QWizardSelector::closeLutWidget()
{
	mLutButton->setEnabled(true);
}

void QWizardSelector::closeOperationWidget()
{
	mOperationButton->setEnabled(true);
}