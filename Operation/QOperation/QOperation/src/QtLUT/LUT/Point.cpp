#include "..\..\..\include\QtLUT\LUT\Point.h"

Point::Point(double a)
	:Point(a, a)
{}

Point::Point(double x, double y)
	:mX{x}, mY{y}
{}

Point Point::operator+(Point const & point)
{
    return Point(mX + point.mX, mY + point.mY);
}
Point Point::operator-(Point const & point)
{
    return Point(mX - point.mX, mY - point.mY);
}
Point Point::operator*(double value)
{
	return Point(mX * value, mY * value);
}

bool Point::operator<(Point const & point) const
{
    return mX < point.mX;
}

bool Point::operator==(Point const & point) const
{
    return mX == point.mX && mY == point.mY;
}



