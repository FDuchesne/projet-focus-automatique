#include "..\..\..\..\include\QtLUT\LUT\DAO\DTO.h"

DTO::DTO()
	:DTO(std::vector<Point>())
{
}

DTO::DTO(std::vector<Point> vector)
	: DTO(vector, 0.0, 0.0)
{}

DTO::DTO(std::vector<Point> vector, double paramA, double paramB)
	:mVector{vector},
	mParamA{ paramA },
	mParamB{ paramB }
{}
