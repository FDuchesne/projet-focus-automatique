#include "..\..\..\include\QtLUT\LUT\LutAbstract.h"
#include "..\..\..\include\QtLUT\QLUT\QLutWidget.h"
#include "..\..\..\include\QtLUT\QLUT\QCustomLineEdit.h"
#include "..\..\..\include\QtLUT\LUT\LutEchelon.h"
#include "..\..\..\include\QtLUT\LUT\LutLinear.h"
#include "..\..\..\include\QtLUT\LUT\LutLinearRegression.h"
#include "..\..\..\include\QtLUT\LUT\LutLogarithme.h"
#include <QWidget>
#include <QLabel>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QRadioButton>
#include <QGroupBox>
#include <QtCharts/QChart>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QErrorMessage>
#include <QComboBox>

using namespace QtCharts;

QLutWidget::QLutWidget( LutAbstract ** lut, QWidget *parent)
	: QWidget(parent),
	mLut{lut},
	mRightLayout{ new QVBoxLayout },
	mLeftLayout{ new QVBoxLayout },
	mTableLayout{ new QHBoxLayout() },
	mExtrapolate{ new QRadioButton("Extrapolate") },
	mInterpolateOnly{ new QRadioButton("Interpolate Only") },
	mDataTable{ new QTableWidget((*lut)->elements().size(), 2, this) },
	mFindYValue{new QCustomLineEdit(this, "Enter a X value", true)},
	mFindYResultLabel{ new QLabel("The y value for the given x is : ") },
	mSelection{ new QComboBox() },
	mChart{ new QChart() },
	mLineSeries{ new QLineSeries() },
	mScatterSeries{ new QScatterSeries() },
	mErrorMessage{ new QErrorMessage() },
	mFormulaInformation{ new QTableWidget(2,2,this) },
	mPrevNextBox{ new QGroupBox() },
	mPrev{ new QRadioButton("Prev") },
	mNext{ new QRadioButton("Next") },
	mPrevNextBoxLayout{ new QHBoxLayout() },
	mSpaceFromAxis{ 5 },
	mErrorDuringFindY{ false },
	mXMin{0},
	mXMax{ 0 },
	mYMax{ 0 },
	mYMin{ 0 }
{
	//LEFT LAYOUT, CONTAINS THE DATA AND SETTINGS//
	mLeftLayout->setMargin(0);
	mLeftLayout->setSpacing(0);
	mLeftLayout->setContentsMargins(0, 0, 0, 0);

	//Label of the selection ComboBox
	QLabel* mComboBoxLabel = new QLabel("Choix de la méthode");
	mLeftLayout->addWidget(mComboBoxLabel);
	mComboBoxLabel->setMaximumWidth(280);

	//Combo box, contains all the usable function
	//Add usable method to the selection
	mSelection->addItem("Echelon");
	mSelection->addItem("Linéaire par partie");
	mSelection->addItem("Regression linéaire");
	mSelection->addItem("Logarithmique");
	mLeftLayout->addWidget(mSelection);

	//Interpolation Method Group Box (Extrapolate or Interpolate Only)
	QGroupBox * mInterpolationMethodBox = new QGroupBox();
	QHBoxLayout * mInterpolationMethodLayout = new QHBoxLayout();
	mInterpolationMethodLayout->addWidget(mExtrapolate);
	mInterpolationMethodLayout->addWidget(mInterpolateOnly);
	mInterpolationMethodBox->setLayout(mInterpolationMethodLayout);
	mLeftLayout->addWidget(mInterpolationMethodBox);
	checkCurrentInterpolation();

	//The data label, contains all the data
	QLabel * mDataLabel = new QLabel("Data");
	mDataLabel->adjustSize();
	mDataLabel->setAutoFillBackground(true);

	//Fill the data tabel with the points in the mLut
	fillDataTable();
	mDataTable->setMaximumWidth(400);

	//Table Layout contains the data table and the parameters table (may be changed)
	mTableLayout->addWidget(mDataTable, 0, Qt::AlignLeft);
	mLeftLayout->addLayout(mTableLayout);

	//The findY section contains the title label, the customTextEdit and the result label
	QLabel * mFindYTitleLabel = new QLabel("Find the Y value");
	mFindYTitleLabel->setMaximumWidth(280);
	mLeftLayout->addWidget(mFindYTitleLabel);

	mFindYValue->setMaximumHeight(30);
	mFindYValue->setMaximumWidth(280);
	mLeftLayout->addWidget(mFindYValue);

	mFindYResultLabel->setMaximumWidth(280);
	mLeftLayout->addWidget(mFindYResultLabel);

	//METHOD SPECIFIC WIDGETS

	//ECHELON
	//The setting for the way the LutEchelon find the Y value
	mPrevNextBox->setLayout(mPrevNextBoxLayout);
	mPrevNextBoxLayout->addWidget(mPrev);
	mPrevNextBoxLayout->addWidget(mNext);

	//Sets the checked button to the default way LutEchelon find the Y value
	LutEchelon * currentEchelon{ dynamic_cast<LutEchelon*>((*mLut)) };
	if (currentEchelon) {
		if (currentEchelon->stepCalculationMethod() == LutEchelon::StepCalculationMethod::Prev) {
			mPrev->setChecked(true);
		}
		else {
			mNext->setChecked(true);
		}
	}
	mLeftLayout->addWidget(mPrevNextBox);

	//WITHFUNCTION
	mFormulaInformation->setMaximumWidth(250);
	mTableLayout->addWidget(mFormulaInformation);

	//Labels for X and Y Axis /*NOT WORKING*/
	QCategoryAxis * mLabelAxisX = new QCategoryAxis();
	QCategoryAxis * mLabelAxisY = new QCategoryAxis();
	mLabelAxisX->append("axe des X", 100);
	mLabelAxisY->append("axe des Y", 143);
	mLabelAxisX->setRange(-100, 100);
	mLabelAxisY->setRange(-120, 143);

	//Characteristic of the points in the scatterSeries
	mScatterSeries->setMarkerShape(QScatterSeries::MarkerShapeCircle);
	mScatterSeries->setMarkerSize(15.0);

	//Add an empty QLineSeries used to draw the result of the function
	mChart->addSeries(mLineSeries);

	//Chart characteristic
	mChart->legend()->hide();
	mChart->setTitle("Graph");

	//ChartView
	QChartView * mChartView = new QChartView(mChart);
	mChartView->setRenderHint(QPainter::Antialiasing);
	mChartView->setMinimumHeight(600);
	mChartView->setMinimumWidth(600);

	//Draw the scatterSeries on the chart
	mChart->addSeries(mScatterSeries);
	mChart->createDefaultAxes();

	//Add the chart to the right widget
	mRightLayout->addWidget(mChartView, 0, Qt::AlignTop);
	
	//MAIN LAYOUT (Add both layout)
	QHBoxLayout * mMainLayout = new QHBoxLayout();
	mMainLayout->addLayout(mLeftLayout);
	mMainLayout->addLayout(mRightLayout);

	setLayout(mMainLayout);

	//SETS THE CURRENTLY SELECTED FUNCTION
	changeFunction();

	//CONNECT

	//RadioButtton change the interpolation method
	connect(mExtrapolate, &QRadioButton::clicked, this, &QLutWidget::changeInterpolation);
	connect(mInterpolateOnly, &QRadioButton::clicked, this, &QLutWidget::changeInterpolation);

	//Update the data un the LUT based on the table
	connect(mDataTable, &QTableWidget::cellChanged, this, &QLutWidget::updateLUTData);

	//Connect the mSelection with the changeFunction method of the QLutWidget class
	connect(mSelection, QOverload<const QString &>::of(&QComboBox::currentIndexChanged), this, &QLutWidget::changeFunction);

	//Connect the mPrev and mNext QRadioButton to the changePrevNext method
	connect(mPrev, &QRadioButton::clicked, this, &QLutWidget::changePrevNext);
	connect(mNext, &QRadioButton::clicked, this, &QLutWidget::changePrevNext);
}

QString QLutWidget::findYResultLabel()
{
	return mFindYResultLabel->text();
}

void QLutWidget::setFindYValue(double value)
{
	mFindYValue->setText(QString::number(value));
}

void QLutWidget::changeInterpolation() {
	if (mExtrapolate->isChecked()) {
		(*mLut)->setInterpolationType(LutAbstract::InterpolationType::Extrapolate);
	}
	else {
		(*mLut)->setInterpolationType(LutAbstract::InterpolationType::Interpolate);
	}
}

void QLutWidget::checkCurrentInterpolation() {
	if ((*mLut)->interpolationType() == LutAbstract::InterpolationType::Interpolate) {
		mInterpolateOnly->setChecked(true);
	}
	else {
		mExtrapolate->setChecked(true);
	}
}

void QLutWidget::findY(double x)
{
	LutAbstract::ErrorType error = (*mLut)->checkErrors(x);
	if (error == LutAbstract::ErrorType::NoError) {
		double y = (*mLut)->y(x);
		mFindYResultLabel->setText("The y value for X = " + QString::number(x) + " is : " + QString::number(y));
		mErrorDuringFindY = false;
	}
	else if (error == LutAbstract::ErrorType::IllegalExtrapolation) {
		mErrorMessage->showMessage("Illegal Extrapolation! Please select extrapolation if you want to extrapolate");
		mErrorDuringFindY = true;
	}
	else if (error == LutAbstract::ErrorType::EmptyVector) {
		mErrorMessage->showMessage("The vector is currently empty");
		mErrorDuringFindY = true;
	}
}

void QLutWidget::loadNewChart()
{
	//Clear the current scatterSeries
	mScatterSeries->clear();

	//Fill the scatterSeries
	for (auto point : (*mLut)->elements()) {
		mScatterSeries->append(point.x(), point.y());
	}
}

void QLutWidget::fitAxisToNewData()
{
	if (mScatterSeries->count() != 0) {
		mChart->axisX()->setRange(mScatterSeries->at(0).x() - mSpaceFromAxis, mScatterSeries->at(mScatterSeries->count() - 1).x() + mSpaceFromAxis);
		mYMin = (*mLut)->elements().at(0).y();
		mYMax = (*mLut)->elements().at(0).y();
		for (int i = 0; i < (*mLut)->elements().size(); i++) {
			if ((*mLut)->elements().at(i).y() < mYMin) {
				mYMin = (*mLut)->elements().at(i).y();
			}
			else if ((*mLut)->elements().at(i).y() > mYMax)
			{
				mYMax = (*mLut)->elements().at(i).y();
			}
		}
		mChart->axisY()->setRange(mYMin - mSpaceFromAxis, mYMax + mSpaceFromAxis);
	}
}

void QLutWidget::updateLUTData(int mRow, int mColumn)
{
	//Y value is changed
	if (mColumn == 1) {
		if (mScatterSeries->count() > mRow){
			mScatterSeries->replace(mRow, mDataTable->item(mRow, mColumn - 1)->text().toDouble(), mDataTable->item(mRow, mColumn)->text().toDouble());	//Change mScatterSeries
			(*mLut)->elements().at(mRow).setY(mDataTable->item(mRow, mColumn)->text().toDouble());
		}
		else {
			mScatterSeries->append(mDataTable->item(mRow, mColumn - 1)->text().toDouble(), mDataTable->item(mRow, mColumn)->text().toDouble());
			(*mLut)->add(Point(mDataTable->item(mRow, mColumn - 1)->text().toDouble(), mDataTable->item(mRow, mColumn)->text().toDouble()));
		}

	}
	//X Value is changed
	else {
		if (mScatterSeries->count() > mRow) {
			mScatterSeries->replace(mRow, mDataTable->item(mRow, mColumn)->text().toDouble(), mDataTable->item(mRow, mColumn + 1)->text().toDouble());
			(*mLut)->elements().at(mRow).setX(mDataTable->item(mRow, mColumn)->text().toDouble());
		}
		else {
			mScatterSeries->append(mDataTable->item(mRow, mColumn)->text().toDouble(), mDataTable->item(mRow, mColumn + 1)->text().toDouble());
			(*mLut)->add(Point(mDataTable->item(mRow, mColumn)->text().toDouble(), mDataTable->item(mRow, mColumn + 1)->text().toDouble()));
		}

	}

	if ((*mLut)->elements().at(mRow).y() > mYMax) {
		mYMax = (*mLut)->elements().at(mRow).y();
		mChart->axisY()->setRange(mYMin - mSpaceFromAxis, mYMax + mSpaceFromAxis);
	}
	else if ((*mLut)->elements().at(mRow).y() < mYMin) {
		mYMin = (*mLut)->elements().at(mRow).y();
		mChart->axisY()->setRange(mYMin - mSpaceFromAxis, mYMax + mSpaceFromAxis);
	}
	if ((*mLut)->elements().at(mRow).x() < mXMin) {
		mXMin = (*mLut)->elements().at(mRow).x();
		mChart->axisX()->setRange(mXMin - mSpaceFromAxis, mXMax + mSpaceFromAxis);
	}
	else if ((*mLut)->elements().at(mRow).x() > mXMax) {
		mXMax = (*mLut)->elements().at(mRow).x();
		mChart->axisX()->setRange(mXMin - mSpaceFromAxis, mXMax + mSpaceFromAxis);
	}

	//If it's a LutWithParameters find the new parameters of that function
	LutWithParameters * temp = dynamic_cast<LutWithParameters*>((*mLut));

	if (temp) {
		temp->findParameters();
		fillParametersTable();
	}	
	drawChart();
	dataChanged();
}

void QLutWidget::addNewData(double rangeFinderValue, double focuseurValue, int dataRow)
{
	if (dataRow  == (*mLut)->elements().size()) {
		mDataTable->setRowCount((*mLut)->elements().size() + 1);
	}
	mDataTable->blockSignals(true);
	mDataTable->setItem(dataRow, 0, new QTableWidgetItem(QString::number(rangeFinderValue)));
	mDataTable->blockSignals(false);
	mDataTable->setItem(dataRow, 1, new QTableWidgetItem(QString::number(focuseurValue)));
}

void QLutWidget::setLut(LutAbstract ** lut)
{
	mLut = lut;
	mLineSeries->removePoints(0, mLineSeries->count());
	changeFunction();
	fillDataTable();
	loadNewChart();
	fitAxisToNewData();
	drawChart();
}

void QLutWidget::fillDataTable()
{
	mDataTable->blockSignals(true);

	mDataTable->setRowCount((*mLut)->elements().size());
	int row = 0, column = 0;
	QTableWidgetItem * pointX, * pointY;
	for (auto point : (*mLut)->elements()) {
		pointX = new QTableWidgetItem(QString::number(point.x()));
		mDataTable->setItem(row, column, pointX);

		column += 1;

		pointY = new QTableWidgetItem(QString::number(point.y()));
		mDataTable->setItem(row, column, pointY);

		column = 0;
		row += 1;
	}
	mDataTable->blockSignals(false);

	//Pour l'option du save
	dataChanged();
}

void QLutWidget::changePrevNext()
{
	LutEchelon * currentEchelon{ dynamic_cast<LutEchelon*>((*mLut)) };
	if (currentEchelon) {
		if (mPrev->isChecked()) {
			currentEchelon->setStepCalculationMethod(LutEchelon::StepCalculationMethod::Prev);
		}
		else {
			currentEchelon->setStepCalculationMethod(LutEchelon::StepCalculationMethod::Next);
		}
	}
	findY(mFindYValue->text().toDouble());
}

void QLutWidget::fillParametersTable()
{
	//Table showing the parameter of the currently used function
	LutWithParameters * tempLUT = static_cast<LutWithParameters*>((*mLut));
	QTableWidgetItem * parameterAText = new QTableWidgetItem("a");
	QTableWidgetItem * parameterBText = new QTableWidgetItem("b");
	QTableWidgetItem * parameterA = new QTableWidgetItem(QString::number(tempLUT->a()));
	QTableWidgetItem * parameterB = new QTableWidgetItem(QString::number(tempLUT->b()));

	mFormulaInformation->setItem(0, 0, parameterAText);
	mFormulaInformation->setItem(0, 1, parameterA);
	mFormulaInformation->setItem(1, 0, parameterBText);
	mFormulaInformation->setItem(1, 1, parameterB);
}

void QLutWidget::changeFunction()
{
	LutAbstract * temp;
	if (mSelection->currentText() == "Echelon") {
		temp = new LutEchelon();
		mPrevNextBox->show();
	}
	else {
		mPrevNextBox->hide();
		if (mSelection->currentText() == "Linéaire par partie") {
			temp = new LutLinear();

		}
		else if (mSelection->currentText() == "Regression linéaire") {
			temp = new LutLinearRegression();
		}
		else {
			temp = new LutLogarithme(mFilePath.toStdString());
		}
	}

	if ((*mLut)->elements().size()) {
		temp->add(&(*mLut)->elements().at(0), (*mLut)->elements().size());
	}
	delete (*mLut);
	(*mLut) = temp;

	LutWithParameters * tempLUT = dynamic_cast<LutWithParameters*>((*mLut));
	if (tempLUT) {
		tempLUT->findParameters();
		fillParametersTable();
		mFormulaInformation->show();
	}
	else {
		mFormulaInformation->hide();
	}

	checkCurrentInterpolation();
	drawChart();
	mFindYValue->setText("Enter a X value");
	mFindYResultLabel->setText("The y value for the given x is : ");
	dataChanged();
}

void QLutWidget::drawChart()
{
	mLineSeries->removePoints(0, mLineSeries->count());
	if ((*mLut)->elements().size()) {
		if (mSelection->currentText() == "Echelon") {
		
			Point p = (*mLut)->elements().at(0);
			for (auto point : (*mLut)->elements()) {
				mLineSeries->append(p.x(), point.y());
				mLineSeries->append(point.x(), point.y());
				p = point;
			}
		}
		else if (mSelection->currentText() == "Linéaire par partie") {
			for (auto point : (*mLut)->elements()) {
				mLineSeries->append(point.x(), point.y());
			}
		}

		if (mSelection->currentText() == "Regression linéaire") {
			mLineSeries->append((*mLut)->elements().at(0).x(), (*mLut)->y((*mLut)->elements().at(0).x()));
			mLineSeries->append((*mLut)->elements().at((*mLut)->elements().size() - 1).x(), (*mLut)->y((*mLut)->elements().at((*mLut)->elements().size() - 1).x()));
		}
	}
}

QLutWidget::~QLutWidget()
{
}