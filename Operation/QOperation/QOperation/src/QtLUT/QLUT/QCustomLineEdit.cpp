#include "..\..\..\include\QtLUT\QLUT\QCustomLineEdit.h"
#include "..\..\..\include\QtLUT\QLUT\QLutWidget.h"
#include <QKeyEvent>
#include <QObject>
#include <QValidator>

QCustomLineEdit::QCustomLineEdit(QWidget * parent, QString text, bool hasDoubleValidator)
{
	setParent(parent);
	this->setText(text);
	if (hasDoubleValidator) {
		setValidator(new QDoubleValidator());
	}
}

QCustomLineEdit::~QCustomLineEdit()
{
}

void QCustomLineEdit::focusInEvent(QFocusEvent *e)
{
	this->clear();
}

void QCustomLineEdit::keyPressEvent(QKeyEvent *e)
{
	if (e->key() == Qt::Key_Return) {
		QLutWidget *theInterface{ dynamic_cast<QLutWidget*>(parent()) };
		if (theInterface) {
			emit theInterface->findY(this->text().toDouble());
		}
	}
	else {
		QLineEdit::keyPressEvent(e);
	}
}