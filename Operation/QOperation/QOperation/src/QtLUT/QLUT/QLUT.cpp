﻿#include "..\..\..\include\QtLUT\QLUT\QLUT.h"
#include "..\..\..\include\QtLUT\QLUT\QLutWidget.h"
#include "..\..\..\include\QtLUT\LUT\LutWithoutFunction.h"
#include <QVBoxLayout>
#include <QPushButton>
#include <QTextCodec>
#include "..\..\..\include\QtLUT\LUT\DAO\DaoCsv.h"
#include "..\..\..\include\QtLUT\LUT\DAO\DTO.h"
#include "..\..\..\include\QtLUT\QLUT\QLutWidget.h"
#include <QFileDialog>
#include <QDesktopServices>
#include <QMessageBox>
#include <QSettings>
#include <QAbstractButton>

QLut::QLut(QWidget *parent)
	: QMainWindow(parent),
	mMenuBar{ new QMenuBar(this) },
	mFileMenu { new QMenu },
	mHelpMenu{ new QMenu },
	mNewFileAction{ new QAction},
	mSaveFileAction{ new QAction },
	mSaveFileAsAction{ new QAction },
	mLoadFileAction{ new QAction },
	mQuitFileAction{ new QAction },
	mAboutAction { new QAction },
	mViewSrc{ new QAction },
	mLutWidgets{ new QTabWidget },
	mLutAbs{ new LutAbstract() },
	unsavedChanges{false}
{
	createActions();
	createStatusBar();

	readSettings();
	mFilePath = "";
	
	resize(1500, 950);

	mMainWidget = new QWidget();

	mLutWidget = new QLutWidget(&mLutAbs, parent);

	mVLayout = new QVBoxLayout();
	mVLayout->addWidget(mLutWidget);

	mMainWidget->setLayout(mVLayout);

	setMenuBar(mMenuBar);
	setCentralWidget(mMainWidget);

	connect(mLutWidget, &QLutWidget::dataChanged, this, &QLut::onDataChange);


	/*Needes a signal that indicates that the data has been changed!*/
	//connect(mLutAbs, &..., this, &QLUT::onDataChange)
}

void QLut::closeEvent(QCloseEvent * event)
{
	/*
		QSettings settings("MyCompany", "MyApp");
		settings.setValue("geometry", saveGeometry());
		settings.setValue("windowState", saveState());
	*/
	if (maybeSave()) {
		writeSettings();
		event->accept();
	}
	else {
		event->ignore();
	}
	emit widgetClosed();
}

void QLut::writeSettings()
{
	/*
	QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
	settings.setValue("geometry", saveGeometry());
	*/
}
	
void QLut::readSettings()
{
	/*
	QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
	const QByteArray geometry = settings.value("geometry", QByteArray()).toByteArray();
	if (geometry.isEmpty()) {
		const QRect availableGeometry = QApplication::desktop()->availableGeometry(this);
		resize(availableGeometry.width() / 3, availableGeometry.height() / 2);
		move((availableGeometry.width() - width()) / 2,
			(availableGeometry.height() - height()) / 2);
	}
	else {
		restoreGeometry(geometry);
	}
	*/
}

void QLut::createActions()
{
	/***    MENU BAR SETUP    ***/

	/*File Menu*/
	mFileMenu->setTitle("Fichier");

	mNewFileAction->setText("Nouveau");
	mNewFileAction->setToolTip("Construire un nouveau fichier");
	mNewFileAction->setShortcut(Qt::Key_N | Qt::CTRL);
	connect(mNewFileAction, &QAction::triggered, this, &QLut::newFile);
	mNewFileAction->setDisabled(true);
	mFileMenu->addAction(mNewFileAction);

	mFileMenu->addSeparator();

	mLoadFileAction->setText("Ouvrir");
	mLoadFileAction->setToolTip("Ouvrir un fichier de liste de points");
	mLoadFileAction->setShortcut(Qt::Key_O | Qt::CTRL);
	connect(mLoadFileAction, &QAction::triggered, this, &QLut::loadFile);
	mFileMenu->addAction(mLoadFileAction);

	mFileMenu->addSeparator();

	mSaveFileAction->setText("Enregistrer");
	mSaveFileAction->setToolTip("Enregistrer le fichier");
	mSaveFileAction->setShortcut(Qt::Key_S | Qt::CTRL);
	connect(mSaveFileAction, &QAction::triggered, this, &QLut::save);
	mFileMenu->addAction(mSaveFileAction);

	mSaveFileAsAction->setText("Enregistrer sous...");
	mSaveFileAsAction->setToolTip("Enregistrer le fichier sous...");
	mSaveFileAsAction->setShortcut(Qt::Key_S | Qt::SHIFT | Qt::CTRL);
	connect(mSaveFileAsAction, &QAction::triggered, this, &QLut::saveAs);
	mFileMenu->addAction(mSaveFileAsAction);

	mFileMenu->addSeparator();

	mQuitFileAction->setText("Quitter");
	mQuitFileAction->setToolTip("Quitter la fenêtre");
	mQuitFileAction->setShortcut(Qt::ALT | Qt::Key_F4);
	connect(mQuitFileAction, &QAction::triggered, this, &QLut::close);
	mFileMenu->addAction(mQuitFileAction);

	/*About Section*/
	mHelpMenu->setTitle("Aide");

	mAboutAction->setText("A Propos");
	mAboutAction->setToolTip("Aide sur le fonctionnement de l'application");
	//mAboutAction->setShortcut(Qt::ALT | Qt::Key_F4);
	connect(mAboutAction, &QAction::triggered, this, &QLut::about);
	//mAboutAction->setDisabled(true);
	mHelpMenu->addAction(mAboutAction);


	mHelpMenu->addSeparator();

	mViewSrc->setText("Voir le code source");
	mViewSrc->setToolTip("Aide sur le fonctionnement de l'application");
	//mViewSrc->setShortcut(Qt::ALT | Qt::Key_F4);
	connect(mViewSrc, &QAction::triggered, this, &QLut::viewSrcInBrowser);
	mHelpMenu->addAction(mViewSrc);

	/*Add to menu bar*/
	mMenuBar->addMenu(mFileMenu);
	mMenuBar->addMenu(mHelpMenu);

}


void QLut::createStatusBar()
{
	///TODO
}

bool QLut::_saveFile()
{

	DaoCsv dao(mFilePath.toStdString());
	if (mLutAbs) {
		dao.write(mLutAbs->elements());
		unsavedChanges = false;
	}
	else {
		//Show som error
	}
	return true;
}

bool QLut::save()
{
	if (!mFilePath.isEmpty() && mFilePath.endsWith(".csv")) {
		return _saveFile();
	}
	else {
		return saveAs();
	}
}

bool QLut::saveAs()
{
	/*
	mFilePath = QFileDialog::getSaveFileName(this, tr("Sauvegarder le Fichier ..."), mFilePath, tr("CSV (*.csv)"));
	return saveFile();
	*/
	
	QFileDialog dialog(this);
	dialog.setNameFilter(tr("CSV (*.csv)"));
	dialog.setFileMode(QFileDialog::AnyFile);
	dialog.setWindowModality(Qt::WindowModal);
	dialog.setAcceptMode(QFileDialog::AcceptSave);
	if (dialog.exec() != QDialog::Accepted)
		return false;
	mFilePath = dialog.selectedFiles().first();
	return _saveFile();

}

void QLut::loadFile()
{
	mFilePath = QFileDialog::getOpenFileName(this, tr("Ouvrir Fichier ..."), mFilePath, tr("CSV (*.csv)"));
	if (!mFilePath.isEmpty() && mFilePath.endsWith(".csv")) {
		//File was loaded
		DaoCsv dao(mFilePath.toStdString());
		DTO dto(dao.read());
		mLutAbs->setElements(dto.vector());


		mLutWidget->setFilePath(mFilePath);
		mLutWidget->changeFunction();
		mLutWidget->fillDataTable();
		mLutWidget->loadNewChart();
		mLutWidget->fitAxisToNewData();
		mLutWidget->drawChart();

		emit fileSuccessfullyOpened();
		if (mLutAbs->elements().size() == 0) {
			emit fileIsEmpty();
		}

	}

	unsavedChanges = false;
}

void QLut::newFile() {

	/*
	if (maybeSave()) {
		///create new file
	}
	*/
}

bool QLut::maybeSave()
{
	if (!unsavedChanges) {
		return true;
	}

	/*Display a message box if changes were made but not saved to a file*/
	QMessageBox msgBox;
	msgBox.setText("The document has been modified.");
	msgBox.setInformativeText("Do you want to save your changes?");
	QPushButton* btnCancel = msgBox.addButton(tr("Annuler"), QMessageBox::ButtonRole::NoRole);
	QPushButton* btnDiscard = msgBox.addButton(tr("Supprimer"), QMessageBox::ButtonRole::ResetRole);
	QPushButton* btnSave = msgBox.addButton(tr("Sauvegarder"), QMessageBox::ButtonRole::YesRole);
	//msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
	//msgBox.setDefaultButton(QMessageBox::Save);
	msgBox.setDefaultButton(btnSave);
	//msgBox.setEscapeButton(QMessageBox::Cancel);
	msgBox.exec();

	QPushButton * ret = static_cast<QPushButton*>(msgBox.clickedButton());
	///TOTO: Rewrite buttons in French

	if (ret == btnSave)
	{
		if (!mFilePath.isEmpty() && mFilePath.endsWith(".csv")) {
			return _saveFile();
		}
		else {
			return saveAs();
		}
	}
	else if (ret == btnDiscard)
	{
		return true;
	}
	else if (ret == btnCancel)
	{
		return false;
	}
	else {
		return false;
	}
}

void QLut::viewSrcInBrowser()
{
	QString link = "https://bitbucket.org/FDuchesne/projetderecherchepourjeunesprogrammeurscelibatairessauflemardi";
	bool success = QDesktopServices::openUrl(QUrl(link));	
}

void QLut::about()
{
	QMessageBox::about(this, tr("About Application"),
		tr("Cette <b>Application</b> est utilisée pour visualiser "
			"un nuage de points sur un graphique cartesien et de "
			"determiner la régression linéaire la plus intéressante."));
}

void QLut::onDataChange()
{
	unsavedChanges = true;
}
