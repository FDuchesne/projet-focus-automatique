#include "..\..\Include\Camera\QMatroxSimpleGrabber.h"


#include <QMutex>


QMatroxSimpleGrabber::QMatroxSimpleGrabber()
{
}

QMatroxSimpleGrabber::~QMatroxSimpleGrabber()
{
	freeMilResources();
}

bool QMatroxSimpleGrabber::initialize(MIL_CONST_TEXT_PTR deviceConfigurationFile, uint32_t nbrBand, QSize const & imageSize)
{
	freeMilResources();

	MappAlloc(M_NULL, M_DEFAULT, &mMilApplication);
	MsysAlloc(M_DEFAULT, M_SYSTEM_USB3_VISION, M_DEFAULT, M_DEFAULT, &mMilSystem);

	MdigAlloc(mMilSystem, M_DEV0, deviceConfigurationFile, M_DEFAULT, &mMilDigitizer);

	mImageSize = imageSize;
	if (nbrBand == 1) {
		MbufAlloc2d(mMilSystem, mImageSize.width(), mImageSize.height(), 8 + M_UNSIGNED, M_IMAGE + M_GRAB, &mMilImage);
	} else {
		MbufAllocColor(mMilSystem, nbrBand, mImageSize.width(), mImageSize.height(), 8 + M_UNSIGNED, M_IMAGE + M_GRAB, &mMilImage);
	}

	mAvailable = true;

	return mAvailable;
}

void QMatroxSimpleGrabber::grab(QImage & image)
{
	if (mAvailable) {
		MdigGrab(mMilDigitizer, mMilImage);

		if (mImageBand == 1) {
			if (image.size() != mImageSize || image.format() != QImage::Format_Grayscale8) {
				image = QImage(mImageSize, QImage::Format_Grayscale8);
			}

			MbufGet2d(
				mMilImage,  //in  
				0, //in
				0, //in
				mImageSize.width(), //in
				mImageSize.height(), //in
				image.bits() //out
			);
		} else {
			if (image.size() != mImageSize || image.format() != QImage::Format_RGBX8888) { // Format_RGBX8888
				image = QImage(mImageSize, QImage::Format_RGBX8888);
			}

			MbufGetColor2d(
				mMilImage, //in 
				M_PACKED + M_RGB32,
				M_ALL_BANDS,
				0, //in
				0, //in
				mImageSize.width(), //in
				mImageSize.height(), //in
				image.bits() //out
			);
		}
	}
}

void QMatroxSimpleGrabber::freeMilResources()
{
	if (mAvailable) {
		MbufFree(mMilImage);
		MdigFree(mMilDigitizer);
		MsysFree(mMilSystem);
		MappFree(mMilApplication);
	}
}









QMatroxJaiGo5000Grabber::QMatroxJaiGo5000Grabber()
{
	QMatroxSimpleGrabber::initialize(MIL_TEXT("usb3vision_currentstate.dcf"), 3, QSize(2560, 2048));
}

QMatroxJaiSp20000Grabber::QMatroxJaiSp20000Grabber()
{
	QMatroxSimpleGrabber::initialize(MIL_TEXT("usb3vision_currentstate.dcf"), 1, QSize(5120, 3840));
}
