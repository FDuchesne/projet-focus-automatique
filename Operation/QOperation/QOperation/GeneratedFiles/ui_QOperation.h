/********************************************************************************
** Form generated from reading UI file 'QOperation.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QOPERATION_H
#define UI_QOPERATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QOperationClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *QOperationClass)
    {
        if (QOperationClass->objectName().isEmpty())
            QOperationClass->setObjectName(QStringLiteral("QOperationClass"));
        QOperationClass->resize(600, 400);
        menuBar = new QMenuBar(QOperationClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        QOperationClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(QOperationClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        QOperationClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(QOperationClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        QOperationClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(QOperationClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        QOperationClass->setStatusBar(statusBar);

        retranslateUi(QOperationClass);

        QMetaObject::connectSlotsByName(QOperationClass);
    } // setupUi

    void retranslateUi(QMainWindow *QOperationClass)
    {
        QOperationClass->setWindowTitle(QApplication::translate("QOperationClass", "QOperation", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QOperationClass: public Ui_QOperationClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QOPERATION_H
