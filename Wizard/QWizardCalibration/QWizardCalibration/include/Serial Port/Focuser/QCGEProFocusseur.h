#ifndef QCGEPROFOCUSSEUR_H
#define QCGEPROFOCUSSEUR_H

#include <QObject>
#include <QTimer>
#include "..\Global\QSerialCommand.h"
#include "QCGEProFocusseur.h"
#include "..\Global\QCommandSerialPort.h"


class QCGEProFocusseur : public QObject
{
	Q_OBJECT

public:
	QCGEProFocusseur(QObject *parent = 0);
	~QCGEProFocusseur();

	static QString const SEPARATOR;
	static QString const TERMINATOR;
	static int const MAX_STEPS = 65536;

	enum class Command {
		InitSwitchAX = 0,
		InitSwitchAY = 1,
		InitSwitchBX = 2,
		InitSwitchBY = 3,
		IsGoingToSwitchX = 4,
		IsGoingToSwitchY = 5,
		GetPosition = 6,
		GoToPos = 7
	};

	QCommandSerialPort mSerial;
	QMap<Command, QSerialCommand const *> mSerialCommands;

	QSerialCommand mTestCommand;
	QSerialCommand mTestCommand2;
	QSerialCommand mTestCommand3;
	QSerialCommand mTestCommand4;
	QSerialCommand mTestCommand5;

	void testCommand();
	void testCommand2();
	void testCommand3();
	void testCommand4();
	void testCommand5();

	void goToPos(double ra, double dec);
	void goToPos(unsigned int ra, unsigned int dec);
	void sendCommandNoParams(Command command);

private:
	unsigned int mZeroPosRA;
	unsigned int mZeroPosDEC;
	bool isGoingtoSwitchPos;
	bool zerosSet;
	void setZeros(unsigned int ra, unsigned int dec);

	/**
		Fills the dictionary of commands of the focuser.
	*/
	void fillDictionary();

private slots :
	void handleMatchingResponse(QByteArray const &response, const QSerialCommand &command);
	void deviceConnected(bool connected);
	
	
};

#endif // QCGEPROFOCUSSEUR_H
