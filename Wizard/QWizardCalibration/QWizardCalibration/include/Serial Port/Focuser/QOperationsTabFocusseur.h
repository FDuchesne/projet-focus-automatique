/*! \class QOperationsTabFocusseur
*
*	\brief Widget d'opération pour la classe QFocusseur.
*
*	Contient les boutons de contrôle et affiche les mesures et les statistiques.
*/



#ifndef QOPERATIONSTABFOCUSSEUR_H
#define QOPERATIONSTABFOCUSSEUR_H

#include <QWidget>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QTabWidget>
#include "QStatisticsWidgetFocusseur.h"
#include "QOperationsOutputWidgetFocusseur.h"
#include "QOperationsControlWidgetFocusseur.h"
#include "QFocusseur.h"

class QMovingStatistics;
class QProgressBar;
class QStackedLayout;
class QLabel;
class QKeyEvent;

class QOperationsTabFocusseur : public QWidget {
	Q_OBJECT

public:
	QOperationsTabFocusseur(QFocusseur &focusseur, QWidget * parent = Q_NULLPTR);
	void keyPressEvent(QKeyEvent * event);
	void keyReleaseEvent(QKeyEvent * event);
	~QOperationsTabFocusseur();

private:
	QFocusseur * mFocusseur;
	QVBoxLayout * mQvbl, *mOperBoxLayout, *mOutputBoxLayout;
	QOperationsOutputWidgetFocusseur * mOperations;
	QOperationsControlWidgetFocusseur * mControl;
	QGroupBox *mOutputBox, *mOperBox;

	QVector<int> * mKeys;
	bool mMoving;
	bool mInSlowMo;
	QTimer * mAutoRepeatCheck;

	static QVector<int> const MINUS_CODES;
	static QVector<int> const PLUS_CODES;
	static double const MESURES[3];

private slots:
	void resetStats();
	void sendSlowMoCmd();
	void stopChrono();

signals:
	void connectionUpdated(bool connected);
	void connectionRequested(bool connect); //true for connect, false for disconnect
};
#endif //QOPERATIONSTABFOCUSSEUR_H