/*! \class QStatisticsWidgetFocusseur
*
*	\brief Affiche les statistiques fournies par la classe QMovingStatistics.
*
*	Le bouton reset permet de réinitialiser le tampon et le QProgressBar permet de connaître son état.
*
*/



#ifndef QSTATISTICSWIDGETFOCUSSEUR_H
#define QSTATISTICSWIDGETFOCUSSEUR_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include "..\Global\RingBuffer.h"

class QMovingStatistics;
class QProgressBar;
class QStackedLayout;
class QLabel;

class QStatisticsWidgetFocusseur : public QWidget {
	Q_OBJECT

public:
	QStatisticsWidgetFocusseur(QWidget * parent = Q_NULLPTR);
	~QStatisticsWidgetFocusseur();

	public slots:
	void showStats(QMovingStatistics const &stats);
	void updateProgBar(QMovingStatistics const &stats);

private:
	QVBoxLayout * mQvbl;
	QLineEdit *mStdDev;
	QPushButton * mReset;
	QGridLayout * mGrid;
	QHBoxLayout *  mQhbl[4];
	QProgressBar * mProgress;
	QStackedLayout * mStackLayout;
	QLabel * mFull;

signals:
	void resetPressed();
};
#endif // QSTATISTICSWIDGETFOCUSSEUR_H