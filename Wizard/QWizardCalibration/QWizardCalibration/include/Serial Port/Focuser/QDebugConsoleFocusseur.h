/*! \class QDebugConsoleFocusseur
*	\brief Widget qui représente une console dans laquelle les données envoyées et reçues d'un port série sont affichées.
*
*	Utilisé dans la class QDevelopmentTabFocusseur
*
*/



#ifndef QDEBUGCONSOLEFOCUSSEUR_H
#define QDEBUGCONSOLEFOCUSSEUR_H

#include <QWidget>
#include <QPlainTextEdit>


class QDebugConsoleFocusseur : public QPlainTextEdit {
	Q_OBJECT

public:
	QDebugConsoleFocusseur(QWidget * parent = Q_NULLPTR);
	~QDebugConsoleFocusseur();

	public slots:
	/**
		Append the command that has just been executed to the list of command alerady shown in the console.

		@param text The most recent command text value
	*/
	void appendMessage(const QString &text);
	void setEcho(bool echo);


private:
	bool mEcho;
};

#endif  //QDEBUGCONSOLEFOCUSSEUR_H