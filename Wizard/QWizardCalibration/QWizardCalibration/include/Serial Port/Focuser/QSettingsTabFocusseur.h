/*! \class QSettingsTabFocusseur
*
*	\brief Widget servant à paramétrer la classe QFocusseur à l'aide de la classe QFocusseurSettings.
*
*	Possède des boutons pour sauvegarder et charger les paramètres.
*
*/


#ifndef QSETTINGSTABFOCUSSEUR_H
#define QSETTINGSTABFOCUSSEUR_H

#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QTooltip>
#include <QFormLayout>
#include <QSerialPort>
#include "..\Global\QAsyncSerialPort.h"

class QFocusseur;
class QSpinBox;
class QDoubleSpinBox;
class QLineEdit;
class QRadioButton;
class QGroupBox;

class QSettingsTabFocusseur : public QWidget {
	Q_OBJECT

public:
	QSettingsTabFocusseur(QFocusseur &focusseur, QWidget * parent = Q_NULLPTR);
	~QSettingsTabFocusseur();
private slots:
	void setFields();
	void updateButtons();
	void updatePortName(int val);
	void updateAccLength();
	void updateBaseSpeed();
	void updateSpeed();
	void updateTestLength();
	void showAccLength(QString response);
	void showBaseSpeed(QString response);
	void showSpeed(QString response);
	void showTestLength(QString response);
	void showState(bool connected);
	void showMaxAccLength(QString response);
	void showMaxTestLength(QString response);
	void showMinAccLength(QString response);
	void showMinTestLength(QString response);
	void save();
	void ok();
	void cancel();
	void revert();
	void defaultClicked();
	void accOptionChange();
	void speedOptionChange();
	void testOptionChange();
signals:
	void changeMade();

private:
	QFocusseur * mFocusseur;
	QFormLayout  *mDeviceLayout, *mCommLayout;
	QGroupBox * mOpBox, *mCommBox, * mAccOptions, *mSpeedOptions, *mTestOptions;
	QPushButton * mRevert, *mSave, *mDefault, *mOk, *mCancel;
	QGridLayout * mOpGrid, *mCommGrid, *mOpLayout;
	QVBoxLayout * mQvbl;
	QHBoxLayout * mButtonLayout;
	QDoubleSpinBox *mAccLength, *mTestLength;
	QSpinBox  * mBaseSpeed, *mSpeed, *mPortName;
	QRadioButton * mAccLong, * mAccMedium, * mAccPerso, * mAccShort, * mSpeedFast, * mSpeedMedium, * mSpeedPerso, * mSpeedSlow, * mTestLong, * mTestMedium, * mTestPerso, * mTestShort;

	void optionChange(QRadioButton * persoOpt, QDoubleSpinBox * toChange, QRadioButton * optMax, QRadioButton * optMed, QRadioButton * optMin);
	void optionChange(QRadioButton * persoOpt, QSpinBox * toChange, QRadioButton * optMax, QRadioButton * optMed, QRadioButton * optMin);

	static const QMap <QAsyncSerialPort::BaudRate, QString> QSettingsTabFocusseur::mBRtoStr;
	static const QMap <QString, QAsyncSerialPort::BaudRate> QSettingsTabFocusseur::mStrtoBR;
	static const QMap <QSerialPort::StopBits, QString> QSettingsTabFocusseur::mSBtoStr;
	static const QMap <QString, QSerialPort::StopBits> QSettingsTabFocusseur::mStrtoSB;
	static const QMap <QSerialPort::Parity, QString> QSettingsTabFocusseur::mParitytoStr;
	static const QMap <QString, QSerialPort::Parity> QSettingsTabFocusseur::mStrtoParity;
	static const QMap <QSerialPort::FlowControl, QString> QSettingsTabFocusseur::mFCtoStr;
	static const QMap <QString, QSerialPort::FlowControl> QSettingsTabFocusseur::mStrtoFC;
	static const QMap <QSerialPort::DataBits, QString> QSettingsTabFocusseur::mDBtoStr;
	static const QMap <QString, QSerialPort::DataBits> QSettingsTabFocusseur::mStrtoDB;
};
#endif //QSETTINGSTABFOCUSSEUR_H