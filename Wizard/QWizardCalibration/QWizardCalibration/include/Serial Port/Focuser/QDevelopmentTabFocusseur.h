/*! \class QDevelopmentTabFocusseur
*
*	\brief Widget générique comprenant un terminal et un dictionnaire de commandes.
*
*	Un double clic sur une commande du dictionnaire l'écrit dans la ligne de commande.
*
*/


#ifndef QDEVELOPMENTTABFOCUSSEUR_H
#define QDEVELOPMENTTABFOCUSSEUR_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPlainTextEdit>
#include <QList>
#include <QMap>
#include "..\Global\QSerialCommand.h"
#include "QDebugConsoleFocusseur.h"
#include "QDebugCommandLineFocusseur.h"
#include "..\Global\QDebugCommandList.h"

class QCommandSerialPort;


class QDevelopmentTabFocusseur : public QWidget {
	Q_OBJECT

public:
	QDevelopmentTabFocusseur(QCommandSerialPort * serial, QList<QSerialCommand const *> const &serialCommands, QWidget *parent = 0);
	~QDevelopmentTabFocusseur();

	public slots:
	void handleResponse(QByteArray data);

	private slots:
	void echoCommand(QString);
	void sendMessage(QString message);

	/**
		Switch the GUI mode to developement mode.
	*/
	void switchToDevMode(QString message);

private:
	QCommandSerialPort * mSerial;
	QVBoxLayout * mQvbl;
	QPlainTextEdit * mCommandInfo;
	QDebugConsoleFocusseur * mConsole;
	QDebugCommandList * mCommandList;
	QDebugCommandLineFocusseur * mCommandLine;
	//void fillMap();
};
#endif // QDEVELOPMENTTABFOCUSSEUR_H