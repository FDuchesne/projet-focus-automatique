/*! \class QFocusseurSettings
*
*	\brief Conteneur des paramètres d'un QFocusseur. Permet la sauvegarde et le chargement de fichier ini.
*
*	save() pour sauvegarder et load() pour charger.
*
*/


#ifndef QFOCUSSEURSETTINGS_H
#define QFOCUSSEURSETTINGS_H

#include <QObject>
#include "..\Global\QCommandSerialPort.h"

class QFocusseurSettings 
	: public QObject 
{
	Q_OBJECT

public:
	QFocusseurSettings(QObject * parent = Q_NULLPTR);
	~QFocusseurSettings();

	static QString const PORTNAME; 
	static QString const BAUDRATE; 
	static QString const STOPBITS; 
	static QString const DATABITS; 
	static QString const PARITY; 
	static QString const FLOWCONTROL; 
	static QString const ACCLENGTH;
	static QString const BASESPEED;
	static QString const SPEED;
	static QString const TESTLENGTH;

	// Serial Port
	QString mPortName;
	QAsyncSerialPort::BaudRate mBaudRate;
	QSerialPort::StopBits mStopBits;
	QSerialPort::DataBits mDataBits;
	QSerialPort::Parity mParity;
	QSerialPort::FlowControl mFlowControl;

	// Focuser
	double mAccLenght;
	int mBaseSpeed;
	int mSpeed;
	double mTestLength;

	void save(QString fileName);
	void load(QString fileName);

private:
	bool isValid();
};
#endif //QRANGEFINDERSETTINGS_H