#ifndef QCGEPRO_H
#define QCGEPRO_H

#include <QObject>
#include <QTimer>
#include "..\Global\QSerialCommand.h"
#include "..\Global\QCommandSerialPort.h"


class QCGEPro : public QObject
{
	Q_OBJECT

public:
	QCGEPro(QObject *parent = 0);
	~QCGEPro();

	static QString const SEPARATOR;
	static QString const TERMINATOR;

	enum class Command {
		InitSwitchAX = 0,
		InitSwitchAY = 1,
		InitSwitchBX = 2,
		InitSwitchBY = 3,
		IsGoingToSwitchX = 4,
		IsGoingToSwitchY = 5,
		GetPosition = 6
	};

	QCommandSerialPort mSerial;
	QMap<Command, QSerialCommand const *> mSerialCommands;

	QSerialCommand mTestCommand;
	QSerialCommand mTestCommand2;
	QSerialCommand mTestCommand3;
	QSerialCommand mTestCommand4;
	QSerialCommand mTestCommand5;

	void testCommand();
	void testCommand2();
	void testCommand3();
	void testCommand4();
	void testCommand5();

	void sendCommandNoParams(Command command);

private:
	void fillDictionary();

private slots :
	void handleMatchingResponse(QByteArray const &response, const QSerialCommand &command);
	void deviceConnected(bool connected);
	
	
};

#endif // QCGEPRO_H
