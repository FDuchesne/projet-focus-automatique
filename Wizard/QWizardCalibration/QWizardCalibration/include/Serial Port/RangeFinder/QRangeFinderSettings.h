/*! \class QRangeFinderSettings
*
*	\brief Conteneur des paramètres d'un QRangeFinderAR2000. Permet la sauvegarde et le chargement de fichier ini.
*
*	save() pour sauvegarder et load() pour charger.
*
*/


#ifndef QRANGEFINDERSETTINGS_H
#define QRANGEFINDERSETTINGS_H

#include <QObject>
#include "..\Global\QCommandSerialPort.h"
//#include "QRangeFinderAR2000.h"

class QRangeFinderSettings 
	: public QObject 
{
	Q_OBJECT

public:
	QRangeFinderSettings(QObject * parent = Q_NULLPTR);
	~QRangeFinderSettings();

	static QString const PORTNAME; 
	static QString const BAUDRATE; 
	static QString const STOPBITS; 
	static QString const DATABITS; 
	static QString const PARITY; 
	static QString const FLOWCONTROL; 
	static QString const AVERAGE; 
	static QString const FREQUENCY; 
	static QString const OFFSET; 
	static QString const MIN_WINDOW; 
	static QString const MAX_WINDOW; 
	static QString const LASER;

	// Serial Port
	QString mPortName;
	QAsyncSerialPort::BaudRate mBaudRate;
	QSerialPort::StopBits mStopBits;
	QSerialPort::DataBits mDataBits;
	QSerialPort::Parity mParity;
	QSerialPort::FlowControl mFlowControl;

	// Range Finder
	int mAverage;		// (1 - 50)
	double mFrequency;	// 0 - 100
	double mOffset;		// -500 - 500
	double mWindowMin;	// -500 - 500
	double mWindowMax;	// -500 - 500
	bool mLaserOnStart;

	void save(QString fileName);
	void load(QString fileName);

private:
	bool isValid();
};
#endif //QRANGEFINDERSETTINGS_H