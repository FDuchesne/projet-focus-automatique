/*! \class QRangeFinderGUI
*
*	\brief Interface graphique permettant de contrôler et paramétrer le télémètre AR2000 par le biais de sa classe.
*
*	Regroupe les onglets d'opérations, de paramétrage et de dévelopement, ainsi qu'une barre d'état.
*
*/


#ifndef QRANGEFINDERGUI_H
#define QRANGEFINDERGUI_H

#include <QWidget>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QMessageBox>
#include "QOperationsTab.h"
#include "QSettingsTab.h"
#include "QDevelopmentTab.h"
#include "QRangeFinderAR2000.h"
//#include "QTestWin.h"

class QRFStatusBar;


class QRangeFinderGUI : public QWidget {
	Q_OBJECT

public:
	QRangeFinderGUI(QWidget * parent = Q_NULLPTR);
	~QRangeFinderGUI();
	QRangeFinderAR2000 & rangeFinder() { return mRangeFinder; }


private slots:
	void tabSelected(int index);

signals:
	void widgetClosed();

private:
	//QTestWin * mTestWin;
	QRangeFinderAR2000 mRangeFinder;
	QTabWidget * mTabs;
	QVBoxLayout * mQvbl;
	QDevelopmentTab * mDevelopment;
	QSettingsTab * mParams;
	QOperationsTab * mOperations;
	QRFStatusBar * mStatusBar;

protected:
	void closeEvent(QCloseEvent * event);
};
#endif //QRANGEFINDERGUI_H