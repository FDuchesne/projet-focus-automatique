/*! \class QOperationsOutputWidget
*
*	\brief Affiche les mesures provenant du signal measurementReceived() de la classe QRangeFinderAR2000
*
*	Utilisé dans la classe QOperationsTab
*/


#ifndef QOPERATIONSOUTPUTWIDGET_H
#define QOPERATIONSOUTPUTWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>

class QRangeFinderAR2000;


class QOperationsOutputWidget : public QWidget {
	Q_OBJECT

public:
	QOperationsOutputWidget(QRangeFinderAR2000 * rangeFinder, QWidget * parent = Q_NULLPTR);
	~QOperationsOutputWidget();

public slots:
	void showMeasurement(double distance, double quality, double temperature);

private:
	QRangeFinderAR2000 * mRangeFinder;
	QGridLayout * mGrid;
	QLineEdit * mMeasurement, *mSignalQuality, *mTemperature;
};
#endif QOPERATIONSOUTPUTWIDGET_H
