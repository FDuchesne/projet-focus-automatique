/*! \class QDevelopmentTab
*
*	\brief Widget générique comprenant un terminal et un dictionnaire de commandes.
*
*	Un double clic sur une commande du dictionnaire l'écrit dans la ligne de commande.
*
*/


#ifndef QDEVELOPMENTTAB_H
#define QDEVELOPMENTTAB_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPlainTextEdit>
#include <QList>
#include <QMap>
#include "..\Global\QSerialCommand.h"
#include "QDebugConsole.h"
#include "QDebugCommandLine.h"
#include "..\Global\QDebugCommandList.h"

class QCommandSerialPort;


class QDevelopmentTab : public QWidget {
	Q_OBJECT

public:
	QDevelopmentTab(QCommandSerialPort * serial, QList<QSerialCommand const *> const &serialCommands, QWidget *parent = 0);
	~QDevelopmentTab();

	public slots:
	void handleResponse(QByteArray data);

	private slots:
	void echoCommand(QString);
	void sendMessage(QString message);
	void switchToDevMode(QString message);

private:
	QCommandSerialPort * mSerial;
	QVBoxLayout * mQvbl;
	QPlainTextEdit * mCommandInfo;
	QDebugConsole * mConsole;
	QDebugCommandList * mCommandList;
	QDebugCommandLine * mCommandLine;
	void fillMap();
};
#endif // QDEVELOPMENTTAB_H