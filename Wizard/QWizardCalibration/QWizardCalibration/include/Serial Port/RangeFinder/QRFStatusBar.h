﻿/*! \class QRFStatusBar
*
*	\brief Affiche les réponses et messages du télémètre ainsi que différents états de celui-ci.
*/



#ifndef QRFSTATUSBAR_H
#define QRFSTATUSBAR_H

#include<QStatusBar>

class  QHBoxLayout;
class QLabel;
class QLEDLight;
class QTimer;
class QRangeFinderAR2000;


class QRFStatusBar : public QStatusBar {
	Q_OBJECT

public:
	QRFStatusBar(QRangeFinderAR2000 * rangeFinder, QWidget * parent = Q_NULLPTR);
	~QRFStatusBar();

private:
	QHBoxLayout * mQhbl;
	QLabel * mMsg;
	QLEDLight * mLaser, *mTracking, *mConnection;
	QTimer * mTimer;
	QRangeFinderAR2000 * mRangeFinder;

private slots:
	void updateConnectionLED(bool connected);
	void updateTrackingLED(bool isTracking);
	void updateLaserLED(bool laserOn);
	void updateDevMode(bool devMode);
	void updateMsg(QString message);
};
#endif // QRFSTATUSBAR_H