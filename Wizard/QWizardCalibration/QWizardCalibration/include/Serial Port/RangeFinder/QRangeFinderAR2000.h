/*! \class QRangeFinderAR2000
*
*	\brief %Point d'entrée des commandes et de sortie des réponses.
*	
*	Contient les dictionnaires de QSerialCommand (mSerialCommands) et de messages d'erreurs de l'appareil (mDeviceMessages).
*	Ils doivent être remplis dans le constructeur à l'aide des méthodes fillDictionary() et fillDeviceMessages().
*	L'envoie de commande s'initie à partir de cette classe. Les méthodes sans paramètres s'envoient avec sendCommandNoParams(), et les autres possèdent leur méthode spécifique.
*	De plus, le traitement des réponses y est effectué avec la méthode handleMatchingResponse() et des messages avec la méthode handleMessageReceived().
*/


#ifndef QRANGEFINDERAR2000_H
#define QRANGEFINDERAR2000_H

#include <QObject>
#include <QList>
#include <QMap>
#include "..\Global\QMovingStatistics.h"
#include "..\Global\QCommandSerialPort.h"
#include "QRangeFinderSettings.h"


class QRangeFinderAR2000 : public QObject
{
	Q_OBJECT

public:
	QRangeFinderAR2000();
	~QRangeFinderAR2000();


	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Attributes
	////////////////////////////////////////////////////////////////////////////////////////////////////

	// Const
	//==================================================================================================

	enum class Command {
		ShowDeviceID = 0,
		ShowCommands = 1,
		ShowTemperature = 2,
		ShowParams = 3,
		ShowDiodeStatus = 4,
		ShowAutoStart = 5,
		ResetParams = 6,
		DisplayOff = 7,
		DisplayOn = 8,
		SetAutoStart = 9,
		SetAverage = 10,
		ShowAverage = 11,
		SetFrequency = 12,
		ShowFrequency = 13,
		SetWindow = 14,
		ShowWindow = 15,
		ShowUnit = 16,
		SetUnit = 17,
		SetOffset = 18,
		ShowOffset = 19,
		SetOutputFormat = 20,
		ShowOutputFormat = 21,
		SetBaudRate = 22,
		ShowBaudRate = 23,
		SetStopBits = 24,
		ShowStopBits = 25,
		SetDefaultTracking = 26,
		ShowDefaultTracking = 27,
		RestartDevice = 28,
		DeactivateLaser = 29,
		ActivateLaser = 30,
		StopTracking = 31,
		StartContinuousTracking = 32,
		StartDistanceTracking = 33,
		DoSingleMeasurement = 34,
		EmptyCommand = 35
	};
	enum class TrackingMode{ None = 0, DistanceTracking = 1, ContinuousTracking = 2, PreciseMeasurement = 3 };

	static QString const SEPARATOR;
	static QString const TERMINATOR;
	static QString const DEFAULT_INI;
	static QString const CURRENT_INI;
	static QString const TEMP_INI;


	// Var
	//==================================================================================================

	QSerialCommand mTestCommand;
	QSerialCommand mTestCommand2;
	QSerialCommand mTestCommand3;
	QSerialCommand mTestCommand4;
	QSerialCommand mTestCommand5;

	QCommandSerialPort mSerial;  // public?
	QMap<Command, QSerialCommand const *> mSerialCommands;
	QRangeFinderSettings mSettings;

	QMovingStatistics mDistanceStats;
	QMovingStatistics mSignalQualityStats;
	QMovingStatistics mTemperatureStats;

	bool mDeviceConnected, mLaserOn, mIsTracking;


	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////

	TrackingMode trackingStatus() { return mTrackingStatus; }

	void testCommand();
	void testCommand2();
	void testCommand3();
	void testCommand4();
	void testCommand5();

	void sendCommandNoParams(Command command);
	void startPreciseMeasurement(QString sa = "");

	void setLaserOn(bool on);
	void setAverage(int average);
	void setMeasuringFrequency(double frequency);
	void setMeasuringWindow(double start, double end);
	void setMeasuringUnit(QString unit);
	void setOffset(double meters);
	void setAutoStart(int command);
	void setBaudRate(QAsyncSerialPort::BaudRate baudRate);
	void setStopBits(QSerialPort::StopBits stopBits);

	bool fileExists(QString path);


private:
	QMap<QString, QString> mDeviceMessages;
	TrackingMode mTrackingStatus;

	void initSettings();
	void fillDictionary();
	void fillDeviceMessages();

	void setOutputFormat(int a, int b, int c, int d);


public slots:
	void handleTrackingRequest(TrackingMode trackingMode);
	void resetStats();
	void applySettings();
	void connectSerialPort();
	void connectionUpdated(bool connected);


private slots :
	void handleMatchingResponse(QByteArray const &response, QSerialCommand const &command);
	void handleMessageReceived(QString const &message);


signals:
	void trackingChanged(bool isTracking);
	void laserChanged(bool isOn);
	void messageReceived(QString const &response);
	void measurementReceived(double distance, double quality, double temperature);
	void matchingResponseReceived(QString response);
	void message(QString message);
};

#endif // QRANGEFINDERAR2000_H
