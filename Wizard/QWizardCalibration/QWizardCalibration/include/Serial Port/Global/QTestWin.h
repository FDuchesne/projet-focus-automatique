/*! \class QTestWin
*
*	\brief Classe de test permetant d'assigner des commandes de la classe QRangeFinderAR2000 aux boutons, et d'afficher les tampons.
*/


#ifndef QTESTWIN_H
#define QTESTWIN_H

#include <QWidget>
#include <QPushButton>
#include "..\RangeFinder\QRangeFinderAR2000.h"


class QTestWin : public QWidget
{
	Q_OBJECT

public:
	QTestWin(QRangeFinderAR2000 * r,  QWidget *parent = 0);
	~QTestWin();

private slots:
	void showCommandsBuffer();
	void showBuffer();
	void test();
	void test2();
	void test3();
	void test4();
	void test5();

private:
	QRangeFinderAR2000 * rf;

	//QList<QPushButton> buttons;
	QPushButton * btnTest;
	QPushButton * btnTest2;
	QPushButton * btnTest3;
	QPushButton * btnTest4;
	QPushButton * btnTest5;

	QPushButton * btnShowCommands;
	QPushButton * btnShowBuffer;


	//void createButton(QString title, void(QPushButton::*signal)(void), void(QTestWin::*slot)(void), int x, int y, int width, int height);
};

#endif // QTESTWIN_H
