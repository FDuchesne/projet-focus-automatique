/*! \class QMovingStatistics
*
*	\brief Calcule la moyenne, l'écart-type, le minimum, et le maximum d'un tampon RingBuffer.
*
*	À chaque fois que la méthode addValue() est appelée, les statistiques sont recalculés.
*
*
*/


#ifndef QMOVINGSTATISTICS_H
#define QMOVINGSTATISTICS_H

#include <QRegulaRexpression>
#include"RingBuffer.h"

class QString;

class QMovingStatistics : public QObject
{

	Q_OBJECT

public:
	QMovingStatistics(int bufferSize, QRegularExpression regex = QRegularExpression());
	~QMovingStatistics();

	RingBuffer<double> const & data() const { return mData; }
	double average() const { return mAverage; }
	double stdDeviation() const { return mStdDeviation; }
	double min() const { return mMin; }
	double max() const { return mMax; }


private:
	RingBuffer<double> mData;
	QRegularExpression mRegex;
	double mAverage;
	double mStdDeviation;
	double mMin;
	double mMax;

	int mNMin;
	int mNMax;
	double mSum;
	double mSumXPow2;

	void calcAverage();
	void calcStdDeviation();
	void calcMin(double addedValue, double removedValue = std::numeric_limits<double>::min());
	void calcMax(double addedValue, double removedValue = std::numeric_limits<double>::min());

	void updateMin(double potentialMin);
	void updateMax(double potentialMin);


public slots:
	void reset();
	void addValue(QString const &line);
	void addValue(double value);

signals:
	void updated(QMovingStatistics const &stats);
	
};

#endif // QMOVINGSTATISTICS_H
