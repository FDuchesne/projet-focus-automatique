/*! \class RingBuffer
*
*	\brief Tableau d'un type t (template) d'une taille x définie, dont seulement les x dernières données ajoutées sont conservées.
*
*	L'opérateur [] est surchargé pour accéder aux données dans l'ordre où elles ont été ajoutées.
*
*/


#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <limits>
#include <QDebug>


template <typename t>
class RingBuffer
{
public:
	RingBuffer(); // = delete
	RingBuffer(unsigned int size);
	//RingBuffer(RingBuffer<t> const &buffer){ 
	//	mBuffer = buffer.mBuffer;
	//	mSize = buffer.mSize;
	//	mLast = buffer.mLast;
	//	mCount = buffer.mCount;
	//}
	~RingBuffer();

	void reset();

	t get(unsigned int i, bool &valid) const;
	bool set(unsigned int i, t value);
	void add(t value);

	unsigned int size() const { return mSize; }
	unsigned int count() const { return mCount; }
	t& operator[](unsigned int i) const { return mBuffer[(i + 1 + mLast) % mSize]; }
	void display();


private:
	t * mBuffer;
	unsigned int mSize;
	unsigned int mLast;
	unsigned int mCount;

};


template <typename t>
RingBuffer<t>::RingBuffer(unsigned int size)
	:	mBuffer{ new t[size] }, 
		mSize{ mBuffer ? size : 0 }, 
		mLast{ std::numeric_limits<unsigned int>::max() }, 
		mCount{ 0 }
{
}

template <typename t>
RingBuffer<t>::~RingBuffer()
{
	if (mBuffer) {
		delete[] mBuffer;
	}
}


template <typename t>
void RingBuffer<t>::reset() {
	mLast = std::numeric_limits<unsigned int>::max();
	mCount = 0;
}

template <typename t>
t RingBuffer<t>::get(unsigned int i, bool &valid) const
{
	if (mSize == 0) {
		valid = false;
		return 0;
	}
	if (i < mCount) {
		valid = true;
		if (mCount < mSize) {
			return mBuffer[i];
		}
		else {
			return mBuffer[(i + 1 + mLast) % mSize];
		}
	}
	valid = false;
	return 0;
}

template <typename t>
bool RingBuffer<t>::set(unsigned int i, t value)
{
	if (mSize == 0) {
		return false;
	}
	if (i < mCount) {
		if (mCount < mSize) {
			mBuffer[i] = value;
		}
		else {
			mBuffer[(i + 1 + mLast) % mSize] = value;
		}
		return true;
	}
	return false;
}

template <typename t>
void RingBuffer<t>::add(t value)
{
	if (mSize == 0) {
		return;
	}
	mLast = (mLast + 1) % mSize;
	mBuffer[mLast] = value;
	mCount = std::min(mCount + 1, mSize);
}

template <typename t>
void RingBuffer<t>::display()
{
	for (int i = 0; i < count(); ++i) {
		bool v;
		qDebug() << i << " " << get(i, v);
	}
}

#endif // RINGBUFFER_H
