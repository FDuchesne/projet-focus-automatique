/*!	\class QSerialCommand
*
*	\brief Représente une commande série. 
*
*	Fonctionne de concert avec la classe QCommandSerialPort. 
*	Voir sa documentation pour des détails sur la gestion des commandes selon leur mode d'opération (mOperationMode).
*
*	Construit la chaine de caractères à envoyer à l'aide de la méthode commandToSend(). 
*	Possède un constructeur pour une commande en QByteArray et un autre pour une commande en QString.
*	Normalement quand on envoie une commande, on s'attend à une réponse qui lui est propre.
*	Pour vérifier qu'elle corresponde, on utilise 2 façons dépendamment du constructeur utilisé.
*	Si la commande est en QByteArray, la réponse sera comparé à un QList<QByteArray>.
*	Si la commande est en QString, la réponse sera évaluée à l'aide d'un QRegularExpression.
*	La méthode getFirstMatch() permet de trouver la première correspondance dans un QByteArray (ou QString) passé en paramètre.
*
*/



#ifndef QSERIALCOMMAND_H
#define QSERIALCOMMAND_H

#include <QString>
#include <QByteArray>
#include <QRegularExpression>
#include <QList>
#include <QVariant>
#include "SerialOperationMode.h"


class QSerialCommand
{
public:
	enum class IOType { In = 1, Out = 2, Io = 3 };

	QSerialCommand() {}
	QSerialCommand(
		QString command,  												// cmd
		QString name, 													// name
		IOType ioType, 													// IOType
		int nParam, 													// # params
		SerialOperationMode::BlockingMode blockingMode, 				// BlockingMode
		SerialOperationMode::FluxMode fluxMode, 						// FluxMode	
		QString eolCar, 												// eol car
		QString separator, 												// separator
		QString family = "", 											// family
		QString shortDesc = "",											// short desc
		QRegularExpression returnExp = QRegularExpression(""),			// response
		QString desc = "",												// description
		QString tooltip = "");											// tooltip

	QSerialCommand(
		QByteArray command,
		QString name,
		IOType ioType,
		int nParam,
		SerialOperationMode::BlockingMode blockingMode,
		SerialOperationMode::FluxMode fluxMode,
		QString eolCar,
		QString separator,
		QString family = "", 		
		QString shortDesc = "",
		QList<QByteArray> expectedResponses = QList<QByteArray>(),
		QString desc = "",						
		QString tooltip = "");

	~QSerialCommand();

	inline bool operator==(QSerialCommand const & command) const;

	QString command() const { return mCommand; }
	int nParam() const { return mNParam; };
	QRegularExpression returnExp() const { return mReturnExp; }

	QString family() const { return mFamily; }
	IOType ioType() const { return mIOType; }
	QString name() const { return mName; }
	QString eolCar() const { return mEOLCar; }
	QString shortDesc() const { return mShortDesc; }
	QString desc() const { return mDesc; }
	QString toolTip() const { return mToolTip; }
	SerialOperationMode operationMode() const { return mOperationMode; }
	QList<QSerialCommand const *> pushModeStopCommands() const { return mPushModeStopCommands; }

	QByteArray commandToSend(const QList<QVariant> & params) const;
	void addPushModeStopCommand(QSerialCommand const * command);
	bool stopsPushMode(QSerialCommand const &command) const;

	QByteArray getFirstMatch(QByteArray const &buffer);


private:
	bool mIsString;
	QByteArray mCommand;
	int mNParam;
	QRegularExpression mReturnExp;
	QString mSeparator;
	QString mEOLCar;

	QString mFamily;
	IOType mIOType;
	QString mName;
	QString mShortDesc;
	QString mDesc;
	QString mToolTip;
	SerialOperationMode mOperationMode;
	QList <QByteArray> mExpectedResponses;

	QList<QSerialCommand const *> mPushModeStopCommands;
	
};

#endif // QSERIALCOMMAND_H
