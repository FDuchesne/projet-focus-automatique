/*! \class QPictureSettingWidget
*
*	\brief Widget where the user can set the dimension of the zone where the focus will occur.
*
*	The user can add and remove zones and move between each one of them.
*	The user can either apply the settings on the current zone, apply it on all of them and quit or cancel the operation.
*
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 17/07/2018
*/

#ifndef Q_PICTURE_SETTING_WIDGET_H
#define Q_PICTURE_SETTING_WIDGET_H

#include <QWidget>

class QPushButton;
class QLineEdit;


class QPictureSettingWidget : public QWidget
{
	Q_OBJECT

public:
	/**
		Constructor

		@param col Reference to the vector containing the column where each zones used during the focus starts.
		@param row Reference to the vector containing the row where each zones used during the focus starts.
		@param width Reference to the vector containing the width of each zones used during the focus.
		@param height Reference to the vector containing the height of each zones used during the focus.
	*/
	QPictureSettingWidget(QVector<int> & col, QVector<int> & row, QVector<int> & width, QVector<int> & height, QWidget *parent = nullptr);

	///Destructor
	~QPictureSettingWidget();

private:
	//@{
	/** Line edit used by the user to modify the value of each zones. The lineEdits have a double validator */
	QLineEdit * mColEdit, *mRowEdit, *mWidthEdit, *mHeightEdit;
	//@}

	//@{
	/** Button used to navigate in the GUI*/
	QPushButton *mApplyAllAndQuit, * mApplyButton, *mExitButton, *mPrevButton, * mNextButton, *mAddButton, *mRemoveButton;
	//@}

	//@{
	/** Pointer to the references of the vector containing the settings used during the focus event*/
	QVector<int> *mCol, *mRow, *mWidth, *mHeight;
	//@}

	//@{
	/** Vectors containing the temporary data for each zones. Allows the user to navigate between zones without saving them each time, which allows him
	to cancel the whole operations if needed*/
	QVector<int> mTempCol, mTempRow, mTempWidth, mTempHeight;
	//@}
	///Variable containing the index of the current zone being modified
	int mCurrentZone;

	/**
		Updates all the temporary datas.
	*/
	void saveTemporary();

private slots:
	/**
		Saves the settings of all the zone and exit the settings tab.
	*/
	void applyAllAndQuit();
	/**
		Modify the original vectors by applying the settings currently stored in their respective temp vector.
	*/
	void applySettings();
	/**
		Exit the settings GUI.
	*/
	void exitSettings();
	/**
		Goes to the previous zone settings
	*/
	void prevZoneSettings();
	/**
		Goes to the next zone settings.
	*/
	void nextZoneSettings();
	/**
		Modify the value in the lineEdits, and enable/disable the prev/next button
	*/
	void pageChanged();
	/**
		Add a new zone
	*/
	void addZone();
	/**
		Remove the current zone
	*/
	void removeZone();

protected:
	///Redefinition of the closeEvent
	void closeEvent(QCloseEvent * event);

signals: 
	///Signals emited when the widget is closing
	void closing();
};
#endif //Q_PICTURE_SETTING_WIDGET_H
