/*! \class QWizardCalibration
*
*	\brief Wizard for the calibration of a focuser used in wind turbine examination
*
*	A Wizard that helps the user to go throught the calibration of a focuser used in wind turbine examination.
*	Currently only able to do automatic focus
*
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 17/07/2018
*/

#ifndef QWIZARD_CALIBRATION_H
#define QWIZARD_CALIBRATION_H

#include <QtWidgets/QMainWindow>
#include <QWizard>


class QWizardCalibration : public QWizard
{
	Q_OBJECT

public:
	///Constructor
	QWizardCalibration(QWidget *parent = Q_NULLPTR);
	
	///Default destructor
	~QWizardCalibration() = default;

	///Order of the pages in the wizard
	enum { Accueil, Intro, Focus, Conclu };
};

#endif // QWIZARDCALIBRATION_H

#ifndef QACCEUIL_PAGE_H
#define QACCEUIL_PAGE_H

/*! \class QAcceuilPage
*
*	\brief First page of the Calibration Wizard, ask basic question such as the operator name
*
*	The first page of the calibration Wizard. Ask basic question such as the operator(s) name(s), the reason for the calibration,
*	the telescope model used, the focuser model used.
*
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 17/07/2018
*/

class QVBoxLayout;
class QLineEdit;
class QComboBox;
class QPushButton;

class QAcceuilPage : public QWizardPage
{
	Q_OBJECT

public:

	///Constructor
	QAcceuilPage(QWidget *parent = nullptr);
	
	///Default Destructor
	~QAcceuilPage();

private:

	//@{
	/** Number of operators allowed */
	const int NB_MAX_OPE{ 5 }, NB_MIN_OPE{ 2 };
	//@}
	QLineEdit *ope1, *ope2, *description;
	QComboBox *modeleTelescope, *modeleFocusseur;
	QPushButton * btnPlus, *btnMoins;
	QVBoxLayout * lesOperateurs;

private slots:
	/**
		Add a lineEdit to the wizard page so that the user can add more operator than the default amount (2)
		Cannot exceed the limits of operator

		@see QAcceuilPage::NB_MAX_OPE
	*/
	void ajoutOperateur();

	/**
		Remove a lineEdit to the wizard page if the operation need less operator than the amount of lineEdit shown on the page
		Cannot go lower than the limits of operator

		@see QAcceuilPage::NB_MIN_OPE
	*/
	void enleverOperateur();
};

#endif //QACCEUIL_PAGE_H


#ifndef QINTRODUCTION_PAGE_H
#define QINTRODUCTION_PAGE_H

/*! \class QIntroductionPage
*
*	\brief Second page of the Calibration Wizard, allow the user to specify how the calibration will be done.
*
*	The Second page of the calibration Wizard. Allow the user to specify how the calibration will be done.
*	Currently allow the user to select between high precision calibration, low precision calibration and personalize calibration
*	While there's a personalize calibration, the minimum and maximum distance between the user and the observed wind turbine is limited.
*
*	@see QIntroductionPage::MIN_HAUT, QIntroductionPage::MAX_HAUT, QIntroductionPage::MIN_BASE, QIntroductionPage::MAX_BASE
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 17/07/2018
*/


class QSpinBox;
class QGroupBox;
class QRadioButton;
class QLineEdit;

class QIntroductionPage : public QWizardPage
{
	Q_OBJECT

public:
	///Constructor
	QIntroductionPage(QWidget *parent = nullptr);

	///Default destructor
	~QIntroductionPage();

private:
	//@{
	/**The distance limits between the user and the observed wind turbine*/
	const int MIN_BASE{ 35 }, MAX_BASE{ 50 }, INTER_BASE{ 1 },
		MIN_HAUT{ 10 }, MAX_HAUT{ 140 }, INTER_HAUT{ 1 };
	//@}
	QGroupBox *mode, *precision;
	QRadioButton *modeManuel, *modeAutomatique, *bassePrecision, *hautePrecision, *precisionPerso;
	QSpinBox /* typeFocusChoisi, */* mMinDist, *mMaxDist, *mInterval;
	QLineEdit *lesOperateurs, *laDescription, *leModele;

private slots:

	/**
		Sets the calibration setting to high precision level. 
		Doesn't allow the user to modify the calibration settings.
	*/
	void hautePrcsn();
	/**
		Sets the calibration settings to low precision level.
		Doesn't allow the user to modify the calibration settings
	*/
	void bassePrcsn();
	/**
		Allows the user to sets the calibration settings by himself.
		The user still has to respect the distance limits.

		@see QIntroductionPage::MIN_BASE, QIntroductionPage::MAX_BASE, QIntroductionPage::MIN_HAUT, QIntroductionPage::MAX_HAUT
	*/
	void prcsnPerso();
};

#endif // QINTRODUCTION_PAGE_H

#ifndef QFOCUS_PAGE_H
#define QFOCUS_PAGE_H

/*! \class QFocusPage
*
*	\brief Third page of the Calibration Wizard, guide the user throught the calibration of the focuser
*
*	The third page of the calibration Wizard. Guide the user throught the calibration of the focuser.
*	Goes step by step based on the settings define in the previous page.
*	Tells the operator at which distance he must place himself before pressing the "focus" button.
*	The user can at anytime redo the autofocus of any distance.
*	When the user is satisfied with result, the program will create a .csv file at the specified location containing the data of the calibration.
*
*	@see QLutWidget.h, QAlgoFocus.h, QRangeFinderGUI.h, LutAbstract.h, LutEchelon.h, DaoCsv.h, DTO.h
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 17/07/2018
*/


class QLutWidget;
class QLabel;
class QProgressBar;
class QAlgoFocus;
class QRangeFinderGUI;
class LutAbstract;
class LutEchelon;
class QRangeFinderAR2000;


class QFocusPage : public QWizardPage
{
	Q_OBJECT

public:
	///Constructor
	QFocusPage(QWidget *parent = nullptr);

	///Default destructor
	~QFocusPage() = default;

	/**
		Redefinition of the initializePage function.
		Instantiate the mIntervalDistance, mMinDistance, mMaxDistance, mNbOfIndex, mCurrentDistance and mCurrentIndex variables
		Sets the title and subtitle of the page.
	*/
	virtual void initializePage() override;

	/**
		Redefinition of the validatePage function.
		Verify if the users has selected a place to save the file and whether the file has successfuly been saved.
	*/
	virtual bool validatePage() override;

	/**
		Redefinition of the isComplete function.
		For the page to be complete, the user most have done all the calibration based on the settings specified in the previous page.
	*/
	virtual bool isComplete() const override;

	/**
		Allows the user to modify a value he has alerady take.
		
		@param itemIndex the index of the measurment the user want to modify
	*/
	void modifyExistingValue(int itemIndex);

private:

	///The subtitle currently shown on the screen
	QString mSubtitle;

	///The range finder control
	QRangeFinderAR2000 * mRangeFinder;

	//@{
	/** Variable used in data manipulation */
	int mCurrentIndex, mNbOfIndex, mIntervalDistance, mMinDistance, mMaxDistance, mCurrentDistance, mBufferIndex, mCurrentRangeFinderDisance;
	//@}

	//@{
	/**Vectors in which the x, y coordinates and the width / height of the zone(s) to focus are stored */
	QVector<int> mCol, mRow, mWidth, mHeight;
	//@}

	///The path where the csv file will be created
	QString mFilePath;

	///The Look Up Table where all the data are stored
	LutAbstract * mLut;

	///The Look Up Table Interface that shows the current collected data on a chart
	QLutWidget * mQLutWidget;

	//@{
	/**The buttons of the interface */
	QPushButton * mBtnFocusAuto, *mBtnOpenRangeFinderGUI, *mBtnCommit, *mBtnGoToCurrent, *mBtnReset, *mBtnPrevFocus, *mBtnNextFocus, *mBtnPicutreSettings;
	//@}

	QLabel *mNomOpe, *mResultatFocus, *mFilePathLabel;
	
	///Specify and allows the user to modify the index of the measurement that is currently being modifed.
	QSpinBox * mIndexBeingModifed;

	QProgressBar * mProgressBar;

	///Algorithim for the automatic focus.
	QAlgoFocus * mAlgoFocus;

	///User interface of the range finder to help the operator to position himself.
	QRangeFinderGUI * mRangeFinderGUI;

	///Stores all the focus result for the current stage of the calibration.
	QVector<double> mFocusResultBuffer;

	/**
		Change the subtitle of the page and the distance where the opeator most position himself when the index is changed.
		Currently it is a simple function, but it may be better to change it to a signal.
	*/
	void indexChanged();

	/**
		Change the result shown in the mResultatFocus label and add/replace the value stored in the lut by the value stored in the buffer at the current index.

		@see mFocusResultBuffer
		@see mBufferIndex
	*/
	void bufferIndexChanged();

public slots:

	/**
		Start the automatic focus and sets the text of the mResultatFocus to the returned value

		@see AlgoFocus
	*/
	void focusImage();

	/**
		Recieve the returns of the auto focus algorithm and creates a point in the lut accordingly.
		Currently create a point with a hard coded distance from the object (x value).
		Should instead get the info from the rangeFinder. 
	*/
	void recieveFocusInfo(int blurryLevel);

	/**
		Opens the range finder graphical user interface

		@see RangeFinderGUI
	*/
	void openRangeFinderGUI();

	/**
		Commit the distance of the focuser calculated automatically.
		The value isn't absolute and can be changed later on at any point.
	*/
	void commitPoint();

	/**
		Goes to the currently needed value index.
		If all index value have already been calculated, enable the save files button.
	*/
	void goToCurrent();

	/**
		Check if the calibration has been completed and if it has, disable the commit button
		and change the subtitle accordingly.
	*/
	void calibrationHasBeenCompleted();

	/**
		Resets the whole calibration (delete all collected data).
	*/
	void resetPoints();

	/**
		Goes to the previous focus result of the current calibration stage
	*/
	void previousFocusResult();

	/**
		Goes to the next focus result of the current calibration stage
	*/
	void nextFocusResult();

	/**
		Opens the QPictureSettingWidget.

		@see QPictureSettingWidget
	*/
	void openPictureSettings();

	/**
		Enable the picture settings button when it's widget is closed
	*/
	void pictureSettingsClosed();

	/**
		Create a point in the lut based on the distance from the object given by the range finder and the focuser current position.

		@param distanceFromObject The distance from the object and the rangeFinder.
		@param quality Not used in the function. Just matches the signal's return.
		@param temperature Not used in the function. Just matches the signal's return
	*/
	void createPoint(double distanceFromObject, double quality, double temperature);
};

#endif //QFOCUS_PAGE_H

#ifndef QCONCLUSION_PAGE_H
#define QCONCLUSION_PAGE_H

/*! \class QConclusionPage
*
*	\brief Last page of the Calibration Wizard, does a recap of the calibration.
*
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 17/07/2018
*/

class QConclusionPage : public QWizardPage
{
	Q_OBJECT

public:
	///Constructor
	QConclusionPage(QWidget *parent = nullptr);
	///Default destructor
	~QConclusionPage();

	/**
		Redefinition of the intializePage function.
		Initialise the page with a recap of the calibration.
	*/
	virtual void initializePage() override;
};
#endif //QCONCLUSION_PAGE_H