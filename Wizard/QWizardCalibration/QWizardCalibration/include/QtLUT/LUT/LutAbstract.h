/*! \class LutAbstract
*
*	\brief Basic template for a Look Up Table.
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef LUT_ABSTRACT_H
#define LUT_ABSTRACT_H

#include <vector>
#include "Point.h"
#include <algorithm>

class LutAbstract
{
    public:

		/**
			Enum class representing the state of extrapolation possibility.
		*/
		enum class InterpolationType {
			Interpolate,
			Extrapolate
		};

		/**
			Enum class representing the errors type handled by the program in the findY function.
		*/
		enum class ErrorType {
			EmptyVector,
			IllegalExtrapolation,
			NoError
		};
		
		// Constructor
        LutAbstract();

		// Default Destructor
        virtual ~LutAbstract() = default;

		// Assignment operator
		LutAbstract& operator=(LutAbstract const & LutAbstract) = default;
        
		// Move Assignement operator
		LutAbstract& operator=(LutAbstract && LutAbstract) = default;

		// Copy constructonr
        LutAbstract(LutAbstract const & LutAbstract) = default;
        
		// Move copy constructor
		LutAbstract(LutAbstract && LutAbstract) = default;

		/**	
			Calculate the Y coordinate value of a given X value based on the accumulated points in the LUT.

			@see Point.h
			@param x X coordonate value.
			@return y Y coordinate value.
		*/
		virtual double y(double x) const { return 0; }
        
		/**
			Addition assignment operator
			Adds a point to the LUT's array
		
			@see Point.h
			@param p A point.
		*/
        LutAbstract& operator+=(Point& p);

		/**
			Move Addition assignment operator
			Adds a point to the LUT's array

			@see Point.h
			@param p A point.
		*/
		LutAbstract& operator+=(Point&& p);

		/**
			Add a point to the LUT's array

			@see Point.h
			@param element a point.
		*/
        virtual void add(Point const & element);
        
		/**
			Add a series of points to the LUT's array

			@see Point.h
			@param elements A pointer to a list of points.
			@param n The number of points to add.
		*/
		virtual void add(Point const * elements, int n);

		/**
			Return the content of the vector mElements which contains a serie of point

			@see Point.h
		*/
		std::vector<Point> & elements() { return mElements; }

		/**
			Set the vector of points (mElements) of this object to the one sent in parameter

			@see Point.h
			@param elements The new array of point
		*/
		void setElements(std::vector<Point> const & elements);

		/**
			Set the state of extrapolation

			@see InterpolationType enum class
			@param interpolation The new interpolation method.
		*/
		void setInterpolationType(InterpolationType interpolation) { mInterpolationType = interpolation; }


		/**
			Get the current state of extrapolation acceptance

			@see InterpolationType enum class
		*/
		InterpolationType interpolationType() const {return mInterpolationType;}

		/**
			Check the presence of various error that may occur when searching for a Y of a certain X value
			which could affect the process.
			Return the first error it find or noError if no errors are found.

			@param x The x value for which we're looking it's associated Y value
		*/
		ErrorType checkErrors(double x) const;



    protected:

		//list of points to store
        std::vector<Point> mElements;

		//extrapolation state
        InterpolationType mInterpolationType;

};

#endif // LUT_ABSTRACT_H
