/*! \class LutWithParameters
*
*	\brief Basic template for a Look Up Table using a function.
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef LUT_WITHPARAMETERS_H
#define LUT_WITHPARAMETERS_H

#include "LutAbstract.h"

class LutWithParameters : public LutAbstract
{
    public:

		/// Constructor
        LutWithParameters();

		/// Desctuctor
        virtual ~LutWithParameters() = default;

		/**
			Calculate the sum of the x value stored in the LUT.

			@see Point.h
		*/
		double sumX() const;

		/**
			Calculate the sum of the square of the x value stored in the LUT.

			@see Point.h
		*/
		double sumXSquared() const;
		
		/**
			Calculate the sum of the x value times the y valye stored in the LUT.

			@see Point.h
		*/
		double sumXY() const;

		/**
			Calculatethe sum of the y value stored in the LUT

			@see Point.h
		*/
		double sumY() const;
		
		
		/**
			Add a point to the LUT's array, then find the parameter of the function based on the new  LUT's array

			@see LutAbstract::add()
			@see Point.h
			@param element A point.
		*/
		void add(Point const & element) override;

		/**
			Add a series of points to the LUT's array, then find the parameter of the function based on the new  LUT's array

			@see Point.h
			@see LutAbstract::add()
			@param elements A pointer to a list of points.
			@param n The number of points to add.
		*/
		void add(Point const * elements, int n) override;

		/**
			Returns the value of the parameter 'a' of the function
		*/
		double a() const { return mA; }

		/**
			Returns the value of the parameter 'b' of the function
		*/
		double b() const {return mB;}

		/**
			Finds the parameter of the current function based on an algorithm defined in the child object
		*/
		virtual void findParameters() {};

    protected:
		///value of the parameter 'a' and 'b' in any kind of function
		double mA, mB;
};

#endif // LUT_WITHPARAMETERS_H
