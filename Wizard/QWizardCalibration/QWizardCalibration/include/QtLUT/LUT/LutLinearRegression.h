	/*! \class LutLinearRegression
*
*	\brief Basic template for a Look Up Table using a linear equation based on a linear regression.
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef LUT_LINEAR_REGRESSION_H
#define LUT_LINEAR_REGRESSION_H

#include "LutWithParameters.h"

class LutLinearRegression : public LutWithParameters
{
public:

	/// Constructor
	LutLinearRegression();

	/// Desctuctor
	~LutLinearRegression() = default;

	/**
		Calculate the Y coordinate value of a given X value based on the accumulated points in the LUT.

		@see Point.h
		@param x X coordonate value.
		@return y Y coordinate value.
	*/
	double y(double x) const override;

	/**
		Redefinition of LutWithParameters::findParameters
		Finds the parameter of the current function based on the algorithm :
			mA = ( _sumY * _sumXSquared - _sumX * _sumXY) / (size * _sumXSquared - _sumXSquared);
			mB = ( size * _sumXY - _sumX * _sumY) / (size * _sumXSquared - _sumX * _sumX);
	*/
	void findParameters() override;

};

#endif // LUT_LINEAR_REGRESSION_H
