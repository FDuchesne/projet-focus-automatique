/*! \class DTO
*
*	\brief Transfer data between the DAO and the program.
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef DTO_H
#define DTO_H

#include "../../LUT/Point.h"

#include <vector>

class DTO
{
public:
	/**
		Default Constructor.
	*/
	DTO();

	DTO::DTO(std::vector<Point> vector);

	/**
		Constructor.
		Initialise the vector with the specified vector.

		@param vector The vector used in initialisation
	*/
	DTO::DTO(std::vector<Point> vector, double paramA, double paramB);

	//Default destructor
	~DTO() = default;

	///Set the vector value
	void setVector(std::vector<Point> vector) { mVector = vector; }
	void setParamA(double paramA) { mParamA = paramA; }
	void setParamB(double paramB) { mParamB = paramB; }

	///Return the vector
	std::vector<Point> vector() const { return mVector; }
	double paramA() const {	return mParamA;}
	double paramB() const {	return mParamB;}

private:
	///The currently stored vector
	std::vector<Point> mVector;
	double mParamA, mParamB;
};

#endif // DTO_H
