/*! \class LutLogarithme
*
*	\brief Basic template for a Look Up Table using a logarithm equation.
*
*	The logarithm equation a and b value are not calculated by the program.
*	Use any programs that calculate a logarithm equations and put the a and b value of the equation at the end of the calibration csv file like so:
*	A,aValue,
*	B,bValue,
*
*	Add a carriage return after the bValue for the DAO to successfully be able to read the file.
*
*	@author Hugo Turgeon-Nozaki
*	@version 0.2 03/07/2018
*/

#ifndef LUT_LOGARITHME_H
#define LUT_LOGARITHME_H

#include "LutWithParameters.h"

class LutLogarithme : public LutWithParameters
{
public:

	/// Constructor
	LutLogarithme(std::string filePath);

	/// Desctuctor
	~LutLogarithme() = default;

	/**
	Calculate the Y coordinate value of a given X value based on the accumulated points in the LUT.

		@see Point.h
		@param x X coordonate value.
		@return y Y coordinate value.
	*/
	double y(double x) const override;

	/**
		Redefinition of LutWithParameters::findParameters
		Reads the parameter of the function in the csv file.
		The csv file most have a A and B value for good result.		
	*/
	void findParameters() override;

private:
	/**
		The path of the file from where the logarithm function will get it's A and B parameter values.
	*/
	std::string mFilePath;

};

#endif // LUT_LOGARITHME_H
