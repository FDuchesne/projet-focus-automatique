/*! \class LutEchelon
*
*	\brief Look Up Table using a tier algorithm.
*
*	@author Lee-Stenio Nazer, Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.2 03/07/2018
*/

#ifndef LUT_ECHELON_H
#define LUT_ECHELON_H

#include "LutWithoutFunction.h"

class LutEchelon : public LutWithoutFunction
{
    public:

		/**
			Flag enum class.x
			Sets whether the returned Y coordinate is equivalent to do y value of the closest point before or after the X value.

			@see Point.h
		*/
		enum class StepCalculationMethod
		{
			Prev,
			Next
		};

		/**
			Constructor
			If no StepCalculationMethod is specified, sets the StepCalculationMethod to Next

			@see StepCalculationMethod enum class
			@param method The lut StepCalculationMethod.
		*/
		LutEchelon(StepCalculationMethod method = StepCalculationMethod::Next);
		
		// Default Destructor
        ~LutEchelon() = default;

		//Default Copy Constructor
		LutEchelon(LutEchelon const & LutEchelon) = default;

		/**
			Calculate the Y coordinate value of a given X value based on the accumulated points in the LUT.
			Uses a tier based algorithm to determine the Y value. The Y value is equal to the Y value of either of the 2 point in the array closest to it.

			@see Point.h
			@see InterpolationType enum class
			@param x X coordonate value.
			@return y Y coordinate value.
		*/
        double y(double x) const override;


		/**
			Sets the flag StepCalculationMethod which determine whether the previous or the next closest value is returned when requesting a Y value.

			@see Point.h
			@see StepCalculationMethod enum class
			@param method The new stepCalculationMethod
		*/
		void setStepCalculationMethod(StepCalculationMethod method) { mStepCalculationMethod = method; }

		/**
			Get the current StepCalculationMethod

			@see StepCalculationMethod enum class
		*/
		StepCalculationMethod stepCalculationMethod() const { return mStepCalculationMethod; }

    protected:
   
		//The object current StepCalculationMethod
		StepCalculationMethod mStepCalculationMethod;

};

#endif // LUT_ECHELON_H
