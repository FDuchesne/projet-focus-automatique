/*! \class QLutWidget
*
*	\brief Main widget of the QLUT GUI Application
*
*
*	@see LutEchelon, LutLinear, LutLinearRegression
*	@author Hugo Turgeon-Nozaki, Simon Prud'Homme
*	@version 0.1 03/07/2018
*/

#ifndef Q_LUTWIDGET_H
#define Q_LUTWIDGET_H

#include <QtCharts/QChart>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QChartView>
#include <QtCharts/QCategoryAxis>
#include <QtCharts/QLineSeries>

class LutAbstract;
class QWidget;
class QLabel;
class QTableWidget;
class QVBoxLayout;
class QHBoxLayout;
class QRadioButton;
class QGroupBox;
class QErrorMessage;
class QLineEdit;
class QComboBox;
class QCustomLineEdit;

using namespace QtCharts;

class QLutWidget : public QWidget
{
	Q_OBJECT

public:
	/**
		Default constructor

		@param parent Parent of this widget
		@param lut Pointer to the main lut stored in the QLut
	*/
	QLutWidget( LutAbstract ** lut = nullptr, QWidget *parent = nullptr);

	///Destructor
	~QLutWidget();

	///Shows error message in a dialog box
	QErrorMessage * mErrorMessage;

	/**
		Draw a chart based on the algorithm used to determine the Y value.

		@see LutLinear
		@see LutLinearRegression
		@see LutEchelon
	*/
	virtual void drawChart();

	/**
		Fill the table widget that contains all the points stored in the currently used lut.
	*/
	virtual void fillDataTable();

	/**
		Sets the lut of the widget to the specified lut and reDraw the whole widget accordingly.

		@param lut The new lut for the widget
	*/
	void setLut(LutAbstract **lut);

	///Returns the content of the findYResultLabel
	QString findYResultLabel();

	///Sets the value of the mFindYValue Label
	void setFindYValue(double value);

	///Returns if there was an error during the last findY
	bool errorDuringFindY() { return mErrorDuringFindY; }

	///Sets the file path from where the lutWidget will get it's data
	void setFilePath(QString filePath) { mFilePath = filePath; }

	///Returns the file path from where the lutWidget is getting it's data
	QString filePath() {return mFilePath;}


protected:
	/**
		Pointer to the main lut stored in the QLut MainWindow
		Can take different forms (Currently supported forms: step, linear, linear regression)

		@see LutLinear
		@see LutLinearRegression
		@see LutEchelon
	*/
	LutAbstract ** mLut;

	//Graphical interface member
	QVBoxLayout  *mLeftLayout,*mRightLayout;
	QHBoxLayout *mTableLayout;
	QLabel *mFindYResultLabel;
	QRadioButton * mExtrapolate, * mInterpolateOnly;
	QLineEdit *mFindYValue;
	QTableWidget * mDataTable;
	QComboBox * mSelection;
	QChart * mChart;
	QChartView * mChartView;
	QLineSeries * mLineSeries;
	QScatterSeries * mScatterSeries;

	///Axis minimum and maximum height
	double mYMin, mYMax, mXMin, mXMax, mSpaceFromAxis;
	QString mFilePath;

	QTableWidget * mFormulaInformation;
	QRadioButton * mPrev, *mNext;
	QGroupBox * mPrevNextBox;
	QHBoxLayout *mPrevNextBoxLayout;

	bool mErrorDuringFindY;
	
	/**
		Change the interpolation method of the mLut based on the selected QRadioButton.

		@see LutAbstract::InterpolationType enum class
	*/
	void changeInterpolation();

	/**
		Check the current interpolation method of the mLut and change the sekected QRadioButton based on it.

		@see LutAbstract::InterpolationType enum class
	*/
	void checkCurrentInterpolation();

	/**
		Fill the table widgets that contains the parameters of the function used by a lut with function to determine a y value for a given x value.
	*/
	void fillParametersTable();

	/**
		Change the stepCalculationMethod used by the lut echelon.

		@see LutEchelon::StepCalculationMethod
	*/
	void changePrevNext();


signals:
	///Signals emited when a change occurs in the widget
	void dataChanged();

public slots:
	/**
		Change the type of the mLut based on the function currently needed to find any given Y value.

		@see LutLinear
		@see LutLinearRegression
		@see LutEchelon
	*/
	virtual void changeFunction();

	/**
		Find the Y value of a given X based on the current type of the mLut and show the result in a QLabel

		@see LutLinear
		@see LutLinearRegression
		@see LutEchelon
		@param x X coordinate value
	*/
	virtual void findY(double x);

	/**
		Fit the chart axis to the new array of data.
	*/
	void fitAxisToNewData();

	/**
		Redraw the chart when a new file is opened.

		@see LutLinear
		@see LutLinearRegression
		@see LutEchelon
	*/
	void loadNewChart();
	
	/**
		Update the lut data when modfied in the data table.
		Determine whether or not the axis value most be changed.
		If the currently selected lut is a lut with function, find it's new parameters.
		Redraw the chart based on the new data.
	*/
	virtual void updateLUTData(int mRow, int mCol);

	/**
		Add a value to the current in use lut
	*/
	void addNewData(double rangeFinderValue, double focuseurValue, int dataRow);
};

#endif // Q_INTERFACE_H

