/*! \class QCustomLineEdit
*
*	\brief A Custom LineEdit that react on focus and enter being pressed
*
*	Enter pressed : will emit the findY Signal of QLutWidget
*	On focus : will erase previously stored information in the LineEdit
*
*	@author Hugo Turgeon-Nozaki
*	@version 0.1 03/07/2018
*/

#ifndef Q_CUSTOM_LINE_EDIT_H
#define Q_CUSTOM_LINE_EDIT_H

#include <QObject>
#include <QLineEdit>

class QCustomLineEdit : public QLineEdit
{
public:
	/**
		Constructor

		Sets the parent of the widget to the given parent.
		Sets the text of the current LineEdit to the specified text, "" if nothing is specified.
		By default, create a double validator except if specified not to.
	*/
	QCustomLineEdit(QWidget * parent, QString text = "", bool hasDoubleValidator = true);

	///Default destructor
	~QCustomLineEdit();
protected:
	/**
		Redefinition of focusInEvent so that the text previously stored in the LineEdit is deleted.
	*/
	virtual void focusInEvent(QFocusEvent *e) override;

	/**
		Redefinition of the keyPressEvent so that on enter press, it emit the QLutWidget::findY signal.
	*/
	virtual void keyPressEvent(QKeyEvent *e) override;
};

#endif //Q_CUSTOM_LINE_EDIT_H