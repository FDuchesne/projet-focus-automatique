/********************************************************************************
** Form generated from reading UI file 'QWizardCalibration.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QWIZARDCALIBRATION_H
#define UI_QWIZARDCALIBRATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QWizardCalibrationClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *QWizardCalibrationClass)
    {
        if (QWizardCalibrationClass->objectName().isEmpty())
            QWizardCalibrationClass->setObjectName(QStringLiteral("QWizardCalibrationClass"));
        QWizardCalibrationClass->resize(600, 400);
        menuBar = new QMenuBar(QWizardCalibrationClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        QWizardCalibrationClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(QWizardCalibrationClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        QWizardCalibrationClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(QWizardCalibrationClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        QWizardCalibrationClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(QWizardCalibrationClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        QWizardCalibrationClass->setStatusBar(statusBar);

        retranslateUi(QWizardCalibrationClass);

        QMetaObject::connectSlotsByName(QWizardCalibrationClass);
    } // setupUi

    void retranslateUi(QMainWindow *QWizardCalibrationClass)
    {
        QWizardCalibrationClass->setWindowTitle(QApplication::translate("QWizardCalibrationClass", "QWizardCalibration", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QWizardCalibrationClass: public Ui_QWizardCalibrationClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QWIZARDCALIBRATION_H
