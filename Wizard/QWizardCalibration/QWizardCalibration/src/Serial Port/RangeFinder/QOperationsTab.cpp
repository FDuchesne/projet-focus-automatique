#include "..\..\..\include\Serial Port\RangeFinder\QOperationsTab.h"


QOperationsTab::QOperationsTab(QRangeFinderAR2000 &rangeFinder, QWidget * parent)
	: QWidget(parent), mRangeFinder{ &rangeFinder }
{
	mQvbl = new QVBoxLayout;
	setLayout(mQvbl);

	mStats = new QTabWidget;

	mStatsBox = new QGroupBox("Statistics");
	mStatsBoxLayout = new QVBoxLayout;
	mStatsBox->setLayout(mStatsBoxLayout);
	mDistanceStats = new QStatisticsWidget;
	mSignalStats = new QStatisticsWidget;
	mTemperatureStats = new QStatisticsWidget;
	mStats->addTab(mDistanceStats, "Distance");
	mStats->addTab(mSignalStats, "Signal Quality");
	mStats->addTab(mTemperatureStats, "Temperature");
	mStatsBoxLayout->addWidget(mStats);

	mResetStats = new QPushButton("Reset All");
	mResetLayout = new QHBoxLayout;
	mResetLayout->addStretch();
	mResetLayout->addWidget(mResetStats);
	mStatsBoxLayout->addLayout(mResetLayout);

	mOutputBox = new QGroupBox("Output");
	mOutputBoxLayout = new QVBoxLayout;
	mOutputBox->setLayout(mOutputBoxLayout);
	mOperations = new QOperationsOutputWidget(mRangeFinder);
	mOutputBoxLayout->addWidget(mOperations);

	mOperBox = new QGroupBox("Operations");
	mOperBoxLayout = new QVBoxLayout;
	mOperBox->setLayout(mOperBoxLayout);
	mControl = new QOperationsControlWidget(mRangeFinder);
	mOperBoxLayout->addWidget(mControl);

	mQvbl->addWidget(mOperBox);
	mQvbl->addWidget(mOutputBox);
	mQvbl->addWidget(mStatsBox);


	connect(mResetStats, &QPushButton::clicked, this, &QOperationsTab::resetStats);
	connect(&mRangeFinder->mDistanceStats, &QMovingStatistics::updated, mDistanceStats, &QStatisticsWidget::showStats);
	connect(&mRangeFinder->mSignalQualityStats, &QMovingStatistics::updated, mSignalStats, &QStatisticsWidget::showStats);
	connect(&mRangeFinder->mTemperatureStats, &QMovingStatistics::updated, mTemperatureStats, &QStatisticsWidget::showStats);
	connect(mDistanceStats, &QStatisticsWidget::resetPressed, &mRangeFinder->mDistanceStats, &QMovingStatistics::reset);
	connect(mSignalStats, &QStatisticsWidget::resetPressed, &mRangeFinder->mSignalQualityStats, &QMovingStatistics::reset);
	connect(mTemperatureStats, &QStatisticsWidget::resetPressed, &mRangeFinder->mTemperatureStats, &QMovingStatistics::reset);
	//connect(&mRangeFinder->mDistanceStats, &QMovingStatistics::updated, mDistanceStats, &QStatisticsWidget::updateProgBar);
	//connect(&mRangeFinder->mSignalQualityStats, &QMovingStatistics::updated, mSignalStats, &QStatisticsWidget::updateProgBar);
	//connect(&mRangeFinder->mTemperatureStats, &QMovingStatistics::updated, mTemperatureStats, &QStatisticsWidget::updateProgBar);
}

QOperationsTab::~QOperationsTab() {
}

void QOperationsTab::resetStats() {
	mRangeFinder->mDistanceStats.reset();
	mRangeFinder->mSignalQualityStats.reset();
	mRangeFinder->mTemperatureStats.reset();
}
