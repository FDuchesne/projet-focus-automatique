﻿#include "..\..\..\include\Serial Port\RangeFinder\QRFStatusBar.h"
#include "..\..\..\include\Serial Port\Global\QLEDLight.h"
#include "..\..\..\include\Serial Port\RangeFinder\QRangeFinderAR2000.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QICon>
#include <QTimer>

QRFStatusBar::QRFStatusBar(QRangeFinderAR2000 * rangeFinder, QWidget * parent)
	:	QStatusBar(parent),
		mRangeFinder{ rangeFinder }
{
	mLaser = new QLEDLight(QSize(24, 24));
	mTracking = new QLEDLight(QSize(24, 24));
	mConnection = new QLEDLight(QSize(24, 24));

	mLaser->addStatus(0, "Laser off", QIcon(":/QSerialComm/redLed"));
	mLaser->addStatus(1, "Laser on", QIcon(":/QSerialComm/greenLed"));
	mLaser->addStatus(2, "Development Mode", QIcon(":/QSerialComm/greyLed"));
	mLaser->setStatus(0);

	mTracking->addStatus(0, "Tracking off", QIcon(":/QSerialComm/redLed"));
	mTracking->addStatus(1, "Tracking on", QIcon(":/QSerialComm/greenLed"));
	mTracking->addStatus(2, "Development Mode", QIcon(":/QSerialComm/greyLed"));
	mTracking->setStatus(0);

	mConnection->addStatus(0, "Not Connected", QIcon(":/QSerialComm/redLed"));
	mConnection->addStatus(1, "Connected", QIcon(":/QSerialComm/greenLed"));
	mConnection->setStatus(0);

	addPermanentWidget(mConnection);
	addPermanentWidget(mTracking);
	addPermanentWidget(mLaser);

	connect(&mRangeFinder->mSerial, &QAsyncSerialPort::connectionUpdated, this, &QRFStatusBar::updateConnectionLED);
	connect(mRangeFinder, &QRangeFinderAR2000::trackingChanged, this, &QRFStatusBar::updateTrackingLED);
	connect(mRangeFinder, &QRangeFinderAR2000::laserChanged, this, &QRFStatusBar::updateLaserLED);
	connect(mRangeFinder, &QRangeFinderAR2000::messageReceived, this, &QRFStatusBar::updateMsg);
	connect(mRangeFinder, &QRangeFinderAR2000::matchingResponseReceived, this, &QRFStatusBar::updateMsg);
	connect(&mRangeFinder->mSerial, &QAsyncSerialPort::updated, this, &QRFStatusBar::updateMsg);
	connect(&mRangeFinder->mSerial, &QCommandSerialPort::developmentModeSwitched, this, &QRFStatusBar::updateDevMode);
}

QRFStatusBar::~QRFStatusBar() {

}

void QRFStatusBar::updateTrackingLED(bool isTracking) {
	if (isTracking) {
		mTracking->setStatus(1);
	}
	else {
		mTracking->setStatus(0);
	}
}

void QRFStatusBar::updateLaserLED(bool laserOn) {
	if (laserOn) {
		mLaser->setStatus(1);
	}
	else {
		mLaser->setStatus(0);
	}
}

void QRFStatusBar::updateConnectionLED(bool connected) {
	if (connected) {
		mConnection->setStatus(1);
	}
	else {
		mConnection->setStatus(0);
	}
}

void QRFStatusBar::updateMsg(QString message) {
	showMessage(message, 5000);
}

void QRFStatusBar::updateDevMode(bool devMode) {
	if (devMode) {
		mLaser->setStatus(2);
		mTracking->setStatus(2);
	}
	else {
		mLaser->setStatus(0);
		mTracking->setStatus(0);
	}
}
