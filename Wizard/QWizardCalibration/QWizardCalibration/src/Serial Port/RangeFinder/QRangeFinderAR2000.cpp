#include "..\..\..\include\Serial Port\RangeFinder\QRangeFinderAR2000.h"

#include <QCoreApplication>
#include <QDebug>
#include <Windows.h>
#include <QTime>
#include <QPair>
#include <QVariant>
#include <QFileInfo>
#include <QDir>
#include "..\..\..\include\Serial Port\RangeFinder\QRangeFinderSettings.h"


QString const QRangeFinderAR2000::SEPARATOR		{ " " };
QString const QRangeFinderAR2000::TERMINATOR	{ "\r\n" };
QString const QRangeFinderAR2000::DEFAULT_INI	{ "defaultSettings" };
QString const QRangeFinderAR2000::CURRENT_INI	{ "currentSettings" };
QString const QRangeFinderAR2000::TEMP_INI		{ "tempSettings" };


QRangeFinderAR2000::QRangeFinderAR2000()
	:	mDistanceStats{ 1000, QRegularExpression("(\\d{3}).(\\d{4})") },
		mSignalQualityStats{ 1000, QRegularExpression("(\\d{5})") },
		mTemperatureStats{ 1000, QRegularExpression(",(\\d{5})\\r\\n") },
		mDeviceConnected{ false },
		mIsTracking{ false },
		mLaserOn{ false }
{
	fillDictionary();
	fillDeviceMessages();
	mSerial.setDeviceMessages(mDeviceMessages.keys(), TERMINATOR);

	initSettings();

	mTestCommand = *mSerialCommands[Command::StartContinuousTracking];
	mTestCommand2 = *mSerialCommands[Command::StartDistanceTracking];
	mTestCommand3 = *mSerialCommands[Command::StopTracking];
	mTestCommand4 = *mSerialCommands[Command::ShowDeviceID];
	mTestCommand5 = *mSerialCommands[Command::SetOutputFormat];
	
	connect(&mSerial, &QCommandSerialPort::responseMatchesCommand, this, &QRangeFinderAR2000::handleMatchingResponse);
	connect(&mSerial, &QCommandSerialPort::messageReceived, this, &QRangeFinderAR2000::handleMessageReceived);
	connect(&mSerial, &QAsyncSerialPort::connectionUpdated, this, &QRangeFinderAR2000::connectionUpdated);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Slots
///////////////////////////////////////////////////////////////////////////////////////////////


void QRangeFinderAR2000::handleTrackingRequest(TrackingMode trackingMode) {
	switch (static_cast<int>(trackingMode)) {
	case static_cast<int>(TrackingMode::None) :
		sendCommandNoParams(Command::StopTracking);
		break;
	case static_cast<int>(TrackingMode::DistanceTracking) :
		sendCommandNoParams(Command::StartDistanceTracking);
		break;
	case static_cast<int>(TrackingMode::ContinuousTracking) :
		sendCommandNoParams(Command::StartContinuousTracking);
		break;
	case static_cast<int>(TrackingMode::PreciseMeasurement) :
		sendCommandNoParams(Command::DoSingleMeasurement);
		break;
	}
}

void QRangeFinderAR2000::handleMatchingResponse(const QByteArray &response, const QSerialCommand &command) {
	QString sResponse(response);
	//qDebug() << command.command() << " : " << response;
	// Tracking
	if (command.operationMode().fluxMode() == SerialOperationMode::FluxMode::Push || command == *mSerialCommands[Command::DoSingleMeasurement]) {
		// Push
		if (command.operationMode().fluxMode() == SerialOperationMode::FluxMode::Push) {
			emit trackingChanged(true);
			mIsTracking = true;
			if (command == *mSerialCommands[Command::StartContinuousTracking]) {
				mTrackingStatus = TrackingMode::ContinuousTracking;
			}
			else {
				mTrackingStatus = TrackingMode::DistanceTracking;
			}
			if ( ! mLaserOn) {
				emit laserChanged(true);
				mLaserOn = true;
			}
		}
		// Pull
		else if (mLaserOn) {
			emit laserChanged(false);
			mLaserOn = false;
		}
		QStringList measurement = sResponse.split(",");
		double distance = measurement[0].remove(0, 1).toDouble();
		double quality = measurement[1].toDouble();
		double temperature = measurement[2].left(5).toDouble();
		emit measurementReceived(distance, quality, temperature);

		mDistanceStats.addValue(distance);
		mSignalQualityStats.addValue(quality);
		mTemperatureStats.addValue(temperature);

		//qDebug() << "		Average : " << mDistanceStats.average();
		//qDebug() << "		Standard Deviation : " << mDistanceStats.stdDeviation();
		//qDebug() << "		Signal Quality Average : " << mSignalQualityStats.average();
		//qDebug() << "		Temperature Average : " << mTemperatureStats.average();
	}
	else {
		emit matchingResponseReceived(command.command() + " : " + response);
		// Stop tracking
		if (command == *mSerialCommands[Command::StopTracking]) {
			emit trackingChanged(false);
			mIsTracking = false;
			mTrackingStatus = TrackingMode::None;
			if (mLaserOn) {
				emit laserChanged(false);
				mLaserOn = false;
			}
		}
		// Laser on
		else if (command == *mSerialCommands[Command::ActivateLaser]) {
			emit laserChanged(true);
			mLaserOn = true;
		}
		// Laser off
		else if (command == *mSerialCommands[Command::DeactivateLaser]) {
			emit laserChanged(false);
			mLaserOn = false;
			if (mIsTracking) {
				emit trackingChanged(false);
				mIsTracking = false;
			}
		}
	}
}

void QRangeFinderAR2000::handleMessageReceived(QString const &message) {
	emit messageReceived(mDeviceMessages[message]);
	//qDebug() << "Message from device : " << mDeviceMessages[message];
}

void QRangeFinderAR2000::resetStats() {
	mDistanceStats.reset();
	mSignalQualityStats.reset();
	mTemperatureStats.reset();
}




//////////////////////////////////////////////////////////////////////////////////////////////
// Methods
//////////////////////////////////////////////////////////////////////////////////////////////

void QRangeFinderAR2000::testCommand() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QRangeFinderAR2000::testCommand2() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand2, QList<QVariant>());
	mSerial.writeToBuffer(command);					
}													
void QRangeFinderAR2000::testCommand3() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand3, QList<QVariant>());
	mSerial.writeToBuffer(command);					
}													
void QRangeFinderAR2000::testCommand4() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand4, QList<QVariant>());
	mSerial.writeToBuffer(command);					
}													
void QRangeFinderAR2000::testCommand5() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand5, QList<QVariant>());
	mSerial.writeToBuffer(command);
}

void QRangeFinderAR2000::connectSerialPort() {
	mSerial.openSerialPort(mSettings.mPortName, mSettings.mBaudRate, mSettings.mDataBits, mSettings.mParity, mSettings.mStopBits, mSettings.mFlowControl);
}

void QRangeFinderAR2000::connectionUpdated(bool connected) {
	if (connected) {
		applySettings();
		mDeviceConnected = true;
	}
	else {
		mDeviceConnected = false;
	}
}

/*! Affectation de mSettings à partir d'un fichier ini sauvegardé. Si aucun n'est sauvegardé, c'est ici qu'on en crée un avec des paramètres par défaut.
*/
void QRangeFinderAR2000::initSettings() {
	if (fileExists(CURRENT_INI)) {
		mSettings.load(CURRENT_INI);
	}
	else {
		// Default Settings
		mSettings.mPortName = "COM3";
		mSettings.mBaudRate = QAsyncSerialPort::BaudRate::BR115200;
		mSettings.mDataBits = QSerialPort::DataBits::Data8;
		mSettings.mStopBits = QSerialPort::StopBits::OneStop;
		mSettings.mParity = QSerialPort::Parity::NoParity;
		mSettings.mFlowControl = QSerialPort::FlowControl::NoFlowControl;
		mSettings.mAverage = 1;
		mSettings.mFrequency = 10;
		mSettings.mOffset = 0;
		mSettings.mWindowMin = -500;
		mSettings.mWindowMax = 500;
		mSettings.mLaserOnStart = false;

		if (!fileExists(DEFAULT_INI)) {
			mSettings.save(DEFAULT_INI);
		}
	}
}

/*! Appelle des méthodes de paramétrage du télémètre en lien avec les variables membres de l'instance mSettings.
*/
void QRangeFinderAR2000::applySettings() {
	//for (int i = 0; i < 100; ++i) {
	//	setAverage(mSettings.mAverage);
	//}
	setBaudRate(mSettings.mBaudRate);
	setStopBits(mSettings.mStopBits);
	setAverage(mSettings.mAverage);
	setMeasuringFrequency(mSettings.mFrequency);
	setOffset(mSettings.mOffset * 10000.0);
	setMeasuringWindow(mSettings.mWindowMin * 10000.0, mSettings.mWindowMax * 10000.0);
	setLaserOn(mSettings.mLaserOnStart);

	 //The program is based on this setup, do not change it!!!
	setMeasuringUnit("m");			// !
	setOutputFormat(1, 1, 1, 0);	// !
	setAutoStart(1);  // Shows the id of the device

	mIsTracking = false;
	mLaserOn = false;
}

void QRangeFinderAR2000::sendCommandNoParams(Command command) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[command], QList<QVariant>());
	mSerial.writeToBuffer(commandAndParams);
}

void QRangeFinderAR2000::startPreciseMeasurement(QString sa) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::DoSingleMeasurement], QList<QVariant>{ sa });
	mSerial.writeToBuffer(commandAndParams);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Set
///////////////////////////////////////////////////////////////////////////////////////////////

void QRangeFinderAR2000::setLaserOn(bool on) {
	mSerial.setDevelopmentMode(false);
	if (on) {
		QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::ActivateLaser], QList<QVariant>());
		mSerial.writeToBuffer(commandAndParams);
	}
	else {
		QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::DeactivateLaser], QList<QVariant>());
		mSerial.writeToBuffer(commandAndParams);
	}
}

void QRangeFinderAR2000::setAverage(int average) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::SetAverage], QList<QVariant>{ average });
	mSerial.writeToBuffer(commandAndParams);
}

void QRangeFinderAR2000::setMeasuringFrequency(double frequency) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::SetFrequency], QList<QVariant>{ frequency });
	mSerial.writeToBuffer(commandAndParams);
}

void QRangeFinderAR2000::setMeasuringWindow(double start, double end) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::SetWindow], QList<QVariant>{ start, end });
	mSerial.writeToBuffer(commandAndParams);
}

void QRangeFinderAR2000::setMeasuringUnit(QString unit) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::SetUnit], QList<QVariant>{ unit });
	mSerial.writeToBuffer(commandAndParams);
}

void QRangeFinderAR2000::setOffset(double meters) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::SetOffset], QList<QVariant>{ meters });
	mSerial.writeToBuffer(commandAndParams);
}

void QRangeFinderAR2000::setOutputFormat(int a, int b, int c, int d) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::SetOutputFormat], QList<QVariant>{ a, b, c, d });
	mSerial.writeToBuffer(commandAndParams);
}

void QRangeFinderAR2000::setAutoStart(int command) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::SetAutoStart], QList<QVariant>{ command });
	mSerial.writeToBuffer(commandAndParams);
}

void QRangeFinderAR2000::setBaudRate(QAsyncSerialPort::BaudRate baudRate) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::SetBaudRate], QList<QVariant>{ static_cast<int>(baudRate) });
	mSerial.writeToBuffer(commandAndParams);
}

void QRangeFinderAR2000::setStopBits(QSerialPort::StopBits stopBits) {
	double sb;
	if (stopBits == QSerialPort::StopBits::OneStop) {
		sb = 1;
	}
	else if (stopBits == QSerialPort::StopBits::OneAndHalfStop) {
		sb = 1.5;
	}
	else if (stopBits == QSerialPort::StopBits::TwoStop) {
		sb = 2;
	}
	else {
		sb = -1;
	}
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[Command::SetStopBits], QList<QVariant>{ sb });
	mSerial.writeToBuffer(commandAndParams);
}



///////////////////////////////////////////////////////////////////////////////////////////////
// Helper Methods
///////////////////////////////////////////////////////////////////////////////////////////////

bool QRangeFinderAR2000::fileExists(QString fileName) {
	QFileInfo check_file(QDir::currentPath() + "/" + fileName + ".ini");
	return (check_file.exists() && check_file.isFile());
}

void QRangeFinderAR2000::fillDictionary() {

	mSerialCommands[Command::ShowDeviceID] = new QSerialCommand( 
		"ID",																// cmd
		"showDeviceID",														// name
		QSerialCommand::IOType::Out,										// IOType
		0,																	// # params
		SerialOperationMode::BlockingMode::Blocking,						// BlockingMode
		SerialOperationMode::FluxMode::Pull,								// FluxMode	
		TERMINATOR,															// eol car
		SEPARATOR,															// separator
		"Device Info",														// family
		"display manufacturer's data",										// short desc
		QRegularExpression("Lumos (\\d{6}) (.+)\\r\\n"));					// response

	mSerialCommands[Command::ShowCommands] = new QSerialCommand( 
		"ID?",
		"showCommands",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Device Info", "overview of all available operations and parameters",
		QRegularExpression("\\r\\n\\r\\nCommand List:((.*)\\r\\n){45}"));

	mSerialCommands[Command::ShowTemperature] = new QSerialCommand(
		"TP",
		"showTemperature",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Device Info",
		"internal temperature in Celsius",
		QRegularExpression("^(\\d+)°C\\r\\n$"));

	mSerialCommands[Command::ShowParams] = new QSerialCommand( 
		"PA",
		"showParams",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Device Info",
		"parameter list with current settings",
		QRegularExpression("Baudrate of serial port((.*)\\r\\n){22}"));

	mSerialCommands[Command::ShowDiodeStatus] = new QSerialCommand( 
		"TLD",
		"showDiodeStatus",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Device Info",
		"prints status of laser diode",
		QRegularExpression("")); //TODO.. cmd not working?

	mSerialCommands[Command::ShowAutoStart] = new QSerialCommand( 
		"AS",
		"showAutoStart",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Device Info",
		"display autostart commands",
		QRegularExpression("Autostart commands .AS.: (.*)\\r\\n"));

	mSerialCommands[Command::ResetParams] = new QSerialCommand( 
		"PR",
		"resetParams",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Device Setup",
		"reset all parameters to factory settings",
		QRegularExpression("Parameters set to firmware defaults((.*)\\r\\n){23}"));

	mSerialCommands[Command::DisplayOff] = new QSerialCommand( 
		"DF",
		"displayOff",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Device Setup",
		"turns off OLED display",
		QRegularExpression("Turning off OLED display. .DF.\\r\\n"));

	mSerialCommands[Command::DisplayOn] = new QSerialCommand( 
		"DN",
		"displayOn",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Device Setup",
		"turns on OLED display",
		QRegularExpression("Turning on OLED display. .DN.\\r\\n"));

	mSerialCommands[Command::SetAutoStart] = new QSerialCommand( 
		"AS",
		"setAutoStart",
		QSerialCommand::IOType::In,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Device Setup",
		"set autostart mode",
		QRegularExpression("Autostart commands .AS.: (.*)\\r\\n"));

	mSerialCommands[Command::SetAverage] = new QSerialCommand( 
		"SA",
		"setAverage",
		QSerialCommand::IOType::In,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Setup",
		"determine the number x of the individual measured values to be averaged for measured value output (1 to 50)",
		QRegularExpression("Average .SA.: ([\\d]|[1-4][\\d]|50)\\r\\n"));

	mSerialCommands[Command::ShowAverage] = new QSerialCommand( 
		"SA",
		"showAverage",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Info",
		"display the number of  individual measured values to be averaged for measured value output",
		QRegularExpression("Average .SA.: ([\\d]|[1-4][\\d]|50)\\r\\n"));

	 mSerialCommands[Command::SetFrequency] = new QSerialCommand( 
		"MF",
		"setFrequency",
		QSerialCommand::IOType::In,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Setup",
		"parameterize the number x of the measured value outputs per second (0.0 to 100.0, 0 = auto)",
		QRegularExpression("Measurement frequency .MF.: ([\\d]|[1-9][\\d]|100).[\\d]\\r\\n"));

	 mSerialCommands[Command::ShowFrequency] = new QSerialCommand( 
		"MF",
		"showFrequency",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Info",
		"display the number x of the measured value outputs per second (0.0 to 100.0, 0 = auto)",
		QRegularExpression("Measurement frequency .MF.: ([\\d]|[1-9][\\d]|100).[\\d]\\r\\n"));

	mSerialCommands[Command::SetWindow] = new QSerialCommand( 
		"MW",
		"setWindow",
		QSerialCommand::IOType::In,
		2,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Setup",
		"parameterize the scope of a measurement window by start x and end y (-500M to 500M)",
		QRegularExpression("Minimum distance from target .MW.: (-?)(\\d+)\\r\\nMaximum distance from target .MW.: (-?)(\\d+)\\r\\n"));

	mSerialCommands[Command::ShowWindow] = new QSerialCommand( 
		"MW",
		"showWindow",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Info",
		"display the scope of a measurement window by start and end  (-500M to 500M)",
		QRegularExpression("Minimum distance from target .MW.: (-?)(\\d+)\\r\\nMaximum distance from target .MW.: (-?)(\\d+)\\r\\n"));

	 mSerialCommands[Command::ShowUnit] = new QSerialCommand( 
		"MUN",
		"showUnit",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Info",
		"display the unit for the output value (mm, cm, dm, m, in/8, in/16, in, ft, yd)",
		QRegularExpression("Unit for the distances .MUN.: (.+)\\r\\n"));

	 mSerialCommands[Command::SetUnit] = new QSerialCommand( 
		"MUN",
		"setUnit",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Setup",
		"set the unit for the output value (mm, cm, dm, m, in/8, in/16, in, ft, yd)",
		QRegularExpression("Unit for the distances .MUN.: (.+)\\r\\n"));

	 mSerialCommands[Command::SetOffset] = new QSerialCommand( 
		"OF",
		"setOffset",
		QSerialCommand::IOType::In,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Setup",
		"determine the user-specific offset x that is added to the measured value",
		QRegularExpression("Offset in .mm . 10. .OF.: (-?)(\\d+)\\r\\n"));

	 mSerialCommands[Command::ShowOffset] = new QSerialCommand( 
		"OF",
		"showOffset",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Info",
		"display the user-specific offset  that is added to the measured value",
		QRegularExpression("Offset in 0.1 mm .100 .m. .OF.: (-?)(\\d+)\\r\\n"));

	mSerialCommands[Command::SetOutputFormat] = new QSerialCommand( 
		"SD",
		"setOutputFormat",
		QSerialCommand::IOType::In,
		4,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Setup",
		"parameterizes the output format and possible output values",
		QRegularExpression("Output format .SD.: [0-5] [0-1] [0-1] [0-1]\\r\\n"));

	mSerialCommands[Command::ShowOutputFormat] = new QSerialCommand( 
		"SD",
		"showOutputFormat",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Info",
		"display the output format and possible output values",
		QRegularExpression("Output format .SD.: [0-5] [0-1] [0-1] [0-1]\\r\\n"));
	
	mSerialCommands[Command::SetBaudRate] = new QSerialCommand( 
		"BR",
		"setBaudRate", QSerialCommand::IOType::In,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Serial Setup",
		"set the baud rate of the serial port",
		QRegularExpression("Baudrate of serial port .BR.: (\\d+)\\r\\n"));

	mSerialCommands[Command::ShowBaudRate] = new QSerialCommand( 
		"BR",
		"showBaudRate",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Serial Info",
		"display the baud rate of the serial port",
		QRegularExpression("Baudrate of serial port .BR.: (\\d+)\\r\\n"));

	mSerialCommands[Command::SetStopBits] = new QSerialCommand( 
		"SB",
		"setStopBits",
		QSerialCommand::IOType::In,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Serial Setup",
		"set the stop bits of the serial port",
		QRegularExpression("Stopbits of serial port .SB.: (\\d+)\\r\\n"));

	mSerialCommands[Command::ShowStopBits] = new QSerialCommand( 
		"SB",
		"showStopBits",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Serial Info",
		"display the stop bits of the serial port",
		QRegularExpression("Stopbits of serial port .SB.: (\\d+)\\r\\n"));

	mSerialCommands[Command::SetDefaultTracking] = new QSerialCommand( 
		"MCT",
		"setDefaultTracking",
		QSerialCommand::IOType::In,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Setup",
		"set default tracking mode, started from the menu (0 standard, 1 continuous)",
		QRegularExpression("Standard tracking mode from menu .MCT.: [0-1]\\r\\n"));

	mSerialCommands[Command::ShowDefaultTracking] = new QSerialCommand( 
		"MCT",
		"showDefaultTracking",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Parameter Info",
		"show default tracking mode, started from the menu (0 standard, 1 continuous)",
		QRegularExpression("Standard tracking mode from menu .MCT.: [0-1]\\r\\n"));

	mSerialCommands[Command::RestartDevice] = new QSerialCommand( 
		"DR",
		"restartDevice",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"restarts the device",
		QRegularExpression(""));

	mSerialCommands[Command::DeactivateLaser] = new QSerialCommand( 
		"LF",
		"deactivateLaser",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"deactivates laser diode",
		QRegularExpression("Laser diode deactivated. .LF.\\r\\n"));

	mSerialCommands[Command::ActivateLaser] = new QSerialCommand( 
		"LN",
		"activateLaser",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"activates laser diode",
		QRegularExpression("Laser diode activated. .LN.\\r\\n"));

	mSerialCommands[Command::StopTracking] = new QSerialCommand( 
		"DT",
		"stopTracking",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"deactivates tracking mode",
		QRegularExpression("Stopping tracking mode.\\r\\n"));

	QSerialCommand * startContinuousTracking = new QSerialCommand( 
		"CT",
		"startContinuousTracking",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Push,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"activates/deactivates continuous tracking mode",
		QRegularExpression("d(\\d{3}).(\\d{4}),(\\d{5}),(\\d{5})\\r\\n"));
	mSerialCommands[Command::StartContinuousTracking] = startContinuousTracking;
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::StopTracking]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::DeactivateLaser]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::ResetParams]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::SetAverage]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::SetFrequency]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::SetWindow]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::SetOffset]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::SetOutputFormat]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::SetBaudRate]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::SetStopBits]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::SetDefaultTracking]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::RestartDevice]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::StartContinuousTracking]);
	startContinuousTracking->addPushModeStopCommand(mSerialCommands[Command::StartDistanceTracking]);

	QSerialCommand * startDistanceTracking =  new QSerialCommand( 
		"DT",
		"startDistanceTracking",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::NonBlockingWithResponse,
		SerialOperationMode::FluxMode::Push,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"activates/deactivates distance tracking mode",
		QRegularExpression("d(\\d{3}).(\\d{4}),(\\d{5}),(\\d{5})\\r\\n")); //manque les autre formats doutput
	mSerialCommands[Command::StartDistanceTracking] = startDistanceTracking;
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::StopTracking]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::DeactivateLaser]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::ResetParams]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::SetAverage]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::SetFrequency]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::SetWindow]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::SetOffset]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::SetOutputFormat]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::SetBaudRate]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::SetStopBits]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::SetDefaultTracking]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::RestartDevice]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::StartContinuousTracking]);
	startDistanceTracking->addPushModeStopCommand(mSerialCommands[Command::StartDistanceTracking]);

	mSerialCommands[Command::DoSingleMeasurement] = new QSerialCommand(
		"DM",
		"doSingleMeasurement",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"does a single measurement, uses %u as average parameter if available",
		QRegularExpression("d(\\d{3}).(\\d{4}),(\\d{5}),(\\d{5})\\r\\n")); //manque les autre formats doutput

	mSerialCommands[Command::EmptyCommand] = new QSerialCommand( 
		"",
		"emptyCommand",
		QSerialCommand::IOType::Out,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"None",
		"command that expects a \"?\" from the device",
		QRegularExpression("?\\r\\n"));
}

void QRangeFinderAR2000::fillDeviceMessages() {

	// Errors
	//-----------------------------------------------------------------------------------
	mDeviceMessages["e1001" + TERMINATOR] = "Unexpected error / hardware problems";
	mDeviceMessages["e1002" + TERMINATOR] = "Unexpected error / hardware problems";
	mDeviceMessages["e1003" + TERMINATOR] = "Unexpected error / hardware problems";
	
	mDeviceMessages["e1101" + TERMINATOR] = "Error in communication with PC";
	mDeviceMessages["e1102" + TERMINATOR] = "Error in communication with PC";
	mDeviceMessages["e1103" + TERMINATOR] = "Laser module error";
	mDeviceMessages["e1104" + TERMINATOR] = "Laser module error";
	mDeviceMessages["e1105" + TERMINATOR] = "Laser module error";
	mDeviceMessages["e1106" + TERMINATOR] = "Hardware error";
	mDeviceMessages["e1107" + TERMINATOR] = "Hardware error";
	mDeviceMessages["e1108" + TERMINATOR] = "Hardware error";
	mDeviceMessages["e1109" + TERMINATOR] = "Hardware error";
	mDeviceMessages["e1110" + TERMINATOR] = "Hardware error";
	mDeviceMessages["e1111" + TERMINATOR] = "Hardware error";
	mDeviceMessages["e1112" + TERMINATOR] = "Hardware error";
	mDeviceMessages["e1113" + TERMINATOR] = "Hardware error";
	
	mDeviceMessages["e1201" + TERMINATOR] = "Measurement impossible / no target";
	mDeviceMessages["e1202" + TERMINATOR] = "Hardware error";
	mDeviceMessages["e1203" + TERMINATOR] = "Target with unsuitable reflectivity";
	mDeviceMessages["e1204" + TERMINATOR] = "Measurement interrupted";
	mDeviceMessages["e1205" + TERMINATOR] = "Measurement still running";
	mDeviceMessages["e1206" + TERMINATOR] = "Target too bright / too much back light";
	mDeviceMessages["e1207" + TERMINATOR] = "Target outside of the measurement window (MW)";
	mDeviceMessages["e1208" + TERMINATOR] = "Incorrect measurement parameterization";
	mDeviceMessages["e1209" + TERMINATOR] = "Hardware error";
	
	// Warnings			   
	//-----------------------------------------------------------------------------------
	mDeviceMessages["w1901" + TERMINATOR] = "Restart being executed";
	mDeviceMessages["w1902" + TERMINATOR] = "Input voltage outside of the specification";
	mDeviceMessages["w1903" + TERMINATOR] = "Input voltage outside of the specification";
	mDeviceMessages["w1904" + TERMINATOR] = "Temperature outside of the specification";
	mDeviceMessages["w1905" + TERMINATOR] = "Temperature outside of the specification";
	mDeviceMessages["w1906" + TERMINATOR] = "Heating active";
	mDeviceMessages["w1910" + TERMINATOR] = "Measurement not completed within predefined period of time";
	mDeviceMessages["w1911" + TERMINATOR] = "Measuring frequency too high";
	mDeviceMessages["w1912" + TERMINATOR] = "Distance changed too much";
}



///////////////////////////////////////////////////////////////////////////////////////////////
// Destructor
///////////////////////////////////////////////////////////////////////////////////////////////

QRangeFinderAR2000::~QRangeFinderAR2000()
{
}
