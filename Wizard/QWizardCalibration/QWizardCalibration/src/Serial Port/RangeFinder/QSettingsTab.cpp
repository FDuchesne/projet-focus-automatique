#include "..\..\..\include\Serial Port\RangeFinder\QSettingsTab.h"
#include "..\..\..\include\Serial Port\RangeFinder\QRangeFinderAR2000.h"
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QMap>
#include <QLineEdit>


const QMap<QAsyncSerialPort::BaudRate, QString> QSettingsTab::mBRtoStr{
	[]()->QMap<QAsyncSerialPort::BaudRate, QString> {
	QMap<QAsyncSerialPort::BaudRate, QString> temp;
	temp[QAsyncSerialPort::BaudRate::BR600] = "600";
	temp[QAsyncSerialPort::BaudRate::BR1200] = "1200";
	temp[QAsyncSerialPort::BaudRate::BR2400] = "2400";
	temp[QAsyncSerialPort::BaudRate::BR4800] = "4800";
	temp[QAsyncSerialPort::BaudRate::BR9600] = "9600";
	temp[QAsyncSerialPort::BaudRate::BR14400] = "14400";
	temp[QAsyncSerialPort::BaudRate::BR19200] = "19200";
	temp[QAsyncSerialPort::BaudRate::BR28800] = "28800";
	temp[QAsyncSerialPort::BaudRate::BR38400] = "38400";
	temp[QAsyncSerialPort::BaudRate::BR56000] = "56000";
	temp[QAsyncSerialPort::BaudRate::BR57600] = "57600";
	temp[QAsyncSerialPort::BaudRate::BR115200] = "115200";
	temp[QAsyncSerialPort::BaudRate::BR128000] = "128000";
	temp[QAsyncSerialPort::BaudRate::BR230400] = "230400";
	temp[QAsyncSerialPort::BaudRate::BR256000] = "256000";
	temp[QAsyncSerialPort::BaudRate::BRUnknown] = "Unknown BaudRate";
	return temp;
}() };
const QMap <QString, QAsyncSerialPort::BaudRate> QSettingsTab::mStrtoBR{
	[]()->QMap<QString, QAsyncSerialPort::BaudRate> {
	QMap<QString, QAsyncSerialPort::BaudRate> temp;
	temp["600"] = QAsyncSerialPort::BaudRate::BR600;
	temp["1200"] = QAsyncSerialPort::BaudRate::BR1200;
	temp["2400"] = QAsyncSerialPort::BaudRate::BR2400;
	temp["4800"] = QAsyncSerialPort::BaudRate::BR4800;
	temp["9600"] = QAsyncSerialPort::BaudRate::BR9600;
	temp["14400"] = QAsyncSerialPort::BaudRate::BR14400;
	temp["19200"] = QAsyncSerialPort::BaudRate::BR19200;
	temp["28800"] = QAsyncSerialPort::BaudRate::BR28800;
	temp["38400"] = QAsyncSerialPort::BaudRate::BR38400;
	temp["56000"] = QAsyncSerialPort::BaudRate::BR56000;
	temp["57600"] = QAsyncSerialPort::BaudRate::BR57600;
	temp["115200"] = QAsyncSerialPort::BaudRate::BR115200;
	temp["128000"] = QAsyncSerialPort::BaudRate::BR128000;
	temp["230400"] = QAsyncSerialPort::BaudRate::BR230400;
	temp["256000"] = QAsyncSerialPort::BaudRate::BR256000;
	temp["Unknown BaudRate"] = QAsyncSerialPort::BaudRate::BRUnknown;
	return temp;
}() };
const QMap <QSerialPort::StopBits, QString> QSettingsTab::mSBtoStr{
	[]()->QMap<QSerialPort::StopBits, QString> {
	QMap<QSerialPort::StopBits, QString> temp;
	temp[QSerialPort::StopBits::OneStop] = "1";
	temp[QSerialPort::StopBits::OneAndHalfStop] = "1.5";
	temp[QSerialPort::StopBits::TwoStop] = "2";
	temp[QSerialPort::StopBits::UnknownStopBits] = "Unknown Stop Bits";
	return temp;
}() };
const QMap <QString, QSerialPort::StopBits> QSettingsTab::mStrtoSB{
	[]()->QMap <QString, QSerialPort::StopBits> {
	QMap <QString, QSerialPort::StopBits> temp;
	temp["1"] = QSerialPort::StopBits::OneStop;
	temp["1.5"] = QSerialPort::StopBits::OneAndHalfStop;
	temp["2"] = QSerialPort::StopBits::TwoStop;
	temp["Unknown Stop Bits"] = QSerialPort::StopBits::UnknownStopBits;
	return temp;
}() };
const QMap <QSerialPort::Parity, QString> QSettingsTab::mParitytoStr{
	[]()->QMap<QSerialPort::Parity, QString> {
	QMap<QSerialPort::Parity, QString> temp;
	temp[QSerialPort::Parity::NoParity] = "No Parity";
	temp[QSerialPort::Parity::EvenParity] = "Even Parity";
	temp[QSerialPort::Parity::OddParity] = "Odd Parity";
	temp[QSerialPort::Parity::MarkParity] = "Mark Parity";
	temp[QSerialPort::Parity::UnknownParity] = "Unknown Parity";
	return temp;
}() };
const QMap <QString, QSerialPort::Parity> QSettingsTab::mStrtoParity{
	[]()->QMap<QString, QSerialPort::Parity> {
	QMap<QString, QSerialPort::Parity> temp;
	temp["No Parity"] = QSerialPort::Parity::NoParity;
	temp["Even Parity"] = QSerialPort::Parity::EvenParity;
	temp["Odd Parity"] = QSerialPort::Parity::OddParity;
	temp["Mark Parity"] = QSerialPort::Parity::MarkParity;
	temp["Unknown Parity"] = QSerialPort::Parity::UnknownParity;
	return temp;
}() };
const QMap <QSerialPort::FlowControl, QString> QSettingsTab::mFCtoStr{
	[]()->QMap<QSerialPort::FlowControl, QString> {
	QMap<QSerialPort::FlowControl, QString> temp;
	temp[QSerialPort::FlowControl::NoFlowControl] = "No Flow Control";
	temp[QSerialPort::FlowControl::HardwareControl] = "HardWare Control";
	temp[QSerialPort::FlowControl::SoftwareControl] = "Software Control";
	temp[QSerialPort::FlowControl::UnknownFlowControl] = "Unknown Flow Control";
	return temp;
}() };
const QMap <QString, QSerialPort::FlowControl> QSettingsTab::mStrtoFC{
	[]()->QMap<QString,QSerialPort::FlowControl> {
	QMap<QString,QSerialPort::FlowControl> temp;
	temp["No Flow Control"] = QSerialPort::FlowControl::NoFlowControl;
	temp["HardWare Control"] = QSerialPort::FlowControl::HardwareControl;
	temp["Software Control"] = QSerialPort::FlowControl::SoftwareControl;
	temp["Unknown Flow Control"] = QSerialPort::FlowControl::UnknownFlowControl;
	return temp;
}() };
const QMap <QSerialPort::DataBits, QString> QSettingsTab::mDBtoStr{
	[]()->QMap<QSerialPort::DataBits, QString> {
	QMap<QSerialPort::DataBits, QString> temp;
	temp[QSerialPort::DataBits::Data5] = "5";
	temp[QSerialPort::DataBits::Data6] = "6";
	temp[QSerialPort::DataBits::Data7] = "7";
	temp[QSerialPort::DataBits::Data8] = "8";
	temp[QSerialPort::DataBits::UnknownDataBits] = "Unknown Data Bits";
	return temp;
}() };
const QMap <QString, QSerialPort::DataBits> QSettingsTab::mStrtoDB{
	[]()->QMap<QString, QSerialPort::DataBits> {
	QMap<QString, QSerialPort::DataBits> temp;
	temp["5"] = QSerialPort::DataBits::Data5;
	temp["6"] = QSerialPort::DataBits::Data6;
	temp["7"] = QSerialPort::DataBits::Data7;
	temp["8"] = QSerialPort::DataBits::Data8;
	temp["Unknown Data Bits"] = QSerialPort::DataBits::UnknownDataBits;
	return temp;
}() };


QSettingsTab::QSettingsTab(QRangeFinderAR2000 &rangeFinder, QWidget * parent)
	:	QWidget(parent),
		mRangeFinder{ &rangeFinder }
{

	mMeasBox = new QGroupBox("Measurement");
	mMeasLayout = new QFormLayout;
	mAverage = new QSpinBox;
	mAverage->setRange(1, 50);
	mMeasLayout->addRow(new QLabel("Average (Values/Output) :"), mAverage);
	mFrequency = new QDoubleSpinBox;
	mFrequency->setRange(0, 100);
	mFrequency->setSingleStep(0.5);
	mMeasLayout->addRow(new QLabel("Measurement Frequency (Hz) :"), mFrequency);
	mWindowMin = new QDoubleSpinBox;
	mWindowMin->setRange(-500, 500);
	mWindowMin->setSingleStep(0.5);
	mMeasLayout->addRow(new QLabel("Measurement Window Minimum (m) :"), mWindowMin);
	mWindowMax = new QDoubleSpinBox;
	mWindowMax->setRange(-500, 500);
	mWindowMax->setSingleStep(0.5);
	mMeasLayout->addRow(new QLabel("Measurement Window Maximum (m) : "), mWindowMax);
	mOffset = new QDoubleSpinBox;
	mOffset->setRange(-500, 500);
	mOffset->setSingleStep(0.5);
	mMeasLayout->addRow(new QLabel("Offset (m) :"), mOffset);
	mMeasBox->setLayout(mMeasLayout);


	mDeviceBox = new QGroupBox("Device");
	mDeviceLayout = new QFormLayout;
	mBaudRate = new QComboBox;
	mBaudRate->addItems(QStringList(mBRtoStr.values()));
	mDeviceLayout->addRow(new QLabel("Baud Rate :"), mBaudRate);
	mStopBits = new QComboBox;
	mStopBits->addItems(QStringList(mSBtoStr.values()));
	mDeviceLayout->addRow(new QLabel("Stop Bits :"), mStopBits);
	mDeviceBox->setLayout(mDeviceLayout);

	mPortBox = new QGroupBox("Serial Port");
	mSerialLayout = new QFormLayout;
	mPortName = new QLineEdit;
	mSerialLayout->addRow(new QLabel("Name"), mPortName);
	mPortBaudRate = new QComboBox;
	mPortBaudRate->addItems(QStringList(mBRtoStr.values()));
	mSerialLayout->addRow(new QLabel("Baud Rate :"), mPortBaudRate);
	mPortStopBits = new QComboBox;
	mPortStopBits->addItems(QStringList(mSBtoStr.values()));
	mSerialLayout->addRow(new QLabel("Stop Bits :"), mPortStopBits);
	mDataBits = new QComboBox;
	mDataBits->addItems(QStringList(mDBtoStr.values()));
	mSerialLayout->addRow(new QLabel("Data Bits :"), mDataBits);
	mParity = new QComboBox;
	mParity->addItems(QStringList(mParitytoStr.values()));
	mSerialLayout->addRow(new QLabel("Parity :"), mParity);
	mFlowControl = new QComboBox;
	mFlowControl->addItems(QStringList(mFCtoStr.values()));
	mSerialLayout->addRow(new QLabel("Flow Control :"), mFlowControl);
	mPortBox->setLayout(mSerialLayout);

	mCommBox = new QGroupBox("Communication");
	mCommLayout = new QVBoxLayout;
	mCommLayout->addWidget(mDeviceBox);
	mCommLayout->addWidget(mPortBox);
	mCommBox->setLayout(mCommLayout);


	mButtonLayout = new QHBoxLayout;
	mRevert = new QPushButton("Revert");
	mRevert->setEnabled(false);
	mSave = new QPushButton("Save");
	mSave->setEnabled(false);
	mDefault = new QPushButton("Default");
	if (!mRangeFinder->fileExists(QRangeFinderAR2000::DEFAULT_INI)) {
		mDefault->setEnabled(false);
	}
	mOk = new QPushButton("Ok");
	mOk->setEnabled(false);
	mCancel = new QPushButton("Cancel");
	mCancel->setEnabled(false);
	mButtonLayout->addWidget(mRevert);
	mButtonLayout->addWidget(mSave);
	mButtonLayout->addWidget(mDefault);
	mButtonLayout->addStretch();
	mButtonLayout->addWidget(mOk);
	mButtonLayout->addWidget(mCancel);

	mQvbl = new QVBoxLayout;
	setLayout(mQvbl);
	mQvbl->addWidget(mMeasBox);
	mQvbl->addWidget(mCommBox);
	mQvbl->addStretch();
	mQvbl->addLayout(mButtonLayout);

	setFields();


	connect(mAverage, static_cast<void (QSpinBox::*)(int)> (&QSpinBox::valueChanged), this, &QSettingsTab::updateAverage);
	connect(mFrequency, static_cast<void (QDoubleSpinBox::*)(double)> (&QDoubleSpinBox::valueChanged), this, &QSettingsTab::updateFrequency);
	connect(mWindowMin, static_cast<void (QDoubleSpinBox::*)(double)> (&QDoubleSpinBox::valueChanged), this, &QSettingsTab::updateMinWindow);
	connect(mWindowMax, static_cast<void (QDoubleSpinBox::*)(double)> (&QDoubleSpinBox::valueChanged), this, &QSettingsTab::updateMaxWindow);
	connect(mOffset, static_cast<void (QDoubleSpinBox::*)(double)> (&QDoubleSpinBox::valueChanged), this, &QSettingsTab::updateOffset);
	connect(mPortName, &QLineEdit::textChanged, this, &QSettingsTab::updatePortName);
	connect(mBaudRate, &QComboBox::currentTextChanged, this, &QSettingsTab::updateDevBaudRate);
	connect(mStopBits, &QComboBox::currentTextChanged, this, &QSettingsTab::updateDevStopBits);
	connect(mPortBaudRate, &QComboBox::currentTextChanged, this, &QSettingsTab::updatePortBaudRate);
	connect(mPortStopBits, &QComboBox::currentTextChanged, this, &QSettingsTab::updatePortStopBits);
	connect(mParity, &QComboBox::currentTextChanged, this, &QSettingsTab::updateParity);
	connect(mFlowControl, &QComboBox::currentTextChanged, this, &QSettingsTab::updateFlowControl);
	connect(mDataBits, &QComboBox::currentTextChanged, this, &QSettingsTab::updateDataBits);
	connect(this, &QSettingsTab::changeMade, this, &QSettingsTab::updateButtons);
	connect(mOk, &QPushButton::clicked, this, &QSettingsTab::ok);
	connect(mCancel, &QPushButton::clicked, this, &QSettingsTab::cancel);
	connect(mSave, &QPushButton::clicked, this, &QSettingsTab::save);
	connect(mDefault, &QPushButton::clicked, this, &QSettingsTab::defaultClicked);
	connect(mRevert, &QPushButton::clicked, this, &QSettingsTab::revert);

}
void QSettingsTab::setFields() {
	mAverage->setValue(mRangeFinder->mSettings.mAverage);
	mFrequency->setValue(mRangeFinder->mSettings.mFrequency);
	mWindowMin->setValue(mRangeFinder->mSettings.mWindowMin);
	mWindowMax->setValue(mRangeFinder->mSettings.mWindowMax);
	mOffset->setValue(mRangeFinder->mSettings.mOffset);
	mBaudRate->setCurrentText(mBRtoStr[mRangeFinder->mSettings.mBaudRate]);
	mStopBits->setCurrentText(mSBtoStr[mRangeFinder->mSettings.mStopBits]);
	mPortBaudRate->setCurrentText(mBRtoStr[mRangeFinder->mSettings.mBaudRate]);
	mPortName->setText(mRangeFinder->mSettings.mPortName);
	mPortStopBits->setCurrentText(mSBtoStr[mRangeFinder->mSettings.mStopBits]);
	mDataBits->setCurrentText(mDBtoStr[mRangeFinder->mSettings.mDataBits]);
	mParity->setCurrentText(mParitytoStr[mRangeFinder->mSettings.mParity]);
	mFlowControl->setCurrentText(mFCtoStr[mRangeFinder->mSettings.mFlowControl]);
}
void QSettingsTab::ok() {
	mRangeFinder->applySettings();
}
void QSettingsTab::save() {
	mRangeFinder->mSettings.save(QRangeFinderAR2000::CURRENT_INI);
	ok();
	mRevert->setEnabled(true);
}
void QSettingsTab::cancel() {
	if (mRangeFinder->fileExists(QRangeFinderAR2000::CURRENT_INI)) {
		mRangeFinder->mSettings.load(QRangeFinderAR2000::CURRENT_INI);
	}
	else if (!mRangeFinder->fileExists(QRangeFinderAR2000::DEFAULT_INI)) {
		mRangeFinder->mSettings.load(QRangeFinderAR2000::DEFAULT_INI);
	}
	setFields();
}
void QSettingsTab::defaultClicked() {
	mRangeFinder->mSettings.load(QRangeFinderAR2000::DEFAULT_INI);
	setFields();
	ok();
}
void QSettingsTab::revert() {
	mRangeFinder->mSettings.load(QRangeFinderAR2000::TEMP_INI);
	setFields();
	mRangeFinder->mSettings.save(QRangeFinderAR2000::CURRENT_INI);
}
void QSettingsTab::updateButtons() {
	//mRevert->setEnabled(true);
	mSave->setEnabled(true);
	//mDefault->setEnabled(true);
	mOk->setEnabled(true);
	mCancel->setEnabled(true);
}

void QSettingsTab::updateAverage(int val) {
	mRangeFinder->mSettings.mAverage = val;
	emit changeMade();
}
void QSettingsTab::updateFrequency(double val) {
	mRangeFinder->mSettings.mFrequency = val;
	emit changeMade();
}
void QSettingsTab::updateMaxWindow(double val) {
	mRangeFinder->mSettings.mWindowMax = val;
	emit changeMade();
}
void QSettingsTab::updateMinWindow(double val) {
	mRangeFinder->mSettings.mWindowMin = val;
	emit changeMade();
}
void QSettingsTab::updateOffset(double val) {
	mRangeFinder->mSettings.mOffset = val;
	emit changeMade();
}
void QSettingsTab::updatePortName(QString val) {
	mRangeFinder->mSettings.mPortName = val;
	emit changeMade();
}
void QSettingsTab::updateDevBaudRate(QString val) {
	mRangeFinder->mSettings.mBaudRate = mStrtoBR[val];
	emit changeMade();
}
void QSettingsTab::updatePortBaudRate(QString val) {
	mRangeFinder->mSettings.mBaudRate = mStrtoBR[val];
	emit changeMade();
}
void QSettingsTab::updateDevStopBits(QString val) {
	mRangeFinder->mSettings.mStopBits = mStrtoSB[val];
	emit changeMade();
}
void QSettingsTab::updatePortStopBits(QString val) {
	mRangeFinder->mSettings.mStopBits = mStrtoSB[val];
	emit changeMade();
}
void QSettingsTab::updateParity(QString val) {
	mRangeFinder->mSettings.mParity = mStrtoParity[val];
	emit changeMade();
}
void QSettingsTab::updateFlowControl(QString val) {
	mRangeFinder->mSettings.mFlowControl = mStrtoFC[val];
	emit changeMade();
}
void QSettingsTab::updateDataBits(QString val) {
	mRangeFinder->mSettings.mDataBits = mStrtoDB[val];
	emit changeMade();
}
QSettingsTab::~QSettingsTab() {

}
