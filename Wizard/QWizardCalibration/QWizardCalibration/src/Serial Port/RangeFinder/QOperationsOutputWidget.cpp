#include "..\..\..\include\Serial Port\RangeFinder\QOperationsOutputWidget.h"
#include <QString>
#include "..\..\..\include\Serial Port\RangeFinder\QRangeFinderAR2000.h"

QOperationsOutputWidget::QOperationsOutputWidget(QRangeFinderAR2000 * rangeFinder, QWidget * parent)
	: QWidget(parent), mRangeFinder{ rangeFinder }
{
	mGrid = new QGridLayout;
	setLayout(mGrid);
	mGrid->addWidget(new QLabel("Measurement : "), 0, 0);
	mGrid->addWidget(new QLabel("Signal Quality : "), 1, 0);
	mGrid->addWidget(new QLabel("Temperature : "), 2, 0);
	mMeasurement = new QLineEdit;
	mMeasurement->setReadOnly(true);
	mSignalQuality = new QLineEdit;
	mSignalQuality->setReadOnly(true);
	mTemperature = new QLineEdit;
	mTemperature->setReadOnly(true);
	mGrid->addWidget(mMeasurement, 0, 1);
	mGrid->addWidget(mSignalQuality, 1, 1);
	mGrid->addWidget(mTemperature, 2, 1);
	mGrid->addWidget(new QLabel("m"), 0, 2);
	mGrid->addWidget(new QLabel("%"), 1, 2);
	mGrid->addWidget(new QLabel("°C"), 2, 2);

	connect(mRangeFinder, &QRangeFinderAR2000::measurementReceived, this, &QOperationsOutputWidget::showMeasurement);
}
QOperationsOutputWidget::~QOperationsOutputWidget() {

}

void QOperationsOutputWidget::showMeasurement(double distance, double quality, double temperature) {
	mMeasurement->setText(QString::number(distance));
	mSignalQuality->setText(QString::number(quality));
	mTemperature->setText(QString::number(temperature));
}