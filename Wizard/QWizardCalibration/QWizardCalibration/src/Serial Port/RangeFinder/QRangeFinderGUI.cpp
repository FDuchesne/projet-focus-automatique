#include "..\..\..\include\Serial Port\RangeFinder\QRangeFinderGUI.h"

#include <QLayout>
#include "..\..\..\include\Serial Port\RangeFinder\QRFStatusBar.h"



//QMessageBox QRangeFinderGUI::OP_MODE_WARNING{
//	[]()->QMessageBox {
//		QMessageBox temp;
//		temp.setWindowTitle("Switching to Operation Mode");
//		temp.setText("Switching to Operation mode. Some changes made will be discarded. Continue?");
//		temp.setIcon(QMessageBox::Question);
//		temp.setStandardButtons(QMessageBox::Yes);
//		temp.addButton(QMessageBox::No);
//		temp.setDefaultButton(QMessageBox::Yes);
//		return temp;
//	}()
//};


QRangeFinderGUI::QRangeFinderGUI(QWidget * parent)
	: QWidget(parent)
{
	mTabs = new QTabWidget;
	mOperations = new QOperationsTab(mRangeFinder);
	mDevelopment = new QDevelopmentTab(&mRangeFinder.mSerial, mRangeFinder.mSerialCommands.values());
	mParams = new QSettingsTab(mRangeFinder);
	mTabs->addTab(mOperations, "Operations");
	mTabs->addTab(mParams, "Parameters");
	mTabs->addTab(mDevelopment, "Development");
	setWindowTitle("Range Finder AR2000 - S.N. 140191");

	mStatusBar = new QRFStatusBar(&mRangeFinder);
	mQvbl = new QVBoxLayout;
	setLayout(mQvbl);
	mQvbl->addWidget(mTabs);
	mQvbl->addWidget(mStatusBar);

	mQvbl->setMargin(5);

	connect(mTabs, &QTabWidget::tabBarClicked, this, &QRangeFinderGUI::tabSelected); //TODO :: voir cmt connecter seulement pour 1 onglet??

	//mTestWin = new QTestWin(&mRangeFinder);
	//mTestWin->show();
}
void QRangeFinderGUI::tabSelected(int index) {
	// Operation mode
	if (index == 0) {
		if (mRangeFinder.mSerial.developmentMode()) {
			QMessageBox opWarning;
			opWarning.setWindowTitle("Switching to Operation Mode");
			opWarning.setText("Switching to Operation mode. Some changes made will be discarded. Continue?");
			opWarning.setIcon(QMessageBox::Question);
			opWarning.setStandardButtons(QMessageBox::Yes);
			opWarning.addButton(QMessageBox::No);
			opWarning.setDefaultButton(QMessageBox::Yes);
			if (opWarning.exec() == QMessageBox::Yes) {
				mRangeFinder.applySettings();
				mRangeFinder.mSerial.setDevelopmentMode(false);
			}
		}
	}

	else if (index == 1) {
		mRangeFinder.mSettings.save(QRangeFinderAR2000::TEMP_INI);
	}
}

void QRangeFinderGUI::closeEvent(QCloseEvent * event)
{
	emit widgetClosed();
}

QRangeFinderGUI::~QRangeFinderGUI() {

}
