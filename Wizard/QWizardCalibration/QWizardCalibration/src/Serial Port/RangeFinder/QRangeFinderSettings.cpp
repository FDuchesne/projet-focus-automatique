#include "..\..\..\include\Serial Port\RangeFinder\QRangeFinderSettings.h"
#include <QSettings>
#include <QCoreApplication>

QString const QRangeFinderSettings::PORTNAME{ "portName" };
QString const QRangeFinderSettings::BAUDRATE{ "baudRate" };
QString const QRangeFinderSettings::STOPBITS{ "stopBits" };
QString const QRangeFinderSettings::DATABITS{ "dataBits" };
QString const QRangeFinderSettings::PARITY{ "parity" };
QString const QRangeFinderSettings::FLOWCONTROL{ "flowControl" };
QString const QRangeFinderSettings::AVERAGE{ "average" };
QString const QRangeFinderSettings::FREQUENCY{ "frequency" };
QString const QRangeFinderSettings::OFFSET{ "offset" };
QString const QRangeFinderSettings::MIN_WINDOW{ "minWindow" };
QString const QRangeFinderSettings::MAX_WINDOW{ "maxWindow" };
QString const QRangeFinderSettings::LASER{ "laser" };


QRangeFinderSettings::QRangeFinderSettings(QObject * parent)
	:	QObject(parent),
		mPortName{ QString() },
		mBaudRate{ QAsyncSerialPort::BaudRate::BRUnknown },
		mStopBits{ QSerialPort::StopBits::UnknownStopBits },
		mDataBits{ QSerialPort::DataBits::UnknownDataBits },
		mParity{ QSerialPort::Parity::UnknownParity },
		mFlowControl{ QSerialPort::FlowControl::UnknownFlowControl },
		mAverage{ -1 },
		mFrequency{ -1 },
		mOffset{ -501 },
		mWindowMin{ -501 },
		mWindowMax{ -501 },
		mLaserOnStart{ false }
{

}

QRangeFinderSettings::~QRangeFinderSettings() {
	
}


void QRangeFinderSettings::save(QString fileName) {
	QSettings settings(fileName + ".ini", QSettings::Format::IniFormat);
	settings.setValue(PORTNAME, mPortName);
	settings.setValue(BAUDRATE, static_cast<int>(mBaudRate));
	settings.setValue(STOPBITS, mStopBits);
	settings.setValue(DATABITS, mDataBits);
	settings.setValue(PARITY, mParity);
	settings.setValue(FLOWCONTROL, mFlowControl);

	settings.setValue(AVERAGE, mAverage);
	settings.setValue(FREQUENCY, mFrequency);
	settings.setValue(OFFSET, mOffset);
	settings.setValue(MIN_WINDOW, mWindowMin);
	settings.setValue(MAX_WINDOW, mWindowMax);
	settings.setValue(LASER, mLaserOnStart);
}

void QRangeFinderSettings::load(QString fileName) {
	QSettings settings(fileName + ".ini", QSettings::Format::IniFormat);
	mPortName = settings.value(PORTNAME).toString();
	mBaudRate = static_cast<QAsyncSerialPort::BaudRate>(settings.value(BAUDRATE).toInt());
	mStopBits = static_cast<QSerialPort::StopBits>(settings.value(STOPBITS).toInt());
	mDataBits = static_cast<QSerialPort::DataBits>(settings.value(DATABITS).toInt());
	mParity = static_cast<QSerialPort::Parity>(settings.value(PARITY).toInt());
	mFlowControl = static_cast<QSerialPort::FlowControl>(settings.value(FLOWCONTROL).toInt());

	mAverage = settings.value(AVERAGE).toInt();
	mFrequency = settings.value(FREQUENCY).toDouble();
	mOffset = settings.value(OFFSET).toDouble();
	mWindowMin = settings.value(MIN_WINDOW).toDouble();
	mWindowMax = settings.value(MAX_WINDOW).toDouble();
	mLaserOnStart = settings.value(LASER).toBool();
}

bool QRangeFinderSettings::isValid() {
	// Serial Port
	if (mPortName.isNull()) {
		return false;
	}
	if (mBaudRate == QAsyncSerialPort::BaudRate::BRUnknown) {
		return false;
	}
	if (mStopBits == QSerialPort::StopBits::UnknownStopBits) {
		return false;
	}
	if (mDataBits == QSerialPort::DataBits::UnknownDataBits) {
		return false;
	}
	if (mParity == QSerialPort::Parity::UnknownParity) {
		return false;
	}
	if (mFlowControl == QSerialPort::FlowControl::UnknownFlowControl) {
		return false;
	}

	// Range Finder
	if (mAverage < 1 || mAverage > 50) {
		return false;
	}
	if (mFrequency < 0 || mFrequency > 100) {
		return false;
	}
	if (mOffset < -500 || mOffset > 500) {
		return false;
	}
	if (mWindowMin < -500 || mWindowMin > 500) {
		return false;
	}
	if (mWindowMax < -500 || mWindowMax > 500) {
		return false;
	}

	return true;
}