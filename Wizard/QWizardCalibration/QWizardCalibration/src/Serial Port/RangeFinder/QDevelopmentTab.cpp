#include "..\..\..\include\Serial Port\RangeFinder\QDevelopmentTab.h"
#include "..\..\..\include\Serial Port\Global\QAsyncSerialPort.h"
#include <QMessageBox>

QDevelopmentTab::QDevelopmentTab(QCommandSerialPort * serial, QList<QSerialCommand const *> const &serialCommands, QWidget *parent)
	: QWidget(parent), mSerial{ serial }
{
	mConsole = new QDebugConsole;
	mCommandLine = new QDebugCommandLine(serialCommands[0]->eolCar());
	mCommandList = new QDebugCommandList(serialCommands);
	mQvbl = new QVBoxLayout;
	mQvbl->addWidget(mConsole);
	mQvbl->addWidget(mCommandList);
	mQvbl->addWidget(mCommandLine);

	setLayout(mQvbl);
	resize(500, height());

	connect(mCommandList, &QDebugCommandList::cmdSent, mCommandLine, &QDebugCommandLine::writeCmd);
	connect(mCommandLine, &QDebugCommandLine::messageSent, mConsole, &QDebugConsole::appendMessage);
	connect(mCommandLine->echoBox(), &QCheckBox::stateChanged, mConsole, &QDebugConsole::setEcho);
	connect(mCommandLine, &QDebugCommandLine::messageSent, this, &QDevelopmentTab::switchToDevMode);
	connect(mSerial, &QAsyncSerialPort::dataRead, this, &QDevelopmentTab::handleResponse);
}

void QDevelopmentTab::sendMessage(QString message) {
	mSerial->setDevelopmentMode(true); // TODO : show warning that commandManagement will completely stop
	mSerial->sendMessage(message);
}

void QDevelopmentTab::echoCommand(QString s)
{
	if (mCommandLine->localEchoEnabled) {
		mConsole->moveCursor(QTextCursor::End);
		mConsole->insertPlainText(s);
	}
}

void QDevelopmentTab::handleResponse(QByteArray data) {
	QString response(data);
	mConsole->moveCursor(QTextCursor::End);
	mConsole->insertPlainText(response);
}
void QDevelopmentTab::switchToDevMode(QString message) {
	if (mSerial->developmentMode()) {
		sendMessage(message); //alrdy in devMode, just send Msg
	}
	else {
		QMessageBox devModeWarning;
		devModeWarning.setWindowTitle("Switching to Development Mode");
		devModeWarning.setText("Switching to Development Mode. Operations will not be managed. Are you sure?");
		devModeWarning.setIcon(QMessageBox::Question);
		devModeWarning.setStandardButtons(QMessageBox::Yes);
		devModeWarning.addButton(QMessageBox::No);
		devModeWarning.setDefaultButton(QMessageBox::Yes);
		if (devModeWarning.exec() == QMessageBox::Yes) {
			sendMessage(message);
		}
	}
}

QDevelopmentTab::~QDevelopmentTab() {

}
