#include "..\..\..\include\Serial Port\Focuser\QFocusseur.h"

#include <QCoreApplication>
#include <QDebug>
#include <Windows.h>
#include <QTime>
#include <QPair>
#include <QVariant>
#include <QFileInfo>
#include <QDir>
#include "..\..\..\include\Serial Port\Focuser\QFocusseurSettings.h"


QString const QFocusseur::SEPARATOR		{ "" };
QString const QFocusseur::TERMINATOR{ "#" };
QString const QFocusseur::DEFAULT_INI	{ "defaultSettingsFocuser" };
QString const QFocusseur::CURRENT_INI	{ "currentSettingsFocuser" };
QString const QFocusseur::TEMP_INI		{ "tempSettingsFocuser" };


QFocusseur::QFocusseur()
	:	mDeviceConnected{ false }

{
	fillDictionary();
	fillDeviceMessages();
	mSerial.setDeviceMessages(mDeviceMessages.keys(), TERMINATOR);

	initSettings();
	
	mTestCommand = *mSerialCommands[Command::InitSeq];
	mTestCommand2 = *mSerialCommands[Command::SetSpeed];
	mTestCommand3 = *mSerialCommands[Command::GoToAbs];
	mTestCommand4 = *mSerialCommands[Command::RelativePlusSeq];
	mTestCommand5 = *mSerialCommands[Command::RelativeMinusSeq];
	mTestCommand6 = *mSerialCommands[Command::GetPosition];

	connect(&mSerial, &QCommandSerialPort::responseMatchesCommand, this, &QFocusseur::handleMatchingResponse);
	connect(&mSerial, &QCommandSerialPort::messageReceived, this, &QFocusseur::handleMessageReceived);
	connect(&mSerial, &QAsyncSerialPort::connectionUpdated, this, &QFocusseur::connectionUpdated);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Slots
///////////////////////////////////////////////////////////////////////////////////////////////


void QFocusseur::handleMatchingResponse(const QByteArray &response, const QSerialCommand &command) {
	QString sResponse(response);
	// Tracking
	if (command.operationMode().fluxMode() == SerialOperationMode::FluxMode::Push /*|| command == *mSerialCommands[Command::DoSingleMeasurement]*/) {
		emit positionReceived(sResponse);

		QStringList measurement = sResponse.split(",");
	}
	else {
		emit matchingResponseReceived(command.command() + " : " + response);
		// Position received
		if (command == *mSerialCommands[Command::GetAccLength])
		{
			emit accLengthReceived(sResponse);
		}
		else if (command == *mSerialCommands[Command::GetBaseSpeed])
		{
			emit baseSpeedReceived(sResponse);
		}
		else if (command == *mSerialCommands[Command::GetPosition])
		{
			//emit positionReceived(response);
			emit positionReceived(sResponse);
		}
		else if (command == *mSerialCommands[Command::GetSpeed])
		{
			emit speedReceived(sResponse);
		}
		else if (command == *mSerialCommands[Command::GetTestLength])
		{
			emit testLengthReceived(sResponse);
		}
		else if (command == *mSerialCommands[Command::InitSeq] || command == *mSerialCommands[Command::GoToAbs]
			|| command == *mSerialCommands[Command::RelativeMinusSeq] || command == *mSerialCommands[Command::RelativePlusSeq]
			|| command == *mSerialCommands[Command::Stop])
		{
			emit positionReceived(sResponse);
		}
		else if (command == *mSerialCommands[Command::GetMaxAccLength])
		{
			emit maxAccLengthReceived(sResponse);
		}
		else if (command == *mSerialCommands[Command::GetMaxTestLength])
		{
			emit maxTestLengthReceived(sResponse);
		}
		else if (command == *mSerialCommands[Command::GetMinAccLength])
		{
			emit minAccLengthReceived(sResponse);
		}
		else if (command == *mSerialCommands[Command::GetMinTestLength])
		{
			emit minTestLengthReceived(sResponse);
		}
	}
}

void QFocusseur::handleMessageReceived(QString const &message) {
	emit messageReceived(mDeviceMessages[message]);
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Methods
//////////////////////////////////////////////////////////////////////////////////////////////

void QFocusseur::testCommand() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand, QList<QVariant>());
	mSerial.writeToBuffer(command);
}
void QFocusseur::testCommand2() {
	QList<QVariant> * params = new QList<QVariant>();
	QVariant * leParam = new QVariant("3");
	params->append(*leParam);
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand2, *params);
	mSerial.writeToBuffer(command);
}													
void QFocusseur::testCommand3() {
	QList<QVariant> * params = new QList<QVariant>();
	QVariant * leParam = new QVariant("000000");
	params->append(*leParam);
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand3, *params);
	mSerial.writeToBuffer(command);
}													
void QFocusseur::testCommand4() {
	QList<QVariant> * params = new QList<QVariant>();
	QVariant * leParam = new QVariant("00500");
	params->append(*leParam);
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand4, *params);
	mSerial.writeToBuffer(command);
}													
void QFocusseur::testCommand5() {
	QList<QVariant> * params = new QList<QVariant>();
	QVariant * leParam = new QVariant("00500");
	params->append(*leParam);
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand5, *params);
	mSerial.writeToBuffer(command);
}
void QFocusseur::testCommand6() {
	QPair<QSerialCommand&, QList<QVariant>> command(mTestCommand6, QList<QVariant>());
	mSerial.writeToBuffer(command);
}

void QFocusseur::connectSerialPort() {
	mSerial.openSerialPort(mSettingsFocusseur.mPortName, mSettingsFocusseur.mBaudRate, mSettingsFocusseur.mDataBits, mSettingsFocusseur.mParity, mSettingsFocusseur.mStopBits, mSettingsFocusseur.mFlowControl);
}

void QFocusseur::connectionUpdated(bool connected) {
	if (connected) {
		applySettings();
		mDeviceConnected = true;
	}
	else {
		mDeviceConnected = false;
	}
}

/*! Affectation de mSettingsFocusseur à partir d'un fichier ini sauvegardé. Si aucun n'est sauvegardé, c'est ici qu'on en crée un avec des paramètres par défaut.
*/
void QFocusseur::initSettings() {
	if (fileExists(CURRENT_INI)) {
		mSettingsFocusseur.load(CURRENT_INI);
	}
	else {
		// Default Settings
		mSettingsFocusseur.mPortName = "COM3";
		mSettingsFocusseur.mBaudRate = QAsyncSerialPort::BaudRate::BR9600;
		mSettingsFocusseur.mDataBits = QSerialPort::DataBits::Data8;
		mSettingsFocusseur.mStopBits = QSerialPort::StopBits::OneStop;
		mSettingsFocusseur.mParity = QSerialPort::Parity::NoParity;
		mSettingsFocusseur.mFlowControl = QSerialPort::FlowControl::NoFlowControl;
		mSettingsFocusseur.mAccLenght = 0.00748;
		mSettingsFocusseur.mBaseSpeed = 50;
		mSettingsFocusseur.mSpeed = 100;
		mSettingsFocusseur.mTestLength = 0.0374;

		if (!fileExists(DEFAULT_INI)) {
			mSettingsFocusseur.save(DEFAULT_INI);
		}
	}
}

/*! Appelle des méthodes de paramétrage du télémètre en lien avec les variables membres de l'instance mSettingsFocusseur.
*/
void QFocusseur::applySettings() {
	setBaudRate(mSettingsFocusseur.mBaudRate);
	setStopBits(mSettingsFocusseur.mStopBits);
	
	//The program is based on this setup, do not change it!!!
	setOutputFormat(1, 1, 1, 0);	// !
	setAutoStart(1);  // Shows the id of the device
}

void QFocusseur::sendCommandNoParams(Command command) {
	mSerial.setDevelopmentMode(false);
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mSerialCommands[command], QList<QVariant>());
	mSerial.writeToBuffer(commandAndParams);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Set
///////////////////////////////////////////////////////////////////////////////////////////////

void QFocusseur::setOutputFormat(int a, int b, int c, int d) {
	mSerial.setDevelopmentMode(false);
}

void QFocusseur::setAutoStart(int command) {
	mSerial.setDevelopmentMode(false);
}

void QFocusseur::setBaudRate(QAsyncSerialPort::BaudRate baudRate) {
	mSerial.setDevelopmentMode(false);
}

void QFocusseur::setStopBits(QSerialPort::StopBits stopBits) {
	double sb;
	if (stopBits == QSerialPort::StopBits::OneStop) {
		sb = 1;
	}
	else if (stopBits == QSerialPort::StopBits::OneAndHalfStop) {
		sb = 1.5;
	}
	else if (stopBits == QSerialPort::StopBits::TwoStop) {
		sb = 2;
	}
	else {
		sb = -1;
	}
	mSerial.setDevelopmentMode(false);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Helper Methods
///////////////////////////////////////////////////////////////////////////////////////////////

bool QFocusseur::fileExists(QString fileName) {
	QFileInfo check_file(QDir::currentPath() + "/" + fileName + ".ini");
	return (check_file.exists() && check_file.isFile());
}

void QFocusseur::fillDictionary() {
	// La reponse qu on recoit le plus est "Done#"
	QList<QByteArray> * stdRep = new QList<QByteArray>;
	QByteArray * rep = new QByteArray("Done#");
	stdRep->append(*rep);

	// Definition des details des differentes commandes
	mSerialCommands[Command::InitSeq] = new QSerialCommand
	(
		"init",
		"InitSeq",
		QSerialCommand::IOType::Io,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Preparation",
		"Initializes the position of the camera",
		//*stdRep
		QRegularExpression("i\\d*.\\d*#")
	);

	mSerialCommands[Command::GetAccLength] = new QSerialCommand
	(
		"getAccLengthM",
		"GetAccLength",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Get the current acceleration length in meters",
		QRegularExpression("A\\d*.\\d*#")
	);

	mSerialCommands[Command::GetBaseSpeed] = new QSerialCommand
	(
		"getBaseSpeed",
		"GetBaseSpeed",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Get the current base speed for acceleration",
		QRegularExpression("B\\d*#")
	);

	mSerialCommands[Command::GetMaxAccLength] = new QSerialCommand
	(
		"getMaxAccLengthM",
		"GetMaxAccLength",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Get the max acceleration length in meters",
		QRegularExpression("MaxAL\\d*.\\d*#")
	);

	mSerialCommands[Command::GetMaxTestLength] = new QSerialCommand
	(
		"getMaxTestLengthM",
		"GetMaxTestLength",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Get the max length of move during testing in meters",
		QRegularExpression("MaxTL\\d*.\\d*#")
	);

	mSerialCommands[Command::GetMinAccLength] = new QSerialCommand
	(
		"getMinAccLengthM",
		"GetMinAccLength",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Get the min acceleration length in meters",
		QRegularExpression("MinAL\\d*.\\d*#")
	);

	mSerialCommands[Command::GetMinTestLength] = new QSerialCommand
	(
		"getMinTestLengthM",
		"GetMinTestLength",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Get the min length of move during testing in meters",
		QRegularExpression("MinTL\\d*.\\d*#")
	);

	mSerialCommands[Command::GetPosition] = new QSerialCommand
	(
		"getPosM",
		"GetPosition",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Get the current position of the camera in meters",
		QRegularExpression("P\\d*.\\d*#")
	);

	mSerialCommands[Command::GetSpeed] = new QSerialCommand
	(
		"getSpeed",
		"GetSpeed",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Get the current speed",
		QRegularExpression("S\\d*#")
	);

	mSerialCommands[Command::GetTestLength] = new QSerialCommand
	(
		"getTestLengthM",
		"GetTestLength",
		QSerialCommand::IOType::Out,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Get the current length of move during testing in meters",
		QRegularExpression("T\\d*.\\d*#")
	);

	mSerialCommands[Command::GoToAbs] = new QSerialCommand
	(
		"=M",
		"GoToAbs",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Moves the camera to an asbolute position in meters",
		//*stdRep
		QRegularExpression("=\\d*.\\d*#")
	);

	mSerialCommands[Command::RelativePlusSeq] = new QSerialCommand
	(
		"+M",
		"RelativePlusSeq",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Moves the camera to a relative position inward in meters",
		//*stdRep
		QRegularExpression("[+]\\d*.\\d*#")
	);

	mSerialCommands[Command::RelativeMinusSeq] = new QSerialCommand
	(
		"-M",
		"RelativeMinusSeq",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Moves the camera to a relative position outward in meters",
		//*stdRep
		QRegularExpression("-\\d*.\\d*#")
	);
	
	mSerialCommands[Command::SetAccLength] = new QSerialCommand
	(
		"AM",
		"SetAccLength",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Configuration",
		"Modifies the acceleration length in meters",
		QRegularExpression("A\\d*#")
	);

	mSerialCommands[Command::SetBaseSpeed] = new QSerialCommand
	(
		"B",
		"SetBaseSpeed",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Configuration",
		"Modifies the base speed for acceleration in meters",
		QRegularExpression("B\\d*#")
	);

	mSerialCommands[Command::SetSpeed] = new QSerialCommand
	(
		"S",
		"SetSpeed",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Configuration",
		"Modifies the speed",
		QRegularExpression("S\\d*#")
	);

	mSerialCommands[Command::SetTestLength] = new QSerialCommand
	(
		"TM",
		"SetTestLength",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Configuration",
		"Modifies the length of move during testing in meters",
		QRegularExpression("T\\d*#")
	);

	mSerialCommands[Command::SlowMotionMinus] = new QSerialCommand
	(
		"move-",
		"SlowMotionMinus",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::NonBlockingNoResponse,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Moves the camera slowly inward while an arrow key is pressed and stops once it's released",
		//QRegularExpression("M\\d*.\\d*#")
		QRegularExpression("")
	);

	mSerialCommands[Command::SlowMotionPlus] = new QSerialCommand
	(
		"move+",
		"SlowMotionPlus",
		QSerialCommand::IOType::Io,
		1,
		SerialOperationMode::BlockingMode::NonBlockingNoResponse,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Moves the camera slowly outward while an arrow key is pressed and stops once it's released",
		//QRegularExpression("M\\d*.\\d*#")
		QRegularExpression("")
	);

	mSerialCommands[Command::Stop] = new QSerialCommand
	(
		"stop",
		"Stop",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::Blocking,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Stop the slow motion process",
		//QRegularExpression("")
		QRegularExpression("[s]\\d*.\\d*#")
	);

	mSerialCommands[Command::Test] = new QSerialCommand
	(
		"test",
		"Test",
		QSerialCommand::IOType::In,
		0,
		SerialOperationMode::BlockingMode::NonBlockingNoResponse,
		SerialOperationMode::FluxMode::Pull,
		TERMINATOR,
		SEPARATOR,
		"Operation",
		"Moves the camera back and forth to test the connection",
		QRegularExpression("")
	);
}

void QFocusseur::fillDeviceMessages() {
}



///////////////////////////////////////////////////////////////////////////////////////////////
// Destructor
///////////////////////////////////////////////////////////////////////////////////////////////

QFocusseur::~QFocusseur()
{
}
