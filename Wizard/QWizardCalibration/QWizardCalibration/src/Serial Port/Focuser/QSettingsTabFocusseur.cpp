#include "..\..\..\include\Serial Port\Focuser\QSettingsTabFocusseur.h"
#include "..\..\..\include\Serial Port\Focuser\QFocusseur.h"
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QMap>
#include <QLineEdit>
#include <Windows.h>
#include <QVBoxLayout>
#include <QButtonGroup>
#include <QRadioButton>

const QMap<QAsyncSerialPort::BaudRate, QString> QSettingsTabFocusseur::mBRtoStr{
	[]()->QMap<QAsyncSerialPort::BaudRate, QString> {
	QMap<QAsyncSerialPort::BaudRate, QString> temp;
	temp[QAsyncSerialPort::BaudRate::BR600] = "600";
	temp[QAsyncSerialPort::BaudRate::BR1200] = "1200";
	temp[QAsyncSerialPort::BaudRate::BR2400] = "2400";
	temp[QAsyncSerialPort::BaudRate::BR4800] = "4800";
	temp[QAsyncSerialPort::BaudRate::BR9600] = "9600";
	temp[QAsyncSerialPort::BaudRate::BR14400] = "14400";
	temp[QAsyncSerialPort::BaudRate::BR19200] = "19200";
	temp[QAsyncSerialPort::BaudRate::BR28800] = "28800";
	temp[QAsyncSerialPort::BaudRate::BR38400] = "38400";
	temp[QAsyncSerialPort::BaudRate::BR56000] = "56000";
	temp[QAsyncSerialPort::BaudRate::BR57600] = "57600";
	temp[QAsyncSerialPort::BaudRate::BR115200] = "115200";
	temp[QAsyncSerialPort::BaudRate::BR128000] = "128000";
	temp[QAsyncSerialPort::BaudRate::BR230400] = "230400";
	temp[QAsyncSerialPort::BaudRate::BR256000] = "256000";
	temp[QAsyncSerialPort::BaudRate::BRUnknown] = "Unknown BaudRate";
	return temp;
}() };
const QMap <QString, QAsyncSerialPort::BaudRate> QSettingsTabFocusseur::mStrtoBR{
	[]()->QMap<QString, QAsyncSerialPort::BaudRate> {
	QMap<QString, QAsyncSerialPort::BaudRate> temp;
	temp["600"] = QAsyncSerialPort::BaudRate::BR600;
	temp["1200"] = QAsyncSerialPort::BaudRate::BR1200;
	temp["2400"] = QAsyncSerialPort::BaudRate::BR2400;
	temp["4800"] = QAsyncSerialPort::BaudRate::BR4800;
	temp["9600"] = QAsyncSerialPort::BaudRate::BR9600;
	temp["14400"] = QAsyncSerialPort::BaudRate::BR14400;
	temp["19200"] = QAsyncSerialPort::BaudRate::BR19200;
	temp["28800"] = QAsyncSerialPort::BaudRate::BR28800;
	temp["38400"] = QAsyncSerialPort::BaudRate::BR38400;
	temp["56000"] = QAsyncSerialPort::BaudRate::BR56000;
	temp["57600"] = QAsyncSerialPort::BaudRate::BR57600;
	temp["115200"] = QAsyncSerialPort::BaudRate::BR115200;
	temp["128000"] = QAsyncSerialPort::BaudRate::BR128000;
	temp["230400"] = QAsyncSerialPort::BaudRate::BR230400;
	temp["256000"] = QAsyncSerialPort::BaudRate::BR256000;
	temp["Unknown BaudRate"] = QAsyncSerialPort::BaudRate::BRUnknown;
	return temp;
}() };
const QMap <QSerialPort::StopBits, QString> QSettingsTabFocusseur::mSBtoStr{
	[]()->QMap<QSerialPort::StopBits, QString> {
	QMap<QSerialPort::StopBits, QString> temp;
	temp[QSerialPort::StopBits::OneStop] = "1";
	temp[QSerialPort::StopBits::OneAndHalfStop] = "1.5";
	temp[QSerialPort::StopBits::TwoStop] = "2";
	temp[QSerialPort::StopBits::UnknownStopBits] = "Unknown Stop Bits";
	return temp;
}() };
const QMap <QString, QSerialPort::StopBits> QSettingsTabFocusseur::mStrtoSB{
	[]()->QMap <QString, QSerialPort::StopBits> {
	QMap <QString, QSerialPort::StopBits> temp;
	temp["1"] = QSerialPort::StopBits::OneStop;
	temp["1.5"] = QSerialPort::StopBits::OneAndHalfStop;
	temp["2"] = QSerialPort::StopBits::TwoStop;
	temp["Unknown Stop Bits"] = QSerialPort::StopBits::UnknownStopBits;
	return temp;
}() };
const QMap <QSerialPort::Parity, QString> QSettingsTabFocusseur::mParitytoStr{
	[]()->QMap<QSerialPort::Parity, QString> {
	QMap<QSerialPort::Parity, QString> temp;
	temp[QSerialPort::Parity::NoParity] = "No Parity";
	temp[QSerialPort::Parity::EvenParity] = "Even Parity";
	temp[QSerialPort::Parity::OddParity] = "Odd Parity";
	temp[QSerialPort::Parity::MarkParity] = "Mark Parity";
	temp[QSerialPort::Parity::UnknownParity] = "Unknown Parity";
	return temp;
}() };
const QMap <QString, QSerialPort::Parity> QSettingsTabFocusseur::mStrtoParity{
	[]()->QMap<QString, QSerialPort::Parity> {
	QMap<QString, QSerialPort::Parity> temp;
	temp["No Parity"] = QSerialPort::Parity::NoParity;
	temp["Even Parity"] = QSerialPort::Parity::EvenParity;
	temp["Odd Parity"] = QSerialPort::Parity::OddParity;
	temp["Mark Parity"] = QSerialPort::Parity::MarkParity;
	temp["Unknown Parity"] = QSerialPort::Parity::UnknownParity;
	return temp;
}() };
const QMap <QSerialPort::FlowControl, QString> QSettingsTabFocusseur::mFCtoStr{
	[]()->QMap<QSerialPort::FlowControl, QString> {
	QMap<QSerialPort::FlowControl, QString> temp;
	temp[QSerialPort::FlowControl::NoFlowControl] = "No Flow Control";
	temp[QSerialPort::FlowControl::HardwareControl] = "HardWare Control";
	temp[QSerialPort::FlowControl::SoftwareControl] = "Software Control";
	temp[QSerialPort::FlowControl::UnknownFlowControl] = "Unknown Flow Control";
	return temp;
}() };
const QMap <QString, QSerialPort::FlowControl> QSettingsTabFocusseur::mStrtoFC{
	[]()->QMap<QString,QSerialPort::FlowControl> {
	QMap<QString,QSerialPort::FlowControl> temp;
	temp["No Flow Control"] = QSerialPort::FlowControl::NoFlowControl;
	temp["HardWare Control"] = QSerialPort::FlowControl::HardwareControl;
	temp["Software Control"] = QSerialPort::FlowControl::SoftwareControl;
	temp["Unknown Flow Control"] = QSerialPort::FlowControl::UnknownFlowControl;
	return temp;
}() };
const QMap <QSerialPort::DataBits, QString> QSettingsTabFocusseur::mDBtoStr{
	[]()->QMap<QSerialPort::DataBits, QString> {
	QMap<QSerialPort::DataBits, QString> temp;
	temp[QSerialPort::DataBits::Data5] = "5";
	temp[QSerialPort::DataBits::Data6] = "6";
	temp[QSerialPort::DataBits::Data7] = "7";
	temp[QSerialPort::DataBits::Data8] = "8";
	temp[QSerialPort::DataBits::UnknownDataBits] = "Unknown Data Bits";
	return temp;
}() };
const QMap <QString, QSerialPort::DataBits> QSettingsTabFocusseur::mStrtoDB{
	[]()->QMap<QString, QSerialPort::DataBits> {
	QMap<QString, QSerialPort::DataBits> temp;
	temp["5"] = QSerialPort::DataBits::Data5;
	temp["6"] = QSerialPort::DataBits::Data6;
	temp["7"] = QSerialPort::DataBits::Data7;
	temp["8"] = QSerialPort::DataBits::Data8;
	temp["Unknown Data Bits"] = QSerialPort::DataBits::UnknownDataBits;
	return temp;
}() };


QSettingsTabFocusseur::QSettingsTabFocusseur(QFocusseur &rangeFinder, QWidget * parent)
	:	QWidget(parent),
		mFocusseur{ &rangeFinder }
{
	mOpBox = new QGroupBox("Operation");
	mOpLayout = new QGridLayout;

	mAccOptions = new QGroupBox(tr("Acceleration options"));
	mAccLong = new QRadioButton(tr("long"));
	mAccMedium = new QRadioButton(tr("medium"));
	mAccShort = new QRadioButton(tr("short"));
	mAccPerso = new QRadioButton(tr("personnalized"));
	mAccPerso->setChecked(true);
	QVBoxLayout * accLayout = new QVBoxLayout;
	accLayout->addWidget(mAccLong);
	accLayout->addWidget(mAccMedium);
	accLayout->addWidget(mAccShort);
	accLayout->addWidget(mAccPerso);
	mAccOptions->setLayout(accLayout);
	mAccOptions->setEnabled(false);

	mSpeedOptions = new QGroupBox(tr("Speed options"));
	mSpeedFast = new QRadioButton(tr("fast"));
	mSpeedMedium = new QRadioButton(tr("medium"));
	mSpeedSlow = new QRadioButton(tr("slow"));
	mSpeedPerso = new QRadioButton(tr("personnalized"));
	mSpeedPerso->setChecked(true);
	QVBoxLayout * speedLayout = new QVBoxLayout;
	speedLayout->addWidget(mSpeedFast);
	speedLayout->addWidget(mSpeedMedium);
	speedLayout->addWidget(mSpeedSlow);
	speedLayout->addWidget(mSpeedPerso);
	mSpeedOptions->setLayout(speedLayout);
	mSpeedOptions->setEnabled(false);

	mTestOptions = new QGroupBox(tr("Test options"));
	mTestLong = new QRadioButton(tr("long"));
	mTestMedium = new QRadioButton(tr("medium"));
	mTestShort = new QRadioButton(tr("short"));
	mTestPerso = new QRadioButton(tr("personnalized"));
	mTestPerso->setChecked(true);
	QVBoxLayout * testLayout = new QVBoxLayout;
	testLayout->addWidget(mTestLong);
	testLayout->addWidget(mTestMedium);
	testLayout->addWidget(mTestShort);
	testLayout->addWidget(mTestPerso);
	mTestOptions->setLayout(testLayout);
	mTestOptions->setEnabled(false);
	mAccLength = new QDoubleSpinBox;
	mAccLength->setSingleStep(0.01);
	mAccLength->setAlignment(Qt::AlignRight);
	mAccLength->setEnabled(false);
	mAccLength->setMaximum(1000);
	mBaseSpeed = new QSpinBox;
	mBaseSpeed->setMaximum(100);
	mBaseSpeed->setAlignment(Qt::AlignRight);
	mBaseSpeed->setEnabled(false);
	mSpeed = new QSpinBox;
	mSpeed->setMaximum(100);
	mSpeed->setAlignment(Qt::AlignRight);
	mSpeed->setEnabled(false);
	mTestLength = new QDoubleSpinBox;
	mTestLength->setSingleStep(0.01);
	mTestLength->setAlignment(Qt::AlignRight);
	mTestLength->setEnabled(false);
	mTestLength->setMaximum(1000);
	mOpLayout->addWidget(mAccOptions, 0, 0);
	mOpLayout->addWidget(mSpeedOptions, 0, 2);
	mOpLayout->addWidget(mTestOptions, 0, 4);
	mOpLayout->addWidget(new QLabel("Acceleration length (mm) : "), 1, 0);
	mOpLayout->addWidget(new QLabel("Base speed for acceleration (0 - 100) : "), 2, 0);
	mOpLayout->addWidget(new QLabel("Speed (0 - 100) : "), 3, 0);
	mOpLayout->addWidget(new QLabel("Length of move during tests (mm) : "), 4, 0);
	mOpLayout->addWidget(mAccLength, 1, 4);
	mOpLayout->addWidget(mBaseSpeed, 2, 4);
	mOpLayout->addWidget(mSpeed, 3, 4);
	mOpLayout->addWidget(mTestLength, 4, 4);
	mOpBox->setLayout(mOpLayout);
	mPortName = new QSpinBox;
	mPortName->setMaximum(99);
	mPortName->setMinimum(1);
	mPortName->setAlignment(Qt::AlignRight);
	mCommBox = new QGroupBox("Communication");
	mCommLayout = new QFormLayout;
	mCommLayout->addRow(new QLabel("Port name"), mPortName);
	mCommBox->setLayout(mCommLayout);


	mButtonLayout = new QHBoxLayout;
	mRevert = new QPushButton("Revert");
	mRevert->setEnabled(false);
	mSave = new QPushButton("Save");
	mSave->setEnabled(false);
	mDefault = new QPushButton("Default");
	if (!mFocusseur->fileExists(QFocusseur::DEFAULT_INI)) {
		mDefault->setEnabled(false);
	}
	mOk = new QPushButton("Ok");
	mOk->setEnabled(false);
	mCancel = new QPushButton("Cancel");
	mCancel->setEnabled(false);
	mButtonLayout->addWidget(mRevert);
	mButtonLayout->addWidget(mSave);
	mButtonLayout->addWidget(mDefault);
	mButtonLayout->addStretch();
	mButtonLayout->addWidget(mOk);
	mButtonLayout->addWidget(mCancel);

	mQvbl = new QVBoxLayout;
	setLayout(mQvbl);
	mQvbl->addWidget(mOpBox);
	mQvbl->addWidget(mCommBox);
	mQvbl->addStretch();
	mQvbl->addLayout(mButtonLayout);

	setFields();
	
	connect(mAccLength, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &QSettingsTabFocusseur::updateAccLength);
	connect(mBaseSpeed, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &QSettingsTabFocusseur::updateBaseSpeed);
	connect(mSpeed, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &QSettingsTabFocusseur::updateSpeed);
	connect(mTestLength, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &QSettingsTabFocusseur::updateTestLength);
	connect(mPortName, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &QSettingsTabFocusseur::updatePortName);
	connect(this, &QSettingsTabFocusseur::changeMade, this, &QSettingsTabFocusseur::updateButtons);
	connect(mOk, &QPushButton::clicked, this, &QSettingsTabFocusseur::ok);
	connect(mCancel, &QPushButton::clicked, this, &QSettingsTabFocusseur::cancel);
	connect(mSave, &QPushButton::clicked, this, &QSettingsTabFocusseur::save);
	connect(mDefault, &QPushButton::clicked, this, &QSettingsTabFocusseur::defaultClicked);
	connect(mRevert, &QPushButton::clicked, this, &QSettingsTabFocusseur::revert);
	connect(mFocusseur, &QFocusseur::accLengthReceived, this, &QSettingsTabFocusseur::showAccLength);
	connect(mFocusseur, &QFocusseur::baseSpeedReceived, this, &QSettingsTabFocusseur::showBaseSpeed);
	connect(mFocusseur, &QFocusseur::speedReceived, this, &QSettingsTabFocusseur::showSpeed);
	connect(mFocusseur, &QFocusseur::testLengthReceived, this, &QSettingsTabFocusseur::showTestLength);
	connect(mFocusseur, &QFocusseur::maxAccLengthReceived, this, &QSettingsTabFocusseur::showMaxAccLength);
	connect(mFocusseur, &QFocusseur::maxTestLengthReceived, this, &QSettingsTabFocusseur::showMaxTestLength);
	connect(mFocusseur, &QFocusseur::minAccLengthReceived, this, &QSettingsTabFocusseur::showMinAccLength);
	connect(mFocusseur, &QFocusseur::minTestLengthReceived, this, &QSettingsTabFocusseur::showMinTestLength);
	connect(&mFocusseur->mSerial, &QAsyncSerialPort::connectionUpdated, this, &QSettingsTabFocusseur::showState);
	connect(mAccLong, &QRadioButton::clicked, this, &QSettingsTabFocusseur::accOptionChange);
	connect(mAccMedium, &QRadioButton::clicked, this, &QSettingsTabFocusseur::accOptionChange);
	connect(mAccShort, &QRadioButton::clicked, this, &QSettingsTabFocusseur::accOptionChange);
	connect(mAccPerso, &QRadioButton::clicked, this, &QSettingsTabFocusseur::accOptionChange);
	connect(mSpeedSlow, &QRadioButton::clicked, this, &QSettingsTabFocusseur::speedOptionChange);
	connect(mSpeedMedium, &QRadioButton::clicked, this, &QSettingsTabFocusseur::speedOptionChange);
	connect(mSpeedFast, &QRadioButton::clicked, this, &QSettingsTabFocusseur::speedOptionChange);
	connect(mSpeedPerso, &QRadioButton::clicked, this, &QSettingsTabFocusseur::speedOptionChange);
	connect(mTestLong, &QRadioButton::clicked, this, &QSettingsTabFocusseur::testOptionChange);
	connect(mTestMedium, &QRadioButton::clicked, this, &QSettingsTabFocusseur::testOptionChange);
	connect(mTestShort, &QRadioButton::clicked, this, &QSettingsTabFocusseur::testOptionChange);
	connect(mTestPerso, &QRadioButton::clicked, this, &QSettingsTabFocusseur::testOptionChange);
}
void QSettingsTabFocusseur::setFields() {
	QString temp = mFocusseur->mSettingsFocusseur.mPortName;
	temp.remove(0, 3);
	mPortName->setValue(temp.toInt());
}

void QSettingsTabFocusseur::accOptionChange()
{
	optionChange(mAccPerso, mAccLength, mAccLong, mAccMedium, mAccShort);
	optionChange(mAccPerso, mBaseSpeed, mAccShort, mAccMedium, mAccLong);
}
void QSettingsTabFocusseur::speedOptionChange()
{
	optionChange(mSpeedPerso, mSpeed, mSpeedFast, mSpeedMedium, mSpeedSlow);
}
void QSettingsTabFocusseur::testOptionChange()
{
	optionChange(mTestPerso, mTestLength, mTestLong, mTestMedium, mTestShort);
}

void QSettingsTabFocusseur::optionChange(QRadioButton * persoOpt,
	QDoubleSpinBox * toChange, QRadioButton * optMax,
	QRadioButton * optMed, QRadioButton * optMin)
{
	double max = toChange->maximum();
	double min = toChange->minimum();

	bool becomeEditable = persoOpt->isChecked();
	toChange->setEnabled(becomeEditable);

	if (optMax->isChecked())
	{
		toChange->setValue(max);
	}
	else if (optMed->isChecked())
	{
		toChange->setValue((max + min) / 2);
	}
	else if (optMin->isChecked())
	{
		toChange->setValue(min);
	}
}

void QSettingsTabFocusseur::optionChange(QRadioButton * persoOpt,
	QSpinBox * toChange, QRadioButton * optMax,
	QRadioButton * optMed, QRadioButton * optMin)
{
	int max = toChange->maximum();
	int min = toChange->minimum();

	bool becomeEditable = persoOpt->isChecked();
	toChange->setEnabled(becomeEditable);

	if (optMax->isChecked())
	{
		toChange->setValue(max);
	}
	else if (optMed->isChecked())
	{
		toChange->setValue((max + min) / 2);
	}
	else if (optMin->isChecked())
	{
		toChange->setValue(min);
	}
}

void QSettingsTabFocusseur::showMaxAccLength(QString response)
{
	response.remove(0, 5);
	response.remove(response.size() - 1, 1);
	mAccLength->setMaximum(response.toDouble() * 1000);
}

void QSettingsTabFocusseur::showMaxTestLength(QString response)
{
	response.remove(0, 5);
	response.remove(response.size() - 1, 1);
	mTestLength->setMaximum(response.toDouble() * 1000);
}

void QSettingsTabFocusseur::showMinAccLength(QString response)
{
	response.remove(0, 5);
	response.remove(response.size() - 1, 1);
	mAccLength->setMinimum(response.toDouble() * 1000);
}

void QSettingsTabFocusseur::showMinTestLength(QString response)
{
	response.remove(0, 5);
	response.remove(response.size() - 1, 1);
	mTestLength->setMinimum(response.toDouble() * 1000);
}

void QSettingsTabFocusseur::ok() {
	mFocusseur->applySettings();
}
void QSettingsTabFocusseur::save() {
	mFocusseur->mSettingsFocusseur.save(QFocusseur::CURRENT_INI);
	ok();
	mRevert->setEnabled(true);
}
void QSettingsTabFocusseur::cancel() {
	if (mFocusseur->fileExists(QFocusseur::CURRENT_INI)) {
		mFocusseur->mSettingsFocusseur.load(QFocusseur::CURRENT_INI);
	}
	else if (!mFocusseur->fileExists(QFocusseur::DEFAULT_INI)) {
		mFocusseur->mSettingsFocusseur.load(QFocusseur::DEFAULT_INI);
	}
	setFields();
}
void QSettingsTabFocusseur::defaultClicked() {
	mFocusseur->mSettingsFocusseur.load(QFocusseur::DEFAULT_INI);
	setFields();
	ok();
}
void QSettingsTabFocusseur::revert() {
	mFocusseur->mSettingsFocusseur.load(QFocusseur::TEMP_INI);
	setFields();
	mFocusseur->mSettingsFocusseur.save(QFocusseur::CURRENT_INI);
}
void QSettingsTabFocusseur::updateButtons() {
	mSave->setEnabled(true);
	mOk->setEnabled(true);
	mCancel->setEnabled(true);
}

void QSettingsTabFocusseur::updateAccLength()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	double entry = mAccLength->value() / 1000;
	mFocusseur->mSettingsFocusseur.mAccLenght = entry;
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::SetAccLength], QList<QVariant>{ entry });
	mFocusseur->mSerial.writeToBuffer(commandAndParams);
}
void QSettingsTabFocusseur::updateBaseSpeed()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	int entry = mBaseSpeed->value();
	mFocusseur->mSettingsFocusseur.mBaseSpeed = entry;
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::SetBaseSpeed], QList<QVariant>{ entry });
	mFocusseur->mSerial.writeToBuffer(commandAndParams);
}
void QSettingsTabFocusseur::updateSpeed()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	int entry = mSpeed->value();
	mFocusseur->mSettingsFocusseur.mSpeed = entry;
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::SetSpeed], QList<QVariant>{ entry });
	mFocusseur->mSerial.writeToBuffer(commandAndParams);
}
void QSettingsTabFocusseur::updateTestLength()
{
	mFocusseur->mSerial.setDevelopmentMode(false);
	double entry = mTestLength->value() / 1000;
	mFocusseur->mSettingsFocusseur.mTestLength = entry;
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocusseur->mSerialCommands[QFocusseur::Command::SetTestLength], QList<QVariant>{ entry });
	mFocusseur->mSerial.writeToBuffer(commandAndParams);
}
void QSettingsTabFocusseur::showAccLength(QString response)
{
	response.remove(0, 1);
	response.remove(response.size() - 1, 1);
	double rep = response.toDouble() * 1000;
	mAccLength->setValue(rep);
	int baseSpeed = mBaseSpeed->value();

	double maxL = mAccLength->maximum();
	double minL = mAccLength->minimum();
	int maxS = mBaseSpeed->maximum();
	int minS = mBaseSpeed->minimum();

	if (baseSpeed == maxS && rep == minL)
	{
		mAccShort->setChecked(true);
	}
	else if (baseSpeed == (maxS + minS) / 2 && rep == (maxL + minL) / 2)
	{
		mAccMedium->setChecked(true);
	}
	else if (baseSpeed == minS && rep == maxL)
	{
		mAccLong->setChecked(true);
	}
	else
	{
		mAccPerso->setChecked(true);
	}
}
void QSettingsTabFocusseur::showBaseSpeed(QString response)
{
	response.remove(0, 1);
	response.remove(response.size() - 1, 1);
	int rep = response.toInt();
	mBaseSpeed->setValue(rep);
	double accLen = mAccLength->value();

	double maxL = mAccLength->maximum();
	double minL = mAccLength->minimum();
	int maxS = mBaseSpeed->maximum();
	int minS = mBaseSpeed->minimum();

	if (rep == maxS && accLen == minL)
	{
		mAccShort->setChecked(true);
	}
	else if (rep == (maxS + minS) / 2 && accLen == (maxL + minL) / 2)
	{
		mAccMedium->setChecked(true);
	}
	else if (rep == minS && accLen == maxL)
	{
		mAccLong->setChecked(true);
	}
	else
	{
		mAccPerso->setChecked(true);
	}
}
void QSettingsTabFocusseur::showSpeed(QString response)
{
	response.remove(0, 1);
	response.remove(response.size() - 1, 1);
	int rep = response.toInt();
	mSpeed->setValue(rep);

	int max = mSpeed->maximum();
	int min = mSpeed->minimum();

	if (rep == max)
	{
		mSpeedFast->setChecked(true);
	}
	else if (rep == (max + min) / 2)
	{
		mSpeedMedium->setChecked(true);
	}
	else if (rep == min)
	{
		mSpeedSlow->setChecked(true);
	}
	else
	{
		mSpeedPerso->setChecked(true);
	}
}
void QSettingsTabFocusseur::showTestLength(QString response)
{
	response.remove(0, 1);
	response.remove(response.size() - 1, 1);
	double rep = response.toDouble() * 1000;
	mTestLength->setValue(rep);

	double max = mTestLength->maximum();
	double min = mTestLength->minimum();

	if (rep == max)
	{
		mTestLong->setChecked(true);
	}
	else if (rep == (max + min) / 2)
	{
		mTestMedium->setChecked(true);
	}
	else if (rep == min)
	{
		mTestShort->setChecked(true);
	}
	else
	{
		mTestPerso->setChecked(true);
	}
}

void QSettingsTabFocusseur::showState(bool connected)
{
	mAccOptions->setEnabled(connected);
	mSpeedOptions->setEnabled(connected);
	mTestOptions->setEnabled(connected);

	mAccLength->setEnabled(connected);
	mBaseSpeed->setEnabled(connected);
	mSpeed->setEnabled(connected);
	mTestLength->setEnabled(connected);
	if (connected)
	{
		mFocusseur->mSerial.setDevelopmentMode(false);
		mFocusseur->sendCommandNoParams(QFocusseur::Command::GetMaxAccLength);
		mFocusseur->mSerial.setDevelopmentMode(false);
		mFocusseur->sendCommandNoParams(QFocusseur::Command::GetMaxTestLength);
		mFocusseur->mSerial.setDevelopmentMode(false);
		mFocusseur->sendCommandNoParams(QFocusseur::Command::GetMinAccLength);
		mFocusseur->mSerial.setDevelopmentMode(false);
		mFocusseur->sendCommandNoParams(QFocusseur::Command::GetMinTestLength);
		mFocusseur->mSerial.setDevelopmentMode(false);
		mFocusseur->sendCommandNoParams(QFocusseur::Command::GetAccLength);
		mFocusseur->mSerial.setDevelopmentMode(false);
		mFocusseur->sendCommandNoParams(QFocusseur::Command::GetBaseSpeed);
		mFocusseur->mSerial.setDevelopmentMode(false);
		mFocusseur->sendCommandNoParams(QFocusseur::Command::GetSpeed);
		mFocusseur->mSerial.setDevelopmentMode(false);
		mFocusseur->sendCommandNoParams(QFocusseur::Command::GetTestLength);
	}
}

void QSettingsTabFocusseur::updatePortName(int val) {
	QString * portName = new QString("COM");
	portName->append(QString::number(val));
	mFocusseur->mSettingsFocusseur.mPortName = *portName;
	emit changeMade();
}

QSettingsTabFocusseur::~QSettingsTabFocusseur() {

}
