#include "..\..\..\include\Serial Port\Focuser\QOperationsOutputWidgetFocusseur.h"
#include <QString>
#include "..\..\..\include\Serial Port\Focuser\QFocusseur.h"

QOperationsOutputWidgetFocusseur::QOperationsOutputWidgetFocusseur(QFocusseur * rangeFinder, QWidget * parent)
	: QWidget(parent), mRangeFinder{ rangeFinder }
{
	mGrid = new QGridLayout;
	setLayout(mGrid);
	mGrid->addWidget(new QLabel("Position : "), 0, 0);
	mPosition = new QLineEdit;
	mPosition->setReadOnly(true);
	mPosition->setAlignment(Qt::AlignRight);
	mGrid->addWidget(mPosition, 0, 1);
	mGrid->addWidget(new QLabel("mm"), 0, 2);

	connect(mRangeFinder, &QFocusseur::positionReceived, this, static_cast<void(QOperationsOutputWidgetFocusseur::*)(QString)>(&QOperationsOutputWidgetFocusseur::showPosition));
}

void QOperationsOutputWidgetFocusseur::showPosition(int noKey)
{
	mPosition->setText(QString::number(noKey));
}

void QOperationsOutputWidgetFocusseur::showPosition(QString response)
{
	QString * showString{ new QString };
	int i{ 1 };
	QChar temp{ response.at(i) };
	while (temp != '#')
	{
		showString->append(temp);
		++i;
		temp = response.at(i);
	}
	double convert = showString->toDouble() * 1000;
	mPosition->setText(QString::number(convert));
}

QOperationsOutputWidgetFocusseur::~QOperationsOutputWidgetFocusseur() {

}
