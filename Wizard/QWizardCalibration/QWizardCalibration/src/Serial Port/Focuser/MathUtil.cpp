#include "..\..\..\include\Serial Port\Focuser\MathUtil.h"
#include "Math.h"

MathUtil::MathUtil() 
: QObject()
{ }

const double MathUtil::pi{ 3.1415926535897932384626433832795 };

const double MathUtil::pi_1_2{ 0.5* pi };

const double MathUtil::pi_2{ 2.0 * pi };

const double MathUtil::deg2Rad{ 0.01745329251994329576923690768489 };

const double MathUtil::rad2Deg{ 57.295779513082320876798154814105 };