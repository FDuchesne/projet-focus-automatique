#include "..\..\..\include\Serial Port\Focuser\QDebugCommandLineFocusseur.h"

QDebugCommandLineFocusseur::QDebugCommandLineFocusseur(QString terminator, QWidget * parent) 
	:	QWidget(parent), 
		mTerminator{ terminator }, 
		localEchoEnabled(true)
{
	mCommand = new QLabel;
	mCommand->setText("Command :");
	mEcho = new QLabel;
	mEcho->setText("Local Echo");
	mCommandLine = new QLineEdit;
	mSendButton = new QPushButton;
	mSendButton->setText("Send");
	mEchoCB = new QCheckBox;
	mEchoCB->setChecked(true);
	mQhbl = new QHBoxLayout;
	mQhbl->addWidget(mCommand);
	mQhbl->addWidget(mCommandLine);
	mQhbl->addWidget(mSendButton);
	mQhbl->addWidget(mEcho);
	mQhbl->addWidget(mEchoCB);
	setLayout(mQhbl);

	connect(mSendButton, &QPushButton::clicked, this, &QDebugCommandLineFocusseur::sendInput);
	connect(mCommandLine, &QLineEdit::returnPressed, this, &QDebugCommandLineFocusseur::sendInput);
}

void QDebugCommandLineFocusseur::writeCmd(QString s) {
	mCommandLine->clear();
	mCommandLine->setText(s);
	mCommandLine->setFocus();
}

void QDebugCommandLineFocusseur::sendInput()
{
	QString in = mCommandLine->text();
	input = new QString(in);
	mCommandLine->clear();
	emit messageSent(in + mTerminator);
}
QDebugCommandLineFocusseur::~QDebugCommandLineFocusseur() {

}
