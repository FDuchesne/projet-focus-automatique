﻿#include "..\..\..\include\Serial Port\Focuser\QRFStatusBarFocusseur.h"
#include "..\..\..\include\Serial Port\Global\QLEDLight.h"
#include "..\..\..\include\Serial Port\Focuser\QFocusseur.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QICon>
#include <QTimer>

QRFStatusBarFocusseur::QRFStatusBarFocusseur(QFocusseur * focusseur, QWidget * parent)
	:	QStatusBar(parent),
		mFocusseur{ focusseur }
{
	mConnection = new QLEDLight(QSize(24, 24));
	mCommunication = new QLEDLight(QSize(24, 24));

	mConnection->addStatus(0, "Not Connected", QIcon(":/QSerialComm/redLed"));
	mConnection->addStatus(1, "Connected", QIcon(":/QSerialComm/greenLed"));
	mConnection->setStatus(0);

	mCommunication->addStatus(0, "No communication", QIcon(":/QSerialComm/redLed"));
	mCommunication->addStatus(1, "Communicating", QIcon(":/QSerialComm/greyLed"));
	mCommunication->addStatus(2, "Ready for communication", QIcon(":/QSerialComm/greenLed"));
	mCommunication->setStatus(0);

	addPermanentWidget(mConnection);
	addPermanentWidget(mCommunication);

	connect(&mFocusseur->mSerial, &QAsyncSerialPort::connectionUpdated, this, &QRFStatusBarFocusseur::updateConnectionLED);
	connect(&mFocusseur->mSerial, &QAsyncSerialPort::messageSent, this, &QRFStatusBarFocusseur::updateToCommunicatingStatus);
	connect(mFocusseur, &QFocusseur::messageReceived, this, &QRFStatusBarFocusseur::updateMsg);
	connect(mFocusseur, &QFocusseur::matchingResponseReceived, this, &QRFStatusBarFocusseur::updateMsg);
	connect(mFocusseur, &QFocusseur::matchingResponseReceived, this, &QRFStatusBarFocusseur::updateToReadyStatus);
	connect(&mFocusseur->mSerial, &QAsyncSerialPort::updated, this, &QRFStatusBarFocusseur::updateMsg);
	connect(&mFocusseur->mSerial, &QCommandSerialPort::developmentModeSwitched, this, &QRFStatusBarFocusseur::updateDevMode);
}

QRFStatusBarFocusseur::~QRFStatusBarFocusseur() {

}

void QRFStatusBarFocusseur::updateConnectionLED(bool connected) {
	if (connected) {
		mConnection->setStatus(1);
		mCommunication->setStatus(2);
	}
	else {
		mConnection->setStatus(0);
		mCommunication->setStatus(0);
	}
}

void QRFStatusBarFocusseur::updateToReadyStatus()
{
	mCommunication->setStatus(2);
}

void QRFStatusBarFocusseur::updateToCommunicatingStatus()
{
	mCommunication->setStatus(1);
}

void QRFStatusBarFocusseur::updateToNoCommunicationStatus()
{
	mCommunication->setStatus(0);
}

void QRFStatusBarFocusseur::updateMsg(QString message) {
	showMessage(message, 5000);
	mCommunication->setStatus(2);
}

void QRFStatusBarFocusseur::updateDevMode(bool devMode) {
	if (devMode) {
	}
	else {
	}
}
