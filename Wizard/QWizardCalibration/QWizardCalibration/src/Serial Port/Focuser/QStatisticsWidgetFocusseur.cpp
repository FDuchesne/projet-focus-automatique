#include "..\..\..\include\Serial Port\Focuser\QStatisticsWidgetFocusseur.h"
#include "..\..\..\include\Serial Port\Global\QMovingStatistics.h"
#include <QProgressBar>
#include <QStackedLayout>

QStatisticsWidgetFocusseur::QStatisticsWidgetFocusseur(QWidget * parent)
	: QWidget(parent)
{

	mQvbl = new QVBoxLayout;
	setLayout(mQvbl);

	mProgress = new QProgressBar;
	mProgress->setOrientation(Qt::Horizontal);
	mFull = new QLabel("Full");
	mStackLayout = new QStackedLayout;
	mStackLayout->insertWidget(0, mProgress);
	mStackLayout->insertWidget(1, mFull);
	mStackLayout->setCurrentIndex(0);

	mQhbl[0] = new QHBoxLayout;
	mQhbl[0]->addWidget(new QLabel("Buffer :"));
	mQhbl[0]->addLayout(mStackLayout);
	mQhbl[0]->addStretch();
	mReset = new QPushButton("Reset");
	mQhbl[0]->addWidget(mReset);
	mQvbl->addLayout(mQhbl[0]);

	mGrid = new QGridLayout;
	mQvbl->addLayout(mGrid);


	mGrid->addWidget(new QLabel("Minimum : "), 0, 0);
	mGrid->addWidget(new QLabel("m"), 0, 2);

	mGrid->addWidget(new QLabel("Maximum : "), 1, 0);
	mGrid->addWidget(new QLabel("m"), 1, 2);

	mGrid->addWidget(new QLabel("Average : "), 2, 0);
	mGrid->addWidget(new QLabel("m"), 2, 2);

	mGrid->addWidget(new QLabel("Standard Deviation : "), 3, 0);
	mStdDev = new QLineEdit;
	mStdDev->setReadOnly(true);
	mGrid->addWidget(mStdDev, 3, 1);
	mGrid->addWidget(new QLabel("m"), 3, 2);


	connect(mReset, &QPushButton::clicked, this, &QStatisticsWidgetFocusseur::resetPressed);
}

QStatisticsWidgetFocusseur::~QStatisticsWidgetFocusseur() {

}

void QStatisticsWidgetFocusseur::showStats(QMovingStatistics const &stats) {
	mStdDev->setText(QString::number(stats.stdDeviation()));

	mProgress->setMaximum(stats.data().size());
	mProgress->setValue(stats.data().count());
	if (mProgress->value() == mProgress->maximum()) {
		mStackLayout->setCurrentIndex(1);
	}
	else
	{
		mStackLayout->setCurrentIndex(0);
	}
}

void QStatisticsWidgetFocusseur::updateProgBar(QMovingStatistics const &stats) {

}