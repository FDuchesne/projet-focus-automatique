#include "..\..\..\include\Serial Port\Focuser\QFocusseurSettings.h"
#include <QSettings>
#include <QCoreApplication>

QString const QFocusseurSettings::PORTNAME{ "portName" };
QString const QFocusseurSettings::BAUDRATE{ "baudRate" };
QString const QFocusseurSettings::STOPBITS{ "stopBits" };
QString const QFocusseurSettings::DATABITS{ "dataBits" };
QString const QFocusseurSettings::PARITY{ "parity" };
QString const QFocusseurSettings::FLOWCONTROL{ "flowControl" };
QString const QFocusseurSettings::ACCLENGTH{ "accLength" };
QString const QFocusseurSettings::BASESPEED{ "baseSpeed" };
QString const QFocusseurSettings::SPEED{ "speed" };
QString const QFocusseurSettings::TESTLENGTH{ "testLength" };

QFocusseurSettings::QFocusseurSettings(QObject * parent)
	:	QObject(parent),
		mPortName{ QString() },
		mBaudRate{ QAsyncSerialPort::BaudRate::BRUnknown },
		mStopBits{ QSerialPort::StopBits::UnknownStopBits },
		mDataBits{ QSerialPort::DataBits::UnknownDataBits },
		mParity{ QSerialPort::Parity::UnknownParity },
		mFlowControl{ QSerialPort::FlowControl::UnknownFlowControl },
		mAccLenght{ -1 },
		mBaseSpeed{ -1 },
		mSpeed{ -1 },
		mTestLength{ -1 }
{

}

QFocusseurSettings::~QFocusseurSettings() {
	
}


void QFocusseurSettings::save(QString fileName) {
	QSettings settings(fileName + ".ini", QSettings::Format::IniFormat);
	settings.setValue(PORTNAME, mPortName);
	settings.setValue(BAUDRATE, static_cast<int>(mBaudRate));
	settings.setValue(STOPBITS, mStopBits);
	settings.setValue(DATABITS, mDataBits);
	settings.setValue(PARITY, mParity);
	settings.setValue(FLOWCONTROL, mFlowControl);
}

void QFocusseurSettings::load(QString fileName) {
	QSettings settings(fileName + ".ini", QSettings::Format::IniFormat);
	mPortName = settings.value(PORTNAME).toString();
	mBaudRate = static_cast<QAsyncSerialPort::BaudRate>(settings.value(BAUDRATE).toInt());
	mStopBits = static_cast<QSerialPort::StopBits>(settings.value(STOPBITS).toInt());
	mDataBits = static_cast<QSerialPort::DataBits>(settings.value(DATABITS).toInt());
	mParity = static_cast<QSerialPort::Parity>(settings.value(PARITY).toInt());
	mFlowControl = static_cast<QSerialPort::FlowControl>(settings.value(FLOWCONTROL).toInt());

}

bool QFocusseurSettings::isValid() {
	// Serial Port
	if (mPortName.isNull()) {
		return false;
	}
	if (mBaudRate == QAsyncSerialPort::BaudRate::BRUnknown) {
		return false;
	}
	if (mStopBits == QSerialPort::StopBits::UnknownStopBits) {
		return false;
	}
	if (mDataBits == QSerialPort::DataBits::UnknownDataBits) {
		return false;
	}
	if (mParity == QSerialPort::Parity::UnknownParity) {
		return false;
	}
	if (mFlowControl == QSerialPort::FlowControl::UnknownFlowControl) {
		return false;
	}


	// Focuser
	if (mAccLenght < 0)
	{
		return false;
	}
	if (mBaseSpeed < 0 || mBaseSpeed > 100)
	{
		return false;
	}
	if (mSpeed < 0 || mSpeed > 100)
	{
		return false;
	}
	if (mTestLength < 0)
	{
		return false;
	}
	return true;
}