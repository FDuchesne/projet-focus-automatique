﻿#include "..\..\..\include\Serial Port\Global\QLEDLight.h"


#include <QPainter>


QLEDLight::QLEDLight(QSize const & size, QWidget *parent)
	:	QLabel(parent), 
		mSize{ size },
		mStatus{ -1 }
{
	setFixedSize(mSize);
}

QLEDLight::~QLEDLight()
{

}

bool QLEDLight::addStatus(int status, QString const & description, QIcon const & icon)
{
	if (mStatusInfo.contains(status)) {
		return false;
	}

	mStatusInfo[status] = QPair<QString, QIcon>(description, icon);
	return true;
}
bool QLEDLight::setStatus(int status)
{
	if (!mStatusInfo.contains(status)) {
		return false;
	}

	if (status != mStatus) {
		mStatus = status;
		mDescription = mStatusInfo[mStatus].first;
		mIcon = mStatusInfo[mStatus].second;

		setToolTip(mDescription);
		repaint();
	}

	return true;
}

void QLEDLight::paintEvent(QPaintEvent * event)
{
	QPainter painter(this);
	mIcon.paint(&painter, rect());
}

