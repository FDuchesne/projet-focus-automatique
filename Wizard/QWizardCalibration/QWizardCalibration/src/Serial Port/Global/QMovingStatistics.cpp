#include "..\..\..\include\Serial Port\Global\QMovingStatistics.h"

#include <math.h>
#include <QString>

QMovingStatistics::QMovingStatistics(int bufferSize, QRegularExpression regex)
	:	mData{ bufferSize },
		mRegex{ regex },
		mAverage { 0 },
		mStdDeviation{ 0 }, 
		mMin{ 0 }, 
		mMax{ 0 },
		mNMin{ 0 },
		mNMax{ 0 },
		mSum{ 0 },
		mSumXPow2{ 0 }
{

}

QMovingStatistics::~QMovingStatistics()
{

}

void QMovingStatistics::reset() {
	mData.reset();
	mAverage = 0;
	mStdDeviation = 0;
	mMin = 0;
	mMax = 0;
	mNMin = 0;
	mNMax = 0;
	mSum = 0;
	mSumXPow2 = 0;
	emit updated(*this);
}

void QMovingStatistics::addValue(QString const &line) {
	QRegularExpressionMatch match = mRegex.match(line);
	if (match.hasMatch()) {
		QString firstMatch = match.captured(0);
		firstMatch.remove(QRegularExpression(QString::fromUtf8("[-`~!@#$%^&*()_—+=|:;<>«»,?/{}\'\"\\[]")));
		//firstMatch.remove(QRegularExpression(QString::fromUtf8("[-`~!@#$%^&*()_—+=|:;<>«»,?/{}\'\"\\\[\\\]\\\\]")));
		addValue(firstMatch.toDouble());
	}
}

void QMovingStatistics::addValue(double value) {
	double firstValue = std::numeric_limits<double>::min();
	if (mData.count() == mData.size()) {
		firstValue = mData[0];
		mSum -= firstValue;
		mSumXPow2 -= firstValue*firstValue;
	}
	mSum += value;
	mSumXPow2 += value*value;

	mData.add(value);

	calcAverage();
	calcStdDeviation();
	calcMin(value, firstValue);
	calcMax(value, firstValue);
	emit updated(*this);
}

void QMovingStatistics::calcAverage() {
	mAverage = mSum / mData.count();
}

void QMovingStatistics::calcStdDeviation() {
	double n = mData.count();
	double variance = (n * mSumXPow2 - mSum*mSum) / (n*(n - 1));
	mStdDeviation = sqrt(variance);
}

void QMovingStatistics::calcMin(double addedValue, double removedValue) {
	if (mData.count() == 1) {
		mMin = addedValue;
		mNMin = 1;
	}
	else {
		updateMin(addedValue);
		if (removedValue == mMin) {
			--mNMin;
			if (mNMin == 0) {
				mMin = std::numeric_limits<double>::max();
				for (int i = 0; i < mData.count(); ++i) {
					updateMin(mData[i]);
				}
			}
		}
	}
}

void QMovingStatistics::calcMax(double addedValue, double removedValue) {
	if (mData.count() == 1) {
		mMax = addedValue;
		mNMax = 1;
	}
	else {
		updateMax(addedValue);
		if (removedValue == mMax) {
			--mNMax;
			if (mNMax == 0) {
				mMax = std::numeric_limits<double>::min();
				for (int i = 0; i < mData.count(); ++i) {
					updateMax(mData[i]);
				}
			}
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////
// Helper Methods
//////////////////////////////////////////////////////////////////////////////////////////////

void QMovingStatistics::updateMin(double potentialMin) {
	if (potentialMin < mMin) {
		mMin = potentialMin;
		mNMin = 1;
	}
	else if (potentialMin == mMin) {
		++mNMin;
	}
}

void QMovingStatistics::updateMax(double potentialMax) {
	if (potentialMax > mMax) {
		mMax = potentialMax;
		mNMax = 1;
	}
	else if (potentialMax == mMax) {
		++mNMax;
	}
}
