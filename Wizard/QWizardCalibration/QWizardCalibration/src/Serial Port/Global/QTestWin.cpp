#include "..\..\..\include\Serial Port\Global\QTestWin.h"

#include <QDebug>
#include <QPair>
#include "..\..\..\include\Serial Port\Global\QSerialCommand.h"

QTestWin::QTestWin(QRangeFinderAR2000 * r, QWidget *parent)
	: QWidget(parent), rf{ r }
{
	btnShowCommands = new QPushButton("Show Commands", this);
	btnShowCommands->setGeometry(QRect(QPoint(0, 0), QSize(100, 100)));
	connect(btnShowCommands, &QPushButton::released, this, &QTestWin::showCommandsBuffer);

	btnShowBuffer = new QPushButton("Show Buffer", this);
	btnShowBuffer->setGeometry(QRect(QPoint(100, 0), QSize(100, 100)));
	connect(btnShowBuffer, &QPushButton::released, this, &QTestWin::showBuffer);

	btnTest = new QPushButton(rf->mTestCommand.command(), this);
	btnTest->setGeometry(QRect(QPoint(300, 0), QSize(100, 100)));
	connect(btnTest, &QPushButton::released, this, &QTestWin::test);

	btnTest2 = new QPushButton(rf->mTestCommand2.command(), this);
	btnTest2->setGeometry(QRect(QPoint(400, 0), QSize(100, 100)));
	connect(btnTest2, &QPushButton::released, this, &QTestWin::test2);

	btnTest3 = new QPushButton(rf->mTestCommand3.command(), this);
	btnTest3->setGeometry(QRect(QPoint(500, 0), QSize(100, 100)));
	connect(btnTest3, &QPushButton::released, this, &QTestWin::test3);

	btnTest4 = new QPushButton(rf->mTestCommand4.command(), this);
	btnTest4->setGeometry(QRect(QPoint(600, 0), QSize(100, 100)));
	connect(btnTest4, &QPushButton::released, this, &QTestWin::test4);

	btnTest5 = new QPushButton(rf->mTestCommand5.command(), this);
	btnTest5->setGeometry(QRect(QPoint(700, 0), QSize(100, 100)));
	connect(btnTest5, &QPushButton::released, this, &QTestWin::test5);


	//btnPushBuffer = new QPushButton("Show Push Buffer", this);
	//btnPushBuffer->setGeometry(QRect(QPoint(100, 0), QSize(100, 100)));
	//connect(btnPushBuffer, &QPushButton::released, this, &QTestWin::showPushBuffer);

	//btnPA = new QPushButton("PA", this);
	//btnPA->setGeometry(QRect(QPoint(300, 0), QSize(100, 100)));
	//connect(btnPA, &QPushButton::released, this, &QTestWin::pa);

	//btnSDT = new QPushButton("SDT", this);
	//btnSDT->setGeometry(QRect(QPoint(400, 0), QSize(100, 100)));
	//connect(btnSDT, &QPushButton::released, this, &QTestWin::sdt);

	//btnLaserOn = new QPushButton("LN", this);
	//btnLaserOn->setGeometry(QRect(QPoint(500, 0), QSize(100, 100)));
	//connect(btnLaserOn, &QPushButton::released, this, &QTestWin::ln);

	//btnLaserOff = new QPushButton("LF", this);
	//btnLaserOff->setGeometry(QRect(QPoint(600, 0), QSize(100, 100)));
	//connect(btnLaserOff, &QPushButton::released, this, &QTestWin::lf);


}

//void QTestWin::createButton(QString title, void(QPushButton::*signal)(void), void(QTestWin::*slot)(void), int x, int y, int width, int height) {
//	buttons.append(QPushButton(title, this));
//	buttons.last().setGeometry(QRect(QPoint(x, y), QSize(width, height)));
//	connect(&buttons.last(), signal, this, slot);
//}


void QTestWin::showCommandsBuffer() {
	qDebug() << "-----Begining of commands buffer-----";
	int i = 0;
	for (auto commandAndParams : rf->mSerial.mCommandsSent) {
		QSerialCommand &command = commandAndParams.first;
		qDebug() << "Commands Buffer " << i << " : " << command.command();
		++i;
	}
	qDebug() << "-----End of commands buffer-----";
}

void QTestWin::showBuffer() {
	qDebug() << "Buffer : " << rf->mSerial.mResponses;
}

void QTestWin::test() {
	rf->testCommand();
}
void QTestWin::test2() {
	rf->testCommand2();
}
void QTestWin::test3() {
	rf->testCommand3();
}
void QTestWin::test4() {
	rf->testCommand4();
}
void QTestWin::test5() {
	rf->testCommand5();
}


QTestWin::~QTestWin()
{

}
