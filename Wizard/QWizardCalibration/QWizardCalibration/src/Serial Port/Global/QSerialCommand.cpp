#include "..\..\..\include\Serial Port\Global\QSerialCommand.h"
#include <QDebug>


QSerialCommand::QSerialCommand(QString command, QString name, IOType ioType, int nParam, SerialOperationMode::BlockingMode blockingMode, SerialOperationMode::FluxMode fluxMode, QString eolCar, QString separator, QString family, QString shortDesc,
	QRegularExpression returnExp, QString desc, QString tooltip)
	:	mIsString{ true },
		mCommand{ command.toLatin1() },
		mName{ name },
		mNParam{ nParam },
		mReturnExp{ returnExp },
		mSeparator{ separator },
		mEOLCar{ eolCar },
		mFamily{ family },
		mShortDesc{ shortDesc },
		mDesc{ desc },
		mToolTip{ tooltip },
		mOperationMode{ blockingMode, fluxMode },
		mIOType{ ioType }
{
	mCommand = command.toLatin1();
	if (tooltip == "") {
		mToolTip = shortDesc;
	}
	else {
		mToolTip = tooltip;
	}
}

QSerialCommand::QSerialCommand(QByteArray command, QString name, IOType ioType, int nParam, SerialOperationMode::BlockingMode blockingMode, SerialOperationMode::FluxMode fluxMode, QString eolCar, QString separator, QString family, QString shortDesc,
	QList<QByteArray> expectedResponses, QString desc, QString tooltip)
	:	mIsString{ false },
		mCommand{ command },
		mName{ name },
		mNParam{ nParam },
		mSeparator{ separator },
		mEOLCar{ eolCar },
		mFamily{ family },
		mShortDesc{ shortDesc },
		mDesc{ desc },
		mToolTip{ tooltip },
		mOperationMode{ blockingMode, fluxMode },
		mIOType{ ioType },
		mExpectedResponses{ expectedResponses }
{
	if (tooltip == "") {
		mToolTip = shortDesc;
	}
	else {
		mToolTip = tooltip;
	}
}

QSerialCommand::~QSerialCommand()
{

}


inline bool QSerialCommand::operator==(QSerialCommand const & command) const {
	return mName == command.mName && mCommand == command.mCommand;
}


QByteArray QSerialCommand::commandToSend(const QList<QVariant> & params) const {
	QByteArray command = mCommand;
	for (const QVariant &param : params) {
		if (param.isValid()) {
			QString sParam;
			if (param.canConvert<int>() || param.canConvert<double>()) {
				sParam = QString::number(param.toDouble(), 'g', 10);
			}
			else {
				sParam = param.toString();
			}
			command += mSeparator.toLatin1() + sParam.toLatin1();
		}
	}
	command += mEOLCar.toLatin1();
	return command;
}

void QSerialCommand::addPushModeStopCommand(QSerialCommand const * command) {
	mPushModeStopCommands.append(command);
}

bool QSerialCommand::stopsPushMode(QSerialCommand const &command) const {
	for (auto stopCommand : mPushModeStopCommands) {
		qDebug() << command.name();
		qDebug() << stopCommand->name();
		if (command == *stopCommand) {
			return true;
		}
	}
	return false;
}

QByteArray QSerialCommand::getFirstMatch(QByteArray const &buffer) {
	if (mIsString) {
		QRegularExpressionMatch match = mReturnExp.match(buffer);
		if (match.hasMatch()) {
			QString firstMatch = match.captured(0);
			return firstMatch.toLatin1();
		}
		return QByteArray();
	}
	else {
		QByteArray response;
		int index = std::numeric_limits<int>::max();
		for (auto ba : mExpectedResponses) {
			if (buffer.indexOf(ba) != -1 && buffer.indexOf(ba) < index) {
				response = ba;
				index = buffer.indexOf(ba);
			}
		}
		return response;
	}
}