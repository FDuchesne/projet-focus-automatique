#include "..\include\QAlgoFocus.h"
#include "..\include\Serial Port\Focuser\QFocusseur.h"
#include "..\include\QImageDisplay.h"
#include "..\include\Camera\MILSimpleInterface.h"
#include "..\include\Camera\QMatroxSimpleGrabber.h"
#include "..\include\Serial Port\Focuser\QFocusseurGUI.h"
#include <QImage>
#include <QApplication>
#include <QDebug>

QAlgoFocus::QAlgoFocus(QObject *parent)
	: QObject(parent),
	mFocuseur{ new QFocusseur() },
	mMatroxSimpleGrabber{ new QMatroxJaiSp20000Grabber },
	mFocuserLength{ 120 },
	//mIndexImage{ 0 },
	//nbBoucle{ 0 },
	mMaxNbOfLoop{ 10 }
{
	connect(mFocuseur, &QFocusseur::positionReceived, this, &QAlgoFocus::takeImage);
}

QAlgoFocus::~QAlgoFocus()
{
	mFocuseur->mSerial.closeSerialPort();
	delete mMatroxSimpleGrabber;
}

void QAlgoFocus::moveFocuseur(double pos) {
	mFocuseur->mSerial.setDevelopmentMode(false);
	pos /= 1000.0;
	QPair<QSerialCommand const &, QList<QVariant>> commandAndParams(*mFocuseur->mSerialCommands[QFocusseur::Command::GoToAbs], QList<QVariant>{ pos });
	mFocuseur->mSerial.writeToBuffer(commandAndParams);
}

void QAlgoFocus::takeImage(QString message) {
	QImage image;
	image.convertToFormat(QImage::Format::Format_ARGB32);

	//reset


	static int currentFile = 0;
	mMatroxSimpleGrabber->grab(image);
	//if (mTesting) {
		//if (currentFile == 40) {
		//	currentFile = 0;
		//}
		//image = QImage(mFileNames[currentFile], "JPG");
		//currentFile += 1;
	//}
	QSize imageSize = image.size();
	QImage grayScale = QImage(imageSize, QImage::Format::Format_Grayscale8);
	colorToGray(image, imageSize, grayScale);
	//currentFile += 1;
	if (!multipleSpots) {
		mPoints[mIndexImage].mColorVariationLevel = colorVariationLevel(grayScale, mCol.at(0), mRow.at(0), mWidth.at(0), mHeight.at(0));
	}
	else {
		mPoints[mIndexImage].mColorVariationLevel = colorVariationLevelOfMultipleSpots(grayScale);
	}
	++mIndexImage;
	emit doneTakingImage();
}

void QAlgoFocus::algorithme(QVector<int> col, QVector<int> row, QVector<int> width, QVector<int> height) {
	mCol = col;
	mRow = row;
	mWidth = width;
	mHeight = height;
	mOperation = false;
	mIndexImage = 0;
	mNbOfLoop = 0;


	//Determine which algorithm to use
	if (col.size() == 1) {
		multipleSpots = false;
	}
	else {
		multipleSpots = true;
	}

	//Connect to do focuser if it's not already done.
	if (!mFocuseur->mDeviceConnected) {
		mFocuseur->connectSerialPort();
	}

	//If successfully connected carry on the algorithm
	if (mFocuseur->mDeviceConnected) {
		connect(this, &QAlgoFocus::doneTakingImage, this, &QAlgoFocus::getStarterPoints);
		mFocuseur->sendCommandNoParams(QFocusseur::Command::InitSeq);
		getStarterPoints();
	}
}

void QAlgoFocus::algorithmeOperation(int position)
{
	mIndexImage = 0;
	mNbOfLoop = 0;
	mOperationPosition = position + 5;
	mOperation = true;
	//Connect to do focuser if it's not already done.
	if (!mFocuseur->mDeviceConnected) {
		mFocuseur->connectSerialPort();
	}

	//If successfully connected carry on the algorithm
	if (mFocuseur->mDeviceConnected) {
		connect(this, &QAlgoFocus::doneTakingImage, this, &QAlgoFocus::getStarterPoints);
		mFocuseur->sendCommandNoParams(QFocusseur::Command::InitSeq);
		getStarterPoints();
	}
}



void QAlgoFocus::getStarterPoints()
{
	if (mIndexImage < 3) {
		if (mOperation) {
			moveAndSetPosition(mOperationPosition + -5 * mIndexImage);
		}
		else {
			moveAndSetPosition(mFocuserLength*(mIndexImage + 1) / 4);
		}
	}
	else {
		algo2();
	}
}

void QAlgoFocus::moveAndSetPosition(double position)
{
	mPoints[mIndexImage].mPos = position;
	moveFocuseur(position);
}

void QAlgoFocus::algo2()
{
	//Find the image with the highest "blurryLevel"
	colorVariationAndPosition & minBlurryLevel = mPoints[3], &maxBlurryLevel = mPoints[4];

	maxBlurryLevel = mPoints[0];

	for (int i{ 1 }; i < 3; i++) {
		if (mPoints[i].mColorVariationLevel > maxBlurryLevel.mColorVariationLevel) {
			maxBlurryLevel = mPoints[i];
		}
	}

	disconnect(this, &QAlgoFocus::doneTakingImage, this, &QAlgoFocus::getStarterPoints);
	connect(this, &QAlgoFocus::doneTakingImage, this, &QAlgoFocus::getLeftAndRight);

	mIndexImage = 6;
	getLeftAndRight();
}

void QAlgoFocus::getLeftAndRight()
{
	int moveLength = 5;
	if (mIndexImage < 8) {
		int length{ (mIndexImage == 6) ? moveLength * -1 : moveLength };
		mPoints[mIndexImage].mPos = mPoints[4].mPos + length;
		moveFocuseur(mPoints[4].mPos + length);
	}
	else {
		algo3();
	}

}

void QAlgoFocus::algo3()
{
	//Sets the max and min blurry level according to the result

	colorVariationAndPosition & leftNf{ mPoints[6] }, &rightNf{ mPoints[7] }, &maxBlurryLevel{ mPoints[4] }, &minBlurryLevel{ mPoints[3] };
	//	Si smallerSize == left & min == (b)->max = (a)
	if (leftNf.mColorVariationLevel > rightNf.mColorVariationLevel && maxBlurryLevel == mPoints[1]) {
		minBlurryLevel = mPoints[0];
	}
	//	Else if smallerSize == right & min == (b)->max = (c)
	else if (rightNf.mColorVariationLevel > leftNf.mColorVariationLevel && maxBlurryLevel == mPoints[1]) {
		minBlurryLevel = mPoints[2];
	}
	//Else if smallerSize == left & min == (a)->max = none
	else if (leftNf.mColorVariationLevel > rightNf.mColorVariationLevel && maxBlurryLevel == mPoints[0]) {
		minBlurryLevel.mColorVariationLevel = -1;
	}
	//	Else if smallerSize == right & min == (a)->max = (b)
	else if (rightNf.mColorVariationLevel > leftNf.mColorVariationLevel && maxBlurryLevel == mPoints[0]) {
		minBlurryLevel = mPoints[1];
	}
	//	Else if smallerSize == left & min == (c)->max = (b)
	else if (leftNf.mColorVariationLevel > rightNf.mColorVariationLevel && maxBlurryLevel == mPoints[2]) {
		minBlurryLevel = mPoints[1];
	}
	//Else if smallerSize == right & min == (c) -> max = none
	else if (rightNf.mColorVariationLevel > leftNf.mColorVariationLevel && maxBlurryLevel == mPoints[2]) {
		minBlurryLevel.mColorVariationLevel = -1;
	}

	disconnect(this, &QAlgoFocus::doneTakingImage, this, &QAlgoFocus::getLeftAndRight);
	connect(this, &QAlgoFocus::doneTakingImage, this, &QAlgoFocus::interprateReturn);

	findBestFocus();
}

void QAlgoFocus::findBestFocus()
{
	colorVariationAndPosition & leftNf{ mPoints[6] }, &rightNf{ mPoints[7] }, &maxBlurryLevel{ mPoints[4] }, &minBlurryLevel{ mPoints[3] };
	int boucleMax = 10; //� mettre global
						//Loop until a condition is reached (max iteration or insignificant movement on the x or y axis)

	double xSteps = 10;	//Arbitraire � changer
	colorVariationAndPosition &temp{ mPoints[5] };
	mIndexImage = 5;
	if (minBlurryLevel.mColorVariationLevel == -1) {
		if (rightNf.mColorVariationLevel > leftNf.mColorVariationLevel) {
			temp.mPos = maxBlurryLevel.mPos + xSteps;
		}
		else {
			temp.mPos = maxBlurryLevel.mPos - xSteps;
		}
		moveFocuseur(temp.mPos);
	}
	else {
		temp.mPos = (minBlurryLevel.mPos + maxBlurryLevel.mPos) / 2;
		qDebug() << temp.mPos;
		moveFocuseur(temp.mPos);
	}
	mNbOfLoop += 1;
}

void QAlgoFocus::interprateReturn()
{
	int boucleMax = 10;

	colorVariationAndPosition &maxBlurryLevel{ mPoints[4] }, &minBlurryLevel{ mPoints[3] }, &temp{ mPoints[5] };
	if (minBlurryLevel.mColorVariationLevel == -1) {
		if (temp.mColorVariationLevel < maxBlurryLevel.mColorVariationLevel) {
			minBlurryLevel.mPos = temp.mPos;
			minBlurryLevel.mColorVariationLevel = temp.mColorVariationLevel;
		}
		else if (temp.mColorVariationLevel > maxBlurryLevel.mColorVariationLevel) { //Si c'est �gale on ignore parce que ca ferait juste switch de lautre bord de la courbe{
			maxBlurryLevel.mPos = temp.mPos;
			maxBlurryLevel.mColorVariationLevel = temp.mColorVariationLevel;
		}
	}
	else {
		if (temp.mColorVariationLevel > maxBlurryLevel.mColorVariationLevel) {
			minBlurryLevel = maxBlurryLevel;
			maxBlurryLevel.mPos = temp.mPos;
			maxBlurryLevel.mColorVariationLevel = temp.mColorVariationLevel;
		}
		else {
			minBlurryLevel.mPos = temp.mPos;
			minBlurryLevel.mColorVariationLevel = temp.mColorVariationLevel;
		}
	}
	//if (nbBoucle < boucleMax && abs(minBlurryLevel.mBlurryLevel - maxBlurryLevel.mBlurryLevel) > 0.0005 && abs(minBlurryLevel.mPos - maxBlurryLevel.mPos) > 0)
	if (mNbOfLoop < boucleMax)
	{
		findBestFocus();
	}
	else {
		disconnect(this, &QAlgoFocus::doneTakingImage, this, &QAlgoFocus::interprateReturn);
		emit algorithmeDone(maxBlurryLevel.mPos);
	}
}

void QAlgoFocus::colorToGray(QImage & image, QSize imageSize, QImage & grayScale) {
	if (!image.isNull())
	{
		const int * curImage = reinterpret_cast<const int*>(image.bits()); //Pourquoi pas constBits()
		uchar * curGrayScale = grayScale.bits();

		const int * curEndImage{ curImage + imageSize.width() * imageSize.height() };


		while (curImage < curEndImage) {
			*curGrayScale = static_cast<uchar>(
				static_cast<int>(
				(*curImage & 0x000000FF) * 0.11 +
					((*curImage & 0x0000FF00) >> 8) * 0.59 +
					((*curImage & 0x00FF0000) >> 16) * 0.3
					)
				);

			++curImage;
			++curGrayScale;
		};
		//QImageDisplay * imageDisplay = new QImageDisplay(&grayScale);
		//imageDisplay->show();
	}
}

//Calcul Flou d'un pixel
double QAlgoFocus::colorVariationLevelOfAPixel(const uchar * pixel, int imgWidth)
{
	int sobelX[3][3] = {
		-1, 0, 1,
		-2, 0, 2,
		-1, 0, 1
	};

	int sobelY[3][3] = {
		-1, -2, -1,
		0, 0, 0,
		1, 2, 1
	};

	double niveauDeFlouX{ 0.0 }, niveauDeFlouY{ 0.0 };
	double a{ 0 };

	if (*(pixel) == 1) {
		int d = 0;
	}
	if (*(pixel) == 0) {
		int d = 0;
	}

	for (int j = -1; j <= 1; ++j) {
		for (int k = -1; k <= 1; ++k) {
			a = *(pixel + j + k * imgWidth) *  sobelX[k + 1][j + 1];
			niveauDeFlouX += a;
		}
	}

	for (int j = -1; j <= 1; ++j) {
		for (int k = -1; k <= 1; ++k) {
			a = *(pixel + j + k * imgWidth) *  sobelY[k + 1][j + 1];
			niveauDeFlouY += a;
		}
	}

	return abs(niveauDeFlouX) / 4.0 + abs(niveauDeFlouY) / 4.0;
}

double QAlgoFocus::colorVariationLevel(QImage & grayScale, int col, int row, int width, int height)
{
	const uchar* startGrayScale{ grayScale.bits() };

	int grayImageHeight{ grayScale.height() };
	int grayImageWidth{ grayScale.width() };
	if (width == -1 || col + width > grayImageWidth) {
		width = grayImageWidth - col - 2;
	}
	if (height == -1 || row + height > grayImageHeight) {
		height = grayImageHeight - row - 2;
	}
	if (col > grayImageWidth) {
		col = 0;
		width = grayImageWidth;
	}
	if (row > grayImageHeight) {
		row = 0;
		height = grayImageHeight;
	}

	//Move the pointer to the right spot
	startGrayScale += row * grayImageWidth + col;
	double blurryLevelOfImage{ 0.0 };

	//If full image most ignore border pixel
	if (col == 0 && row == 0 && width == grayImageWidth && height == grayImageHeight) {
		for (int currentCol = 1; currentCol < grayImageWidth - 1; ++currentCol) {
			for (int currentRow = 1; currentRow < grayImageHeight - 1; ++currentRow) {
				const uchar* pixel{ startGrayScale + currentRow * grayImageWidth + currentCol };
				blurryLevelOfImage += colorVariationLevelOfAPixel(pixel, grayImageWidth);
			}
		}
	}
	else {
		for (int currentCol = 1; currentCol < width; ++currentCol) {
			for (int currentRow = 1; currentRow < height; ++currentRow) {
				const uchar* pixel{ startGrayScale + currentRow * grayImageWidth + currentCol };
				blurryLevelOfImage += colorVariationLevelOfAPixel(pixel, grayImageWidth);
			}
		}
	}
	return blurryLevelOfImage / (width * height);
}

double QAlgoFocus::colorVariationLevelOfMultipleSpots(QImage & grayScale)
{
	int i{ 0 };
	double result = 0;
	while (i < mCol.size()) {
		result += colorVariationLevel(grayScale, mCol.at(i), mRow.at(i), mWidth.at(i), mHeight.at(i));
		i++;
	}
	return result;
}

