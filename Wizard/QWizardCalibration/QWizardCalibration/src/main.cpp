/*! \mainpage
*
* \section intro_sec Introduction
*
*
*	Ce programme guide un usager � travers la calibration d'une cam�ra. Le processus consiste � d�terminer le point focal de la cam�ra selon la distance
*	entre celui-ci et l'objet observ� � plusieurs reprises afin de d�terminer une tendance et d'enregistrer celle-ci dans un fichier csv.
*	La mise au foyer se fait � l'aide de l'algorithme de focus passif qui d�place le focusseur et prend des images � plusieurs reprises � l'aide d'une feedback loop 
*	jusqu'a ce que l'image obtenue est suffisament nette.
*
*
*
*/


//wizard calibration
//#include "QWizardCalibration.h"
//#include "include/QAlgoFocus.h"
#include "..\include\QWizardCalibration.h"
#include <QtWidgets/QApplication>

//algo genetique
//#include "include/QtLUT/QLUT/QLUT.h"


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QWizardCalibration w;
	w.show();
	//QAlgoFocus algo;
	//algo.blurryLevelImageTest("BackGroundRouge");
	//algo.testonsLeVieuxCode();

	return a.exec();
	//QApplication a(argc, argv);
	//QLut qLut;
	//qLut.show();
	
	//return a.exec();
}
