#include "..\..\include\Camera\MILSimpleInterface.h"

#include <mil.h>

#include <QVBoxLayout>
#include <QPainter>
#include <QMutex>
#include <QDebug>

MILSimpleInterface::MILSimpleInterface(QWidget *parent)
	: QMainWindow(parent)
{
	//ui.setupUi(this);

	//QWidget * controlWidgets{ new QWidget };
	QVBoxLayout * controlLayout{ new QVBoxLayout };
	//controlWidgets->setLayout(controlLayout);
	mGrabButton = new QPushButton("Grab");
	mGrabButton->setFixedWidth(250);
	controlLayout->addWidget(mGrabButton);
	controlLayout->addStretch();

	mScrollArea = new QScrollArea;
	mImage = new QLabel;
	mScrollArea->setWidget(mImage);
	mScrollArea->setBackgroundRole(QPalette::Dark);
	
	QWidget * mainWidget{ new QWidget };
	QHBoxLayout * mainLayout{ new QHBoxLayout };
	mainWidget->setLayout(mainLayout);
	mainLayout->addLayout(controlLayout);
	mainLayout->addWidget(mScrollArea);

	setCentralWidget(mainWidget);

	//mMatroxSimpleGrabber = new QMatroxJaiGo5000Grabber;
	mMatroxSimpleGrabber = new QMatroxJaiSp20000Grabber;


	connect(mGrabButton, &QPushButton::clicked, this, &MILSimpleInterface::grab);
}

QImage MILSimpleInterface::grab()
{
	//mGrabButton->setEnabled(false);
	//qApp->processEvents();

	QImage grabbedImage;
	mMatroxSimpleGrabber->grab(grabbedImage);

	// QImage workingImage{ grabbedImage.convertToFormat(QImage::Format_ARGB32) }; // Hugo

	QPixmap pixmap(grabbedImage.size());
	QPainter painter(&pixmap);
	painter.drawImage(QRect(QPoint(0, 0), grabbedImage.size()), grabbedImage);
	mImage->setPixmap(pixmap);
	mImage->setFixedSize(grabbedImage.size());
	mImage->update();

	//mGrabButton->setEnabled(true);

	return grabbedImage;
}

MILSimpleInterface::~MILSimpleInterface()
{
	delete mMatroxSimpleGrabber;
}
