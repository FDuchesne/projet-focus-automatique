#include <QComboBox>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QGroupBox>
#include <QSpinBox>
#include <QLabel>
#include <QProgressBar>
#include <QWizard>
#include <QFileDialog>
#include <QVector>

#include "..\include\QPictureSettingWidget.h"
#include "..\include\QWizardCalibration.h"
#include "..\include\QtLUT\QLUT\QLutWidget.h"
#include "..\include\QAlgoFocus.h"
#include "..\include\Serial Port\RangeFinder\QRangeFinderGUI.h"
#include "..\include\QtLUT\LUT\LutEchelon.h"
#include "..\include\QtLUT\LUT\DAO\DTO.h"
#include "..\include\QtLUT\LUT\DAO\DaoCsv.h"
#include "..\include\Serial Port\RangeFinder\QRangeFinderAR2000.h"


QWizardCalibration::QWizardCalibration(QWidget *parent)
	: QWizard(parent)
{
	setWindowTitle(tr("Calibration"));

	setPage(Accueil, new QAcceuilPage);
	setPage(Intro, new QIntroductionPage);
	setPage(Focus, new QFocusPage);
	setPage(Conclu, new QConclusionPage);

	setMinimumSize(1200, 1000);

	setStartId(Accueil);
	//setStartId(Intro);


}

QAcceuilPage::QAcceuilPage(QWidget *parent)
	: QWizardPage(parent),
	modeleFocusseur{ new QComboBox() }
{
	setTitle(tr("Calibration de correspondance télémètre / focuseur."));

	QFormLayout * formLayout = new QFormLayout;

	lesOperateurs = new QVBoxLayout;
	ope1 = new QLineEdit;
	ope2 = new QLineEdit;
	lesOperateurs->addWidget(ope1);
	lesOperateurs->addWidget(ope2);

	QHBoxLayout * lesBtns = new QHBoxLayout;
	btnPlus = new QPushButton("+ Ajouter un operateur");
	btnMoins = new QPushButton("- Enlever un operateur");
	btnMoins->setEnabled(false);

	lesBtns->addWidget(btnPlus);
	lesBtns->addWidget(btnMoins);

	description = new QLineEdit;
	modeleTelescope = new QComboBox;

	ope1->setPlaceholderText("Entrez le nom du 1er operateur ici.");
	ope2->setPlaceholderText("Entrez le nom du 2e operateur ici.");

	description->setPlaceholderText("ex : Calibration mandataire du mois de juillet.");

	modeleTelescope->addItem("");
	modeleTelescope->addItem("modele 1");
	modeleTelescope->addItem("modele 2");
	modeleTelescope->addItem("modele 3");

	modeleFocusseur->addItem("");
	modeleFocusseur->addItem("modele 1");
	modeleFocusseur->addItem("modele 2");
	modeleFocusseur->addItem("modele 3");

	formLayout->addRow("Nom des operateurs", lesOperateurs);
	formLayout->addRow("", lesBtns);
	formLayout->addRow("&Justification de la calibration", description);
	formLayout->addRow("&Modele du telescope", modeleTelescope);
	formLayout->addRow("&Modele du focusseur", modeleFocusseur);


	setLayout(formLayout);

	connect(btnPlus, &QPushButton::clicked, this, &QAcceuilPage::ajoutOperateur);
	connect(btnMoins, &QPushButton::clicked, this, &QAcceuilPage::enleverOperateur);

	registerField("ope1*", ope1);
	registerField("ope2*", ope2);

	registerField("description*", description);
	registerField("modele*", modeleTelescope);
	registerField("modeleFocusseur*", modeleFocusseur);

}
QAcceuilPage::~QAcceuilPage()
{

}
void QAcceuilPage::ajoutOperateur()
{
	btnMoins->setEnabled(true);
	int nbLignes = lesOperateurs->count();
	if (nbLignes < NB_MAX_OPE)
	{
		QLineEdit * nouveauOpe{ new QLineEdit };
		nouveauOpe->setPlaceholderText("Entrez le nom du " + QString::number(nbLignes + 1) + "e operateur ici.");
		lesOperateurs->addWidget(nouveauOpe);

		if (nbLignes == NB_MAX_OPE - 1)
		{
			btnPlus->setEnabled(false);
		}
	}
}
void QAcceuilPage::enleverOperateur()
{
	btnPlus->setEnabled(true);
	int nbLignes = lesOperateurs->count();
	if (nbLignes > NB_MIN_OPE)
	{
		QLayoutItem * temp{ lesOperateurs->itemAt(nbLignes - 1) };
		lesOperateurs->removeItem(temp);
		delete temp->widget();
		delete temp;

		if (nbLignes == NB_MIN_OPE + 1)
		{
			btnMoins->setEnabled(false);
		}
	}
}

QIntroductionPage::QIntroductionPage(QWidget *parent)
	: QWizardPage(parent)
{
	setTitle(tr("Calibration de correspondance télémètre / focuseur."));

	QFormLayout * formLayout = new QFormLayout;

	mode = new QGroupBox(tr("Choisissez le mode de focus."));
	modeAutomatique = new QRadioButton(tr("Mode &automatique"));
	modeManuel = new QRadioButton(tr("Mode &manuel"));
	modeAutomatique->setChecked(true);

	QVBoxLayout * vbox1 = new QVBoxLayout;
	vbox1->addWidget(modeAutomatique);
	vbox1->addWidget(modeManuel);

	mode->setLayout(vbox1);

	precision = new QGroupBox(tr("Choisissez la precision de la calibration."));
	hautePrecision = new QRadioButton(tr("&Haute precision"));
	bassePrecision = new QRadioButton(tr("&Basse precision"));
	precisionPerso = new QRadioButton(tr("&Personnalisee"));
	hautePrecision->setChecked(true);

	QVBoxLayout * vbox2 = new QVBoxLayout;
	vbox2->addWidget(hautePrecision);
	vbox2->addWidget(bassePrecision);
	vbox2->addWidget(precisionPerso);

	precision->setLayout(vbox2);

	mMinDist = new QSpinBox;
	mMaxDist = new QSpinBox;
	mInterval = new QSpinBox;

	mMinDist->setRange(MIN_HAUT, MIN_BASE);
	mMinDist->setSingleStep(10);
	mMaxDist->setRange(MAX_BASE, MAX_HAUT);
	mMaxDist->setSingleStep(10);
	mInterval->setRange(1, INTER_BASE);
	mInterval->setSingleStep(1);

	connect(hautePrecision, &QRadioButton::clicked, this, &QIntroductionPage::hautePrcsn);
	connect(bassePrecision, &QRadioButton::clicked, this, &QIntroductionPage::bassePrcsn);
	connect(precisionPerso, &QRadioButton::clicked, this, &QIntroductionPage::prcsnPerso);


	mMinDist->setValue(MIN_HAUT);
	mMaxDist->setValue(MAX_HAUT);
	mInterval->setValue(INTER_HAUT);

	mMinDist->setEnabled(false);
	mMaxDist->setEnabled(false);
	mInterval->setEnabled(false);

	formLayout->addRow(mode);
	formLayout->addRow(precision);
	formLayout->addRow("Distance mi&nimum", mMinDist);
	formLayout->addRow("Distance ma&ximum", mMaxDist);
	formLayout->addRow("&Interval", mInterval);

	setLayout(formLayout);

	registerField("minDist", mMinDist);
	registerField("maxDist", mMaxDist);
	registerField("interval", mInterval);
	registerField("modeAuto", modeAutomatique);

	//setCommitPage(true); //Mettre en commentaire si on veut revenir en arrière
}
QIntroductionPage::~QIntroductionPage()
{

}


void QIntroductionPage::hautePrcsn()
{
	mMinDist->setValue(MIN_HAUT);
	mMaxDist->setValue(MAX_HAUT);
	mInterval->setValue(INTER_HAUT);

	mMinDist->setEnabled(false);
	mMaxDist->setEnabled(false);
	mInterval->setEnabled(false);
}
void QIntroductionPage::bassePrcsn()
{
	mMinDist->setValue(MIN_BASE);
	mMaxDist->setValue(MAX_BASE);
	mInterval->setValue(INTER_BASE);

	mMinDist->setEnabled(false);
	mMaxDist->setEnabled(false);
	mInterval->setEnabled(false);
}
void QIntroductionPage::prcsnPerso()
{
	mMinDist->setEnabled(true);
	mMaxDist->setEnabled(true);
	mInterval->setEnabled(true);
}


QFocusPage::QFocusPage(QWidget * parent) :
	QWizardPage(parent),
	mBtnFocusAuto{ new QPushButton("Faire le focus automatique") },
	mBtnOpenRangeFinderGUI{ new QPushButton("Ouvrir l'interface du rangeFinder") },
	mBtnCommit{ new QPushButton("Commit") },
	mBtnGoToCurrent{ new QPushButton("Go to the current distance") },
	mBtnReset{ new QPushButton("Reset la calibration") },
	mBtnPrevFocus{ new QPushButton("Prev") },
	mBtnNextFocus{ new QPushButton("Next") },
	mBtnPicutreSettings{ new QPushButton("Zone") },
	mNomOpe{ new QLabel(field("ope1").toString()) },
	mResultatFocus{ new QLabel() },
	mProgressBar{ new QProgressBar() },
	mAlgoFocus{ new QAlgoFocus() },
	mLut{ new LutEchelon() },
	mQLutWidget{ new QLutWidget(&mLut) },
	mSubtitle{ tr("Veuillez positionner la cible à %1 mètre et ajuster la position du focuseur.\nÉtape %2 de %3") },
	mIndexBeingModifed{ new QSpinBox() },
	mFilePathLabel{ new QLabel() },
	mCol{ QVector<int>(1,2310) },
	mRow{ QVector<int>(1,1670) },
	mWidth{ QVector<int>(1,500) },
	mHeight{ QVector<int>(1,500) }
{
	QVBoxLayout * vLayout = new QVBoxLayout();
	QHBoxLayout * bottomLayout = new QHBoxLayout();
	QVBoxLayout * bottomRightLayout = new QVBoxLayout();
	QHBoxLayout * indexLayout = new QHBoxLayout();
	QHBoxLayout * prevNextLayout = new QHBoxLayout();
	QVBoxLayout * bottomLeftLayout = new QVBoxLayout();
	QHBoxLayout * pictureLayout = new QHBoxLayout();
	QLabel * mCurrentIndexTitle = new QLabel("Current index");
	mQLutWidget->setMinimumSize(600,550);
	vLayout->setSpacing(5);

	prevNextLayout->addWidget(mBtnPrevFocus);
	prevNextLayout->addWidget(mBtnNextFocus);

	bottomLeftLayout->addWidget(mBtnGoToCurrent);
	bottomLeftLayout->addWidget(mBtnCommit);

	indexLayout->addWidget(mCurrentIndexTitle);
	indexLayout->addWidget(mIndexBeingModifed);

	bottomRightLayout->addLayout(indexLayout);
	bottomRightLayout->addWidget(mBtnReset);

	bottomLayout->addLayout(bottomLeftLayout);
	bottomLayout->addLayout(bottomRightLayout);

	pictureLayout->addWidget(mBtnFocusAuto);
	mBtnFocusAuto->setMinimumWidth(800);
	mBtnPicutreSettings->setMaximumWidth(150);
	pictureLayout->addWidget(mBtnPicutreSettings);
	
	vLayout->addWidget(mProgressBar);
	vLayout->addLayout(pictureLayout);
	vLayout->addWidget(mBtnOpenRangeFinderGUI);
	vLayout->addWidget(mResultatFocus);
	vLayout->addLayout(prevNextLayout);
	vLayout->addWidget(mQLutWidget);
	vLayout->addLayout(bottomLayout);

	setLayout(vLayout);

	mIndexBeingModifed->setRange(1, mLut->elements().size()+1);
	mIndexBeingModifed->setSingleStep(1);

	mBtnPrevFocus->setEnabled(false);
	mBtnNextFocus->setEnabled(false);

	setButtonText(QWizard::CommitButton, "Save File");
	setCommitPage(true);


	connect(mBtnOpenRangeFinderGUI, &QPushButton::clicked, this, &QFocusPage::openRangeFinderGUI);
	connect(mBtnFocusAuto, &QPushButton::clicked, this, &QFocusPage::focusImage);

	connect(mBtnCommit, &QPushButton::clicked, this, &QFocusPage::commitPoint);
	connect(mIndexBeingModifed, QOverload<int>::of(&QSpinBox::valueChanged), this, &QFocusPage::modifyExistingValue);
	connect(mBtnGoToCurrent, &QPushButton::clicked, this, &QFocusPage::goToCurrent);
	connect(mBtnReset, &QPushButton::clicked, this, &QFocusPage::resetPoints);
	connect(mBtnPrevFocus, &QPushButton::clicked, this, &QFocusPage::previousFocusResult);
	connect(mBtnNextFocus, &QPushButton::clicked, this, &QFocusPage::nextFocusResult);
	connect(mBtnPicutreSettings, &QPushButton::clicked, this, &QFocusPage::openPictureSettings);
	connect(mAlgoFocus, &QAlgoFocus::algorithmeDone, this, &QFocusPage::recieveFocusInfo);


}

void QFocusPage::focusImage()
{

	mBtnFocusAuto->setEnabled(false);
	mBtnFocusAuto->repaint();
	mAlgoFocus->algorithme(mCol, mRow, mWidth, mHeight);

}

void QFocusPage::recieveFocusInfo(int blurryLevel){
	mFocusResultBuffer.insert(mBufferIndex, blurryLevel);
	mResultatFocus->setText(QString::number(mFocusResultBuffer.at(mBufferIndex)));

	/****************************************************************************/
	//This Should be done in the createPoint function below
	mQLutWidget->addNewData(mCurrentDistance, mFocusResultBuffer.at(mBufferIndex), mCurrentIndex);
	mIndexBeingModifed->setRange(1, mLut->elements().size() + 1);
	mBufferIndex += 1;
	if (mFocusResultBuffer.size() != 1) {
		mBtnPrevFocus->setEnabled(true);
	}
	mBtnFocusAuto->setEnabled(true);
	/***************************************************************************/

	//The code below is a tentative to ask the range finder for the real distance from the object.
	//The test didn't succeed, but the code should look like this.

	//if (!mRangeFinderGUI) {
	//	mRangeFinder = &mRangeFinderGUI->rangeFinder();
	//}
	//else {
	//	mRangeFinder = new QRangeFinderAR2000();
	//}

	/*The connect here is really important*/
	//connect(mRangeFinder, &QRangeFinderAR2000::measurementReceived, this, &QFocusPage::createPoint);


	//if (mRangeFinder->mDeviceConnected) {
	//	mRangeFinder->connectSerialPort();
	//}
	//if (mRangeFinder->mDeviceConnected) {
	//	mRangeFinder->sendCommandNoParams(QRangeFinderAR2000::Command::DoSingleMeasurement);
	//}
}

void QFocusPage::createPoint(double distanceFromObject, double quality, double temperature)
{
	mCurrentDistance = distanceFromObject;
	mQLutWidget->addNewData(mCurrentRangeFinderDisance, mFocusResultBuffer.at(mBufferIndex), mCurrentIndex);
	mIndexBeingModifed->setRange(1, mLut->elements().size() + 1);
	mBufferIndex += 1;
	if (mFocusResultBuffer.size() != 1) {
		mBtnPrevFocus->setEnabled(true);
	}
	mBtnFocusAuto->setEnabled(true);
}

void QFocusPage::openRangeFinderGUI()
{
	mRangeFinderGUI = new QRangeFinderGUI();
	mRangeFinderGUI->show();
}

void QFocusPage::commitPoint()
{
	mQLutWidget->addNewData(mCurrentDistance, mResultatFocus->text().toDouble(), mCurrentIndex);
	mIndexBeingModifed->setRange(1, mLut->elements().size() + 1);

	if (mCurrentIndex < mNbOfIndex) {
		mCurrentIndex += 1;
		indexChanged();
		mProgressBar->setValue(mCurrentIndex);
	}
	mBufferIndex = 0;
	mFocusResultBuffer.clear();
	mBtnPrevFocus->setEnabled(false);
	mBtnNextFocus->setEnabled(false);
	mResultatFocus->setText("");
	calibrationHasBeenCompleted();
}

void QFocusPage::resetPoints()
{
	mLut = new LutEchelon();
	mQLutWidget->setLut(&mLut);
	mCurrentIndex = 0;
	indexChanged();
	mResultatFocus->setText("");
}

void QFocusPage::modifyExistingValue(int itemIndex)
{
	mCurrentIndex = itemIndex-1;
	mBtnCommit->setEnabled(true);
	indexChanged();
}

void QFocusPage::goToCurrent()
{
	mCurrentIndex = mLut->elements().size();
	calibrationHasBeenCompleted();
	indexChanged();
}

void QFocusPage::indexChanged()
{
	mCurrentDistance = mMinDistance + mIntervalDistance * (mCurrentIndex);
	setSubTitle(mSubtitle.arg(mCurrentDistance).arg(mCurrentIndex + 1).arg(mNbOfIndex));
	mIndexBeingModifed->blockSignals(true);
	mIndexBeingModifed->setValue(mCurrentIndex+1);
	mIndexBeingModifed->blockSignals(false);
	mProgressBar->setValue(mCurrentIndex);
	emit completeChanged();
}

void QFocusPage::calibrationHasBeenCompleted()
{
	if (mCurrentIndex == mNbOfIndex) {
		mBtnCommit->setEnabled(false);
		setSubTitle("Toutes les valeurs ont été entrées, veuillez les vérifier et enregistrer le fichier lorsque vous êtes satisfait");
	}
}

void QFocusPage::bufferIndexChanged()
{
	mResultatFocus->setText(QString::number(mFocusResultBuffer.at(mBufferIndex)));
	mQLutWidget->addNewData(mCurrentDistance, mFocusResultBuffer.at(mBufferIndex), mCurrentIndex);
}

void QFocusPage::previousFocusResult()
{
	if (mBufferIndex == mFocusResultBuffer.size()) {
		mBufferIndex -= 2;
	}
	else {
		mBufferIndex -= 1;
	}
	if (mBufferIndex == 0) {
		mBtnPrevFocus->setEnabled(false);
	}
	mBtnNextFocus->setEnabled(true);
	bufferIndexChanged();
}

void QFocusPage::nextFocusResult()
{
	mBufferIndex += 1;
	mBtnPrevFocus->setEnabled(true);
	bufferIndexChanged();
	if (mBufferIndex == mFocusResultBuffer.size() - 1) {
		mBtnNextFocus->setEnabled(false);
		mBufferIndex += 1;
	}
}

void QFocusPage::openPictureSettings()
{
	mBtnPicutreSettings->setEnabled(false);
	QPictureSettingWidget * pictureSettingsWidget = new QPictureSettingWidget(mCol, mRow, mWidth, mHeight);
	pictureSettingsWidget->show();
	connect(pictureSettingsWidget, &QPictureSettingWidget::closing, this, &QFocusPage::pictureSettingsClosed);
}

void QFocusPage::pictureSettingsClosed()
{
	mBtnPicutreSettings->setEnabled(true);
}

void QFocusPage::initializePage()
{
	mIntervalDistance = wizard()->field("interval").toInt();
	mMinDistance = wizard()->field("minDist").toInt();
	mMaxDistance = wizard()->field("maxDist").toInt();
	mNbOfIndex = (mMaxDistance - mMinDistance) / mIntervalDistance +1;
	mCurrentDistance = mMinDistance;
	mCurrentIndex = 0;
	mBufferIndex = 0;
	mBtnCommit->setEnabled(true);
	mLut = new LutEchelon();
	mQLutWidget->setLut(&mLut);
	setTitle(tr("Calibration automatique étape par étape"));
	indexChanged();

	mBtnFocusAuto->setEnabled(wizard()->field("modeAuto").toBool());
	if (wizard()->field("modeAuto").toBool()) {
		setTitle(tr("Calibration automatique étape par étape"));
	}
	else {
		setTitle(tr("Calibration manuelle étape par étape"));
	}

	//Progress bar
	mProgressBar->setRange(0, mNbOfIndex);
	mProgressBar->setValue(mCurrentIndex);
}

bool QFocusPage::validatePage()
{
	QFileDialog dialog(this);
	dialog.setNameFilter(tr("CSV (*.csv)"));
	dialog.setFileMode(QFileDialog::AnyFile);
	dialog.setWindowModality(Qt::WindowModal);
	dialog.setAcceptMode(QFileDialog::AcceptSave);
	dialog.selectFile(wizard()->field("description").toString());
	dialog.show();
	if (dialog.exec()) {
		mFilePath = dialog.selectedFiles().first();
		DTO mDto = DTO(mLut->elements());
		DaoCsv mDaoCsv = DaoCsv(mFilePath.toStdString());
		mDaoCsv.write(mDto.vector());
		mFilePathLabel->setText(mFilePath);
		registerField("filePath", mFilePathLabel);
	}
	else {
		return false;
	}

	return true;

}

bool QFocusPage::isComplete() const
{
	return mCurrentIndex == mNbOfIndex;
}


QConclusionPage::QConclusionPage(QWidget *parent)
	: QWizardPage(parent)
{
	setTitle(tr("Calibration terminée."));
}
QConclusionPage::~QConclusionPage()
{
}
void QConclusionPage::initializePage()
{
	int nMesure = (wizard()->field("maxDist").toInt() - wizard()->field("minDist").toInt()) / wizard()->field("interval").toInt() + 1;
	setSubTitle(tr("%1 mesure%2 ont été prises.").arg(nMesure).arg(nMesure > 1 ? "s" : ""));

	QVBoxLayout * leLayout{ new QVBoxLayout };
	QLabel * confirmation{ new QLabel("Le fichier de calibration fut enregistré avec succès") };
	QLabel * responsable{ new QLabel("Opérateurs responsables") };
	QLabel * ope1{ new QLabel(wizard()->field("ope1").toString()) };
	QLabel * ope2{ new QLabel(wizard()->field("ope2").toString()) };
	QLabel * ope3{ new QLabel(wizard()->field("ope3").toString()) };
	QLabel * ope4{ new QLabel(wizard()->field("ope4").toString()) };
	QLabel * ope5{ new QLabel(wizard()->field("ope5").toString()) };
	QLabel * description{ new QLabel("La calibration \" " + wizard()->field("description").toString() + " \" est terminé")};
	QLabel * filePath{ new QLabel("La calibration a été sauvegardé à : " + wizard()->field("filePath").toString()) };

	leLayout->addWidget(confirmation);
	leLayout->addWidget(responsable);
	leLayout->addWidget(ope1);
	leLayout->addWidget(ope2);
	leLayout->addWidget(ope3);
	leLayout->addWidget(ope4);
	leLayout->addWidget(ope5);
	leLayout->addWidget(description);
	leLayout->addWidget(filePath);

	setLayout(leLayout);
}