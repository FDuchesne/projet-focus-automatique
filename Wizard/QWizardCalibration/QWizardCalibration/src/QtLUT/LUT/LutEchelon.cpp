#include "..\..\..\include\QtLUT\LUT\LutEchelon.h"

LutEchelon::LutEchelon(StepCalculationMethod method)
:LutWithoutFunction()
{
	setStepCalculationMethod(method);
}

double LutEchelon::y(double x) const
{
	Point previousValue, nextValue;


	getCloseValues(x, previousValue, nextValue);

	if (x < previousValue.x()) {
		return previousValue.y();
	}
	else if (x > nextValue.x()) {
		return nextValue.y();
	}

	if (mStepCalculationMethod == StepCalculationMethod::Prev) {
		return previousValue.y();
	}
	else {
		return nextValue.y();
	}
	
}

