#include "..\..\..\include\QtLUT\LUT\LutAbstract.h"
#include <algorithm>

LutAbstract::LutAbstract()
{
    mInterpolationType = InterpolationType::Interpolate;
}

void LutAbstract::add(Point const & element)
{
    mElements.emplace_back(element);
	std::sort(mElements.begin(), mElements.end());
}

void LutAbstract::add(Point const * elements, int n)
{
    for(int i = 0; i < n; ++i){
        mElements.emplace_back(elements[i]);
    }
	std::sort(mElements.begin(), mElements.end());
}

LutAbstract& LutAbstract::operator+=(Point& p){
    add(p);
    return *this;
}

LutAbstract & LutAbstract::operator+=(Point && p)
{
	add(p);
	return *this;
}

LutAbstract::ErrorType LutAbstract::checkErrors(double x) const
{
	if (mElements.empty()) {
		//throw std::runtime_error("ERROR: vecteur vide");
		return LutAbstract::ErrorType::EmptyVector;
	}

	if (mInterpolationType == InterpolationType::Interpolate) {
		if ((x < mElements.at(0).x() || x > mElements.at(mElements.size() - 1).x())) {
			//throw std::runtime_error("ERROR: flag interpolation et valeur hors limite");
			return LutAbstract::ErrorType::IllegalExtrapolation;
		}
	}

	return LutAbstract::ErrorType::NoError;
}

void LutAbstract::setElements(std::vector<Point> const & elements) {
	mElements = elements;
	std::sort(mElements.begin(), mElements.end());
}