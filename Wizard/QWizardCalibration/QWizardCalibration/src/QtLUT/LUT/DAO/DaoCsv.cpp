#include "..\..\..\..\include\QtLUT\LUT\DAO/DaoCsv.h"
#include <iostream>

DaoCsv::DaoCsv(std::string path)
	:DaoFile(path)
{}

void DaoCsv::write(std::vector<Point> const & vector) const
{
	std::ofstream file(mPath);

	for (Point point : vector) {
		file << point.x() << "," << point.y() << ",\n";
	}

	file.close();
}

std::vector<Point> DaoCsv::read()
{
	std::ifstream file(mPath);
	std::vector<Point> vector;

	if (file.is_open()) {

		std::string str;
		double x{ 0.0 }, y{ 0.0 };

		if (file.peek() != std::ifstream::traits_type::eof()) {
			while (!file.eof()) {

				//Read the x value
				std::getline(file, str, ',');

				//Because of the '/n' in the write there's always an empty line at the end
				if (str.at(0) == '\n' && str.size() == 1 || str == "\0") {
					return vector;
				}

				if (str.at(0) == '\n') {
					str.erase(str.begin());
				}

				if (str != "A" && str != "B") {
					x = atof(str.c_str());


					//Read the y value
					std::getline(file, str, ',');
					y = atof(str.c_str());

					//Insert a Point in the vector
					vector.emplace_back(Point(x, y));
				}
				else {
					std::getline(file, str, ',');
				}
			}
			return vector;
		}
	}

	return vector;
}

double DaoCsv::readParameterA()
{
	return readParameter("A");
}

double DaoCsv::readParameterB()
{
	return readParameter("B");
}

double DaoCsv::readParameter(std::string parameterName)
{
	std::ifstream file(mPath);
	std::string mParamString{ "" };

	while (mParamString!= parameterName && !file.eof()) {
		std::getline(file, mParamString, ',');
		if (mParamString.at(0) == '\n') {
			mParamString.erase(mParamString.begin());
		}
	}
	if (file.eof()) {
		return 0;
	}
	else {
		std::getline(file, mParamString, ',');
		return atof(mParamString.c_str());
	}


}