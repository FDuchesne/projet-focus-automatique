#include "..\..\..\include\QtLUT\LUT\LutWithoutFunction.h"
#include <stdexcept>

LutWithoutFunction::LutWithoutFunction()
:LutAbstract()
{
    //ctor
}

void LutWithoutFunction::getCloseValues(double x, Point & previousValue, Point & nextValue) const
{
    int startIndex = 0;
    int endIndex = mElements.size() - 1;

    //vérifie si x est égal au point zéro (ce qui n'est pas vérifié dans le while)
    if(mElements.at(0).x() == x){
        previousValue = mElements.at(0);
        nextValue = previousValue;
        return;
    }

    while(startIndex < endIndex - 1){
        int middleIndex = ((endIndex - startIndex) / 2) + startIndex;
        double midX = mElements.at(middleIndex).x();
        //vérifie si x est égal à une valeur du vecteur
        if(midX == x){
            previousValue = mElements.at(middleIndex);
            nextValue = previousValue;
			
            return;
        }
        else if(midX < x){
            startIndex = middleIndex;
        }
        else{
            endIndex = middleIndex;
        }
    }
    previousValue = mElements.at(startIndex);
    nextValue = mElements.at(endIndex);
}
