#include "..\..\..\include\QtLUT\LUT\LutLinear.h"

LutLinear::LutLinear() :
	LutWithoutFunction()
{
}

double LutLinear::y(double x) const
{
	Point p1, p2;

	//p1 = previous, p2 = next
	getCloseValues(x, p1, p2);

	//In order to avoid division by 0
	if (p1.x() == p2.x()) {
		return p1.y();
	}
	return 	(x - p1.x()) / (p2.x() - p1.x()) * (p2.y() - p1.y()) + p1.y();
}
