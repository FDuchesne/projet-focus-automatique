#include "..\..\..\include\QtLUT\LUT\LutWithParameters.h"

LutWithParameters::LutWithParameters()
	:LutAbstract()
{
}

double LutWithParameters::sumX() const
{
	double sum{ 0.0 };

	for (auto element : mElements) {
		sum += element.x();
	}

	return sum;
}

double LutWithParameters::sumXSquared() const
{
	double sum{ 0.0 };

	for (auto element : mElements) {
		sum += element.x() * element.x();
	}

	return sum;
}

double LutWithParameters::sumXY() const
{
	double sum{ 0.0 };

	for (auto element : mElements) {
		sum += element.x() * element.y();
	}

	return sum;
}

double LutWithParameters::sumY() const
{
	double sum{ 0.0 };

	for (auto element : mElements) {
		sum += element.y();
	}

	return sum;
}
void LutWithParameters::add(Point const & element)
{
	LutAbstract::add(element);
	findParameters();
}
void LutWithParameters::add(Point const * elements, int n)
{
	LutAbstract::add(elements, n);
	findParameters();
}

