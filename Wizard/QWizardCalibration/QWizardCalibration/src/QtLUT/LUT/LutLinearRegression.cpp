#include "..\..\..\include\QtLUT\LUT\LutLinearRegression.h"

LutLinearRegression::LutLinearRegression():
	LutWithParameters()
{
}

void LutLinearRegression::findParameters()
{
	double _sumY = sumY();
	double _sumX = sumX();
	double _sumXY = sumXY();
	double _sumXSquared = sumXSquared();
	int size = mElements.size();
	mA = ( _sumY * _sumXSquared - _sumX * _sumXY) / (size * _sumXSquared - _sumX * _sumX);
	mB = ( size * _sumXY - _sumX * _sumY) / (size * _sumXSquared - _sumX * _sumX);
}

double LutLinearRegression::y(double x) const 
{
	return mA + mB * x;
}
