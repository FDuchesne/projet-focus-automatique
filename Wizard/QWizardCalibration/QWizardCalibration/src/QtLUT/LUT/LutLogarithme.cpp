#include "..\..\..\include\QtLUT\LUT\LutLogarithme.h"
#include "..\..\..\include\QtLUT\LUT\DAO\DTO.h"
#include "..\..\..\include\QtLUT\LUT\DAO\DaoCsv.h"
#include <cmath>

LutLogarithme::LutLogarithme(std::string filePath) :
	LutWithParameters(),
	mFilePath{ filePath }
{
}

void LutLogarithme::findParameters()
{
	if (mFilePath != "") {
		DaoCsv dao(mFilePath);
		DTO dto(dao.read(), dao.readParameterA(), dao.readParameterB());
		mA = dto.paramA();
		mB = dto.paramB();
	}
}

double LutLogarithme::y(double x) const
{
	return mA * log(x) + mB;
}
