#include "..\include\QPictureSettingWidget.h"
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QValidator>
#include <QMenuBar>

QPictureSettingWidget::QPictureSettingWidget(QVector<int> & col, QVector<int> & row, QVector<int> & width, QVector<int> & height, QWidget *parent )
	: QWidget(parent),
	mColEdit{ new QLineEdit(QString::number(col.at(0))) },
	mRowEdit{ new QLineEdit(QString::number(row.at(0))) },
	mWidthEdit{ new QLineEdit(QString::number(width.at(0))) },
	mHeightEdit{ new QLineEdit(QString::number(height.at(0))) },
	mApplyAllAndQuit{ new QPushButton("Apply all and quit") },
	mApplyButton{ new QPushButton("Apply") },
	mExitButton{ new QPushButton("Exit") },
	mPrevButton{ new QPushButton("Previous") },
	mNextButton{ new QPushButton("Next") },
	mAddButton{ new QPushButton("Add") },
	mRemoveButton{ new QPushButton("Remove") },
	mCurrentZone{ 0 }

{
	mCol = &col;
	mRow = &row;
	mWidth = &width;
	mHeight = &height;
	mTempCol = *mCol;
	mTempRow = *mRow;
	mTempWidth = *mWidth;
	mTempHeight = *mHeight;
	QVBoxLayout * mainLayout = new QVBoxLayout();
	QHBoxLayout * settingsLayout = new QHBoxLayout();
	QVBoxLayout * labelLayout = new QVBoxLayout();
	QVBoxLayout * lineEditLayout = new QVBoxLayout();
	QHBoxLayout * buttonLayout = new QHBoxLayout();
	QHBoxLayout * prevNextLayout = new QHBoxLayout();
	QHBoxLayout * addRemoveLayout = new QHBoxLayout();

	setWindowTitle("Picture Settings");

	addRemoveLayout->addWidget(mAddButton);
	addRemoveLayout->addWidget(mRemoveButton);
	prevNextLayout->addWidget(mPrevButton);
	prevNextLayout->addWidget(mNextButton);
	mPrevButton->setEnabled(false);
	if (col.size() == 1) {
		mNextButton->setEnabled(false);
	}

	mColEdit->setValidator(new QDoubleValidator());
	mRowEdit->setValidator(new QDoubleValidator());
	mWidthEdit->setValidator(new QDoubleValidator());
	mHeightEdit->setValidator(new QDoubleValidator());
	lineEditLayout->addWidget(mColEdit);
	lineEditLayout->addWidget(mRowEdit);
	lineEditLayout->addWidget(mWidthEdit);
	lineEditLayout->addWidget(mHeightEdit);

	QLabel * mColLabel = new QLabel("Start Column");
	QLabel * mRowLabel = new QLabel("Start Row");
	QLabel * mWidthLabel = new QLabel("Width (-1 == full size)");
	QLabel * mHeightLabel = new QLabel("Height (-1 == full size)");
	labelLayout->addWidget(mColLabel);
	labelLayout->addWidget(mRowLabel);
	labelLayout->addWidget(mWidthLabel);
	labelLayout->addWidget(mHeightLabel);

	settingsLayout->addLayout(labelLayout);
	settingsLayout->addLayout(lineEditLayout);

	buttonLayout->addWidget(mApplyAllAndQuit);
	buttonLayout->addWidget(mApplyButton);
	buttonLayout->addWidget(mExitButton);

	mainLayout->addLayout(addRemoveLayout);
	mainLayout->addLayout(prevNextLayout);
	mainLayout->addLayout(settingsLayout);
	mainLayout->addLayout(buttonLayout);

	setLayout(mainLayout);

	connect(mApplyAllAndQuit, &QPushButton::clicked, this, &QPictureSettingWidget::applyAllAndQuit);
	connect(mApplyButton, &QPushButton::clicked, this, &QPictureSettingWidget::applySettings);
	connect(mExitButton, &QPushButton::clicked, this, &QPictureSettingWidget::exitSettings);
	connect(mPrevButton, &QPushButton::clicked, this, &QPictureSettingWidget::prevZoneSettings);
	connect(mNextButton, &QPushButton::clicked, this, &QPictureSettingWidget::nextZoneSettings);
	connect(mAddButton, &QPushButton::clicked, this, &QPictureSettingWidget::addZone);
	connect(mRemoveButton, &QPushButton::clicked, this, &QPictureSettingWidget::removeZone);

}

QPictureSettingWidget::~QPictureSettingWidget()
{
}

void QPictureSettingWidget::applyAllAndQuit()
{
	saveTemporary();
	for (mCurrentZone = 0; mCurrentZone < mTempCol.size(); ++mCurrentZone) {
		(*mCol).replace(mCurrentZone, mTempCol.at(mCurrentZone));
		(*mRow).replace(mCurrentZone, mTempRow.at(mCurrentZone));
		(*mWidth).replace(mCurrentZone, mTempWidth.at(mCurrentZone));
		(*mHeight).replace(mCurrentZone, mTempHeight.at(mCurrentZone));
	}
	emit closing();
	emit close();
}

void QPictureSettingWidget::applySettings()
{
	saveTemporary();
	(*mCol).replace(mCurrentZone, mTempCol.at(mCurrentZone));
	(*mRow).replace(mCurrentZone, mTempRow.at(mCurrentZone));
	(*mWidth).replace(mCurrentZone, mTempWidth.at(mCurrentZone));
	(*mHeight).replace(mCurrentZone, mTempHeight.at(mCurrentZone));
}

void QPictureSettingWidget::exitSettings()
{
	emit close();
}

void QPictureSettingWidget::prevZoneSettings()
{
	saveTemporary();
	mCurrentZone -= 1;
	pageChanged();
}

void QPictureSettingWidget::nextZoneSettings()
{
	saveTemporary();
	mCurrentZone += 1;
	pageChanged();
}

void QPictureSettingWidget::addZone()
{
	(*mCol).append(0);
	(*mRow).append(0);
	(*mWidth).append(-1);
	(*mHeight).append(-1);
	mTempCol.append(0);
	mTempRow.append(0);
	mTempWidth.append(-1);
	mTempHeight.append(-1);
	saveTemporary();
	mCurrentZone += 1;
	pageChanged();
}

void QPictureSettingWidget::removeZone()
{
	(*mCol).removeAt(mCurrentZone);
	(*mRow).removeAt(mCurrentZone);
	(*mWidth).removeAt(mCurrentZone);
	(*mHeight).removeAt(mCurrentZone);
	mTempCol.removeAt(mCurrentZone);
	mTempRow.removeAt(mCurrentZone);
	mTempWidth.removeAt(mCurrentZone);
	mTempHeight.removeAt(mCurrentZone);
	mCurrentZone -= 1;
	pageChanged();
}

void QPictureSettingWidget::pageChanged()
{
	if (mCurrentZone == 0) {
		mPrevButton->setEnabled(false);
	}
	else {
		mPrevButton->setEnabled(true);
	}

	if (mCurrentZone == mCol->size() - 1) {
		mNextButton->setEnabled(false);
	}
	else {
		mNextButton->setEnabled(true);
	}
	mColEdit->setText(QString::number(mTempCol.at(mCurrentZone)));
	mRowEdit->setText(QString::number(mTempRow.at(mCurrentZone)));
	mWidthEdit->setText(QString::number(mTempWidth.at(mCurrentZone)));
	mHeightEdit->setText(QString::number(mTempHeight.at(mCurrentZone)));
}

void QPictureSettingWidget::saveTemporary()
{
	mTempCol.replace(mCurrentZone, mColEdit->text().toInt());
	mTempRow.replace(mCurrentZone, mRowEdit->text().toInt());
	mTempWidth.replace(mCurrentZone, mWidthEdit->text().toInt());
	mTempHeight.replace(mCurrentZone, mHeightEdit->text().toInt());
}

void QPictureSettingWidget::closeEvent(QCloseEvent * event)
{
	emit closing();
}